@extends('layouts.app')

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Categorías</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a>Home</a>

            </li>
            <li class="breadcrumb-item">
                <a>Categorías</a>
            </li>
            {{-- <li class="breadcrumb-item active">
                <strong>Basic Form</strong>
            </li> --}}
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>Categorias<small></small></h5>
                    <div class="ibox-tools">

                        {{-- <button class="btn btn-xs btn-white" onclick="windows.history.back();">
                            <i class="fa fa-arrow-left"></i> Atras
                        </button> --}}


                    </div>
                </div>
                <div class="ibox-content">
                    <div class="m-b-sm m-t-sm">
                        <form role="search" method="POST" action="{{ route('categories.search') }}">
                            @csrf
                            @method('GET')
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <a href="{{ route('categories.index') }}" tabindex="-1" class="btn btn-white btn-sm"
                                        type="button"><i class="fa fa-refresh"></i>
                                        Refrescar</a>
                                </div>
                                <input type="text" class="form-control form-control-sm" name="search">
                                <div class="input-group-append">
                                    <button tabindex="-1" class="btn btn-primary btn-sm" type="submit">Buscar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>

                                    <th class="text-center">#ID</th>
                                    <th class="text-center">Nombre </th>
                                    <th class="text-center">Subcategorias </th>
                                    <th class="text-center">Fecha de creación </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($categories as $categorie)
                                <tr class="row-{{$categorie->id}}">
                                    <td class="text-center">{{$categorie->id}}</td>
                                    <td class="text-center">{{$categorie->name}}</td>
                                    <td>
                                        @foreach ($categorie->subcategories as $item)
                                        <span class="label label-warning-light">{{$item->name}}</span>
                                        @endforeach
                                    </td>
                                    <td class="text-center">{{$categorie->created_at->format('d/m/Y')}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="text-center">
                            {{$categories->links()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @endsection
    @push('scripts')

    @routes
    <script>
        $(document).ready(function () {

    });
    </script>
    @endpush