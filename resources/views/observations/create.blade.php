@extends('layouts.app')
@push('styles')
<link href="{{asset('admin/css/plugins/summernote/summernote.css')}}" rel="stylesheet">

@endpush
@section('content')
    <div>
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2>Observaciones</h2>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a>Home</a>

                    </li>
                    <li class="breadcrumb-item">
                        <a>Publicaciónes</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a>Observaciones</a>
                    </li>
                    <li class="breadcrumb-item active">
                        <strong>Nuevo</strong>
                    </li>
                </ol>
            </div>
            <div class="col-lg-2">

            </div>
        </div>

        <div class="wrapper wrapper-content animated fadeInUp">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox ">
                        <div class="ibox-title">
                            <h5>Observación</h5>
                            <div class="ibox-tools">
                                <a type="button" href="{{ url()->previous() }}" class="btn btn-white btn-sm"> <i
                                        class="fa fa-arrow-left"></i> Atras</a>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-sm-12">
                                    <form role="form" method="POST"
                                        action="{{ route('observations.store', $publication) }}">
                                        @csrf

                                        @include('observations.form')

                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
<script src="{{asset('admin/js/plugins/summernote/summernote.js')}}"></script>
<script src="{{asset('admin/js/plugins/summernote/lang/summernote-es-ES.js')}}"></script>
<script>
    $(document).ready(function(){

        $('.summernote').summernote({
            height: 150,
            lang: 'es-ES', // default: 'en-US'
            language: "es",
            toolbar: [
                // [groupName, [list of button]]
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['insert', ['link']]
            ]
        });
        $('.note-btn').removeAttr('title');

    });
    </script>
@endpush