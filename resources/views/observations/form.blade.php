    @if ($errors->any())
    <div class="alert alert-danger">
        <h4>Se ha encontrado los siguientes campos sin completar o erroneos</h4>
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <div class="row">
        <div class="col-sm-12">
            <div class="form-group">
                <label for="description">Publicación</label>
                <p><b>{{ $publication->title }}</b></p>
            </div>
        </div>

        <div class="col-sm-12">
            <div class="form-group">
                <label for="description">Descripción</label>

                    <textarea placeholder="Escriba aqui una observación de la publicación" id="summernote" class="summernote form-control"
                    name="description">{{isset($observation) ? $observation->description : old('description')}}</textarea>
                </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
        <div class="form-group">
            <button class="btn btn-sm btn-primary float-right m-t-n-xs" type="submit"><i class="fa fa-save"></i>
                Guardar</button>
        </div>
        </div>
    </div>
