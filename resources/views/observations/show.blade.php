@extends('layouts.app')
@section('content')
    <div>
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2>Observaciones</h2>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a>Home</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a>Publicaciónes</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a>Observaciones</a>
                    </li>
                    <li class="breadcrumb-item active">
                        <strong>Detalle</strong>
                    </li>
                </ol>
            </div>
            <div class="col-lg-2">

            </div>
        </div>

        <div class="wrapper wrapper-content animated fadeInUp">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox ">
                        <div class="ibox-title">
                            <h5>Observación</h5>
                            <div class="ibox-tools">
                                <a type="button" href="{{ url()->previous() }}" class="btn btn-white btn-sm"> <i
                                        class="fa fa-arrow-left"></i> Atras</a>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content">

                            <div class="row">
                                <div class="col-sm-12">

                                    <div class="alert alert-info"><span class="fa fa-info-circle"> </span> Para la correcta
                                        publicación del producto en el sitio web es necesario cumplir con las observaciones
                                        dadas por el administrador. Click en "Corregido" cuando tenga lista las correcciónes
                                        sugeridas. En caso de dudas comunicarse con el administrador del sitio.</div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="description">Publicación</label>
                                        <h4><b>{{ $publication->title }}</b></h4>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <label for="description">Fecha de observación</label>
                                    <h4><b>{{ $observation->created_at }} hs</b></h4>

                                </div>
                                <div class="col-sm-12">
                                    <label for="description">Descripción</label>

                                    <div class="alert alert-warning">{!! $observation->description !!}</div>
                                    </h4>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <form role="form" method="POST"
                                        action="{{ route('observations.check', [$publication, $observation]) }}">
                                        @csrf
                                        @method('PUT')
                                        <div class="form-group">
                                            <button class="btn btn-sm btn-primary float-right m-t-n-xs" type="submit">
                                                <i class="fa fa-check"></i> Corregido</button>
                                        </div>
                                    </form>
                                    <a type="button"
                                    class="btn btn-warning"
                                        href="{{route('products.edit',[$publication->typePublication,$publication->id,$publication->products->first()->id])}}">
                                        <i class="fa fa-edit" ></i> Ir a la publicación
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
