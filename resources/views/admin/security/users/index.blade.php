@extends('layouts.app')

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Usuarios</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a>Home</a>

            </li>
            <li class="breadcrumb-item">
                <a>Usuarios</a>
            </li>
            {{-- <li class="breadcrumb-item active">
                <strong>Basic Form</strong>
            </li> --}}
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>Usuarios<small></small></h5>
                    <div class="ibox-tools">

                        <a class="btn btn-xs btn-primary" href="{{route('users.new')}}">
                            <i class="fa fa-plus"></i> Nuevo
                        </a>


                    </div>
                </div>
                <div class="ibox-content">
                    <div class="m-b-sm m-t-sm">
                        <div class="row">
                            <div class="col-sm-12">
                                <form role="search" method="GET" action="{{ route('users.search') }}">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <a href="{{ route('users.index') }}" tabindex="-1"
                                                class="btn btn-white btn-sm" type="button"><i class="fa fa-refresh"></i>
                                                Refrescar</a>
                                        </div>
                                        <input type="text" style="height:33px !important;" class="form-control form-control-sm" name="search" value="{{ old('search') }}">

                                        <select class="form-control" name="status">
                                            <option type="hidden" value=""> -- Seleccionar estado --</option>
                                            <option value="active" {{ request()->input('status') == 'active' ? 'selected':""}}>Activos</option>
                                            <option value="delete"  {{request()->input('status') == 'delete' ? 'selected':""}}>Eliminados</option>
                                            <option value="all"  {{ request()->input('status') == 'all' ? 'selected':""}}>Todos</option>
                                        </select>
                                        <div class="input-group-append">
                                            <button tabindex="-1" class="btn btn-primary btn-sm" type="submit">Buscar</button>
                                        </div>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>#ID</th>
                                    <th>Nombre de usuario</th>
                                    <th>Email</th>
                                    {{-- <th>Nombre y Apellido</th> --}}
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                <div><i class="fa fa-info-circle" style="color:#ffdf7e;"> </i> Normal</div>
                                <div><i class="fa fa-info-circle" style="color:#1c84c6;"> </i> Premium</div>
                                <div><i class="fa fa-info-circle" style="color:#1ab394;"> </i> Administrador</div>
                                <div><i class="fa fa-info-circle" style="color:#ed969e;"> </i> No activo</div>

                                @foreach ($users as $user)

                                <tr id="row-{{$user->id}}" class="row-{{$user->id}} @if(!$user->is_verified) table-danger @elseif($user->isNormal())  table-warning @elseif($user->isPremium())  table-info @elseif($user->isAdmin()) table-success @endif " >
                                    <td>{{$user->id}}</td>
                                    <td>{{$user->name}}</td>
                                    <td>{{$user->email}}</td>
                                    {{-- <td>{{ isset($user->person) ? $user->person->nameComplete: ''}}</td> --}}
                                    <td>
                                        <div class="btn-group">
                                            <a type="button" class="btn btn-white btn-sm"
                                                href="{{route('users.show',$user->id)}}"><i
                                                    class="fa fa-eye" data-toggle="tooltip" data-placement="top" title="Ver"></i>
                                            </a>

                                            @if($user->deleted_at == null)
                                                <a type="button" class="btn btn-white btn-sm"

                                                   href="{{route('users.edit',$user->id)}}"><i class="fa fa-edit" data-toggle="tooltip" data-placement="top" title="Editar"></i>
                                                </a>
                                                @if($user->is_verified)
                                                <button type="button" class="btn btn-white btn-sm btn-pause"
                                                    data-id="{{$user->id}}"><i class="fa fa-pause" data-toggle="tooltip" data-placement="top" title="Pausar"></i>
                                                </button>
                                                @else
                                                <button type="button" class="btn btn-white btn-sm btn-active"
                                                    data-id="{{$user->id}}"><i class="fa fa-play" data-toggle="tooltip" data-placement="top" title="Activar"></i>
                                                </button>
                                                @endif
                                                <button type="button" class="btn btn-white btn-sm btn-danger"
                                                        data-id="{{$user->id}}"><i class="fa fa-trash" data-toggle="tooltip" data-placement="top" title="Eliminar"></i>
                                                </button>
                                            @else
                                                <button type="button" class="btn btn-white btn-sm btn-restore"
                                                        data-id="{{$user->id}}"><i class="fa fa-check" data-toggle="tooltip" data-placement="top" title="Restaurar"></i>
                                                </button>
                                            @endif


                                        </div>
                                    </td>
                                </tr>
                                @endforeach

                            </tbody>
                        </table>
                        <div class="text-center">
                            {{$users->links()}}
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@push('scripts')

@routes
<script>
    $(document).ready(function () {

        $('.btn-danger').on('click', function () {
            var id = $(this).data('id');
            var url = route('users.destroy', {id: id});

            Swal.fire({
                title: 'Confirmar',
                text: "¿Eliminar usuario?",
                type: 'info',
                showCancelButton: true,
                cancelButtonText: "Cancelar",
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Confirmar'
            }).then(function (result) {
                if (result.value) {
                    axios.delete(url)
                        .then(function (res) {

                            if (res.status === 204) {

                                const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 3000,
                                timerProgressBar: true,
                                onOpen: (toast) => {
                                    toast.addEventListener('mouseenter', Swal.stopTimer)
                                    toast.addEventListener('mouseleave', Swal.resumeTimer)
                                    }
                                })

                                Toast.fire({
                                    icon: 'success',
                                    title: 'Usuario elimininada'
                                })

                                $('.row-'+id).remove();
                            }

                        }).catch(function (res) {
                            swal("No tiene los privilegios para realizar esta acción!", '', 'error');
                        });
                }
            });
        });

        $('.btn-pause').on('click', function () {
            var user = $(this).data('id');
            var url = route('users.stop', user);

            axios.put(url).then(function (res) {
                                const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 3000,
                                timerProgressBar: true,
                                onOpen: (toast) => {
                                    toast.addEventListener('mouseenter', Swal.stopTimer)
                                    toast.addEventListener('mouseleave', Swal.resumeTimer)
                                    }
                                })

                                Toast.fire({
                                    icon: 'success',
                                    title: 'Usuario pausado'
                                })

                                $('#row-'+user).removeClass().addClass('table-danger');

                            });

        });

        $('.btn-active').on('click', function () {
            var user = $(this).data('id');
            var url = route('users.active',user);


            axios.put(url).then(function (res) {
                                const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 3000,
                                timerProgressBar: true,
                                onOpen: (toast) => {
                                    toast.addEventListener('mouseenter', Swal.stopTimer)
                                    toast.addEventListener('mouseleave', Swal.resumeTimer)
                                    }
                                })

                                Toast.fire({
                                    icon: 'success',
                                    title: 'Usuario activo'
                                })

                                $('#row-'+user).removeClass().addClass('table-warning');

            });

        });

        $('.btn-restore').on('click', function () {
            var user = $(this).data('id');
            var url = route('users.restore',user);


            axios.post(url).then(function (res) {
                const Toast = Swal.mixin({
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 3000,
                    timerProgressBar: true,
                    onOpen: (toast) => {
                        toast.addEventListener('mouseenter', Swal.stopTimer)
                        toast.addEventListener('mouseleave', Swal.resumeTimer)
                    }
                })

                Toast.fire({
                    icon: 'success',
                    title: 'Usuario restaurado'
                })
                $('.row-'+user).remove();

            });

        });
    });
</script>
@endpush
