<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label>Nombre de usuario</label>
            <input type="text" placeholder="Nombre de usuario" name="name" class="form-control"
                value="{{isset($user) ? $user->name : old('name')}}">
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Email</label>
            <input type="text" placeholder="Email" name="email" class="form-control"
                value="{{isset($user) ? $user->email : old('email')}}">
        </div>
    </div>

    {{-- Si se crea un usuario de muestra esta opcion --}}
    @if(!isset($user))


    <div class="col-sm-6">
        <div class="form-group">
            <label>Contraseña</label>
            <input type="text" placeholder="Contraseña" name="password" class="form-control"
                value="{{isset($user) ? $user->password : old('password')}}">
        </div>
    </div>


    @endif
    {{-- Si se edita un usuario de muestra esta opcion --}}

    <div class="col-sm-6">
        <label>Persona</label>
        <select class="form-control m-b" id="person" name="person" placeholder="Persona">
            <option value="{{isset($user->person) ? $user->person->id :  '' }}" selected="selected">
                {{isset($user->person) ? $user->person->nameComplete : old('person')}}</option>

        </select>
    </div>
    <div class="col-sm-6">
        <label>Roles</label>
        <select class="form-control" id="roles" name="roles[]" multiple="" placeholder="Roles">
            <option type="hidden" value="" disabled> -- Seleccionar rol --</option>
            @foreach ($roles as $rol)

            <option {{isset($user) && $user->roles()->get()->contains($rol) ? 'selected' : ""}} value="{{$rol->name}}">
                {{$rol->name}}</option>
            @endforeach
        </select>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="form-group">
            <br>
            <button class="btn btn-sm btn-primary float-right m-t-n-xs" type="submit"><i class="fa fa-save"></i>
                Guardar</button>
        </div>
    </div>
</div>