@extends('layouts.app')
@push('styles')
    <link href="{{ asset('admin/css/plugins/select2/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('admin/css/plugins/select2/select2-bootstrap4.min.css') }}" rel="stylesheet">
@endpush
@section('content')
    <div>
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2>Usuarios</h2>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a>Home</a>

                    </li>
                    <li class="breadcrumb-item">
                        <a>Usuarios</a>
                    </li>
                    <li class="breadcrumb-item active">
                        <strong>Ver ({{ $user->name }})</strong>
                    </li>
                </ol>
            </div>
            <div class="col-lg-2">

            </div>
        </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox ">
                        <div class="ibox-title">
                            <h5>Ver usuario <small></small></h5>
                            <div class="ibox-tools">
                                <a type="button" href="{{ url()->previous() }}" class="btn btn-white btn-xs"> <i
                                        class="fa fa-arrow-left"></i> Atras</a>
                            </div>
                        </div>
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Nombre de usuario</label>
                                                <p>
                                                    <strong>{{ $user->name }}</strong>
                                                </p>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Email</label>
                                                <p>
                                                    <strong>{{ $user->email }}</strong>
                                                </p>
                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="form-group">

                                                <label>Persona</label>
                                                <p>
                                                    <strong>{{ isset($user->person) ? $user->person->nameComplete . ' - ' . 'DNI: ' . $user->person->dni : '' }}</strong>
                                                </p>
                                            </div>
                                        </div>

                                        <div class="col-sm-12">
                                            <div class="form-group">

                                                <label>Permisos</label>
                                                <p>
                                                    <div class="table-responsive">
                                                        <table class="table table-bordered">
                                                            <thead>
                                                    <th>Roles</th>
                                                    <th>Permisos</th>
                                                            </thead>
                                                    <tbody>
                                                        @foreach ($user->roles as $rol)
                                                            <td>{{ $rol->name }}</td>
                                                            <td>
                                                                @foreach ($rol->permissions as $permission)
                                                                    <span class="badge badge-light">
                                                                        {{ $permission->name }}
                                                                    </span>
                                                                @endforeach
                                                            </td>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
