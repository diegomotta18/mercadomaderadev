@extends('layouts.app')

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Roles</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a>Home</a>

            </li>
            <li class="breadcrumb-item">
                <a>Roles</a>
            </li>
            {{-- <li class="breadcrumb-item active">
                <strong>Basic Form</strong>
            </li> --}}
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>Roles<small></small></h5>
                    <div class="ibox-tools">

                        <a class="btn btn-xs btn-primary" href="{{route('roles.new')}}">
                            <i class="fa fa-plus"></i> Nuevo
                        </a>


                    </div>
                </div>
                <div class="ibox-content">
                    <div class="m-b-sm m-t-sm">
                        <form role="search" method="POST" action="{{ route('roles.search') }}">
                            @csrf
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <a href="{{ route('roles.index') }}" tabindex="-1"
                                        class="btn btn-white btn-sm" type="button"><i class="fa fa-refresh"></i>
                                        Refrescar</a>
                                </div>
                                <input type="text" class="form-control form-control-sm" name="search">
                                <div class="input-group-append">
                                    <button tabindex="-1" class="btn btn-primary btn-sm" type="submit">Buscar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>

                                    <th>#ID</th>
                                    <th>Nombre </th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($roles as $role)

                                <tr class="row-{{$role->id}}">
                                    <td>{{$role->id}}</td>
                                    <td>{{$role->name}}</td>
                                    <td>
                                        <div class="btn-group" role="group"
                                            aria-label="Basic example">
                                            <a type="button" class="btn btn-white btn-sm"
                                                href="{{route('permissions.index',$role->id)}}"><i
                                                    class="fa fa-list"></i> Permisos</a>
                                            <a type="button" class="btn btn-white btn-sm"
                                                href="{{route('roles.edit',$role->id)}}"><i class="fa fa-edit"></i>
                                                Editar</a>
                                            <button type="button" class="btn btn-white btn-sm btn-danger"
                                                data-id="{{$role->id}}"><i class="fa fa-trash"></i> Eliminar</button>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach

                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@push('scripts')

@routes
<script>
    $(document).ready(function () {

        $('.btn-danger').on('click', function () {
            var id = $(this).data('id');
            var url = route('roles.destroy', {id: id});

            Swal.fire({
                title: 'Confirmar',
                text: "¿Eliminar rol?",
                type: 'info',
                showCancelButton: true,
                cancelButtonText: "Cancelar",
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Confirmar'
            }).then(function (result) {
                if (result.value) {
                    axios.delete(url)
                        .then(function (res) {

                            if (res.status === 204) {

                                const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 3000,
                                timerProgressBar: true,
                                onOpen: (toast) => {
                                    toast.addEventListener('mouseenter', Swal.stopTimer)
                                    toast.addEventListener('mouseleave', Swal.resumeTimer)
                                    }
                                })

                                Toast.fire({
                                    icon: 'success',
                                    title: 'Rol elimininada'
                                })

                                $('.row-'+id).remove();
                            } 

                        }).catch(function (res) {
                            swal("No tiene los privilegios para realizar esta acción!", '', 'error');
                        });
                }
            });
        });
    });
</script>
@endpush