<div class="row">

@foreach ($publications as $publication)
        <div class="col-md-4">
            <figure class="card card-product-grid" style="cursor: pointer;"
                    onclick="window.location='{{ route('view.publications.show',$publication->id)}}'">

                <div class="img-wrap">
                    <!--<span class="badge badge-danger"> NEW </span>-->
                    <img
                        src="@if (!$publication->hasMedia('images')) https://via.placeholder.com/500 @else {{$publication->getMedia('images')[0]->getUrl()}}@endif">
                </div>
                <figcaption class="info-wrap">
                    <a href="{{ route('view.publications.show',$publication->id)}}"
                       class="title mb-2">{{$publication->title}}</a>
                    <div class="price-wrap">
                        <span class="price">@if(!$publication->isAuction())
                                ${{ $publication->total }}@else {{ $publication->auction->price_init  }}@endif</span>
                        <small class="text-muted"></small>
                    </div>
                    <p class="mb-3">

                        @if(isset($publication->products()->first()->typeWood))
                            <span
                                class="tag">@if(isset($publication->products()->first()->typeWood->name)){{$publication->products()->first()->typeWood->name}} @endif</span>
                        @endif

                        @if(isset($publication->products()->first()->kind))
                            <span
                                class="tag">@if(isset($publication->products()->first()->kind->name)){{$publication->products()->first()->kind->name}} @endif</span>
                        @endif

                        @if(isset($publication->products()->first()->typeProduct))
                            <span
                                class="tag">@if(isset($publication->products()->first()->typeProduct->name)){{$publication->products()->first()->typeProduct->name}}@endif</span>
                        @endif

                        @if(isset($publication->products()->first()->dimension->actualThickness))<span class="tag">Espesor {{$publication->products()->first()->dimension->actualThickness}} mm </span>@endif
                        @if(isset($publication->products()->first()->dimension->actualWidth))<span class="tag">Ancho {{$publication->products()->first()->dimension->actualWidth}} mm </span>@endif</td>
                        @if(isset($publication->products()->first()->dimension->actualLength))<span class="tag">Largo {{$publication->products()->first()->dimension->actualLength}} mm </span>@endif</td>
                    </p>
                </figcaption>
            </figure>

        </div>
@endforeach

</div>
<div class="row">
<div class="text-center">
    {{$publications->appends(request()->query())->links()}}

</div>
</div>
