{{--@if(count($items))--}}
{{--    <ul class="dropdown-menu dropdown-messages dropdown-menu-right">--}}
{{--            <li>--}}
{{--                <div class="text-center link-block">--}}
{{--                    <i class="fa fa-star-half-o"></i> <strong>No tienes productos por calificar</strong>--}}
{{--                </div>--}}
{{--            </li>--}}
{{--    </ul>--}}
{{--@else--}}
    <ul class="dropdown-menu dropdown-messages dropdown-menu-right @if(count($items)>0 ) show @endif ">
        <li>
            <div class="text-center link-block">
                <i class="fa fa-star-half-o"></i> <strong>Tienes productos por calificar</strong>
            </div>
        </li>
        <li class="dropdown-divider"></li>
        @foreach($items as $item)
        <li>
                <div class="dropdown-messages-box">
                    <a href="{{route('califications.index',$item->buy_id)}}"  class="dropdown-item float-left">
                        <img alt="image" class="rounded-circle" src="@if (!$item->publication->hasMedia('images')) https://place-hold.it/120 @else {{$item->publication->getMedia('images')[0]->getUrl('thumb')}}@endif">
                    </a>
                    <div class="media-body">
                        <small>{{$item->created_at->diffForHumans()}}</small><br>

                        <strong>{{$item->seller->person->nameComplete}}</strong> espera tu calificación <br>
                        <small class="text-muted">{{$item->publication->title}}</small>
                    </div>
                </div>
        </li>
{{--        <li class="dropdown-divider"></li>--}}
        @endforeach
{{--        --}}{{--        <li>--}}
{{--        --}}{{--            <div class="text-center link-block">--}}
{{--        --}}{{--                <a href="mailbox.html" class="dropdown-item">--}}
{{--        --}}{{--                    <i class="fa fa-envelope"></i> <strong>Ver todos</strong>--}}
{{--        --}}{{--                </a>--}}
{{--        --}}{{--            </div>--}}
{{--        --}}{{--        </li>--}}
    </ul>
{{--@endif--}}
