<li class="nav-item  item-menu-fix  dropdown">
    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button"
        aria-haspopup="true" aria-expanded="true"> Categorias </a>
    <ul class="dropdown-menu">
        @foreach ($categories as $item)
        <li class="dropdown-submenu" style="padding: 0px !important;">
            <a href="#" class="nav-link dropdown-toggle  item-menu-fix" data-toggle="dropdown" role="button"
                aria-haspopup="true" aria-expanded="true">
                {{ __($item->name) }}</a>

            <ul class="dropdown-menu">
                @foreach ($item->subcategories as $c)
                <li><a class="dropdown-item  item-menu-fix" href="{{ route('bycategories.index',$c->id) }}">{{ __($c->name)}}</a></li>
                @endforeach
            </ul>
        </li>
        @endforeach
    </ul>
</li>
