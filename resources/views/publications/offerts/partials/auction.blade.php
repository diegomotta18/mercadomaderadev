<div id="auction_show">
    <div class="ibox-title">
        <h5>Subasta <small></small></h5>
        <div class="ibox-tools">

        </div>
    </div>
    <div class="ibox-content">
        <div class="row">
            <div class="col-sm-3">
                <div class="form-group">
                    <label for="price_init">&nbsp;</label>
                    <div class="form-check">
                        <input type="checkbox" class="form-check-input" id="auction" name="auction"
                            {{ isset($publication->auction) && $publication->has('auction') ? 'checked="checked"' : '' }}>
                        <label class="form-check-label" for="auction">Subastar</label>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group" id="price_init">
                    <label for="price_init">Precio base</label>
                    <div class="input-group">
                        <input style="height: 40px !important;" type="number" step="0.01" class="form-control"
                            name="price_init" placeholder="Precio base" id="price_init"
                            value="{{ isset($publication->auction->price_init) ? $publication->auction->price_init : old('price_init') }}">
                        <div class="input-group-append">
                            <span class="input-group-text" id="basic-addon2">$</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group" id="quantityy">
                    <label for="quantity" >Cantidad</label>
                    <div class="input-group">
                        <input style="height: 40px !important;" type="number" step="0.01" class="form-control"
                            name="quantity" placeholder="Cantidad" id="quantity"
                            value="{{ isset($publication->auction->quantity) ? $publication->auction->quantity : old('quantity') }}">
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group" id="finish_datee">
                    <label for="price_init">Fecha de finalización</label>
                    <div class="input-group">
                        <input placeholder="Fecha de finalización"  id="finish_date" name="finish_date" type="text" class="form-control"
                            value="{{ isset($publication->auction) ? $publication->auction->finish_date : old('finish_date') }}">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
