<div id="way_pay">
    <div class="ibox-title">
        <h5>Forma de pago <small></small></h5>
        <div class="ibox-tools">
        </div>
    </div>
    <div class="ibox-content">
        <div class="row">
            <div class="col-sm-3">

                <label for="money">Moneda</label>
                <select class="form-control" name="money" id="money">
                    <option type="hidden" value=""> -- Seleccionar moneda--</option>
                    <option {{ isset($product->wayPay) && $product->wayPay->money == '$' ? 'selected="selected"' : '' }}
                        value="$">$</option>
                    <option
                        {{ isset($product->wayPay) && $product->wayPay->money == 'U$D' ? 'selected="selected"' : '' }}
                        value="U$D">U$D</option>
                </select>
            </div>
            <div class="col-sm-3">
                <label for="paymentType">Forma de pago</label>
                @foreach ($paymentTypes as $paymentType)
                    <div class="form-check">
                        <input class="form-check-input" name="paymentType[]" value="{{ $paymentType->id }}"
                            {{ isset($product->wayPay) && $product->wayPay->paymentTypes->contains($paymentType) ? 'checked="checked"' : '' }}
                            type="checkbox" id="{{ $paymentType->paymentType }}">
                        <label class="form-check-label" for="{{ $paymentType->id }}">
                            {{ $paymentType->paymentType }}
                        </label>
                    </div>
                @endforeach
            </div>
            <div class="col-sm-3">
                <label>&nbsp; </label>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="iva" id="iva"
                        {{ isset($product->wayPay->iva) && $product->wayPay->iva ? 'checked="checked"' : '' }}>
                    <label class="form-check-label" for="iva">
                        ¿Incluir IVA?
                    </label>
                </div>
            </div>
            <div class="col-sm-3">
                <label for="ammount">Precio</label>
                <div class="input-group">
                    <input style="height: 40px !important;" type="number" step="0.01" class="form-control" id="ammount"
                        name="ammount" placeholder="Precio"
                        value="{{ isset($product->wayPay->ammount) ? $product->wayPay->ammount : old('ammount') }}">
                    <div class="input-group-append">
                        <span class="input-group-text" id="basic-addon2">$/PT</span>
                    </div>
                </div>
                <small id="auction_active" style="color: red"></small>
            </div>
        </div>
    </div>
</div>
