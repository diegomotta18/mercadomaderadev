

<div id="address">
    <div class="ibox-title">
        <h5>Dirección del producto<small></small></h5>
        <div class="ibox-tools">

        </div>
    </div>
    <div class="ibox-content">
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="control-label">País</label>
                            <select class="form-control" name="country" id="country">
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="control-label">Provincia</label>
                            <select class="form-control" name="province" id="province">
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="control-label">Departamento</label>
                            <select class="form-control" name="department" id="department">
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="control-label">Localidad</label>
                            <select class="form-control" name="city" id="city">
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="control-label">Calle</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-home"></i></span><input placeholder="Calle"
                                    id="street" name="street" type="text" class="form-control"
                                    value="{{ request()->get('street') }}" />
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="control-label">Altura</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-home"></i></span><input placeholder="Altura"
                                    id="number" name="number" type="number" class="form-control"
                                    value="{{ request()->input('number') }}" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="control-label">Dpto</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-home"></i></span><input placeholder="Dpto"
                                    id="dpto" name="dpto" type="text" class="form-control"
                                    value="{{ request()->input('dpto') }}" />
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="control-label">Piso</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-home"></i></span><input placeholder="Piso"
                                    id="floor" name="floor" type="text" class="form-control"
                                    value="{{ request()->input('floor') }}" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="control-label">Latitud</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-map-marker"></i></span><input placeholder="Latitud"
                                    id="lat" name="lat" type="text" class="form-control"
                                    value="{{ request()->input('lat') }}" />
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="control-label">Longitud</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-map-marker"></i></span><input placeholder="Longitud"
                                    id="lon" name="lon" type="text" class="form-control"
                                    value="{{ request()->input('lon') }}" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <div class="justify-content-center aling-items-center">
                            <label class="control-label">&nbsp;&nbsp;</label>

                            <div id="map"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
