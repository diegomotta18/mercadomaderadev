<div class="ibox-title">
    <h5>Titulo y descripción <small></small></h5>
    <div class="ibox-tools">

    </div>
</div>
<div class="ibox-content">
    @if ($errors->any())
    <div class="alert alert-danger">
        <h4>Se ha encontrado los siguientes campos sin completar o erroneos</h4>
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <div class="row">
        <div class="col-sm-12">
            <div class="form-group">
                <label for="title">Titulo</label>
                <input type="text" id="title" placeholder="Titulo" name="title" class="form-control"
                    value="{{isset($publication) ? $publication->title : old('title')}}">
            </div>
        </div>
        <div class="col-sm-12">
            <div class="form-group">
                <label for="description">Descripción</label>
                <textarea id="summernote" class="summernote form-control"
                    name="description">{{isset($publication) ? $publication->description : old('description')}}</textarea>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="form-group">
                <label for="typeWood">Tipos de madera</label>
                <select class="form-control" name="typeWood" id="typeWood">
                    <option type="hidden" value=""> -- Seleccionar tipo de madera --</option>
                    @foreach ($typeWoods as $typewood)
                        <option value="{{$typewood->id}}"
                            {{ isset($product->typeWood) &&  $product->typeWood->id == $typewood->id ? 'selected="selected"' : '' }}>
                            {{$typewood->name}}
                        </option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="form-group">
                <label for="kind">Especie</label>
                <input type="hidden" id="kind_id" value="{{ isset($product->kind) ? $product->kind->id : ''}}"/>
                <select class="form-control" name="kind" id="kind">
                </select>
            </div>
        </div>

        <div class="col-sm-4">
            <div class="form-group">
                <label for="typeProduct">Tipo de producto</label>
                <select class="form-control" name="typeProduct" id="typeProduct">
                    <option type="hidden" value=""> -- Seleccionar producto --</option>
                    @foreach ($typeProducts as $typeproduct)
                        <option value="{{$typeproduct->id}}"
                            {{ isset($product->typeProduct) &&  $product->typeProduct->id == $typeproduct->id ? 'selected="selected"' : '' }}>
                            {{$typeproduct->name}}
                        </option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
</div>
