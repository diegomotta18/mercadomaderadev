<div id="condition">

    <div class="ibox-title">
        <h5>Condiciones del paquete<small></small></h5>
        <div class="ibox-tools">

        </div>
    </div>
    <div class="ibox-content">
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="piece">Piezas</label>
                    <input type="number" class="form-control" id="piece" name="piece" placeholder="Piezas"
                        value="{{isset($product->condition) ? $product->condition->piece : old('piece')}}">
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="weight">Peso</label>
                    <input type="number" step="0.01" class="form-control" id="weight" name="weight" placeholder="Peso"
                        value="{{isset($product->condition) ? $product->condition->weight : old('weight')}}">
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="length">Largo</label>
                    <input type="number" step="0.01" class="form-control" id="length" name="length" placeholder="Largo"
                        value="{{isset($product->condition) ? $product->condition->length : old('length')}}">
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="width">Ancho</label>
                    <input type="number" step="0.01" class="form-control" id="width" name="width" placeholder="Ancho"
                        value="{{isset($product->condition) ? $product->condition->width : old('width')}}">
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="tall">Alto</label>
                    <input type="number" step="0.01" class="form-control" id="tall" name="tall" placeholder="Alto"
                        value="{{isset($product->condition) ? $product->condition->tall : old('tall')}}">
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="zunchos">Zunchos</label>
                    <select class="form-control" name="zunchos" id="zunchos">
                        <option type="hidden" value=""> -- Seleccionar zunchos--</option>
                        <option
                            {{ isset($product->condition) &&  $product->condition->zunchos == 'No'? 'selected="selected"' : '' }}
                            value="No">No</option>
                        <option
                            {{ isset($product->condition) &&  $product->condition->zunchos == 'Yes'? 'selected="selected"' : '' }}
                            value="Yes">Si</option>
                    </select>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="wrapping">Envoltorio</label>
                    <select class="form-control" name="wrapping" id="wrapping">
                        <option type="hidden" value=""> -- Seleccionar envoltorio--</option>
                        <option
                            {{ isset($product->condition) &&  $product->condition->wrapping == 'No'? 'selected="selected"' : '' }}
                            value="No">No</option>
                        <option
                            {{ isset($product->condition) &&  $product->condition->wrapping == 'Yes'? 'selected="selected"' : '' }}
                            value="Yes">Si</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
</div>