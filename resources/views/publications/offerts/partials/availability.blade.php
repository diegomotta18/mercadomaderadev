<div id="availability">
    <div class="ibox-title">
        <h5>Disponibilidad<small></small></h5>
        <div class="ibox-tools">

        </div>
    </div>
    <div class="ibox-content">
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="unitMeasurement">Unidad de medida</label>
                    <select class="form-control" name="unitMeasurement" id="unitMeasurement">
                        <option type="hidden" value=""> -- Seleccionar unidad de medidad--</option>
                        <option value="Pie tablar"
                            {{ isset($product->availability) &&  $product->availability->unitMeasurement == 'Pie tablar' ? 'selected="selected"' : '' }}>
                            Pie tablar</option>
                    </select>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="measurementTerm">Plazo de producción</label>
                    <input type="number" class="form-control" id="measurementTerm" name="measurementTerm"
                        placeholder="Plazo de producción"
                        value="{{isset($product->availability) ? $product->availability->measurementTerm : old('measurementTerm')}}">
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="availabilityy">Disponibilidad</label>
                    <select class="form-control availabilityy" name="availability" id="availability">
                        <option type="hidden" value=""> -- Seleccionar disponibilidad--</option>
                        <option value="continuous"
                            {{ isset($product->availability) &&  $product->availability->availability == 'continuous' ? 'selected="selected"' : '' }}>
                            Continua</option>
                        <option value="limited"
                            {{ isset($product->availability) &&  $product->availability->availability == 'limited'? 'selected="selected"' : '' }}>
                            Limitada</option>
                    </select>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group cantitude">
                    <label for="cantitude">Cantidad/Disponibilidad mensual</label>
                    <input type="number" class="form-control" id="cantitude" name="cantitude"
                        placeholder="Cantidad/Disponibilidad mensual"
                        value="{{isset($product->availability) ? $product->availability->cantitude : old('cantitude')}}">
                </div>
            </div>

        </div>
    </div>
</div>
@push('scripts')
<script>
    $(document).ready(function(){

        $('.availabilityy').on('change',function(){
            var value = $(this).val();
            console.log(value);
            if(value === 'limited'){
                $('.cantitude').hide();
            }else{
                $('.cantitude').show();
            }
        });
    });
</script>
@endpush
