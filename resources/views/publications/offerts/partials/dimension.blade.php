<div id="dimension">

    <div class="ibox-title">
        <h5>Dimensiones de la pieza<small></small></h5>
        <div class="ibox-tools">

        </div>
    </div>
    <div class="ibox-content">
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="nominalThickness">Espesor nominal</label>
                    <div class="input-group">
                        <input style="height: 40px !important;" type="number" step="0.01" id="nominalThickness"
                            placeholder="Espesor nominal" name="nominalThickness" class="form-control"
                            value="{{isset($product->dimension) ? $product->dimension->nominalThickness : old('nominalThickness')}}">
                        <div class="input-group-append">
                            <span class="input-group-text" id="basic-addon2">"</span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group">
                    <label for="nominalWidth">Ancho nominal</label>
                    <div class="input-group">
                        <input style="height: 40px !important;" type="number" step="0.01" id="nominalLength"
                            placeholder="Ancho nominal" name="nominalWidth" class="form-control"
                            value="{{isset($product->dimension) ? $product->dimension->nominalWidth : old('nominalWidth')}}">
                        <div class="input-group-append">
                            <span class="input-group-text" id="basic-addon2">"</span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group">
                    <label for="nominalLength">Largo nominal</label>
                    <div class="input-group">
                        <input style="height: 40px !important;" type="number" step="0.01" id="nominalLength"
                            placeholder="Largo nominal" name="nominalLength" class="form-control"
                            value="{{isset($product->dimension) ? $product->dimension->nominalLength : old('nominalLength')}}">
                        <div class="input-group-append">
                            <span class="input-group-text" id="basic-addon2">'</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="actualThickness">Espesor real</label>
                    <div class="input-group">
                        <input style="height: 40px !important;" type="number" step="0.01" id="actualThickness"
                            placeholder="Espesor real" name="actualThickness" class="form-control"
                            value="{{isset($product->dimension) ? $product->dimension->actualThickness : old('actualThickness')}}">
                        <div class="input-group-append">
                            <span class="input-group-text" id="basic-addon2">mm</span>
                        </div>
                    </div>

                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group">
                    <label for="actualWidth">Ancho real</label>
                    <div class="input-group">
                        <input style="height: 40px !important;" type="number" step="0.01" id="actualWidth"
                            placeholder="Ancho real" name="actualWidth" class="form-control"
                            value="{{isset($product->dimension) ? $product->dimension->actualWidth : old('actualWidth')}}">
                        <div class="input-group-append">
                            <span class="input-group-text" id="basic-addon2">mm</span>
                        </div>
                    </div>

                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group">
                    <label for="actualLength">Largo real</label>
                    <div class="input-group">
                        <input style="height: 40px !important;" type="number" step="0.01" id="actualLength"
                            placeholder="Largo real" name="actualLength" class="form-control"
                            value="{{isset($product->dimension) ? $product->dimension->actualLength : old('actualLength')}}">
                        <div class="input-group-append">
                            <span class="input-group-text" id="basic-addon2">mm</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="isMoreThickness">Es mas espesor</label>
                    <div class="input-group">

                        <input style="height: 40px !important;" type="number" step="0.01" id="isMoreThickness"
                            placeholder="Es mas espesor" name="isMoreThickness" class="form-control"
                            value="{{isset($product->dimension) ? $product->dimension->isMoreThickness : old('isMoreThickness')}}">
                        <div class="input-group-append">
                            <span class="input-group-text" id="basic-addon2">mm</span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group">
                    <label for="isMoreWidth">Es mas ancho</label>
                    <div class="input-group">

                        <input style="height: 40px !important;" type="number" step="0.01" id="isMoreWidth"
                            placeholder="Es mas ancho" name="isMoreWidth" class="form-control"
                            value="{{isset($product->dimension) ? $product->dimension->isMoreWidth : old('isMoreWidth')}}">
                        <div class="input-group-append">
                            <span class="input-group-text" id="basic-addon2">mm</span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group">
                    <label for="isMoreLength">Es mas largo</label>

                    <div class="input-group">
                        <input style="height: 40px !important;" type="number" step="0.01" id="isMoreLength"
                            placeholder="Es mas largo" name="isMoreLength" class="form-control"
                            value="{{isset($product->dimension) ? $product->dimension->isMoreLength : old('isMoreLength')}}">
                        <div class="input-group-append">
                            <span class="input-group-text" id="basic-addon2">mm</span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group">
                    <label for="isLessThickness">Es menos espesor</label>
                    <div class="input-group">
                        <input style="height: 40px !important;" type="number" step="0.01" id="isLessThickness"
                            placeholder="Es menos espesor" name="isLessThickness" class="form-control"
                            value="{{isset($product->dimension) ? $product->dimension->isLessThickness : old('isLessThickness')}}">
                        <div class="input-group-append">
                            <span class="input-group-text" id="basic-addon2">mm</span>
                        </div>
                    </div>
                </div>
            </div>



            <div class="col-sm-4">
                <div class="form-group">
                    <label for="isLessWidth">Es menos ancho</label>
                    <div class="input-group">
                        <input style="height: 40px !important;" type="number" step="0.01" id="isLessWidth"
                            placeholder="Es menos ancho" name="isLessWidth" class="form-control"
                            value="{{isset($product->dimension) ? $product->dimension->isLessWidth : old('isLessWidth')}}">
                        <div class="input-group-append">
                            <span class="input-group-text" id="basic-addon2">mm</span>
                        </div>
                    </div>
                </div>
            </div>



            <div class="col-sm-4">
                <div class="form-group">
                    <label for="isLessLength">Es menos largo</label>
                    <div class="input-group">
                        <input style="height: 40px !important;" type="number" step="0.01" id="isLessLength"
                            placeholder="Es mas largo" name="isLessLength" class="form-control"
                            value="{{isset($product->dimension) ? $product->dimension->isLessLength : old('isLessLength')}}">
                        <div class="input-group-append">
                            <span class="input-group-text" id="basic-addon2">mm</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
