<div id="shipping_time">
    <div class="ibox-title">
        <h5>Tipo de envio <small></small></h5>
        <div class="ibox-tools">

        </div>
    </div>
    <div class="ibox-content">
        <div class="row">
            <div class="col-sm-6">
                <label for="envio">Envio</label>
                <select class="form-control" name="send" id="send">
                    <option type="hidden" value=""> -- Seleccionar tipo envio--</option>
                    <option
                        {{ isset($product->shippingType) &&  $product->shippingType->send == 'query'? 'selected="selected"' : '' }}
                        value="query">Entrega a acordar con el vendedor</option>
                        
                    <option
                        {{ isset($product->shippingType) &&  $product->shippingType->send == 'free'? 'selected="selected"' : '' }}
                        value="free">Gratis</option>
                    <option
                        {{ isset($product->shippingType) &&  $product->shippingType->send == 'free from'? 'selected="selected"' : '' }}
                        value="free from">Gratis a partir de</option>
                </select>
            </div>
            <div class="col-sm-6">
                <label for="description_send">&nbsp;</label>
                <input placeholder=""
                    value="{{isset($product->shippingType) ? $product->shippingType->description : old('description_send')}}"
                    class="form-control" name="description_send" id="description_send" />
            </div>
        </div>
    </div>
</div>