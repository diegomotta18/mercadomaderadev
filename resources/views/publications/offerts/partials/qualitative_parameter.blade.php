<div id="qualitative_parameter">
    <div class="ibox-title">
        <h5>Parametros cualitativos<small></small></h5>
        <div class="ibox-tools">
        </div>
    </div>
    <div class="ibox-content">
        <div class="row">
            <div class="col-sm-3">
                <div class="form-group">
                    <label for="calidad">Calidad</label>
                    <select class="form-control" name="calidad" id="calidad">
                        <option type="hidden" value=""> -- Seleccionar calidad--</option>
                        <option value="Primera"
                            {{ isset($product->qualitativeParameter) &&  $product->qualitativeParameter->calidad == 'Primera'? 'selected="selected"' : '' }}>
                            Primera</option>
                        <option value="Segunda"
                            {{ isset($product->qualitativeParameter) &&  $product->qualitativeParameter->calidad == 'Segunda'? 'selected="selected"' : '' }}>
                            Segunda</option>
                        <option value="Tercera"
                            {{ isset($product->qualitativeParameter) &&  $product->qualitativeParameter->calidad == 'Tercera'? 'selected="selected"' : '' }}>
                            Tercera</option>
                    </select>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    <label for="nudo">Nudos</label>
                    <select class="form-control" name="nudo" id="nudo">
                        <option type="hidden" value=""> -- Seleccionar nudo--</option>
                        <option value="Si"
                            {{ isset($product->qualitativeParameter) &&  $product->qualitativeParameter->nudo == 'Si'? 'selected="selected"' : '' }}>
                            Si</option>
                        <option value="No"
                            {{ isset($product->qualitativeParameter) &&  $product->qualitativeParameter->nudo == 'No'? 'selected="selected"' : '' }}>
                            No</option>
                    </select>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    <label for="nudoMuerto">Nudos muertos</label>
                    <select class="form-control" name="nudoMuerto" id="nudoMuerto">
                        <option type="hidden" value=""> -- Seleccionar nudo muerto--</option>
                        <option value="Si"
                            {{ isset($product->qualitativeParameter) &&  $product->qualitativeParameter->nudoMuerto == 'Si'? 'selected="selected"' : '' }}>
                            Si</option>
                        <option value="No"
                            {{ isset($product->qualitativeParameter) &&  $product->qualitativeParameter->nudoMuerto == 'No'? 'selected="selected"' : '' }}>
                            No</option>
                        <option value="No a simple vista"
                            {{ isset($product->qualitativeParameter) &&  $product->qualitativeParameter->nudoMuerto == 'No a simple vista'? 'selected="selected"' : '' }}>
                            No a simple vista</option>
                    </select>
                </div>
            </div>

            <div class="col-sm-3">
                <div class="form-group">
                    <label for="manchaHongo">Mancha de hongos</label>
                    <select class="form-control" name="manchaHongo" id="manchaHongo">
                        <option type="hidden" value=""> -- Seleccionar mancha de hongo--</option>
                        <option value="No se nota"
                            {{ isset($product->qualitativeParameter) &&  $product->qualitativeParameter->manchaHongo == 'No se nota'? 'selected="selected"' : '' }}>
                            No se nota</option>
                        <option value="Se nota"
                            {{ isset($product->qualitativeParameter) &&  $product->qualitativeParameter->manchaHongo == 'Se nota'? 'selected="selected"' : '' }}>
                            Se nota</option>
                        <option value="Sin"
                            {{ isset($product->qualitativeParameter) &&  $product->qualitativeParameter->manchaHongo == 'Sin'? 'selected="selected"' : '' }}>
                            Sin</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <div class="form-group">
                    <label for="secado">Secado</label>
                    <select class="form-control" name="secado" id="secado">
                        <option type="hidden" value=""> -- Seleccionar secado--</option>
                        <option value="Natural"
                            {{ isset($product->qualitativeParameter) &&  $product->qualitativeParameter->secado == 'Natural'? 'selected="selected"' : '' }}>
                            Natural</option>
                        <option value="Artificial"
                            {{ isset($product->qualitativeParameter) &&  $product->qualitativeParameter->secado == 'Artificial'? 'selected="selected"' : '' }}>
                            Artificial</option>
                        <option value="Sin especificar"
                            {{ isset($product->qualitativeParameter) &&  $product->qualitativeParameter->secado == 'Sin especificar'? 'selected="selected"' : '' }}>
                            Sin especificar</option>
                    </select>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    <label for="humedaMax">Humedad Max</label>
                    <select class="form-control" name="humedadMax" id="humedadMax">
                        <option type="hidden" value=""> -- Seleccionar humedad máxima--</option>
                        <option
                            {{ isset($product->qualitativeParameter) &&  $product->qualitativeParameter->humedadMax == 'Indiferente'? 'selected="selected"' : '' }}
                            value="Indiferente">Indiferente</option>
                        <option
                            {{ isset($product->qualitativeParameter) &&  $product->qualitativeParameter->humedadMax == '7'? 'selected="selected"' : '' }}
                            value="7">7</option>
                        <option
                            {{ isset($product->qualitativeParameter) &&  $product->qualitativeParameter->humedadMax == '8'? 'selected="selected"' : '' }}
                            value="8">8</option>
                        <option
                            {{ isset($product->qualitativeParameter) &&  $product->qualitativeParameter->humedadMax == '9'? 'selected="selected"' : '' }}
                            value="9">9</option>
                        <option
                            {{ isset($product->qualitativeParameter) &&  $product->qualitativeParameter->humedadMax == '10'? 'selected="selected"' : '' }}
                            value="10">10</option>
                        <option
                            {{ isset($product->qualitativeParameter) &&  $product->qualitativeParameter->humedadMax == '11'? 'selected="selected"' : '' }}
                            value="11">11</option>
                        <option
                            {{ isset($product->qualitativeParameter) &&  $product->qualitativeParameter->humedadMax == '12'? 'selected="selected"' : '' }}
                            value="12">12</option>
                        <option
                            {{ isset($product->qualitativeParameter) &&  $product->qualitativeParameter->humedadMax == '13'? 'selected="selected"' : '' }}
                            value="13">13</option>
                        <option
                            {{ isset($product->qualitativeParameter) &&  $product->qualitativeParameter->humedadMax == '14'? 'selected="selected"' : '' }}
                            value="14">14</option>
                        <option
                            {{ isset($product->qualitativeParameter) &&  $product->qualitativeParameter->humedadMax == '15'? 'selected="selected"' : '' }}
                            value="15">15</option>
                        <option
                            {{ isset($product->qualitativeParameter) &&  $product->qualitativeParameter->humedadMax == '16'? 'selected="selected"' : '' }}
                            value="16">17</option>
                        <option
                            {{ isset($product->qualitativeParameter) &&  $product->qualitativeParameter->humedadMax == '17'? 'selected="selected"' : '' }}
                            value="17">17</option>
                        <option
                            {{ isset($product->qualitativeParameter) &&  $product->qualitativeParameter->humedadMax == 'Verde'? 'selected="selected"' : '' }}
                            value="Verde">Verde</option>
                    </select>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    <label for="humedadMin">Humedad Min</label>
                    <select class="form-control" name="humedadMin" id="humedadMin">
                        <option type="hidden" value=""> -- Seleccionar humedad mínima--</option>
                        <option
                            {{ isset($product->qualitativeParameter) &&  $product->qualitativeParameter->humedadMin == 'Indiferente'? 'selected="selected"' : '' }}
                            value="Indiferente">Indiferente</option>
                        <option
                            {{ isset($product->qualitativeParameter) &&  $product->qualitativeParameter->humedadMin == '7'? 'selected="selected"' : '' }}
                            value="7">7</option>
                        <option
                            {{ isset($product->qualitativeParameter) &&  $product->qualitativeParameter->humedadMin == '8'? 'selected="selected"' : '' }}
                            value="8">8</option>
                        <option
                            {{ isset($product->qualitativeParameter) &&  $product->qualitativeParameter->humedadMin == '9'? 'selected="selected"' : '' }}
                            value="9">9</option>
                        <option
                            {{ isset($product->qualitativeParameter) &&  $product->qualitativeParameter->humedadMin == '10'? 'selected="selected"' : '' }}
                            value="10">10</option>
                        <option
                            {{ isset($product->qualitativeParameter) &&  $product->qualitativeParameter->humedadMin == '11'? 'selected="selected"' : '' }}
                            value="11">11</option>
                        <option
                            {{ isset($product->qualitativeParameter) &&  $product->qualitativeParameter->humedadMin == '12'? 'selected="selected"' : '' }}
                            value="12">12</option>
                        <option
                            {{ isset($product->qualitativeParameter) &&  $product->qualitativeParameter->humedadMin == '13'? 'selected="selected"' : '' }}
                            value="13">13</option>
                        <option
                            {{ isset($product->qualitativeParameter) &&  $product->qualitativeParameter->humedadMin == '14'? 'selected="selected"' : '' }}
                            value="14">14</option>
                        <option
                            {{ isset($product->qualitativeParameter) &&  $product->qualitativeParameter->humedadMin == '15'? 'selected="selected"' : '' }}
                            value="15">15</option>
                        <option
                            {{ isset($product->qualitativeParameter) &&  $product->qualitativeParameter->humedadMin == '16'? 'selected="selected"' : '' }}
                            value="16">17</option>
                        <option
                            {{ isset($product->qualitativeParameter) &&  $product->qualitativeParameter->humedadMin == '17'? 'selected="selected"' : '' }}
                            value="17">17</option>
                        <option
                            {{ isset($product->qualitativeParameter) &&  $product->qualitativeParameter->humedadMin == 'Verde'? 'selected="selected"' : '' }}
                            value="Verde">Verde</option>
                    </select>
                </div>
            </div>
            <div class="col-sm-3">

                <div class="form-group">
                    <label for="ubicacionTronco">Ubicación de tronco</label>
                    <select class="form-control" name="ubicacionTronco" id="ubicacionTronco">
                        <option type="hidden" value=""> -- Seleccionar ubicación de tronco--</option>
                        <option
                            {{ isset($product->qualitativeParameter) &&  $product->qualitativeParameter->ubicacionTronco == 'Indiferente'? 'selected="selected"' : '' }}
                            value="Indiferente">Indiferente</option>
                        <option
                            {{ isset($product->qualitativeParameter) &&  $product->qualitativeParameter->ubicacionTronco == 'Central'? 'selected="selected"' : '' }}
                            value="Central">Central</option>
                        <option
                            {{ isset($product->qualitativeParameter) &&  $product->qualitativeParameter->ubicacionTronco == 'Lateral'? 'selected="selected"' : '' }}
                            value="Lateral">Lateral</option>
                        <option
                            {{ isset($product->qualitativeParameter) &&  $product->qualitativeParameter->ubicacionTronco == 'Ambas'? 'selected="selected"' : '' }}
                            value="Ambas">Ambas</option>
                        <option
                            {{ isset($product->qualitativeParameter) &&  $product->qualitativeParameter->ubicacionTronco == 'Sin especificar'? 'selected="selected"' : '' }}
                            value="Sin especificar">Sin especificar</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <div class="form-group">
                    <label for="grieta">Grietas</label>
                    <select class="form-control" name="grieta" id="grieta">
                        <option type="hidden" value=""> -- Seleccionar grieta--</option>
                        <option
                            {{ isset($product->qualitativeParameter) &&  $product->qualitativeParameter->grieta == 'Indiferente'? 'selected="selected"' : '' }}
                            value="Indiferente">Indiferente</option>
                        <option
                            {{ isset($product->qualitativeParameter) &&  $product->qualitativeParameter->grieta == 'No'? 'selected="selected"' : '' }}
                            value="No">No</option>
                        <option
                            {{ isset($product->qualitativeParameter) &&  $product->qualitativeParameter->grieta == 'Si'? 'selected="selected"' : '' }}
                            value="Si">Si</option>
                    </select>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    <label for="rajaduras">Rajaduras</label>
                    <select class="form-control" name="rajaduras" id="rajaduras">
                        <option type="hidden" value=""> -- Seleccionar rajadura--</option>
                        <option
                            {{ isset($product->qualitativeParameter) &&  $product->qualitativeParameter->rajaduras == 'Indiferente'? 'selected="selected"' : '' }}
                            value="Indiferente">Indiferente</option>
                        <option
                            {{ isset($product->qualitativeParameter) &&  $product->qualitativeParameter->rajaduras == 'Si'? 'selected="selected"' : '' }}
                            value="Si">Si</option>
                        <option
                            {{ isset($product->qualitativeParameter) &&  $product->qualitativeParameter->rajaduras == 'No'? 'selected="selected"' : '' }}
                            value="No">No</option>
                        <option
                            {{ isset($product->qualitativeParameter) &&  $product->qualitativeParameter->rajaduras == 'No a simple vista'? 'selected="selected"' : '' }}
                            value="No a simple vista">No a simple vista</option>
                    </select>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    <label for="cantoMuerto">Cantos muertos</label>
                    <select class="form-control" name="cantoMuerto" id="cantoMuerto">
                        <option type="hidden" value=""> -- Seleccionar canto muerto--</option>
                        <option
                            {{ isset($product->qualitativeParameter) &&  $product->qualitativeParameter->cantoMuerto == 'Indiferente'? 'selected="selected"' : '' }}
                            value="Indiferente">Indiferente</option>
                        <option
                            {{ isset($product->qualitativeParameter) &&  $product->qualitativeParameter->cantoMuerto == 'Si'? 'selected="selected"' : '' }}
                            value="Si">Si</option>
                        <option
                            {{ isset($product->qualitativeParameter) &&  $product->qualitativeParameter->cantoMuerto == 'No'? 'selected="selected"' : '' }}
                            value="No">No</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="ibox-title">
        <h5>Alaveos, combas y torceduras<small></small></h5>
        <div class="ibox-tools">
        </div>
    </div>
    <div class="ibox-content">
        <div class="row">
            <div class="col-md-4 col-xs-12">
                <div class="form-group">
                    <label for="alaveos">Alaveos</label>
                    <input type="number" step="0.01" class="form-control" id="alaveos" name="alaveos" placeholder="Alaveos"
                        value="{{isset($product->qualitativeParameter) ? $product->qualitativeParameter->alaveos : old('alaveos')}}">
                </div>
            </div>

            <div class="col-md-4 col-xs-12">
                <div class="form-group">
                    <label for="torceduras">Torceduras</label>
                    <input type="number" step="0.01" class="form-control" id="torceduras" name="torceduras"
                        placeholder="Torceduras"
                        value="{{isset($product->qualitativeParameter) ? $product->qualitativeParameter->torceduras : old('torceduras')}}">
                </div>
            </div>

            <div class="col-md-4 col-xs-12">
                <div class="form-group">
                    <label for="combas">Combas</label>
                    <input type="number" step="0.01" class="form-control" id="combas" name="combas" placeholder="Combas"
                        value="{{isset($product->qualitativeParameter) ? $product->qualitativeParameter->combas : old('combas')}}">
                </div>
            </div>
        </div>
    </div>
</div>