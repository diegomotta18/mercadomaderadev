@include('publications.offerts.partials.titleAndDescription')
@include('publications.offerts.partials.images')
@include('publications.offerts.partials.auction')
@include('publications.offerts.partials.waypay')
@include('publications.offerts.partials.dimension')
@include('publications.offerts.partials.availability')
@include('publications.offerts.partials.condition')
@include('publications.offerts.partials.qualitative_parameter')
@include('publications.offerts.partials.address')
@include('publications.offerts.partials.shippingTime')


<div class="ibox-content">
    <div class="col-sm-12">
        <div class="row">
            <div class="form-group">
                <a class="btn btn-sm btn-default auction_show"><i class="fa fa-plus"></i>
                    Subasta
                </a>
                <a class="btn btn-sm btn-default way_pay"><i class="fa fa-plus"></i>
                    Forma de pago
                </a> 
                <a class="btn btn-sm btn-default dimension"><i class="fa fa-plus"></i>
                    Dimensión
                </a>
        
                <a class="btn btn-sm btn-default availability"><i class="fa fa-plus"></i>
                    Disponibilidad
                </a>
        
                <a class="btn btn-sm btn-default condition"><i class="fa fa-plus"></i>
                    Condición
                </a>

                <a class="btn btn-sm btn-default qualitative_parameter"><i class="fa fa-plus"></i>
                    Parámetros cualitativos
                </a>
                <a class="btn btn-sm btn-default address"><i class="fa fa-home"></i>
                    Dirección
                </a>
                <a class="btn btn-sm btn-default shipping_time"><i class="fa fa-plus"></i>
                    Tipo de envio
                </a>

            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
            <div class="form-group">
                <button class="btn btn-sm btn-primary float-right m-t-n-xs" type="submit"><i class="fa fa-save"></i>
                    Guardar</button>
            </div>
            </div>
        </div>
    </div>
</div>