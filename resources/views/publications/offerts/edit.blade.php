@extends('layouts.app')
@push('styles')
<link href="{{asset('admin/css/plugins/summernote/summernote.css')}}" rel="stylesheet">
<link href="{{asset('admin/css/plugins/select2/select2.min.css')}}" rel="stylesheet">
<link href="{{asset('admin/css/plugins/select2/select2-bootstrap4.min.css')}}" rel="stylesheet">
<link href="{{ asset('admin/css/plugins/dropzone/dropzone.css') }}" rel="stylesheet">
<link href="{{asset('admin/css/plugins/select2/select2.min.css')}}" rel="stylesheet">
<link href="{{asset('admin/css/plugins/select2/select2-bootstrap4.min.css')}}" rel="stylesheet">
<link href="{{asset('admin/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css')}}" rel="stylesheet">
<style>
    label {
        font-size : 13px;
    }
    #map {
        margin: auto;
        width: 80%;
        height: 40vh;
        border: 1px solid black;
    }
</style>
@endpush
@section('content')
<div>
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>@if(asset($type_publication)){{ __($type_publication) }} @endif</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a>Home</a>

                </li>
                <li class="breadcrumb-item">
                    <a>@if(asset($type_publication)){{ __($type_publication) }} @endif</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Editar</strong> <small>({{$publication->title}})
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        {{-- <h5><small></small></h5> --}}
                        <div class="ibox-tools">
                            <a type="button" href="{{ url()->previous() }}" class="btn btn-sm btn-default"> <i
                                    class="fa fa-arrow-left"></i> Atras</a>
                        </div>
                    </div>
                    <form role="form" method="POST" action="{{ route('products.update',[$publication->id, $product->id]) }}">
                        @csrf
                        @method('PUT')
                        <input type="hidden" value="{{$type_publication}}" name="typePublication">
                        @include('publications.offerts.partials.form')
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
@routes
<script src="{{asset('admin/js/plugins/select2/select2.full.min.js')}}"></script>
<script src="{{asset('admin/js/plugins/summernote/summernote.js')}}"></script>
<script src="{{asset('admin/js/plugins/summernote/lang/summernote-es-ES.js')}}"></script>
<script src="{{asset('admin/js/dropz/dropzone.js')}}"></script>
<script src="{{asset('admin/js/plugins/moment/moment.js')}}"></script>
<script src="{{asset('admin/js/plugins/moment/es.js')}}"></script>
<script src="{{asset('admin/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js')}}"></script>
<script>

    $(document).ready(function(){

        $('#price_init').hide();
        $('#finish_datee').hide();
        $('#quantityy').hide();

        if ($('#auction').is(':checked')) {
            $('#price_init').show();
            $('#finish_datee').show();
            $('#quantityy').show();
        }

        $('#auction').change(function () {
            if ($(this).prop("checked")) {
                // checked
                $('#price_init').show();
                $('#finish_datee').show();
                $('#quantityy').show();

                $('#finish_date').datetimepicker({
                    inline: false,
                    locale: 'es',
                    format: "L",

                });
                return;
            }
            $('#price_init').hide();
            $('#finish_datee').hide();
            $('#quantityy').hide();

        });

        $('#finish_date').datetimepicker({
                inline: false,
                locale: 'es',
                format: "L",
        });

        $('.summernote').summernote({
            height: 150,
            lang: 'es-ES', // default: 'en-US'
            language: "es",
            toolbar: [
                // [groupName, [list of button]]
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['insert', ['link']]
            ]
        });
        $('.note-btn').removeAttr('title');
        var idTypeWood =$('#typeWood').val();
        if (idTypeWood){
            $.ajax({
                type:"get",
                url: route('kinds.getKinds', {id: idTypeWood}),
                success:function(res)
                {
                    if(res)
                    {
                        $("#kind").empty();

                        $("#kind").append('<option type="hidden" value=""> -- Seleccionar especie --</option>');
                        $.each(res,function(key,value){
                            $("#kind").append('<option value="'+value.id+'">'+value.name+'</option>');
                        });
                        $('#kind option[value='+$('#kind_id').val() +']').prop("selected", true);

                    }
                }
            });
        }
        $('#typeWood').on('change',function(){
            var id = $(this).val();
            if(id){
                $.ajax({
                    type:"get",
                    url: route('kinds.getKinds', {id: id}),
                    success:function(res)
                    {
                        if(res)
                        {
                            $("#kind").empty();
                            $("#kind").append('<option type="hidden" value=""> -- Seleccionar especie --</option>');
                            $.each(res,function(key,value){
                                $("#kind").append('<option value="'+value.id+'">'+value.name+'</option>');
                            });
                        }
                    }
                });
            }
        });
        $('#dimension').hide();
        $('#availability').hide();
        $('#condition').hide();
        $('#qualitative_parameter').hide();
        $('#shipping_time').hide();
        $('#way_pay').hide();
        $('#address').hide();
        $('#auction_show').hide();

        $('.auction_show').click(function(event){
            event.stopPropagation();
            $('.auction_show').hide();
            $('#auction_show').show();
        });

        $('.address').click(function(event){
            event.stopPropagation();
            $('.address').hide();
            $('#address').show();
        });

        $('.shipping_time').click(function(event){
            event.stopPropagation();
            $('.shipping_time').hide();
            $('#shipping_time').show();
        });

        $('.dimension').click(function(event){
            event.stopPropagation();
            $('.dimension').hide();
            $('#dimension').show();
        });

        $('.availability').click(function(event){
            event.stopPropagation();
            $('.availability').hide();
            $('#availability').show();
        });

        $('.condition').click(function(event){
            event.stopPropagation();
            $('.condition').hide();
            $('#condition').show();
        });

        $('.qualitative_parameter').click(function(event){
            event.stopPropagation();
            $('.qualitative_parameter').hide();
            $('#qualitative_parameter').show();
        });

        $('.way_pay').click(function(event){
             event.stopPropagation();
            $('.way_pay').hide();
             $('#way_pay').show();
        });

        $('#description_send').hide();

        $('#send').on('change',function(){
            var id = $(this).val();
            if(id === 'free from'){
                $('#description_send').show();
            }else{
                $('#description_send').hide();
            }
        });

        if($('#description_send').val() !== ""){
            $('#description_send').show();
        }
    });

    var uploadedDocumentMap = {}
    Dropzone.options.documentDropzone = {
            dictDefaultMessage: 'Clic aqui para seleccionar imagen',
            dictRemoveFile: 'Eliminar',
            dictCancelUpload : 'Cancelar imagen',
            url: '{{route('admin.archivos.store')}}',
            maxFiles: 5,
            maxFilesize: 100, // MB
            addRemoveLinks: true,
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            success: function(file, response) {
                $('form').append('<input type="hidden" name="document[]" value="' + response.name + '">')
                uploadedDocumentMap[file.name] = response.name
            },
            removedfile: function(file) {
                    console.log(file)
                    file.previewElement.remove()
                    var name = ''
                    if (typeof file.file_name !== 'undefined') {
                        name = file.file_name
                    } else {
                        name = uploadedDocumentMap[file.name]
                    }
                    $('form').find('input[name="document[]"][value="' + name+ '"]').remove()
                    var id = "{!!$publication->id!!}"
                    var url = route('admin.archivos.delete',{id: id,file: name})//'/admin/archivos/'+ file.name+'/destroyFileEmpresa';
                    axios.delete(url).then(function(response) {
                        console.log(response)
                    })
                    .catch(function(error) {
                        console.log(error);
                    });
            },
            maxfilesexceeded: function(file){
                this.removeFile(file);
            },
        init: function() {
            @if(isset($publication) && $publication->getMedia('images'))
                @foreach($publication->getMedia('images') as $fotos)
                    var url = "{!!$fotos->getUrl('thumb')!!}"
                    var name = "{!!$fotos->name!!}"
                    var size = "{!!$fotos->size!!}"
                    var file_name = "{!!$fotos->file_name!!}"
                    var mockFile = {
                        name: name,
                        size: size
                    };
                    this.options.addedfile.call(this, mockFile)
                    this.options.thumbnail.call(this, mockFile, url)
                    this.options.complete.call(this, mockFile)
                    uploadedDocumentMap[name] = file_name
                    $('form').append('<input type="hidden" name="document[]" value="' + file_name + '">')
                @endforeach
            @endif
        }
    }
</script>

@if(($publication->address != null))
    <script>
        $( document ).ready(function(){
            var map = null;
            var canvas = null;
            var coordinates = null;

            const address = @json($publication->address);
            const country_id = address.country_id;
            const province_id = address.province_id;
            const department_id = address.department_id;
            const city_id = address.city_id;


            $("#street").val(address.street);
            $("#number").val(address.number);
            $("#lat").val(address.latitude);
            $("#lon").val(address.longitude);
            //carga el mapa incial con los datos traidos del back
            const changeInitMap = () =>{
                //cargar mapa incial con la posicion si tiene address.longitud,address.latitud

                mapboxgl.accessToken = 'pk.eyJ1IjoicnViZW5tb3YiLCJhIjoiY2tqMGN1Nnk3MGZ3NDJycGh2eWJ4NW5ubSJ9.ac6ojU1TR6GcYSPfV_F0WQ';
                map = new mapboxgl.Map({
                    container: 'map',
                    style: 'mapbox://styles/mapbox/streets-v11',
                    center: [address.longitude,address.latitude],
                    zoom: 6
                });

                canvas = map.getCanvasContainer();

                coordinates = document.getElementById('coordinates');

                map.addControl(new mapboxgl.NavigationControl());

                geojson = {
                    'type': 'FeatureCollection',
                    'features': [{
                        'type': 'Feature',
                        'geometry': {
                            'type': 'Point',
                            'coordinates': [address.longitude,address.latitude]
                        }
                    }]
                };

                map.on('load',function(){
                    map.addSource('point', {
                    'type': 'geojson',
                    'data': geojson
                    });

                    map.addLayer({
                        'id': 'point',
                        'type': 'circle',
                        'source': 'point',
                        'paint': {
                            'circle-radius': 10,
                            'circle-color': '#3887be'
                        }
                    });

                    // Set a UI indicator for dragging.
                    canvas.style.cursor = 'grabbing';

                    // Update the Point feature in `geojson` coordinates
                    // and call setData to the source layer `point` on it.
                    geojson.features[0].geometry.coordinates = [address.longitude,address.latitude];

                    map.getSource('point').setData(geojson);

                    map.flyTo({
                        center: geojson.features[0].geometry.coordinates,
                        speed: 0.5,
                        zoom:15
                    });

                    map.on('mouseenter', 'point', function() {
                        map.setPaintProperty('point', 'circle-color', '#3bb2d0');
                            canvas.style.cursor = 'move';
                        });

                        map.on('mouseleave', 'point', function() {
                            map.setPaintProperty('point', 'circle-color', '#3887be');
                            canvas.style.cursor = '';
                        });

                        map.on('mousedown', 'point', function(e) {
                            // Prevent the default map drag behavior.
                            e.preventDefault();

                            canvas.style.cursor = 'grab';

                            map.on('mousemove', onMove);
                            map.once('mouseup', onUp);
                        });

                        map.on('touchstart', 'point', function(e) {
                            if (e.points.length !== 1) return;

                            // Prevent the default map drag behavior.
                            e.preventDefault();

                            map.on('touchmove', onMove);
                            map.once('touchend', onUp);
                        });
                })
            }

            const changeSelectAttributesAddress = ()=>{
                $.ajax({
                    type: "get",
                    url: route('countries.index'),
                    success: function(res) {
                        if (res.data) {
                            $("#country").append('< value="">-- Seleccionar país --</option>');
                            $.each(res.data, function(key, value) {
                                $("#country").append(`<option value="${value.id}">${value.name}</option>`);
                            });
                            $('#country').val(country_id)
                        }
                    }
                });

                $.ajax({
                    type: "get",
                    url: route('countries.provinces.index', [country_id]),
                    success: function(res) {
                        $("#province").append('< value="">-- Seleccionar provincia --</option>');
                        $.each(res.data, function(key, value) {
                            $("#province").append(`<option value="${value.id}">${value.name}</option>`);
                        });
                        $('#province').val(province_id);
                    }
                });

                $.ajax({
                    type: "get",
                    url: route('countries.provinces.departments.index', [country_id, province_id]),
                    success: function(res) {
                        $("#department").append(
                            '<option value="">-- Seleccionar departamento --</option>');
                        $.each(res.data, function(key, value) {
                            $("#department").append(`<option value="${value.id}">${value.name}</option>`);
                        });
                        $('#department').val(department_id)
                    }
                });

                $.ajax({
                    type: "get",
                    url: route('countries.provinces.departments.cities.index', [country_id, province_id, department_id]),
                    success: function(res) {
                        $("#city").append('<option value="">-- Seleccionar localidad --</option>');
                        $.each(res.data, function(key, value) {
                            $("#city").append(`<option value="${value.id}">${value.name}</option>`);
                        });
                        $('#city').val(city_id)

                    }
                });
            }

            changeInitMap();

            changeSelectAttributesAddress();

            $('#country').on('change', function() {

                const country = $(this).val();
                $.ajax({
                    type: "get",
                    url: route('countries.provinces.index', [country]),
                    success: function(res) {
                        $("#province").empty();
                        $("#department").empty();
                        $("#city").empty();
                        $("#province").append('< value="">-- Seleccionar provincia --</option>');
                        $.each(res.data, function(key, value) {
                            $("#province").append(`<option value="${value.id}">${value.name}</option>`);
                        });
                        $('#province').val('')
                    }
                });
            });

            $('#province').change(function() {
                const province = $(this).val();
                const country = $('#country').val();
                $.ajax({
                    type: "get",
                    url: route('countries.provinces.departments.index', [country, province]),
                    success: function(res) {
                        $("#department").empty();
                        $("#city").empty();
                        $("#department").append(
                            '<option value="">-- Seleccionar departamento --</option>');
                        $.each(res.data, function(key, value) {
                            $("#department").append(`<option value="${value.id}">${value.name}</option>`);
                        });
                        $('#department').val('')
                    }
                });
            });

            $('#department').change(function() {
                const department = $(this).val();
                const province = $('#province').val();
                const country = $('#country').val();
                $("#street").val("");
                $("#number").val("");
                $("#lat").val("");
                $("#lon").val("");
                changeMap();
                $.ajax({
                    type: "get",
                    url: route('countries.provinces.departments.cities.index', [country, province, department]),
                    success: function(res) {
                        $("#city").empty();
                        $("#city").append('<option value="">-- Seleccionar localidad --</option>');
                        $.each(res.data, function(key, value) {
                            $("#city").append(`<option value="${value.id}">${value.name}</option>`);
                        });
                        $('#city').val('')

                    }
                });
            });

            $('#city').change(function() {
                $("#street").val("");
                $("#number").val("");
                $("#lat").val("");
                $("#lon").val("");
                changeMap();
            });

            $('#street').change(function(){
            let street =  $(this).val();
                if(street.length > 3){
                    changeMap();
                }
            })

            $('#number').change(function(){
            let number =  $(this).val();
                if(number.length > 0){
                    changeMap();
                }
            })

            const onMove = (e) => {
                var coords = e.lngLat;

                // Set a UI indicator for dragging.
                canvas.style.cursor = 'grabbing';

                // Update the Point feature in `geojson` coordinates
                // and call setData to the source layer `point` on it.
                geojson.features[0].geometry.coordinates = [coords.lng, coords.lat];
                $('#lon').val(coords.lng);
                $('#lat').val(coords.lat);
                map.getSource('point').setData(geojson);
            }

            const onUp = (e) => {
                /* var coords = e.lngLat;

                // Print the coordinates of where the point had
                // finished being dragged to on the map.
                coordinates.style.display = 'block';
                coordinates.innerHTML =
                    'Longitude: ' + coords.lng + '<br />Latitude: ' + coords.lat; */
                canvas.style.cursor = '';

                // Unbind mouse/touch events
                map.off('mousemove', onMove);
                map.off('touchmove', onMove);
            }

            const removeMapLayer = () => {
                if (map.getLayer('point')){
                    map.removeLayer('point');
                }

                if (map.getSource('point')){
                    map.removeSource('point');
                }
            }
            //recarga el mapa con los datos recientes de los selectores
            const changeMap = () =>{

                const country = $("#country option:selected").text();
                const province = $("#province option:selected").val() !==  "" ? $("#province option:selected").text(): "";
                const department = $("#department option:selected").val() !==  "" ? $("#department option:selected").text(): "";
                const city = $("#city option:selected").val() !==  "" ? $("#city option:selected").text(): department;
                const street = $("#number").val().length > 0 ? $("#street").val() + " " +  $("#number").val() : $("#street").val();

                $.ajax({
                        type: "get",
                        url:  `https://nominatim.openstreetmap.org/?format=json&street=${street}&city=${city}&state=${province}&country=Argentina`,
                        success: function(res) {

                        /*  if(!res.length || res[0].lat === undefined){
                                coordinates.style.display = 'block';
                                coordinates.innerHTML =  'No se encuentra coordenada para la localidad seleccionada';
                                canvas.style.cursor = '';
                                return;
                            } */
                            if(res.length){


                                let latitude = res[0].lat;
                                let longitude = res[0].lon;

                                removeMapLayer();

                                geojson = {
                                    'type': 'FeatureCollection',
                                    'features': [{
                                        'type': 'Feature',
                                        'geometry': {
                                            'type': 'Point',
                                            'coordinates': [longitude,latitude]
                                        }
                                    }]
                                };

                                map.addSource('point', {
                                    'type': 'geojson',
                                    'data': geojson
                                });

                                map.addLayer({
                                    'id': 'point',
                                    'type': 'circle',
                                    'source': 'point',
                                    'paint': {
                                        'circle-radius': 10,
                                        'circle-color': '#3887be'
                                    }
                                });

                                // Set a UI indicator for dragging.
                                canvas.style.cursor = 'grabbing';

                                // Update the Point feature in `geojson` coordinates
                                // and call setData to the source layer `point` on it.
                                geojson.features[0].geometry.coordinates = [longitude, latitude];
                                $('#lon').val(longitude);
                                $('#lat').val(latitude);
                                map.getSource('point').setData(geojson);
                                map.flyTo({
                                    center: geojson.features[0].geometry.coordinates,
                                    speed: 0.5,
                                    zoom:15
                                    });
                                map.on('mouseenter', 'point', function() {
                                    map.setPaintProperty('point', 'circle-color', '#3bb2d0');
                                    canvas.style.cursor = 'move';
                                });

                                map.on('mouseleave', 'point', function() {
                                    map.setPaintProperty('point', 'circle-color', '#3887be');
                                    canvas.style.cursor = '';
                                });

                                map.on('mousedown', 'point', function(e) {
                                    // Prevent the default map drag behavior.
                                    e.preventDefault();

                                    canvas.style.cursor = 'grab';

                                    map.on('mousemove', onMove);
                                    map.once('mouseup', onUp);
                                });

                                map.on('touchstart', 'point', function(e) {
                                    if (e.points.length !== 1) return;

                                    // Prevent the default map drag behavior.
                                    e.preventDefault();

                                    map.on('touchmove', onMove);
                                    map.once('touchend', onUp);
                                });
                            }
                        }
                });
            }
        })
    </script>
@endif
@endpush
