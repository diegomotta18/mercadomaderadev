@extends('layouts.app')
@push('styles')
<link href="{{asset('admin/css/plugins/summernote/summernote.css')}}" rel="stylesheet">
<link href="{{asset('admin/css/plugins/select2/select2.min.css')}}" rel="stylesheet">
<link href="{{asset('admin/css/plugins/select2/select2-bootstrap4.min.css')}}" rel="stylesheet">
<link href="{{ asset('admin/css/plugins/dropzone/dropzone.css') }}" rel="stylesheet">
<link href="{{asset('admin/css/plugins/select2/select2.min.css')}}" rel="stylesheet">
<link href="{{asset('admin/css/plugins/select2/select2-bootstrap4.min.css')}}" rel="stylesheet">
<link href="{{asset('admin/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css')}}" rel="stylesheet">
<style>
    #map {
        margin: auto;
        width: 80%;
        height: 40vh;
        border: 1px solid black;
    }
</style>
@endpush
@section('content')
<div>
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>@if(asset($type_publication)){{ __($type_publication) }} @endif</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a>Home</a>

                </li>
                <li class="breadcrumb-item">
                    <a>@if(asset($type_publication)){{ __($type_publication) }} @endif</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Nuevo</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5><small></small></h5>
                        <div class="ibox-tools">
                            <a type="button" href="{{ url()->previous() }}" class="btn btn-sm btn-default"> <i
                                    class="fa fa-arrow-left"></i> Atras</a>
                        </div>
                    </div>
                    <form role="form" method="POST"
                        action="{{ route('products.store',[$type_publication,$subcategory->id]) }}">
                        @csrf
                        <input type="hidden" name="typePublicacion" value="{{ $type_publication }}">
                        @include('publications.offerts.partials.form')
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
@routes
<script src="{{asset('admin/js/plugins/select2/select2.full.min.js')}}"></script>
<script src="{{asset('admin/js/plugins/summernote/summernote.js')}}"></script>
<script src="{{asset('admin/js/plugins/summernote/lang/summernote-es-ES.js')}}"></script>
<script src="{{asset('admin/js/plugins/summernote/summernote.js')}}"></script>
<script src="{{asset('admin/js/plugins/summernote/lang/summernote-es-ES.js')}}"></script>
<script src="{{asset('admin/js/dropz/dropzone.js')}}"></script>
<script src="{{asset('admin/js/plugins/moment/moment.js')}}"></script>
<script src="{{asset('admin/js/plugins/moment/es.js')}}"></script>
<script src="{{asset('admin/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js')}}"></script>


<script>

    $(document).ready(function(){

        $('#price_init').hide();
        $('#finish_datee').hide();
        $('#quantityy').hide();

        $('.summernote').summernote({
            height: 150,
            lang: 'es-ES', // default: 'en-US'
            language: "es",
            toolbar: [
                // [groupName, [list of button]]
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['insert', ['link']]
            ]
        });

        $('.note-btn').removeAttr('title');
        $('#dimension').hide();
        $('#availability').hide();
        $('#condition').hide();
        $('#qualitative_parameter').hide();
        $('#shipping_time').hide();
        $('#auction_show').hide();
        $('#way_pay').hide();
        $('#address').hide();

        $('.auction_show').click(function(event){
            event.stopPropagation();
            $('.auction_show').hide();
            $('#auction_show').show();
        });

        $('.address').click(function(event){
            event.stopPropagation();
            $('.address').hide();
            $('#address').show();
        });

        $('.shipping_time').click(function(event){
            event.stopPropagation();
            $('.shipping_time').hide();
            $('#shipping_time').show();
        });

        $('.dimension').click(function(event){
            event.stopPropagation();
            $('.dimension').hide();
            $('#dimension').show();
        });

        $('.availability').click(function(event){
            event.stopPropagation();
            $('.availability').hide();
            $('#availability').show();
        });

        $('.condition').click(function(event){
            event.stopPropagation();
            $('.condition').hide();
            $('#condition').show();
        });

        $('.qualitative_parameter').click(function(event){
            event.stopPropagation();
            $('.qualitative_parameter').hide();
            $('#qualitative_parameter').show();
        });

        $('.way_pay').click(function(event){
            event.stopPropagation();
            $('.way_pay').hide();
            $('#way_pay').show();
        });

        $("#typeWood").select2({
            theme: 'bootstrap4',
        });

        $('#typeWood').on('change',function(){
            var id = $(this).val();
            if(id){
                $.ajax({
                    type:"get",
                    url: route('kinds.getKinds', {id: id}),
                    success:function(res)
                    {
                        if(res)
                        {
                            $("#kind").empty();
                            $("#kind").append('<option type="hidden" value=""> -- Seleccionar especie --</option>');
                            $.each(res,function(key,value){
                                $("#kind").append('<option value="'+value.id+'">'+value.name+'</option>');
                            });
                        }
                    }
                });
            }
        });

        $('#description_send').hide();

        $('#send').on('change',function(){
            var id = $(this).val();
            if(id === 'free from'){
                $('#description_send').show();
            }else{
                $('#description_send').hide();
            }
        });

        $('#auction').change(function () {
            if ($(this).prop("checked")) {
                // checked
                $('#price_init').show();
                $('#finish_datee').show();
                $('#quantityy').show();
                $('#finish_date').datetimepicker({
                    inline: false,
                    locale: 'es',
                    format: "L",

                });
                $('#ammount').prop('disabled',true);
                $('#ammount').val(0.00);
                $('#auction_active').html("Este campo es bloquedado porque esta activo la opción <b>subastar</b>");
                return;
            }else{
                $('#ammount').prop('disabled',false);
                $('#ammount').val(0.00);
                $('#auction_active').html("");

            }
            $('#price_init').hide();
            $('#finish_datee').hide();
            $('#quantityy').hide();

        });


    });


    var uploadedDocumentMap = {}
    Dropzone.options.documentDropzone = {
        dictDefaultMessage: 'Clic aqui para seleccionar imagen',
        dictRemoveFile: 'Eliminar',
        dictCancelUpload : 'Cancelar imagen',
        url: '{{ route('admin.archivos.store')}}',
        maxFilesize: 5, // MB
        addRemoveLinks: true,
        headers: {
            'X-CSRF-TOKEN': "{{ csrf_token() }}"
        },
        success: function(file, response) {
            $('form').append('<input type="hidden" name="document[]" value="' + response.name + '">')
            uploadedDocumentMap[file.name] = response.name
        },
        removedfile: function(file) {
            file.previewElement.remove()
            var name = ''
            if (typeof file.file_name !== 'undefined') {
                name = file.file_name
            } else {
                name = uploadedDocumentMap[file.name]
            }
            $('form').find('input[name="document[]"][value="' + name + '"]').remove()
        },
        init: function() {
        }
    }
</script>



<script>
        $("#country").select2({
            placeholder: "-- Seleccionar país --",
            allowClear: true,
            language: { noResults: () => "Sin resultados"},
            theme: 'bootstrap4',
        });
        $("#province").select2({
            placeholder: "-- Seleccionar provincia --",
            language: { noResults: () => "Sin resultados"},
            theme: 'bootstrap4',
            allowClear: true
        });
        $("#department").select2({
            placeholder: "-- Seleccionar departamento --",
            language: { noResults: () => "Sin resultados"},
            theme: 'bootstrap4',
            allowClear: true
        });
        $("#city").select2({
            placeholder: "-- Seleccionar ciudad --",
            language: { noResults: () => "Sin resultados"},
            theme: 'bootstrap4',
            allowClear: true
        });

</script>

<script>

    $( document ).ready(function(){
        $.ajax({
            type: "get",
            url: route('countries.index'),
            success: function(res) {
                if (res.data) {
                    $("#province").empty().append('< value="">-- Seleccionar provincia --</option>');
                    $("#department").empty().append('< value="">-- Seleccionar departamento --</option>');
                    $("#city").empty().append('< value="">-- Seleccionar ciudad --</option>');
                    $("#country").append('< value="">-- Seleccionar país --</option>');
                    $.each(res.data, function(key, value) {
                        $("#country").append(`<option value="${value.id}">${value.name}</option>`);
                    });
                    $('#country').val('')
                }
            }
        });

        mapboxgl.accessToken = 'pk.eyJ1IjoicnViZW5tb3YiLCJhIjoiY2tqMGN1Nnk3MGZ3NDJycGh2eWJ4NW5ubSJ9.ac6ojU1TR6GcYSPfV_F0WQ';
        const map = new mapboxgl.Map({
            container: 'map',
            style: 'mapbox://styles/mapbox/streets-v11',
            center: [-54.6922327,-27.042191],
            zoom: 6
        });

        var canvas = map.getCanvasContainer();

        var coordinates = document.getElementById('coordinates');

        $('#country').on('change', function() {
            console.log($(this).val());
            const country = $(this).val();
            $.ajax({
                type: "get",
                url: route('countries.provinces.index', [country]),
                success: function(res) {
                    $("#province").empty();
                    $("#department").empty();
                    $("#city").empty();
                    $("#province").append('< value="">-- Seleccionar provincia --</option>');
                    $.each(res.data, function(key, value) {
                        $("#province").append(`<option value="${value.id}">${value.name}</option>`);
                    });
                    $('#province').val('')
                }
            });
        });

        $('#province').change(function() {
            const province = $(this).val();
            const country = $('#country').val();
            $.ajax({
                type: "get",
                url: route('countries.provinces.departments.index', [country, province]),
                success: function(res) {
                    $("#department").empty();
                    $("#city").empty();
                    $("#department").append(
                        '<option value="">-- Seleccionar departamento --</option>');
                    $.each(res.data, function(key, value) {
                        $("#department").append(`<option value="${value.id}">${value.name}</option>`);
                    });
                    $('#department').val('')
                }
            });
        });

        $('#department').change(function() {
            const department = $(this).val();
            const province = $('#province').val();
            const country = $('#country').val();
            changeMap();
            $.ajax({
                type: "get",
                url: route('countries.provinces.departments.cities.index', [country, province, department]),
                success: function(res) {
                    $("#city").empty();
                    $("#city").append('<option value="">-- Seleccionar localidad --</option>');
                    $.each(res.data, function(key, value) {
                        $("#city").append(`<option value="${value.id}">${value.name}</option>`);
                    });
                    $('#city').val('')

                }
            });
        });

        $('#city').change(function() {
            changeMap();
        });


        $('#street').change(function(){
           let street =  $(this).val();
            if(street.length > 3){
                changeMap();
            }
        })

        $('#number').change(function(){
           let number =  $(this).val();
            if(number.length > 0){
                changeMap();
            }
        })

        const onMove = (e) => {
            var coords = e.lngLat;

            // Set a UI indicator for dragging.
            canvas.style.cursor = 'grabbing';

            // Update the Point feature in `geojson` coordinates
            // and call setData to the source layer `point` on it.
            geojson.features[0].geometry.coordinates = [coords.lng, coords.lat];
            $('#lon').val(coords.lng);
            $('#lat').val(coords.lat);
            map.getSource('point').setData(geojson);
        }

        const onUp = (e) => {
            /* var coords = e.lngLat;

            // Print the coordinates of where the point had
            // finished being dragged to on the map.
            coordinates.style.display = 'block';
            coordinates.innerHTML =
                'Longitude: ' + coords.lng + '<br />Latitude: ' + coords.lat; */
            canvas.style.cursor = '';

            // Unbind mouse/touch events
            map.off('mousemove', onMove);
            map.off('touchmove', onMove);
        }

        const removeMapLayer = () => {
            if (map.getLayer('point')){
                map.removeLayer('point');
            }

            if (map.getSource('point')){
                map.removeSource('point');
            }
        }

        map.addControl(new mapboxgl.NavigationControl());

        const changeMap = () =>{

            const country = $("#country option:selected").text();
            const province = $("#province option:selected").val() !==  "" ? $("#province option:selected").text(): "";
            const department = $("#department option:selected").val() !==  "" ? $("#department option:selected").text(): "";
            const city = $("#city option:selected").val() !==  "" ? $("#city option:selected").text(): department;
            const street = $("#number").val().length > 0 ? $("#street").val() + " " +  $("#number").val() : $("#street").val();

            $.ajax({
                    type: "get",
                    url:  `https://nominatim.openstreetmap.org/?format=json&street=${street}&city=${city}&state=${province}&country=Argentina`,
                    success: function(res) {

                       /*  if(!res.length || res[0].lat === undefined){
                            coordinates.style.display = 'block';
                            coordinates.innerHTML =  'No se encuentra coordenada para la localidad seleccionada';
                            canvas.style.cursor = '';
                            return;
                        } */
                        if(res.length){


                            let latitude = res[0].lat;
                            let longitude = res[0].lon;

                            removeMapLayer();

                            geojson = {
                                'type': 'FeatureCollection',
                                'features': [{
                                    'type': 'Feature',
                                    'geometry': {
                                        'type': 'Point',
                                        'coordinates': [longitude,latitude]
                                    }
                                }]
                            };

                            map.addSource('point', {
                                'type': 'geojson',
                                'data': geojson
                            });

                            map.addLayer({
                                'id': 'point',
                                'type': 'circle',
                                'source': 'point',
                                'paint': {
                                    'circle-radius': 10,
                                    'circle-color': '#3887be'
                                }
                            });

                            // Set a UI indicator for dragging.
                            canvas.style.cursor = 'grabbing';

                            // Update the Point feature in `geojson` coordinates
                            // and call setData to the source layer `point` on it.
                            geojson.features[0].geometry.coordinates = [longitude, latitude];
                            $('#lon').val(longitude);
                            $('#lat').val(latitude);
                            map.getSource('point').setData(geojson);
                            map.flyTo({
                                center: geojson.features[0].geometry.coordinates,
                                speed: 0.5,
                                zoom:15
                                });
                            map.on('mouseenter', 'point', function() {
                                map.setPaintProperty('point', 'circle-color', '#3bb2d0');
                                canvas.style.cursor = 'move';
                            });

                            map.on('mouseleave', 'point', function() {
                                map.setPaintProperty('point', 'circle-color', '#3887be');
                                canvas.style.cursor = '';
                            });

                            map.on('mousedown', 'point', function(e) {
                                // Prevent the default map drag behavior.
                                e.preventDefault();

                                canvas.style.cursor = 'grab';

                                map.on('mousemove', onMove);
                                map.once('mouseup', onUp);
                            });

                            map.on('touchstart', 'point', function(e) {
                                if (e.points.length !== 1) return;

                                // Prevent the default map drag behavior.
                                e.preventDefault();

                                map.on('touchmove', onMove);
                                map.once('touchend', onUp);
                            });
                        }
                    }
            });
        }
    })
    </script>

@endpush
