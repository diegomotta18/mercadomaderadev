@extends('layouts.app')
@push('styles')


@endpush
@section('content')
<div>
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2> Estado de la publicación</h2>

            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a>Home</a>

                </li>
                <li class="breadcrumb-item">
                    <a>Estado de publicación</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Editar</strong> <small>({{$publication->title}})
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5> <small></small></h5>
                        <div class="ibox-tools">
                            <a type="button" href="{{ url()->previous() }}" class="btn btn-sm btn-default"> <i
                                    class="fa fa-arrow-left"></i> Atras</a>
                        </div>
                    </div>

                    <form role="form" method="POST" action="{{ route('publications.status.update',$publication->id) }}">
                        @csrf
                        @method('PUT')
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="title">Titulo</label>
                                        <h5><strong>{{$publication->title}}</strong></h5>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="status">Estado</label>
                                        <select class="form-control" name="status" id="status">
                                            <option type="hidden" value=""> -- Seleccionar estado--</option>


                                            <option
                                                {{ isset($publication->status) &&  $publication->status == 'actived'? 'selected="selected"' : '' }}
                                                value="actived">Activo
                                            </option>
                                            <option
                                                {{ isset($publication->status) &&  $publication->status == 'paused'? 'selected="selected"' : '' }}
                                                value="paused">Pausado
                                            </option>
                                            @if(auth()->user()->can('Cambiar.estado'))
                                            <option
                                                {{ isset($publication->status) &&  $publication->status == 'pending'? 'selected="selected"' : '' }}
                                                value="pending">Pendiente
                                            </option>
                                            <option
                                                {{ isset($publication->status) &&  $publication->status == 'review'? 'selected="selected"' : '' }}
                                                value="review">Revisión
                                            </option>

                                            <option
                                                {{ isset($publication->status) &&  $publication->status == 'approved'? 'selected="selected"' : '' }}
                                                value="approved">Aprobado
                                            </option>

                                            <option
                                                {{ isset($publication->status) &&  $publication->status == 'disapproved'? 'selected="selected"' : '' }}
                                                value="disapproved">Desaprobado
                                            </option>
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <button class="btn btn-sm btn-primary float-right m-t-n-xs" type="submit"><i
                                                class="fa fa-save"></i>
                                            Guardar</button>

                                    </div>
                                </div>

                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>

</div>
@endsection
@push('scripts')
@routes


<script>

</script>
@endpush
