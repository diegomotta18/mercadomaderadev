
<div class="ibox-content">
    @if ($errors->any())
    <div class="alert alert-danger">
        <h4>Se ha encontrado los siguientes campos sin completar o erroneos</h4>
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <div class="row">
        <div class="col-sm-12">
            <div class="form-group">
                <label for="title">Titulo</label>
                <input type="text" id="title" placeholder="Titulo" name="title" class="form-control"
                    value="{{isset($simple) ? $simple->title : old('title')}}">
            </div>
        </div>
        <div class="col-sm-12">
            <div class="form-group">
                <label for="description">Descripción</label>
                <textarea id="summernote" class="summernote form-control"
                    name="description">{{isset($simple) ? $simple->description : old('description')}}</textarea>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="form-group">
                <label for="price">Precio</label>
                <input type="number" step="0.01" id="price" placeholder="Precio" name="price" class="form-control"
                    value="{{isset($simple) ? $simple->price : old('price')}}">
            </div>
        </div>
    </div>
    
</div>
@include('publications.simples.partials.images')

<div class="ibox-content">
    <div class="col-sm-12">
        <div class="row">
            <div class="col-sm-12">
            <div class="form-group">
                <button class="btn btn-sm btn-primary float-right m-t-n-xs" type="submit"><i class="fa fa-save"></i>
                    Guardar</button>
            </div>
            </div>
        </div>
    </div>
</div>