@extends('layouts.app')
@push('styles')
<link href="{{asset('admin/css/plugins/summernote/summernote.css')}}" rel="stylesheet">
<link href="{{ asset('admin/css/plugins/dropzone/dropzone.css') }}" rel="stylesheet">

@endpush
@section('content')
<div>
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2> Publicación simple</h2>

            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a>Home</a>

                </li>
                <li class="breadcrumb-item">
                    <a>Publicación simple</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Editar</strong> <small>({{$simple->title}})
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5> <small></small></h5>
                        <div class="ibox-tools">
                            <a type="button" href="{{ url()->previous() }}" class="btn btn-sm btn-default"> <i
                                    class="fa fa-arrow-left"></i> Atras</a>
                        </div>
                    </div>

                    <form role="form" method="POST" action="{{ route('simples.update',$simple->id) }}">
                        @csrf
                        @method('PUT')
                        @include('publications.simples.partials.form')
                    </form>
                    
                </div>
            </div>
        </div>
    </div>

</div>
@endsection
@push('scripts')
@routes
<script src="{{asset('admin/js/plugins/summernote/summernote.js')}}"></script>
<script src="{{asset('admin/js/plugins/summernote/lang/summernote-es-ES.js')}}"></script>
<script src="{{asset('admin/js/dropz/dropzone.js')}}"></script>

<script>
    $(document).ready(function(){
        $('.summernote').summernote({
            height: 150,
            lang: 'es-ES', // default: 'en-US'
            language: "es",
            toolbar: [
                // [groupName, [list of button]]
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['insert', ['link']]
            ]
        });
        $('.note-btn').removeAttr('title');
    });
    var uploadedDocumentMap = {}
    Dropzone.options.documentDropzone = {
            dictDefaultMessage: 'Clic aqui para seleccionar imagen',
            dictRemoveFile: 'Eliminar',
            dictCancelUpload : 'Cancelar imagen',
            url: '{{route('admin.archivos.store')}}',
            maxFiles: 1,
            maxFilesize: 100, // MB
            addRemoveLinks: true,
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            success: function(file, response) {
                $('form').append('<input type="hidden" name="document[]" value="' + response.name + '">')
                uploadedDocumentMap[file.name] = response.name
            },
            removedfile: function(file) {
                    console.log(file)
                    file.previewElement.remove()
                    var name = ''
                    if (typeof file.file_name !== 'undefined') {
                        name = file.file_name
                    } else {
                        name = uploadedDocumentMap[file.name]
                    }
                    $('form').find('input[name="document[]"][value="' + name+ '"]').remove()
                    var id = "{!!$simple->id!!}"
                    var url = route('admin.archivos.delete',{id: id,file: file.name})//'/admin/archivos/'+ file.name+'/destroyFileEmpresa';
                    axios.delete(url).then(function(response) {
                        console.log(response)
                    })
                    .catch(function(error) {
                        console.log(error);
                    });
            },
            maxfilesexceeded: function(file){
                this.removeFile(file);
            },
        init: function() {
            @if(isset($simple) && $simple->getMedia('images'))
                @foreach($simple->getMedia('images') as $fotos)
                    var url = "{!!$fotos->getUrl('thumb')!!}"
                    var name = "{!!$fotos->name!!}"
                    var size = "{!!$fotos->size!!}"
                    var file_name = "{!!$fotos->file_name!!}"
                    var mockFile = {
                        name: name,
                        size: size
                    };
                    this.options.addedfile.call(this, mockFile)
                    this.options.thumbnail.call(this, mockFile, url)
                    this.options.complete.call(this, mockFile)
                    uploadedDocumentMap[name] = file_name
                    $('form').append('<input type="hidden" name="document[]" value="' + file_name + '">')
                @endforeach
            @endif
        }
    }
</script>
@endpush