@extends('layouts.app')
@push('styles')
<style>
    a.disableded {
        pointer-events: none;
        cursor: default;
        opacity: 0.5;
    }
</style>
@endpush
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Publicación simple</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a>Home</a>

            </li>
            <li class="breadcrumb-item">
                <a>Publicación simple</a>
            </li>
            {{-- <li class="breadcrumb-item active">
                <strong>Basic Form</strong>
            </li> --}}
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5> <small></small></h5>
                    <div class="ibox-tools">
                        <a style="color: #fff !important;
                        background-color: #23c6c8  !important;
                        border-color: #23c6c8 !important; " type="button" href="{{ route('simples.categories') }}" class="btn btn-sm btn-info">
                            <i class="fa fa-plus"></i>
                            Nuevo
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="m-b-sm m-t-sm">
                        <form role="search" method="POST" action="{{ route('simples.search') }}">
                            @csrf
                            @method('GET')
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <a href="{{ route('simples.index') }}" tabindex="-1" class="btn btn-white btn-sm"
                                        type="button"><i class="fa fa-refresh"></i>
                                        Refrescar</a>
                                </div>
                                <input type="text" class="form-control form-control-sm" name="title">
                                <div class="input-group-append">
                                    <button tabindex="-1" class="btn btn-primary btn-sm" type="submit">Buscar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    {{-- <th>#ID</th> --}}
                                    <th class="text-center"></th>
                                    <th class="text-center">Nombre </th>
                                    <th class="text-center">Precio/Unidad</th>
                                    <th class="text-center">Fecha de creación </th>
                                    <th class="text-center">Estado de la publicación</th>
                                    <th class="text-center"></th>

                                </tr>
                            </thead>
                            <tbody>
                                @foreach($simples as $publication)
                                <tr class="row-{{$publication->id}}">
                                    {{-- <td>{{$publication->id}}</td> --}}
                                    <td class="text-center align-middle"><img class="img-thumbnail"  src="@if (!$publication->hasMedia('images')) http://placehold.it/120 @else {{$publication->getMedia('images')[0]->getUrl('thumb')}}@endif"></td>
                                    <td class="text-center align-middle">{{$publication->title}}</td>
                                    <td class="text-center align-middle" style="width: auto !important;">@if($publication->price) <h2>${{$publication->price}}</h2>@endif</td>
                                    <td class="text-center align-middle">{{$publication->created_at->format('d/m/Y')}}</td>
                                    <td class="text-center align-middle"><span class="label label-warning">{{__($publication->status)}}</span></td>

                                    <td class="text-center align-middle" style="width: 200px !important;">

                                        @foreach($publication->products as $product)
                                        <div class="btn-group">

                                                @if($publication->isReview() || $publication->isPending() || $publication->isDisapproved())
                                                    <button type="button"
                                                        class="btn btn-white btn-block" disabled>
                                                        <span class="fa fa-flag-o" data-toggle="tooltip" data-placement="top" title="Estado en revisión"></span>
                                                    </button>
                                                @else
                                                    <a type="button"
                                                        class="btn btn-white btn-block"
                                                        href="{{route('publications.status.edit',$publication->id)}}" data-toggle="tooltip" data-placement="top" title="Cambiar estado">
                                                        <span class="fa fa-flag-o"></span>
                                                    </a>
                                                @endif
                                                @if(isset($publication->observation) && $publication->isObservation())
                                                <a type="button"
                                                    class="btn btn-white btn-sm"
                                                    href="{{route('observations.show',[$publication,$publication->observation])}}"><span class="fa fa-info-circle" style="color:#1ab394;" data-toggle="tooltip" data-placement="top" title="Ver observación"></span></a>
                                                @else
                                                <button class="btn btn-white" disabled ><span class="fa fa-minus-circle" style="color:#1c84c6;" data-toggle="tooltip" data-placement="top" title="Sin observación"></span></button>
                                                @endif

                                            <a type="button" class="btn btn-white btn-sm"
                                                data-toggle="tooltip" data-placement="top" title="Editar"
                                                href="{{route('simples.edit',$publication->id)}}"><i
                                                    class="fa fa-pencil"></i>
                                            </a>



                                            <a type="button" class="btn btn-white btn-sm  btn-dgr"
                                                data-toggle="tooltip" data-placement="top" title="Eliminar"
                                                data-id="{{$publication->id}}"><i class="fa fa-trash"></i>
                                            </a>

                                        </div>
                                        @endforeach
                                    </td>
                                </tr>
                                @endforeach
                                    <div><i class="fa fa-info-circle" style="color:#1ab394;"> </i> Publicación con observaciones</div>
                                    <div><i class="fa fa-minus-circle" style="color:#1c84c6;"> </i> Publicación sin observaciones</div>
                            </tbody>
                        </table>
                        {{-- <div class="text-center">
                            {{$simples->links()}}
                        </div> --}}
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

@endsection
@push('scripts')

@routes
<script>
    $(document).ready(function () {

        $('.btn-dgr').on('click', function () {
            var id = $(this).data('id');
            var url = route('simples.destroy', {id: id});

            Swal.fire({
                title: 'Confirmar',
                text: "¿Eliminar publicación?",
                type: 'info',
                showCancelButton: true,
                cancelButtonText: "Cancelar",
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Confirmar'
            }).then(function (result) {
                if (result.value) {
                    axios.delete(url)
                        .then(function (res) {

                            if (res.status === 204) {

                                const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 3000,
                                timerProgressBar: true,
                                onOpen: (toast) => {
                                    toast.addEventListener('mouseenter', Swal.stopTimer)
                                    toast.addEventListener('mouseleave', Swal.resumeTimer)
                                    }
                                })

                                Toast.fire({
                                    icon: 'success',
                                    title: 'Publicación elimininado'
                                })

                                $('.row-'+id).remove();
                            }

                        }).catch(function (res) {
                            swal("No tiene los privilegios para realizar esta acción!", '', 'error');
                        });
                }
            });
        });
    });
</script>
@endpush
