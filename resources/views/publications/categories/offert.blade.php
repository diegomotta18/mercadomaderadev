@extends('layouts.app')

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Publicar oferta</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a>Home</a>

            </li>
            <li class="breadcrumb-item">
                <strong>Eligir categoría</strong>
            </li>

        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>Elige una categoría</h5>
                </div>
                <div class="ibox-content">
                    @foreach ($categories as $categorie)
                    <div class="row">

                        <div class="col-sm-6 btn-group @if($categorie->subcategories->count()  ) dropright @endif">
                            <button type="button"
                                class="btn btn-default block full-width @if($categorie->subcategories->count()  ) dropdown-toggle @endif"
                                data-toggle="dropdown">
                                {{__($categorie->name)}}
                            </button>
                            @if($categorie->subcategories->count() )
                            <div class="dropdown-menu">
                                @foreach ($categorie->subcategories as $subcategory)
                                    <a class="dropdown-item" href="{{route("products.create",['offert',$subcategory->id])}}">{{ __($subcategory->name) }}</a>
                                @endforeach
                            </div>
                            @endif
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
 @endsection

