@extends('layouts.app')
@push('styles')
<style>
    a.disableded {
        pointer-events: none;
        cursor: default;
        opacity: 0.5;
    }
</style>
@endpush
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Publicaciones</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a>Home</a>

            </li>
            <li class="breadcrumb-item">
                <strong><a>Publicaciones</a></strong>
            </li>

        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ">
                <div class="ibox-content">
                    <div class="m-b-sm m-t-sm">
                        <form role="search" method="POST" action="{{ route('publications.search') }}">
                            @csrf
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <a href="{{ route('publications.index') }}" tabindex="-1"
                                        class="btn btn-white btn-sm" type="button"><i class="fa fa-refresh"></i>
                                        Refrescar</a>
                                </div>
                                <input type="text" style="height: 33px !important;" class="form-control form-control-sm" name="search" placeholder="Título">
                                <select class="form-control" name="status" id="status">
                                    <option type="hidden" value=""> -- Seleccionar estado--</option>
                                    <option
                                        value="inactived">Inactivo
                                    </option>
                                    <option
                                        value="paused">Pausado
                                    </option>
                                    <option
                                        value="pending">Pendiente
                                    </option>
                                    <option
                                        value="review">Revisión
                                    </option>

                                    <option
                                        value="approved">Aprobado
                                    </option>

                                    <option
                                        value="disapproved">Desaprobado
                                    </option>
                                </select>
                                <div class="input-group-append">
                                    <button tabindex="-1" class="btn btn-primary btn-sm" type="submit">Buscar</button>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    {{-- <th>#ID</th> --}}
                                    <th class="text-center"></th>
                                    <th class="text-center">Nombre </th>
                                    <th class="text-center">Precio/Unidad</th>
                                    <th class="text-center">Unidad</th>
                                    <th class="text-center">Total</th>
                                    <th class="text-center">Fecha de creación </th>
                                    <th class="text-center">Tipo de publicación</th>
                                    <th class="text-center">Estado de la publicación</th>
                                    <th class="text-center"></th>

                                </tr>
                            </thead>
                            <tbody>
                                @foreach($publications as $publication)
                                <tr class="row-{{$publication->id}}">
                                    {{-- <td>{{$publication->id}}</td> --}}
                                    <td class="text-center align-middle"><img class="img-thumbnail"  src="@if (!$publication->hasMedia('images')) https://place-hold.it/120 @else {{$publication->getMedia('images')[0]->getUrl('thumb')}}@endif"></td>
                                    <td class="text-center align-middle">{{$publication->title}}</td>
                                    <td class="text-center align-middle" style="width: auto !important;">@if($publication->price) <h2>${{$publication->price}}</h2>@endif</td>
                                    <td class="text-center align-middle">{{$publication->unit}}</td>
                                    <td class="text-center align-middle" style="width: auto !important;"><h2>@if($publication->total)${{$publication->total}}@endif</h2></td>
                                    <td class="text-center align-middle">{{$publication->created_at->format('d/m/Y')}}</td>
                                    <td class="text-center align-middle">{{__($publication->typePublication)}}</td>
                                    <td class="text-center align-middle"><span class="label label-warning">{{__($publication->status)}}</span></td>

                                    <td class="text-center align-middle" style="width: auto !important;">

                                        @foreach($publication->products as $product)
                                        <div class="btn-group">
                                            @if(auth()->user()->hasRole('ROOT|Administrador'))
                                            <a type="button" class="btn btn-white btn-block"
                                            data-toggle="tooltip" data-placement="top" title="Cambiar estado"
                                                href="{{route('publications.status.edit',$publication->id)}}"><i
                                                    class="fa fa-flag-o" ></i>
                                            </a>
                                            <a type="button" class="btn btn-white"
                                            data-toggle="tooltip" data-placement="top" title="Crear observación"
                                                href="{{route('observations.create',$publication)}}"><i
                                                    class="fa fa-eye" ></i>
                                            </a>
                                            @else
                                                @if($publication->isReview() || $publication->isPending() || $publication->isDisapproved())
                                                    <button type="button"
                                                        class="btn btn-white btn-block" disabled>
                                                        <span class="fa fa-flag-o" data-toggle="tooltip" data-placement="top" title="Estado en revisión"></span>
                                                    </button>
                                                @else
                                                    <a type="button"
                                                        class="btn btn-white btn-block"
                                                        href="{{route('publications.status.edit',$publication->id)}}" data-toggle="tooltip" data-placement="top" title="Cambiar estado">
                                                        <span class="fa fa-flag-o"></span>
                                                    </a>
                                                @endif
                                            @endif
                                            @if(isset($publication->observation) && $publication->isObservation())
                                            <a type="button"
                                                class="btn btn-white btn-block"
                                                href="{{route('observations.show',[$publication,$publication->observation])}}"><span class="fa fa-info-circle" style="color:#1ab394;" data-toggle="tooltip" data-placement="top" title="Ver observación"></span></a>
                                            @else
                                            <button class="btn btn-white" disabled ><span class="fa fa-minus-circle" style="color:#1c84c6;" data-toggle="tooltip" data-placement="top" title="Sin observación"></span></button>
                                            @endif
                                            @can('Modificar.oferta')
                                            {{-- <button type="button" class="btn btn-white btn-edit"
                                                data-toggle="tooltip" data-placement="top" title="Editar"
                                                data-category="{{$product->subcategory_id}}"
                                                data-publication="{{$publication->id}}" data-product="{{$product->id}}"
                                                data-typepublication="{{$publication->typePublication}}">
                                                <i class="fa fa-edit" ></i>
                                            </button> --}}
                                            <a type="button"
                                            class="btn btn-white"
                                               data-toggle="tooltip" data-placement="top" title="Editar"
                                                href="{{route('products.edit',[$publication->typePublication,$publication->id,$product->id])}}">
                                                <i class="fa fa-edit" ></i>
                                            </a>
                                            @else
                                            <a type="button" class="btn btn-white"
                                                data-toggle="tooltip" data-placement="top" title="Editar"
                                                href="{{route('publications.edit',$publication->id)}}"><i
                                                    class="fa fa-pencil" ></i>
                                            </a>
                                            @endcan
                                            <a type="button" class="btn btn-white btn-dgr"
                                                data-toggle="tooltip" data-placement="top" title="Eliminar"
                                                data-id="{{$publication->id}}" ><i class="fa fa-trash"></i>
                                                </a>

                                            <a type="button" class="btn btn-white"
                                                data-toggle="tooltip" data-placement="top" title="Calculadora de costos"
                                                href="{{route('calculator.index',$publication)}}"
                                                ><i class="fa fa-calculator"></i>
                                                </a>
                                        </div>
                                        @endforeach
                                    </td>
                                </tr>
                                @endforeach
                                    <div><i class="fa fa-info-circle" style="color:#1ab394;"> </i> Publicación con observaciones</div>
                                    <div><i class="fa fa-minus-circle" style="color:#1c84c6;"> </i> Publicación sin observaciones</div>
                            </tbody>
                        </table>
                        <div class="text-center">
                            {{$publications->links()}}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@push('scripts')

@routes
<script>
    $(document).ready(function () {
        $('.btn-edit').on('click', function(){
            let category_id = $(this).data('category');
            let publication_id = $(this).data('publication');
            let product_id = $(this).data('product');
            let type_publication = $(this).data('typepublication');
            console.log(category_id,publication_id,product_id,type_publication);
            let url = route('publications.subcategory', {id: category_id});
            axios.get(url).then(function(result){
                if(result.data.name === 'sawmills' ){
                    let url = route('products.edit', {type_publication: type_publication,publication_id: publication_id,product_id:product_id});
                    window.location.replace(url);
                }
                if(result.data.name === 'corrals' ){
                    let url = route('corrals.edit', {type_publication: type_publication,publication_id: publication_id,product_id:product_id});
                    console.log(url);
                    window.location.replace(url);
                }
            }).catch(function (res) {
                swal("Notifique al admninistrador de sistema, esta acción no esta permitida", '', 'error');
            });;
        })
        $('.btn-dgr').on('click', function () {
            var id = $(this).data('id');
            var url = route('publications.destroy', {id: id});

            Swal.fire({
                title: 'Confirmar',
                text: "¿Eliminar publicación?",
                type: 'info',
                showCancelButton: true,
                cancelButtonText: "Cancelar",
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Confirmar'
            }).then(function (result) {
                if (result.value) {
                    axios.delete(url)
                        .then(function (res) {

                            if (res.status === 204) {

                                const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 3000,
                                timerProgressBar: true,
                                onOpen: (toast) => {
                                    toast.addEventListener('mouseenter', Swal.stopTimer)
                                    toast.addEventListener('mouseleave', Swal.resumeTimer)
                                    }
                                })

                                Toast.fire({
                                    icon: 'success',
                                    title: 'Producto elimininado'
                                })

                                $('#row-'+id).remove();
                            }

                        }).catch(function (res) {
                            swal("No tiene los privilegios para realizar esta acción!", '', 'error');
                        });
                }
            });
        });
    });
</script>
@endpush
