@extends('layouts.app')

@section('content')
@if ($request->collection_status == "approved")
    <div class="alert alert-primary" role="alert">
        Felicidades usted se ha suscripto como usuario Premium.
    </div>
@endif
@if ($request->collection_status == "pending")
    <div class="alert alert-warning" role="alert">
        Usted no completó el proceso de pago, deberá completarlo para ser usuario Premium.
    </div>
@endif
@if ($request->collection_status == "rejected")
    <div class="alert alert-danger" role="alert">
        El pago fue rechazado, intente nuevamente.
    </div>
@endif

<div class="container">
</div>
@endsection
