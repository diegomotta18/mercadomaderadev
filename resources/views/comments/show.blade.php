@extends('layouts.app')
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Comentarios</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a>Home</a>

            </li>
            <li class="breadcrumb-item">
                <strong><a>Comentarios</a></strong>
            </li>

        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ">
                <div class="ibox-content">
                    @if (isset($comment))
                            <div class="row">
                                <div class="col-md-12 col-xs-12" id="">
                                    <small class="active">{{ $comment->user->name }}
                                        {{ $comment->created_at }}</small>
                                        <p class="card-text list-group-item  show-reply"><a href="{{ route('view.publications.show',$comment->publication->id)}}"  data-toggle="tooltip" data-placement="top" title="Ver producto"><img class="img-thumbnail" style="width:5%;"  src="@if (!$comment->publication->hasMedia('images')) http://placehold.it/120 @else {{$comment->publication->getMedia('images')[0]->getUrl('thumb')}}@endif"></a><strong> {{ $comment->publication->title }}</strong></p>
                                    <p class="card-text list-group-item active show-reply"
                                        id="show-reply-{{ $comment->id }}" data-id="{{ $comment->id }}">
                                        <svg class="bi bi-chat-square" width="1em" height="1em" viewBox="0 0 16 16"
                                            fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                            <path fill-rule="evenodd"
                                                d="M14 1H2a1 1 0 00-1 1v8a1 1 0 001 1h2.5a2 2 0 011.6.8L8 14.333 9.9 11.8a2 2 0 011.6-.8H14a1 1 0 001-1V2a1 1 0 00-1-1zM2 0a2 2 0 00-2 2v8a2 2 0 002 2h2.5a1 1 0 01.8.4l1.9 2.533a1 1 0 001.6 0l1.9-2.533a1 1 0 01.8-.4H14a2 2 0 002-2V2a2 2 0 00-2-2H2z"
                                                clip-rule="evenodd" />
                                        </svg>
                                        {{ $comment->comment }}

                                    </p>

                                    <form class="reply-{{ $comment->id }}" style="display: none;" method="POST"
                                        action="{{ route('comments.reply', $comment->id) }}">
                                        @csrf
                                        @method('PUT')

                                        <div class="form-group">
                                            <textarea class="form-control" id="subcomment" name="subcomment"
                                                placeholder="Escribe tu respuesta" rows="2"></textarea>
                                        </div>

                                        @csrf
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-success"
                                                data-id="{{ $comment->id }}">
                                                <i class="fa fa-send" aria-hidden="true"></i>
                                                Responder
                                            </button>

                                            <button type="button" class="btn btn-light btn-cancel"
                                                data-id="{{ $comment->id }}">
                                                <i class="fa fa-send" aria-hidden="true"></i>
                                                Cancelar
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')

<script>
    $(document).ready(function() {
        $('.show-reply').on('click', function() {
            console.log("mostrar input para responder comentario")
            var id = $(this).data('id');
            $(".reply-" + id).show()
            $("#show-reply-" + id).removeClass("active");
        });

        $('.btn-cancel').on('click', function() {
            console.log("cancelar comentario");
            var id = $(this).data('id');
            $(".reply-" + id).hide()
            $("#show-reply-" + id).addClass("active");
        });
    });
</script>

@endpush
