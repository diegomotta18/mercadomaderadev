@extends('layouts.public')

@section('content')
<section class="section-content padding-y">
<div class="container">

<div class="row">
	<main class="col-md-9">
<div class="card">

<table class="table table-borderless table-shopping-cart">
<thead class="text-muted">
<tr class="small text-uppercase">
  <th scope="col">Producto</th>
  <th scope="col" width="120">Cantidad/
    m² a cubrir </th>
  <th scope="col" width="120">Precio</th>
  <th scope="col" class="text-right" width="200"> </th>
</tr>
</thead>
<tbody>
    @foreach($contents as $row)

    <tr>
        <td>
            <figure class="itemside">
                <div class="aside"><img src="{{ $row->attributes->image }}" class="img-sm"></div>
                <figcaption class="info">
                    <a href="page-shopping-cart.html#" class="title text-dark">{{  $row->name}}</a>
                   <span class="tag">Espesor {{ $row->associatedModel->dimension->actualThickness }} mm</span>
                    <span class="tag">Ancho {{ $row->associatedModel->dimension->actualWidth }} mm</span>
                    <span class="tag">Largo {{ $row->associatedModel->dimension->actualLength }} mm</span>

                </figcaption>
            </figure>
        </td>
        <td>
            {{ $row->quantity }}  {{ $row->attributes->option_buy }}
        </td>
        <td>
            <div class="price-wrap">
                <var class="price"> $ {{$row->price}} </var>
                <small class="text-muted"> $/PT {{ $row->attributes->price }}</small>
            </div> <!-- price-wrap .// -->
        </td>
        <td class="text-right">
        {{-- <a data-original-title="Save to Wishlist" title="" href="page-shopping-cart.html" class="btn btn-light" data-toggle="tooltip"> <i class="fa fa-heart"></i></a> --}}
        <a class="btn btn-light delete-item" data-item-id="{{ $row->id }}"> Eliminar</a>
        </td>
    </tr>
    @endforeach



</tbody>
</table>

<div class="card-body border-top">
	<a class="btn btn-primary float-md-right confirm-cart" style="color: white;"> Confirmar pedido<i class="fa fa-chevron-right"></i> </a>
	<a href="{{ route('public.home')  }}" class="btn btn-light"> <i class="fa fa-chevron-left"></i> Continuar comprando</a>
</div>
</div> <!-- card.// -->

{{--<div class="alert alert-success mt-3">--}}
{{--	<p class="icontext"><i class="icon text-success fa fa-truck"></i> Free Delivery within 1-2 weeks</p>--}}
{{--</div>--}}

	</main> <!-- col.// -->
	<aside class="col-md-3">
		{{-- <div class="card mb-3">
			<div class="card-body">
			<form>
				<div class="form-group">
					<label>Have coupon?</label>
					<div class="input-group">
						<input type="text" class="form-control" name="" placeholder="Coupon code">
						<span class="input-group-append">
							<button class="btn btn-primary">Apply</button>
						</span>
					</div>
				</div>
			</form>
			</div>
		</div> --}}
		<div class="card">
			<div class="card-body">
					<dl class="dlist-align">
					  <dt>Total:</dt>
					  <dd class="text-right  h5"><strong>$ {{ $total }}</strong></dd>
					</dl>
					<hr>
					{{-- <p class="text-center mb-3">
						<img src="http://bootstrap-ecommerce.com/templates/alistyle-html/images/misc/payments.png" height="26">
					</p> --}}

			</div> <!-- card-body.// -->
		</div>  <!-- card .// -->
	</aside> <!-- col.// -->
</div>

</div> <!-- container .//  -->
</section>
@endsection
@push('scripts')
@routes
<script>
            $('.delete-item').on('click', function(){
                var myData = {
                    item_id: $(this).data("item-id")
                };
                console.log(myData);

                axios.post(route("shopping_cart.delete", myData)).then(res => {
                    window.location = res.data.redirect;
                })
                .catch(error => {
                    console.log(error)
                });

            })

            $('.confirm-cart').on('click', function(){
                Swal.fire({
                    title: '¿Seguro de realizar la compra?',
                    text: "No podrá revertir esta acción",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#ff6a00',
                    cancelButtonColor: '#e5e7ea',
                    confirmButtonText: 'Si',
                    cancelButtonText: 'Cancelar',
                    confirmButtonClass: 'btn btn-primary',
                    cancelButtonClass: 'btn btn-light',
                    buttonsStyling: true
                }).then(function () {

                    axios.post(route("shopping_cart.confirm")).then(res => {
                        window.location = res.data.redirect;
                    })
                        .catch(error => {
                            console.log(error)
                        });
                    //success method
                }, function (dismiss) {
                    // dismiss can be 'cancel', 'overlay',
                    // 'close', and 'timer'

                })


            })


 </script>
@endpush
