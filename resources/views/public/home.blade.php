@extends('layouts.public')
@section('content')
<div class="container">
{{--    <section class="section-main padding-y">--}}
{{--        <main class="card">--}}
{{--            <div class="card-body">--}}
{{--                <div class="row">--}}
{{--                    <div class="col-md-9 col-xl-7 col-lg-7">--}}
{{--                        <div id="carousel1_indicator" class="slider-home-banner carousel slide" data-ride="carousel">--}}
{{--                            <ol class="carousel-indicators">--}}
{{--                                <li data-target="#carousel1_indicator" data-slide-to="0" class="active"></li>--}}
{{--                                <li data-target="#carousel1_indicator" data-slide-to="1"></li>--}}
{{--                                <li data-target="#carousel1_indicator" data-slide-to="2"></li>--}}
{{--                            </ol>--}}
{{--                            <div class="carousel-inner">--}}
{{--                                <div class="carousel-item active">--}}
{{--                                    <img src="{{ asset('main/images/banners/slide1.jpg')}}" alt="First slide">--}}
{{--                                </div>--}}
{{--                                <div class="carousel-item">--}}
{{--                                    <img src="{{ asset('main/images/banners/slide2.jpg')}}" alt="Second slide">--}}
{{--                                </div>--}}
{{--                                <div class="carousel-item">--}}
{{--                                    <img src="images/banners/slide3.jpg" alt="Third slide">--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <a class="carousel-control-prev" href="#carousel1_indicator" role="button"--}}
{{--                                data-slide="prev">--}}
{{--                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>--}}
{{--                                <span class="sr-only">Previous</span>--}}
{{--                            </a>--}}
{{--                            <a class="carousel-control-next" href="#carousel1_indicator" role="button"--}}
{{--                                data-slide="next">--}}
{{--                                <span class="carousel-control-next-icon" aria-hidden="true"></span>--}}
{{--                                <span class="sr-only">Next</span>--}}
{{--                            </a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="col-md d-none d-lg-block flex-grow-1">--}}
{{--                        <aside class="special-home-right">--}}
{{--                            <h6 class="">Categorias</h6>--}}
{{--                            @foreach ($categoriesLimits as $item)--}}
{{--                            <div class="card-banner border-bottom">--}}
{{--                                <div class="py-3" style="width:80%">--}}
{{--                                    <h6 class="card-title">{{__($item->name)}}</h6>--}}
{{--                                    <a href="#" class="btn btn-secondary btn-sm"> Ver más </a>--}}
{{--                                </div>--}}
{{--                                <img src="{{asset('images/items/1.jpg')}}" height="80" class="img-bg">--}}
{{--                            </div>--}}
{{--                            @endforeach--}}
{{--                        </aside>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </main>--}}
{{--    </section>--}}
    <section class="padding-bottom-sm">
        <header class="section-heading heading-line">
            <h4 class="title-section text-uppercase">Productos</h4>
        </header>
        <div class="row row-sm">
            @foreach ($publications as $publication)
            <div class="col-md-3">
                <figure class="card card-product-grid" style="cursor: pointer;" onclick="window.location='{{ route('view.publications.show',$publication->id)}}'">

                        <div class="img-wrap">
                            <img src="@if (!$publication->hasMedia('images')) https://place-hold.it/700x400 @else {{$publication->getMedia('images')[0]->getUrl()}}@endif">
                        </div>
                        <figcaption class="info-wrap">
                            <a href="{{ route('view.publications.show',$publication->id)}}" class="title mb-2">{{$publication->title}}</a>
                            <div class="price-wrap">
                                <span class="price">@if(!$publication->isAuction())${{ $publication->total }}@else {{ $publication->auction->price_init  }}@endif</span>
                                <small class="text-muted"></small>
                            </div>
                            <p class="mb-3">

                                @if(isset($publication->products()->first()->typeWood))
                                <span class="tag">@if(isset($publication->products()->first()->typeWood->name)){{$publication->products()->first()->typeWood->name}} @endif</span>
                                @endif

                                @if(isset($publication->products()->first()->kind))
                                <span class="tag">@if(isset($publication->products()->first()->kind->name)){{$publication->products()->first()->kind->name}} @endif</span>
                                @endif

                                @if(isset($publication->products()->first()->typeProduct))
                                <span class="tag">@if(isset($publication->products()->first()->typeProduct->name)){{$publication->products()->first()->typeProduct->name}}@endif</span>
                                @endif

                                @if(isset($publication->products()->first()->dimension->actualThickness))<span class="tag">Espesor {{$publication->products()->first()->dimension->actualThickness}} mm </span>@endif
                                @if(isset($publication->products()->first()->dimension->actualWidth))<span class="tag">Ancho {{$publication->products()->first()->dimension->actualWidth}} mm </span>@endif</td>
                                @if(isset($publication->products()->first()->dimension->actualLength))<span class="tag">Largo {{$publication->products()->first()->dimension->actualLength}} mm </span>@endif</td>
                            </p>
                        </figcaption>
                </figure>

            </div>
            @endforeach

        </div>
        <div class="row row-sm">
            <div style="margin: 0 auto !important; text-align: center !important; ">
                {{$publications->links()}}
            </div>
        </div>


    </section>
    <section class="padding-bottom">

        <div class="row">
            <div class="col-md-12">
                <div class="card-banner banner-quote overlay-gradient">
                    <div class="card-img-overlay white">
                        <h3 class="card-title">¿Está buscando nuevos proveedores o clientes para sus productos madereros y corralón?</h3>
                        <p class="card-text" style="max-width: 400px">Nosotros podemos ayudarle.</p>
                        <a href="#" class="btn btn-primary rounded-pill">Contáctenos</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
