@extends('layouts.public')

@section('content')

<section class="section-pagetop bg-light">
	<div class="container">
		<h2 class="title-page">Vendedor</h2>
	</div> <!-- container .// -->
</section>


<!-- ========================= SECTION CONTENT ========================= -->
<section class="section-content bg-white padding-y">
	<div class="container">
		<div class="row mt-5">

			<div class="col-md-6">
				@if (isset($company->name))
				<h5 class="card-title">
					Empresa del vendedor
				</h5>
				<p>
					<span class="fa fa-bank"> </span> {{$company->name}}
				</p>
				@endif
				@if (isset($company->cuit))
				<h5 class="card-title">
					Cuit
				</h5>
				<p>
					<span class="fa fa-hashtag"> </span> {{$company->cuit}}
				</p>
				@endif
				@if (isset($company->trajectory))
				<h5 class="card-title">
					Trayectoria
				</h5>
				<p>
					<span class="fa fa-signal"> </span> {{$company->trajectory}}
				</p>
				@endif
				@if (isset($company->compromise))
				<h5 class="card-title">
					Compromiso
				</h5>
				<p>
					<span class="fa fa-start"> </span> {{$company->compromise}}
				</p>
				@endif
				@if (isset($company->technologies))
				<h5 class="card-title">
					Tecnologias
				</h5>
				<p>
					<span class="fa fa-cogs"> </span> {{$company->technologies}}
				</p>
				@endif
				@if (isset($company->others))
				<h5 class="card-title">
					Otros
				</h5>
				<p>
					<span class="fa fa-info"> </span> {{$company->others}}
				</p>
				@endif
				@if ($company->addressLocation() != null || $company->addressComplete() != null)
				<h5 class="card-title">
					Dirección
				</h5>
				@if($company->addressLocation()!= null)
				<p><span class="fa fa-globe"> </span> {{$company->addressLocation()}}</p>
				@endif
				@if(($company->addressComplete()!= null))
				<p><i class="fa fa-map-marker"></i> {{$company->addressComplete()}}</p>
				@endif
				@endif
				{{-- @if(isset($company->contact->telephone))
				<h5 class="card-title">
					Telefono
				</h5>
				<p>
					<span class="fa fa-phone"> </span> {{$company->contact->telephone}}
				</p>
				@endif
				@if(isset($company->contact->celphone))
				<h5 class="card-title">
					Celular
				</h5>
				<p>
					<span class="fa fa-mobile"> </span> {{$company->contact->celphone}}
				</p>
				@endif
				@if(isset($company->contact->email))
				<h5 class="card-title">
					Email
				</h5>
				<p>
					<span class="fa fa-envelope"> </span> {{$company->contact->email}}
				</p>
				@endif --}}
				{{-- @if(isset($company->contact->web))
				<h5 class="card-title">
					Web
				</h5>
				<p>
					<span class="fa fa-globe"> </span> {{$company->contact->web}}
				</p>
				@endif --}}
			</div>

			<div class="col-md-6">
				@if(isset($profile->nameComplete))
				<h5>
					Nombres y Apellidos del Vendedor
				</h5>
				<p>
					<span class="fa fa-id-card-o"> </span> {{$profile->nameComplete}}
				</p>
				@endif
				@if(isset($profile->dni))
				<h5>
					Número de documento
				</h5>
				<p>
					<span class="fa fa-hashtag"> </span> {{$profile->dni}}
				</p>
				@endif
				@if(isset($profile->cuil))
				<h5>
					Cuil
				</h5>
				<p>
					<span class="fa fa-hashtag"> </span> {{$profile->cuil}}
				</p>
				@endif
				{{-- @if(isset($profile->burndate))
				<h5>
					Fecha de nacimiento
				</h5>
				<p>
					<span class="fa fa-calendar"> </span> {{$profile->burndate}}
				</p>
				@endif --}}

				{{-- @if($profile->addressLocation() !=null|| $profile->addressComplete()!= null)

				<h5>
					Dirección
				</h5>
				@if(($profile->addressLocation()) != null)
				<p><span class="fa fa-globe"> </span> {{$profile->addressLocation()}}</p>
				@endif
				@if(($profile->addressComplete()) !=null)
				<p><i class="fa fa-map-marker"></i> {{$profile->addressComplete()}}</p>
				@endif
				@endif --}}
                @if (auth()->check() && auth()->user()->hasRole('Premium'))
                    @if(isset($profile->contact->telphone))
                    <h5>
                        Telefono
                    </h5>
                    <p>
                        <span class="fa fa-phone"> </span> {{$profile->contact->telphone}}
                    </p>
                    @endif
                    @if(isset($profile->contact->celphone))
                    <h5>
                        Celular
                    </h5>
                    <p>
                        <span class="fa fa-mobile"> </span> {{$profile->contact->celphone}}
                    </p>
                    @endif
                    @if(isset($profile->contact->email))
                    <h5>
                        Email
                    </h5>
                    <p>
                        <span class="fa fa-envelope"> </span> {{$profile->contact->email}}
                    </p>
                    @endif
                @endif
               {{--
				@if(isset($profile->contact->web))
				<h5>
					Web
				</h5>
				<p>
					<span class="fa fa-globe"> </span> {{$profile->contact->web}}
				</p>
				@endif --}}
			</div>
		</div>
        <header class="section-heading heading-line">
            <h4 class="title-section text-uppercase" style="background-color: white;">Productos</h4>
        </header>
		<div class="row row-sm">



			@foreach ($publications as $item)
			<div class="col-xl-2 col-lg-3 col-md-4 col-6">
				<div class="card card-sm card-product-grid">
					<a href="{{ route('view.publications.show',$item->id)}}" class="img-wrap"> <img src="@if (!$item->hasMedia('images')) https://place-hold.it/480 @else {{$item->getMedia('images')[0]->getUrl()}}@endif"> </a>
					<figcaption class="info-wrap">
					<a href="{{ route('view.publications.show',$item->id)}}" class="title">{!!Str::words($item->title,3,' ...') !!}</a>
					<div class="price mt-1">$ {{ $item->total}}</div> <!-- price-wrap.// -->
					</figcaption>
				</div>
			</div> <!-- col.// -->
			@endforeach


		</div> <!-- container .//  -->
		<div class="row row-sm">
			<br><br>

			<a href="javascript: history.back()" class="btn btn-light"> &laquo Atras</a>
		</div>
	</div>
</section>

@endsection
