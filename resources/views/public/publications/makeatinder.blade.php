@extends('layouts.public')

@section('content')

    <section class="section-pagetop bg-light">
        <div class="container">
            <h2 class="title-page">{{ $publication->title }}</h2>
        </div>
    </section>

    <section class="section-content padding-y">
        <div class="container">
            <div class="row">

                <main class="col-md-9">

                    <div class="card">
                        <div class="card-body">
                            <a href="javascript: history.back()" class="btn btn-light"> <i class="fa fa-chevron-left"></i>
                                Atrás</a>
                                <h5 class="title-description">Descripción</h5>
                                <p>{!!$publication->description!!}</p>
                        </div>

                        @if (isset($product->availability) && isset($product->dimension) && isset($product->condition) && isset($product->qualitativeParameter))

                            <table class="table table-borderless table-shopping-cart">
                                <tbody>
                                    <tr>
                                        <th colspan="2">Disponibilidad</th>
                                    </tr>
                                    <tr>
                                        <td>Unidad de medida</td>

                                        <td>
                                            @if (isset($product->availability->unitMeasurement))
                                                {{ $product->availability->unitMeasurement }}@endif
                                        </td>

                                    </tr>
                                    <tr>
                                        <td>Disponibilidad</td>
                                        <td>
                                            @if (isset($product->availability->availability))
                                                {{ __($product->availability->availability) }}@endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Plazo de producción</td>
                                        <td>
                                            @if (isset($product->availability->measurementTerm))
                                                {{ $product->availability->measurementTerm }}@endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Cantidad/Disponibilidad mensual</td>
                                        <td>
                                            @if (isset($product->availability->cantitude))
                                                {{ $product->availability->cantitude }}@endif
                                        </td>
                                    </tr>


                                    <tr>
                                        <th colspan="2">Dimensiones</th>
                                    </tr>
                                    <tr>
                                        <td>Espesor nominal</td>
                                        <td>
                                            @if (isset($product->dimension->nominalThickness))
                                                {{ $product->dimension->nominalThickness }} " @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Ancho nominal</td>
                                        <td>
                                            @if (isset($product->dimension->nominalWidth))
                                                {{ $product->dimension->nominalWidth }} " @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Largo nominal </td>
                                        <td>
                                            @if (isset($product->dimension->nominalLength))
                                                {{ $product->dimension->nominalLength }} "@endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Espesor real </td>
                                        <td>
                                            @if (isset($product->dimension->actualThickness))
                                                {{ $product->dimension->actualThickness }} mm @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Ancho real</td>
                                        <td>
                                            @if (isset($product->dimension->actualWidth))
                                                {{ $product->dimension->actualWidth }} mm @endif
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>Largo real</td>
                                        <td>
                                            @if (isset($product->dimension->actualLength))
                                                {{ $product->dimension->actualLength }} mm @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Es mas espesor</td>
                                        <td>
                                            @if (isset($product->dimension->isMoreThickness))
                                                {{ $product->dimension->isMoreThickness }} mm @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Es menos espesor</td>
                                        <td>
                                            @if (isset($product->dimension->isLessThickness))
                                                {{ $product->dimension->isLessThickness }} mm @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Es mas ancho</td>
                                        <td>
                                            @if (isset($product->dimension->isMoreWidth))
                                                {{ $product->dimension->isMoreWidth }} mm @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Es menos ancho</td>
                                        <td>
                                            @if (isset($product->dimension->isLessWidth))
                                                {{ $product->dimension->isLessWidth }} mm @endif
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>Es mas largo</td>
                                        <td>
                                            @if (isset($product->dimension->isMoreLength))
                                                {{ $product->dimension->isMoreLength }} mm @endif
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>Es menos largo</td>
                                        <td>
                                            @if (isset($product->dimension->isLessLength))
                                                {{ $product->dimension->isLessLength }} mm @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <th colspan="2">Condiciones del paquete</th>
                                    </tr>
                                    <tr>
                                        <td>Piezas</td>
                                        <td>
                                            @if ($product->condition->piece)
                                                {{ $product->condition->piece }}@endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Peso</td>
                                        <td>
                                            @if ($product->condition->weight)
                                                {{ $product->condition->weight }}@endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Largo</td>
                                        <td>
                                            @if ($product->condition->length)
                                                {{ $product->condition->length }}@endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Ancho</td>
                                        <td>
                                            @if ($product->condition->width)
                                                {{ $product->condition->width }}@endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Alto</td>
                                        <td>
                                            @if ($product->condition->tall)
                                                {{ $product->condition->tall }}@endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Zunchos</td>
                                        <td>
                                            @if ($product->condition->zunchos)
                                                {{ $product->condition->zunchos }}@endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Zunchos</td>
                                        <td>
                                            @if ($product->condition->wrapping)
                                                {{ $product->condition->wrapping }}@endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <th colspan="2">Parametros cualitativos</th>
                                    </tr>
                                    <tr>
                                        <td>Calidad</td>
                                        <td>
                                            @if ($product->qualitativeParameter->nudoMuerto)
                                                {{ $product->qualitativeParameter->nudoMuerto }}@endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Mancha de hongo</td>
                                        <td>
                                            @if ($product->qualitativeParameter->manchaHongo)
                                                {{ $product->qualitativeParameter->manchaHongo }}@endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Secado</td>
                                        <td>
                                            @if ($product->qualitativeParameter->secado)
                                                {{ $product->qualitativeParameter->secado }}@endif
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>Humedad Máxima</td>
                                        <td>
                                            @if ($product->qualitativeParameter->humedadMax)
                                                {{ $product->qualitativeParameter->humedadMax }}@endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Humedad Mínima</td>
                                        <td>
                                            @if ($product->qualitativeParameter->humedadMin)
                                                {{ $product->qualitativeParameter->humedadMin }}@endif
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>Ubicación de tronco</td>
                                        <td>
                                            @if ($product->qualitativeParameter->ubicacionTronco)
                                                {{ $product->qualitativeParameter->ubicacionTronco }}
                                            @endif
                                        </td>
                                    </tr>


                                    <tr>
                                        <td>Grietas</td>
                                        <td>
                                            @if ($product->qualitativeParameter->grieta)
                                                {{ $product->qualitativeParameter->grieta }}@endif
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>Rajaduras</td>
                                        <td>
                                            @if ($product->qualitativeParameter->rajaduras)
                                                {{ $product->qualitativeParameter->rajaduras }}@endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Canto muerto</td>
                                        <td>
                                            @if ($product->qualitativeParameter->cantoMuerto)
                                                {{ $product->qualitativeParameter->cantoMuerto }}@endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Alaveos</td>
                                        <td>
                                            @if ($product->qualitativeParameter->alaveos)
                                                {{ $product->qualitativeParameter->alaveos }}@endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Combas</td>
                                        <td>
                                            @if ($product->qualitativeParameter->combas)
                                                {{ $product->qualitativeParameter->combas }}@endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Torceduras</td>
                                        <td>
                                            @if ($product->qualitativeParameter->torceduras)
                                                {{ $product->qualitativeParameter->torceduras }}@endif
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        @endif
                    </div>

                </main>

                <aside class="col-md-3">
                    <form role="form" method="POST"
                        action="{{ route('publication.auction.makeATender', $publication->id) }}">
                        @csrf
                        <div class="card mb-3">
                            <div class="card-body">
                                <dl class="dlist-align">
                                    <dt>Precio inicial:</dt>
                                    <dd class="text-right">$ {{ $publication->auction->price_init }}</dd>
                                </dl>
                                @if(isset($publication->auction->bidders) && count($publication->auction->bidders)> 0)
                                <dl class="dlist-align">
                                    <dt>Ultimo precio ofertado:</dt>
                                    <dd class="text-right">${{ $publication->auction->bidders->last()->ammount }}</dd>
                                </dl>
                                @endif
                                <hr>
                                <p class="text-center mb-3">
                                </p>

                            </div> <!-- card-body.// -->
                        </div>

                        <div class="card mb-3">
                            <div class="card-body">
                                <div class="form-group">

                                @if ($errors->any())
                                <div class="alert alert-danger">
                                        @foreach ($errors->all() as $error)
                                        <span>{{ $error }}</span>
                                        @endforeach
                                </div>
                                @endif
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="number" step="0.01"  class="form-control" name="ammount"
                                            placeholder="Precio a ofertar">
                                        <span class="input-group-append">
                                            <button type="submit" class="btn btn-primary">Enviar</button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </aside>

            </div>
        </div>
    </section>

@endsection
