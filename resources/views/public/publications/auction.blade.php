@extends('layouts.public')

@section('content')

    <section class="section-pagetop bg-light">
        <div class="container">
            <h2 class="title-page">{{ $publication->title }}</h2>
        </div>
    </section>

    <section class="section-content bg-white padding-y">
        <div class="container">
            <div class="row">
                <div class="col-md-12 mb-3">
                    <h5 class="title-description">Descripción</h5>
                    <p>{!! $publication->description !!}</p>
                </div>
                <div class="col-md-12 mb-3">
                    <h5 class="title-description">Precio incial</h5>
                    <p>$ {{ $publication->auction->price_init }}</p>
                </div>
                <div class="col-md-12 mb-3">
                    <h5 class="title-description">Cantidad</h5>
                    <p>$ {{ $publication->auction->quantity }}</p>
                </div>
                <div class="col-md-12 mb-3">
                    <h5 class="title-description">Fecha de finalización</h5>
                    <p>{{ $publication->auction->finish_date }}</p>
                </div>
                <div class="col-md-12 mb-3">
                    <h5 class="title-description">Estado</h5>
                    <p style="font-size: 1.5rem;">@if($publication->auction->isActived()) <span class="badge badge-success">Activo</span> @else <span class="badge badge-danger">Finalizado</span> @endif </p>
                </div>
                @if($publication->auction->user_id != null)
                <div class="col-md-12 mb-3">
                    <h5 class="title-description">Ganador</h5>
                    <p style="font-size: 1.5rem;"><span class="badge badge-info"> {{ $publication->auction->user->name}} </span></p>
                </div>
                @endif
            </div>
            <div class="row">
                <div class="col-md-6">
                    <h5 class="title-description">Participantes</h5>
                </div>
                <div class="col-md-6 mb-3">
                    <div class="float-right">
                    <a href="{{ route('publication.auction.createTinder',$publication->id) }}" class="btn btn-success"><i class="fas fa-hand-holding-usd"></i> Ofertar</a>
                    </div>
                </div>
                @if(isset($publication->auction) &&  !empty($publication->auction->bidders))
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Nombre </th>
                                        <th>Fecha ofertada</th>
                                        <th>Precio sugerido </th>

                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($publication->auction->bidders as $bidder)
                                    <tr  class="row-{{$bidder->id}} @if($publication->auction->isWinner($bidder->id)) table-success  @endif">
                                        <td>{{$bidder->user->name}}</td>
                                        <td>{{$bidder->created_at}}</td>
                                        <td>$ {{$bidder->ammount}}</td>
                                    </tr>
                                    @endforeach
                                </tbody>

                            </table>
                        </div>
                    </div>
                @endif
            </div>

            <div class="row row-sm">
                <br><br>

                <a href="javascript: history.back()" class="btn btn-light"> &laquo Atras</a>
            </div>
        </div>
    </section>

@endsection
