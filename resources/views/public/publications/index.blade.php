@extends('layouts.public')
@section('content')
    <div class="container">
        <div class="row">

            <div class="col-md-3 padding-y">

                    <article class="filter-group">
                        <header class="card-header">
                            <a href="#" data-toggle="collapse" data-target="#collapse_aside1" aria-expanded="false"
                               class="collapsed">
                                <i class="icon-control fa fa-chevron-down"></i>
                                <h6 class="title">Orden de publicación </h6>
                            </a>
                        </header>
                        <div class="filter-content " id="collapse_aside1" style="">
                            <div class="card-body">


                                <label class="custom-control custom-radio">
                                    <input type="radio" name="order" value="most_relevant" id="order-publication-1"
                                           class="custom-control-input">
                                    <div class="custom-control-label">Relevante</div>
                                </label>

                                <label class="custom-control custom-radio">
                                    <input type="radio" name="order" value="higer_price" id="order-publication-2"
                                           class="custom-control-input">
                                    <div class="custom-control-label">Mayor precio</div>
                                </label>

                                <label class="custom-control custom-radio">
                                    <input type="radio" name="order" value="lower_price" id="order-publication-3"
                                           class="custom-control-input">
                                    <div class="custom-control-label">Menor precio</div>
                                </label>
                                <label class="custom-control custom-radio">
                                    <input type="radio" name="order" value="most_score" id="order-publication-4"
                                           class="custom-control-input">
                                    <div class="custom-control-label">Mayor calificación</div>
                                </label>

                                <label class="custom-control custom-radio">
                                    <input type="radio" name="order" value="lower_score" id="order-publication-5"
                                           class="custom-control-input">
                                    <div class="custom-control-label">Menor calificación</div>
                                </label>

                            </div>
                        </div>
                    </article>

                    <article class="filter-group">
                        <header class="card-header">
                            <a href="#" data-toggle="collapse" data-target="#collapse_aside2" aria-expanded="false"
                               class="collapsed">
                                <i class="icon-control fa fa-chevron-down"></i>
                                <h6 class="title">Tipo de publicación </h6>
                            </a>
                        </header>
                        <div class="filter-content " id="collapse_aside2" style="">
                            <div class="card-body">

                                <label class="custom-control custom-radio">
                                    <input type="radio" name="type" value="offert" id="type-publication-1"
                                           class="custom-control-input">
                                    <div class="custom-control-label">Oferta</div>
                                </label>

                                <label class="custom-control custom-radio">
                                    <input type="radio" name="type" value="demand" id="type-publication-2"
                                           class="custom-control-input">
                                    <div class="custom-control-label">Demanda</div>
                                </label>

                                <label class="custom-control custom-radio">
                                    <input type="radio" name="type" value="auction" id="type-publication-3"
                                           class="custom-control-input">
                                    <div class="custom-control-label">Subasta</div>
                                </label>
                                <label class="custom-control custom-radio">
                                    <input type="radio" name="type" value="" id="type-publication-4"
                                           class="custom-control-input">
                                    <div class="custom-control-label">Todos</div>
                                </label>
                            </div>

                        </div>
                    </article>

                    <article class="filter-group">
                        <header class="card-header">
                            <a href="#" data-toggle="collapse" data-target="#collapse_aside3" aria-expanded="false"
                               class="collapsed">
                                <i class="icon-control fa fa-chevron-down"></i>
                                <h6 class="title">Rango de precios </h6>
                            </a>
                        </header>
                        <div class="filter-content collapse" id="collapse_aside3" style="">
                            <div class="card-body">
                                {{--                        <input type="range" class="custom-range" min="0" max="100000" name="">--}}
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label>Min</label>
                                        <input class="form-control" placeholder="0.0" type="number"
                                               id="publication-min-price"
                                               name="max_price">
                                    </div>
                                    <div class="form-group text-right col-md-6">
                                        <label>Max</label>
                                        <input class="form-control" placeholder="0.0" type="number"
                                               id="publication-max-price"
                                               name="min_price">
                                    </div>
                                </div> <!-- form-row.// -->
                                <button class="btn btn-block btn-primary" id="apply_range">Aplicar</button>

                            </div>
                        </div>
                    </article>

                    <article class="filter-group">
                        <header class="card-header">
                            <a href="#" data-toggle="collapse" data-target="#collapse_aside4" aria-expanded="false"
                               class="collapsed">
                                <i class="icon-control fa fa-chevron-down"></i>
                                <h6 class="title">Producto </h6>
                            </a>
                        </header>
                        <div class="filter-content collapse" id="collapse_aside4" style="">

                            <div class="card-body">


                                <div class="row">
                                    @foreach($types_woods as $type_wood)
                                        <div class="col-md-6">
                                            <label class="custom-control custom-checkbox">
                                                <input type="checkbox" name="type_product" value="{{$type_wood->id}}"
                                                       id="publication-type-product-{{$type_wood->id}}"
                                                       class="custom-control-input">
                                                <div class="custom-control-label">{{$type_wood->name}} </div>
                                            </label>
                                        </div> <!-- col.// -->
                                    @endforeach
                                </div> <!-- row.// -->
                                {{--                        <a href="#" class="btn btn-light btn-sm mt-2">Reset</a>--}}
                            </div>
                        </div>
                    </article>


                    <article class="filter-group">
                        <header class="card-header">
                            <a href="#" data-toggle="collapse" data-target="#collapse_aside5" aria-expanded="false"
                               class="collapsed">
                                <i class="icon-control fa fa-chevron-down"></i>
                                <h6 class="title">Formas de pago </h6>
                            </a>
                        </header>
                        <div class="filter-content collapse" id="collapse_aside5" style="">
                            <div class="card-body">

                                <div class="row">
                                    @foreach($way_pays as $way_pay)
                                        <div class="col-md-6">
                                            <label class="custom-control custom-checkbox">
                                                <input type="checkbox" name="way_pay" value="{{$way_pay->id}}"
                                                       id="publication-way-pay-{{$way_pay->id}}"
                                                       class="custom-control-input">
                                                <div class="custom-control-label">{{$way_pay->paymentType}} </div>
                                            </label>
                                        </div> <!-- col.// -->
                                    @endforeach
                                </div> <!-- row.// -->
                                {{--                        <a href="#" class="btn btn-light btn-sm mt-2">Reset</a>--}}
                            </div>
                        </div>
                    </article>

                    <article class="filter-group">
                        <header class="card-header">
                            <a href="#" data-toggle="collapse" data-target="#collapse_aside6" aria-expanded="false"
                               class="collapsed">
                                <i class="icon-control fa fa-chevron-down"></i>
                                <h6 class="title">Escuadria </h6>
                            </a>
                        </header>
                        <div class="filter-content collapse" id="collapse_aside6" style="">
                            <div class="card-body">
                                {{--                        <input type="range" class="custom-range" min="0" max="100000" name="">--}}
                                <div class="form-row">
                                    <div class="form-group col-md-4">
                                        <label>Espesor</label>
                                        <input class="form-control" placeholder="0.0" type="number"
                                               id="publication-thick"
                                               name="thick">
                                    </div>
                                    <div class="form-group text-right col-md-4">
                                        <label>Ancho</label>
                                        <input class="form-control" placeholder="0.0" type="number"
                                               id="publication-wide"
                                               name="wide">
                                    </div>
                                    <div class="form-group text-right col-md-4">
                                        <label>Largo</label>
                                        <input class="form-control" placeholder="0.0" type="number"
                                               id="publication-long"
                                               name="long">
                                    </div>
                                </div> <!-- form-row.// -->
                                <button class="btn btn-block btn-primary" id="apply_squad">Aplicar</button>
                            </div>
                        </div>
                    </article>


                    <article class="filter-group">
                        <header class="card-header">
                            <a href="#" data-toggle="collapse" data-target="#collapse_aside7" aria-expanded="false"
                               class="collapsed">
                                <i class="icon-control fa fa-chevron-down"></i>
                                <h6 class="title">Formas de envio </h6>
                            </a>
                        </header>
                        <div class="filter-content collapse" id="collapse_aside7" style="">
                            <div class="card-body">

                                <div class="row">
                                    @foreach($shipping_types as $shipping_type)
                                        <div class="col-md-12">
                                            <label class="custom-control custom-checkbox">
                                                <input type="checkbox" name="shipping_type"
                                                       value="{{$shipping_type->id}}"
                                                       id="publication-shipping-type-{{$shipping_type->id}}"
                                                       class="custom-control-input">
                                                <div class="custom-control-label">{{__($shipping_type->send)}} </div>
                                            </label>
                                        </div> <!-- col.// -->
                                    @endforeach
                                </div> <!-- row.// -->
                                {{--                        <a href="#" class="btn btn-light btn-sm mt-2">Reset</a>--}}
                            </div>
                        </div>
                    </article>

                    <article class="filter-group">
                        <header class="card-header">
                            <a href="#" data-toggle="collapse" data-target="#collapse_aside8" aria-expanded="false"
                               class="collapsed">
                                <i class="icon-control fa fa-chevron-down"></i>
                                <h6 class="title">Disponibilidad </h6>
                            </a>
                        </header>
                        @if(isset($type_availabilities) && !is_null($type_availabilities) )
                        <div class="filter-content collapse" id="collapse_aside8" style="">
                            <div class="card-body">
                                @foreach($type_availabilities as $type_availability)

                                <label class="custom-control custom-radio">
                                    <input type="radio" name="availability" value="{{($type_availability->name)}}" id="publication-availability-{{$type_availability->id}}"
                                           class="custom-control-input">
                                    <div class="custom-control-label">{{__($type_availability->name)}}</div>
                                </label>
                                @endforeach
                                    <label class="custom-control custom-radio">
                                        <input type="radio" name="availability" value="" id="publication-availability-{{$type_availabilities->count()+1}}"
                                               class="custom-control-input">
                                        <div class="custom-control-label">Todos</div>
                                    </label>
                            </div>
                        </div>
                        @endif
                    </article>
            </div>

            <div class="col-md ">
                <section class="section-main padding-y">
                    <div id="filter-container">
                        @include('renders._filterList')
                    </div>
                </section>
            </div>
        </div>
    </div>

@endsection

@push('scripts')
    @routes

    <script>


        $(document).ready(function () {
            //filter by type of publication
            function getAllUrlParams(url) {

                // get query string from url (optional) or window
                var queryString = url ? url.split('?')[1] : window.location.search.slice(1);

                // we'll store the parameters here
                var obj = {};

                // if query string exists
                if (queryString) {

                    // stuff after # is not part of query string, so get rid of it
                    queryString = queryString.split('#')[0];

                    // split our query string into its component parts
                    var arr = queryString.split('&');

                    for (var i = 0; i < arr.length; i++) {
                        // separate the keys and the values
                        var a = arr[i].split('=');

                        // set parameter name and value (use 'true' if empty)
                        var paramName = a[0];
                        var paramValue = typeof (a[1]) === 'undefined' ? true : a[1];

                        // (optional) keep case consistent
                        paramName = paramName.toLowerCase();
                        if (typeof paramValue === 'string') paramValue = paramValue.toLowerCase();

                        // if the paramName ends with square brackets, e.g. colors[] or colors[2]
                        if (paramName.match(/\[(\d+)?\]$/)) {

                            // create key if it doesn't exist
                            var key = paramName.replace(/\[(\d+)?\]/, '');
                            if (!obj[key]) obj[key] = [];

                            // if it's an indexed array e.g. colors[2]
                            if (paramName.match(/\[\d+\]$/)) {
                                // get the index value and add the entry at the appropriate position
                                var index = /\[(\d+)\]/.exec(paramName)[1];
                                obj[key][index] = paramValue;
                            } else {
                                // otherwise add the value to the end of the array
                                obj[key].push(paramValue);
                            }
                        } else {
                            // we're dealing with a string
                            if (!obj[paramName]) {
                                // if it doesn't exist, create property
                                obj[paramName] = paramValue;
                            } else if (obj[paramName] && typeof obj[paramName] === 'string'){
                                // if property does exist and it's a string, convert it to an array
                                obj[paramName] = [obj[paramName]];
                                obj[paramName].push(paramValue);
                            } else {
                                // otherwise add the property
                                obj[paramName].push(paramValue);
                            }
                        }
                    }
                }

                return obj;
            }

            const filter = {
                title :getAllUrlParams().title !== undefined? getAllUrlParams().title:null,
                type:getAllUrlParams().type !== undefined? getAllUrlParams().type:null,
                order: getAllUrlParams().order !== undefined? getAllUrlParams().order:null,
                min_price:getAllUrlParams().min_price !== undefined? getAllUrlParams().min_price:null,
                max_price:getAllUrlParams().max_price !== undefined? getAllUrlParams().max_price:null,
                type_product: getAllUrlParams().type_product !== undefined? getAllUrlParams().type_product :[],
                thick:  getAllUrlParams().thick !== undefined? getAllUrlParams().thick:null,
                wide:  getAllUrlParams().wide !== undefined? getAllUrlParams().wide:null,
                long: getAllUrlParams().long !== undefined? getAllUrlParams().long:null,
                availability: getAllUrlParams().availability !== undefined? getAllUrlParams().availability:null,
                way_pay:getAllUrlParams().way_pay !== undefined? getAllUrlParams().way_pay:[],
                shipping_type:getAllUrlParams().shipping_type !== undefined? getAllUrlParams().shipping_type:[],
            };

            $('.custom-control-input').change(function () {

                // filter.type = null;
                // filter.order = null;

                $("input:radio").each(function () {
                    var id = $(this).attr("id");
                    var name = $(this).attr("name");

                    if ($("input:radio[id=" + id + "]:checked").length != 0) {
                        switch (name) {
                            case 'order':
                                filter.order = $("#" + id).val();
                                break;
                            case  'type':
                                filter.type = $("#" + id).val();
                                break;
                            case  'availability':
                                filter.availability = $("#" + id).val();
                                break;
                        }
                    }
                });

                $("input:checkbox").each(function () {
                    var id = $(this).attr("id");
                    var name = $(this).attr("name");

                    if ($("input:checkbox[id=" + id + "]:checked").length !== 0) {
                        switch (name) {
                            case 'type_product':
                                var indexPush = filter.type_product.indexOf($("#" + id).val());
                                if (indexPush === -1) {
                                    filter.type_product.push($("#" + id).val());
                                }
                                break;
                            case 'way_pay':
                                var indexPush = filter.way_pay.indexOf($("#" + id).val());
                                if (indexPush === -1) {
                                    filter.way_pay.push($("#" + id).val());
                                }
                                break;
                            case 'shipping_type':
                                var indexPush = filter.shipping_type.indexOf($("#" + id).val());
                                if (indexPush === -1) {
                                    filter.shipping_type.push($("#" + id).val());
                                }
                                break;
                           /*  case 'availability':
                                var indexPush = filter.availability.indexOf($("#" + id).val());
                                if (indexPush === -1) {
                                    filter.availability.push($("#" + id).val());
                                }
                                break; */

                        }
                    } else {
                        switch (name) {
                            case 'type_product':
                                var indexPop = filter.type_product.indexOf($("#" + id).val());
                                if (indexPop !== -1) {
                                    filter.type_product.splice(indexPop, 1);
                                }
                                break;
                            case 'way_pay':
                                var indexPop = filter.way_pay.indexOf($("#" + id).val());
                                if (indexPop !== -1) {
                                    filter.way_pay.splice(indexPop, 1);
                                }
                                break;
                            case 'shipping_type':
                                var indexPop = filter.shipping_type.indexOf($("#" + id).val());
                                if (indexPop !== -1) {
                                    filter.shipping_type.splice(indexPop, 1);
                                }
                                break;
                          /*   case 'availability':
                                var indexPop = filter.availability.indexOf($("#" + id).val());
                                if (indexPop !== -1) {
                                    filter.availability.splice(indexPop, 1);
                                }
                                break; */
                        }
                    }
                });

                var url = route('filter.byProducts', filter);
                window.location.href = url;
                axios.get(url).then(res => {
                    $('#filter-container').hide().html(res.data).fadeIn();
                })
                    .catch(error => {
                        Swal.fire({
                            icon: 'error',
                            title: 'Error inesperado',
                        })
                    });
            });

            $('#apply_range').click(function () {
                console.log("apply_range");
                filter.max_price = $('#publication-max-price').val();
                filter.min_price = $('#publication-min-price').val();
                if (filter.min_price >= 0 && filter.max_price) {
                    console.log(filter);
                    var url = route('filter.byProducts', filter);
                    window.location.href = url;
                    axios.get(url).then(res => {
                        $('#filter-container').hide().html(res.data).fadeIn();
                    })
                        .catch(error => {
                            Swal.fire({
                                icon: 'error',
                                title: 'Error inesperado',
                            })
                        });
                }
            })

            $('#apply_squad').click(function () {
                filter.thick = $('#publication-thick').val();
                filter.wide = $('#publication-wide').val();
                filter.long = $('#publication-long').val();
                if ((filter.thick !== "" && filter.wide !== "" && filter.long !== "") && (filter.thick >= 0 && filter.wide >= 0 && filter.long >= 0)) {
                    var url = route('filter.byProducts', filter);
                    window.location.href = url;
                    axios.get(url).then(res => {
                        $('#filter-container').hide().html(res.data).fadeIn();
                    })
                        .catch(error => {
                            Swal.fire({
                                icon: 'error',
                                title: 'Error inesperado',
                            })
                        });
                } else {
                    Swal.fire({
                        icon: 'error',
                        title: 'Ingrese correctamente las medidas para realizar la  búsqueda',
                    })
                }
            })


            if(getAllUrlParams().way_pay!==undefined){
                getAllUrlParams().way_pay.forEach(element => $(`#publication-way-pay-${element}`).prop('checked', true));

            }
            if(getAllUrlParams().type_product!==undefined){
                getAllUrlParams().type_product.forEach(element => $(`#publication-type-product-${element}`).prop('checked', true));
            }
            if(getAllUrlParams().shipping_type!==undefined){
                getAllUrlParams().shipping_type.forEach(element => $(`#publication-shipping-type-${element}`).prop('checked', true));
            }

            if(getAllUrlParams().thick!==undefined){
                $("#publication-thick").val(getAllUrlParams().thick);
            }
            if(getAllUrlParams().wide!==undefined){
                $("#publication-wide").val(getAllUrlParams().wide);
            }
            if(getAllUrlParams().long!==undefined){
                 $("#publication-long").val(getAllUrlParams().long);
            }
            if(getAllUrlParams().max_price!==undefined){
                $("#publication-max-price").val(getAllUrlParams().max_price);
            }
            if(getAllUrlParams().min_price!==undefined){
                $("#publication-min-price").val(getAllUrlParams().min_price);
            }
            if(getAllUrlParams().order!==undefined){
                $("input:radio").each(function () {
                    var id = $(this).attr("id");

                    if ($("input:radio[id=" + id + "]").val() === getAllUrlParams().order &&  $(this).attr("name") === 'order' ) {
                        $(`#${id}`).prop('checked', true)
                    }
                });
            }
            if(getAllUrlParams().type!==undefined){
                $("input:radio").each(function () {
                    var id = $(this).attr("id");

                    if ($("input:radio[id=" + id + "]").val() === getAllUrlParams().type &&  $(this).attr("name") === 'type' ) {
                        $(`#${id}`).prop('checked', true)
                    }
                });
            }

            if(getAllUrlParams().availability!==undefined){
                $("input:radio").each(function () {
                    var id = $(this).attr("id");

                    if ($("input:radio[id=" + id + "]").val() === getAllUrlParams().availability &&  $(this).attr("name") === 'availability' ) {
                        $(`#${id}`).prop('checked', true)
                    }
                });
            }
        });

    </script>
@endpush
