@extends('layouts.public')
@push('styles')
<style>
    .content
    {
        margin: 0 auto;
    }
    .inner
    {
        display:inline-block;
    }


    #map {
        margin: auto;
        width: 80%;
        height: 40vh;
        border: 1px solid black;
    }

</style>
@endpush
@section('content')
<div class="container">
  <section class="py-3 bg-light">
    <div class="container">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('public.home')}}">Home</a></li>
        <li class="breadcrumb-item"><a
            href="page-detail-product.html#">@if(isset($product->subcategory->category->name)){{__($product->subcategory->category->name)}}@endif</a>
        </li>
        <li class="breadcrumb-item"><a
            href="page-detail-product.html#">@if(isset($product->subcategory->name)){{__($product->subcategory->name)}}@endif</a>
        </li>

      </ol>
    </div>
  </section>

  <!-- ========================= SECTION CONTENT ========================= -->
  <section class="section-content bg-white padding-y">
    <div class="container">

      <!-- ============================ ITEM DETAIL ======================== -->
      <div class="row">
        <aside class="col-md-6">
          <div class="card">
            <article class="gallery-wrap">
                @if (!$publication->hasMedia('images'))
                    <div class="img-big-wrap">
                        <div> <img class="img-sm"
                            src="https://place-hold.it/500">
                        </div>
                    </div>
                @else
                <ul id="glasscase" class="gc-start">
                    @if ($publication->hasMedia('images'))
                        @foreach ($publication->getMedia('images')->take(5) as $key=>$item)
                            <li><img src="{{$item->getUrl()}}"></li>

                        @endforeach
                    @endif
                </ul>
                @endif
            </article> <!-- gallery-wrap .end// -->
          </div> <!-- card.// -->
        </aside>
        <main class="col-md-6">
          <article class="product-info-aside">
            <h2 class="title mt-3"> @if($publication->title) {{$publication->title}} @endif </h2>
            <dl class="row">
              @if(isset($publication->company))
              <dt class="col-sm-3">Empresa</dt>
              <dd class="col-sm-9">@if(isset($publication->company->name)){{$publication->company->name}}@endif</dd>
              @endif
              @if(isset($profile))
              <dt class="col-sm-3">Vendedor</dt>
              <dd class="col-sm-9">@if(isset($profile->nameComplete)){{$profile->nameComplete}}<a
                  href="{{route('view.company.show',[$publication->id,$publication->company->id])}}" class="btn btn-sm btn-white"
                  data-toggle="tooltip" data-placement="top" title="Ver vendedor"><i class="fa fa-eye"
                    style="color: blue;"></i></a>@endif</dd>
              @endif
            </dl>
            <div class="rating-wrap my-3">
                <h2 style="    color: rgba(0,0,0,.8);font-size: 64px;font-weight: 300;">
                    {{$calification}}
                </h2>
                <div id="rateYo" data-rateyo-precision="{{$calification}}"></div>
                <small class="label-rating text-muted">Promedio entre {{ $publication->califications->count()}} opiniones</small>




              {{-- <small class="label-rating text-success"> <i class="fa fa-clipboard-check"></i> 154 ordenes </small> --}}
            </div>
              <div class="rating-wrap my-3">
                  @if(!$publication->isSimple() )
                      <small class="label-rating text-muted">{{ count($publication->comments)}} comentarios</small>
                  @endif
              </div>
            @if($publication->isOffert())
                @if(!$publication->isAuction() )
                    <small> $/PT {{  $publication->price }}</small>
                    <div class="mb-3">
                    <var class="price h4"> Precio @if(isset($product->wayPay)) $ {{$publication->total }} @else
                        $ {{$publication->total}} @endif @if(isset($product->wayPay) && $product->wayPay->iva) + IVA @endif</var>
                    </div>
                @else
                    <div class="mb-3">
                        <var class="price h4"> Precio base @if(isset($publication->auction)) ${{ $publication->auction->price_init }} @endif</var>
                    </div>
                @endif
            @endif



            <dl class="row">
                <dt class="col-sm-3">Tipo de publicación</dt>
                <dd class="col-sm-9">{{ __($publication->typePublication) }}</dd>
            <dt class="col-sm-3">Escuadria</dt>
                @if(isset($publication->products()->first()->dimension->actualThickness))<span class="tag">Espesor {{$publication->products()->first()->dimension->actualThickness}} mm </span>@endif
                @if(isset($publication->products()->first()->dimension->actualWidth))<span class="tag">Ancho {{$publication->products()->first()->dimension->actualWidth}} mm </span>@endif</td>
                @if(isset($publication->products()->first()->dimension->actualLength))<span class="tag">Largo {{$publication->products()->first()->dimension->actualLength}} mm </span>@endif</td>

              @if(isset($product->typeWood))
              <dt class="col-sm-3">Tipo de madera</dt>
              <dd class="col-sm-9">@if(isset($product->typeWood->name)){{$product->typeWood->name}} @endif</dd>
              @endif
              @if(isset($product->kind))
              <dt class="col-sm-3">Especie</dt>
              <dd class="col-sm-9">@if(isset($product->kind->name)){{$product->kind->name}} @endif</dd>
              @endif
              @if(isset($product->typeProduct))
              <dt class="col-sm-3">Producto</dt>
              <dd class="col-sm-9">@if(isset($product->typeProduct->name)){{$product->typeProduct->name}}@endif</dd>
              @endif
              @if(isset($product->wayPay))

                @if(count($product->wayPay->paymentTypes)> 0)
                <dt class="col-sm-3">Forma de pago</dt>
                <dd class="col-sm-9">
                @foreach ($product->wayPay->paymentTypes as $item)
                <span class="badge badge-info">{{$item->paymentType}}</span>
                @endforeach
              </dd>
                @endif

              @endif
              {{-- @if(isset($product->shippingType))

                @if(isset($product->shippingType->send))
                <dt class="col-sm-3">Envio</dt>
                <dd class="col-sm-9">
                <p> {{__($product->shippingType->send)}} </p>
                <p> {{$product->shippingType->description}} </p>
              </dd>
                @endif

              @endif --}}
            </dl>
            @if(!$publication->isAuction() && !$publication->isSimple() && $publication->isOffert())
            <h4>Selecciona una opción</h4>
            <div class="row">
                <div class="col-md-6">
                 <input class="form-check-input" type="radio" name="optionBuy" id="gridOptionBuy" value="1" checked> <h5>Precio x m² </h5>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <button class="btn btn-light" type="button" id="button-plus"><i class="fa fa-calculator"></i> </button>
                  </div>
                  <input
                    placeholder="Ingrese m²"
                    id="superficie"
                    name="superficie"
                    type="number"
                    step="0.01"
                    class="form-control col-md-12 superficie"
                    value="0.00">

                </div>
                <br>
                  <div class="form-group total">
                    <h4 class="content alert alert-info">
                        $<span class="inner" id="total"></span>
                    </h4>
                  </div>
                </div>

                <div class="col-md-6">
                    <input class="form-check-input" type="radio" name="optionBuy"  value="2" > <h5>Precio x unidad </h5>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <button class="btn btn-light" type="button" id="button-plus"><i class="fa fa-calculator"></i> </button>
                      </div>
                      <input
                        placeholder="Ingrese cantidad"
                        id="cantidad"
                        name="cantidad"
                        type="number"
                        step="0.01"
                        class="form-control cantidad"
                        value="0.00">

                      </div>
                    <br>
                    <div class="form-group total-cantidad" >
                        <h4 class="content alert alert-info">
                            $<span class="inner" id="total-cantidad"></span>
                        </h4>
                    </div>
                </div>
                <div class="form-row total"></div>
            </div>

            @endif
            <div class="form-row">

                @if(!$publication->isSimple())
                    <div class="form-group col-md">
                        @if(!$publication->isAuction())

                        <button class="btn btn-block btn-primary btn-add-cart">
                        <i class="fa fa-cart-plus"></i>
                        @if($publication->isOffert())<span class="text">Comprar</span> @endif
                        @if($publication->isDemand())<span class="text">Vender</span> @endif

                        </button>
                        @endif
                        @if($publication->isAuction())
                        <a href="{{ route('view.publications.auction',$publication->id) }}" class="btn btn-block btn-light">
                            <i class="fa fa-bullhorn "></i> <span class="text">Subastar</span>
                        </a>
                        @endif
                        @if(auth()->check())
                            <a href="#contactar" class="btn btn-block btn-light" id="contacted">
                                <i class="fa fa-whatsapp"></i> <span class="text">Contactar</span>
                            </a>
                        @endif
                    </div>
                @endif
            </div>
          </article>
        </main>
      </div>

      <!-- ================ ITEM DETAIL END .// ================= -->


    </div> <!-- container .//  -->
  </section>
  <!-- ========================= SECTION CONTENT END// ========================= -->

  <!-- ========================= SECTION  ========================= -->
  <section class="section-name padding-y bg">
    <div class="container">
        <div class="row">
        <div class="col-md-8">
            <h5 class="title-description">Descripción</h5>
            <p>{!!$publication->description!!}</p>
            @if(isset($publication->address))
                <h5 class="title-address">Dirección</h5>

                <div class="justify-content-center aling-items-center">
                    <label class="control-label">&nbsp;&nbsp;</label>

                    <div id="map"></div>
                </div>
            @endif

          @if(isset($product->availability) &&
          isset($product->dimension) &&
          isset($product->condition) &&
          isset($product->qualitativeParameter) )

          <h5 class="title-description">Especificaciones</h5>
          <table class="table table-bordered">
            <tr>
              <th colspan="2">Disponibilidad</th>
            </tr>
            <tr>
              <td>Unidad de medida</td>

              <td>@if(isset($product->availability->unitMeasurement)){{$product->availability->unitMeasurement}}@endif
              </td>

            </tr>
            <tr>
              <td>Disponibilidad</td>
              <td>@if(isset($product->availability->availability)){{__($product->availability->availability)}}@endif
              </td>
            </tr>
            <tr>
              <td>Plazo de producción</td>
              <td>@if(isset($product->availability->measurementTerm)){{$product->availability->measurementTerm}}@endif
              </td>
            </tr>
            <tr>
              <td>Cantidad/Disponibilidad mensual</td>
              <td>@if(isset($product->availability->cantitude)){{$product->availability->cantitude}}@endif</td>
            </tr>


            <tr>
              <th colspan="2">Dimensiones</th>
            </tr>
            <tr>
              <td>Espesor nominal</td>
              <td>@if(isset($product->dimension->nominalThickness)){{$product->dimension->nominalThickness}} " @endif</td>
            </tr>
            <tr>
              <td>Ancho nominal</td>
              <td>@if(isset($product->dimension->nominalWidth)){{$product->dimension->nominalWidth}} " @endif</td>
            </tr>
            <tr>
              <td>Largo nominal </td>
              <td>
                @if(isset($product->dimension->nominalLength)){{$product->dimension->nominalLength}} "@endif
              </td>
            </tr>
            <tr>
              <td>Espesor real </td>
              <td>
                @if(isset($product->dimension->actualThickness)){{$product->dimension->actualThickness}} mm @endif
              </td>
            </tr>
            <tr>
              <td>Ancho real</td>
              <td>@if(isset($product->dimension->actualWidth)){{$product->dimension->actualWidth}} mm @endif</td>
            </tr>

            <tr>
              <td>Largo real</td>
              <td>@if(isset($product->dimension->actualLength)){{$product->dimension->actualLength}} mm @endif</td>
            </tr>
            <tr>
              <td>Es mas espesor</td>
              <td>@if(isset($product->dimension->isMoreThickness)){{$product->dimension->isMoreThickness}} mm @endif</td>
            </tr>
            <tr>
              <td>Es menos espesor</td>
              <td>@if(isset($product->dimension->isLessThickness)){{$product->dimension->isLessThickness}} mm @endif</td>
            </tr>
            <tr>
              <td>Es mas ancho</td>
              <td>@if(isset($product->dimension->isMoreWidth)){{$product->dimension->isMoreWidth}} mm @endif</td>
            </tr>
            <tr>
              <td>Es menos ancho</td>
              <td>@if(isset($product->dimension->isLessWidth)){{$product->dimension->isLessWidth}} mm @endif</td>
            </tr>

            <tr>
              <td>Es mas largo</td>
              <td>@if(isset($product->dimension->isMoreLength)){{$product->dimension->isMoreLength}} mm @endif</td>
            </tr>

            <tr>
              <td>Es menos largo</td>
              <td>@if(isset($product->dimension->isLessLength)){{$product->dimension->isLessLength}} mm @endif</td>
            </tr>
            <tr>
              <th colspan="2">Condiciones del paquete</th>
            </tr>
            <tr>
              <td>Piezas</td>
              <td>@if($product->condition->piece){{$product->condition->piece}}@endif</td>
            </tr>
            <tr>
              <td>Peso</td>
              <td>@if($product->condition->weight){{$product->condition->weight}}@endif</td>
            </tr>
            <tr>
              <td>Largo</td>
              <td>@if($product->condition->length){{$product->condition->length}}@endif</td>
            </tr>
            <tr>
              <td>Ancho</td>
              <td>@if($product->condition->width){{$product->condition->width}}@endif</td>
            </tr>
            <tr>
              <td>Alto</td>
              <td>@if($product->condition->tall){{$product->condition->tall}}@endif</td>
            </tr>
            <tr>
              <td>Zunchos</td>
              <td>@if($product->condition->zunchos){{$product->condition->zunchos}}@endif</td>
            </tr>
            <tr>
              <td>Zunchos</td>
              <td>@if($product->condition->wrapping){{$product->condition->wrapping}}@endif</td>
            </tr>
            <tr>
              <th colspan="2">Parametros cualitativos</th>
            </tr>
            <tr>
              <td>Calidad</td>
              <td>@if($product->qualitativeParameter->nudoMuerto){{$product->qualitativeParameter->nudoMuerto}}@endif
              </td>
            </tr>
            <tr>
              <td>Mancha de hongo</td>
              <td>@if($product->qualitativeParameter->manchaHongo){{$product->qualitativeParameter->manchaHongo}}@endif
              </td>
            </tr>
            <tr>
              <td>Secado</td>
              <td>@if($product->qualitativeParameter->secado){{$product->qualitativeParameter->secado}}@endif</td>
            </tr>

            <tr>
              <td>Humedad Máxima</td>
              <td>@if($product->qualitativeParameter->humedadMax){{$product->qualitativeParameter->humedadMax}}@endif
              </td>
            </tr>
            <tr>
              <td>Humedad Mínima</td>
              <td>@if($product->qualitativeParameter->humedadMin){{$product->qualitativeParameter->humedadMin}}@endif
              </td>
            </tr>

            <tr>
              <td>Ubicación de tronco</td>
              <td>
                @if($product->qualitativeParameter->ubicacionTronco){{$product->qualitativeParameter->ubicacionTronco}}@endif
              </td>
            </tr>


            <tr>
              <td>Grietas</td>
              <td>@if($product->qualitativeParameter->grieta){{$product->qualitativeParameter->grieta}}@endif</td>
            </tr>

            <tr>
              <td>Rajaduras</td>
              <td>@if($product->qualitativeParameter->rajaduras){{$product->qualitativeParameter->rajaduras}}@endif</td>
            </tr>
            <tr>
              <td>Canto muerto</td>
              <td>@if($product->qualitativeParameter->cantoMuerto){{$product->qualitativeParameter->cantoMuerto}}@endif
              </td>
            </tr>
            <tr>
              <td>Alaveos</td>
              <td>@if($product->qualitativeParameter->alaveos){{$product->qualitativeParameter->alaveos}}@endif</td>
            </tr>
            <tr>
              <td>Combas</td>
              <td>@if($product->qualitativeParameter->combas){{$product->qualitativeParameter->combas}}@endif</td>
            </tr>
            <tr>
              <td>Torceduras</td>
              <td>@if($product->qualitativeParameter->torceduras){{$product->qualitativeParameter->torceduras}}@endif
              </td>
            </tr>
          </table>
          @endif
        </div> <!-- col.// -->

        <aside class="col-md-4">

          <div class="box">

            <!-- <h5 class="title-description">Productos relacionados</h5>
            <p>

            </p> -->

            <h5 class="title-description">Productos relacionados</h5>
            @if(isset($publicationRandom ) && $publicationRandom!= null)
            @foreach ($publicationRandom as $item)
            <article class="media mb-3">
              <a href="{{ route('view.publications.show',$item->id)}}"><img class="img mr-3"
                  src="@if (!$item->hasMedia('images')) https://place-hold.it/120 @else {{$item->getMedia('images')[0]->getUrl('thumb')}}@endif"></a>
              <div class="media-body">
                <h6 class="mt-0"><a
                    href="{{ route('view.publications.show',$item->id)}}">{{Str::words($item->title,10,' ...')}}</a>
                </h6>
                <p>
                  <span class="badge badge-success">
                    @if(isset($product->wayPay)){{$product->wayPay->money}} @else $ @endif {{$publication->price}}
                  </span>
                  {{-- <p class="mb-1"> {!!Str::words($item->description,8,' ...') !!}</p> --}}
                </p>
              </div>
            </article>
            @endforeach
            @endif
          </div> <!-- box.// -->
        </aside> <!-- col.// -->
      </div> <!-- row.// -->

    </div> <!-- container .//  -->
  </section>
  @if(!$publication->isSimple())

  <section class="section-content bg-white padding-y">
    <div class="container">
      @if ($errors->any())
      <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
      @endif
      @if(auth()->check())
      <div class="row" id="contactar">
        <h5 class="title-description">Contactarse con el vendedor</h5>

        <div class="col-md-12">
        <form method="POST" action="{{ route('comments.store',$publication) }}">
          @csrf
          <div class="form-group">

            <textarea class="form-control" placeholder="Escriba aqui su consulta" name="comment"></textarea>
            <small id="emailHelp" class="form-text text-muted">Su consulta va ser respondida dentro de las 24 hs por el vendedor del producto</small>
          </div>
          <button type="submit" class="btn btn-primary " ><i class="fa fa-send"></i>Enviar</button>

        </form>

      </div>
      @endif
      <div class="row">
        <div class="col-md-12  p-4">
        @if(count($publication->comments))
          @foreach($publication->comments as $comment)

              <div class="media text-muted pt-3">
                  <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                      <strong class="d-block text-gray-dark">{{ isset($comment->user->person) ? $comment->user->person->nameComplete : $comment->user->name }}</strong>
                      <svg class="bi bi-chat-square" width="1em" height="1em" viewBox="0 0 16 16"
                      fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                      <path fill-rule="evenodd"
                          d="M14 1H2a1 1 0 00-1 1v8a1 1 0 001 1h2.5a2 2 0 011.6.8L8 14.333 9.9 11.8a2 2 0 011.6-.8H14a1 1 0 001-1V2a1 1 0 00-1-1zM2 0a2 2 0 00-2 2v8a2 2 0 002 2h2.5a1 1 0 01.8.4l1.9 2.533a1 1 0 001.6 0l1.9-2.533a1 1 0 01.8-.4H14a2 2 0 002-2V2a2 2 0 00-2-2H2z"
                          clip-rule="evenodd" />
                      </svg>    {{ $comment->comment }}
                  </p>

            </div>
            @if($comment->subcomment != null)
             <div class="media text-muted  m-3  comment{{ $comment->id }}" id="comment{{ $comment->id }}">
              <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                <strong class="d-block text-gray-dark">Respuesta</strong>
                <i class="fa fa-check"> </i> {{ $comment->subcomment }}
              </p>

          </div>
        @endif
          @endforeach
        @endif
      </div>
      </div>
    </div>
  </section>
  @endif
  <!-- ========================= SECTION CONTENT END// ========================= -->
</div>
@endsection
@push('scripts')
<script>
    $(document).ready(function(){
        var extract_url = window.location.href
        var comment = extract_url.split('#')[1]
        if (comment!= undefined){
            var commentId = '.'+extract_url.split('#')[1]
            var scrollTop     = $(window).scrollTop(),
            elementOffset = $(commentId).offset().top,
            distance      = (elementOffset - scrollTop);
            $("html, body").animate({
                scrollTop: distance
            }, 200);
            document.getElementById(comment).style.backgroundColor = 'rgb(241 241 241)';
        }
        $(".total").hide();
        var dimension = {{ isset($product->dimension->actualThickness) ? $product->dimension->actualThickness : null}}
        if (dimension !==undefined){
            $(".superficie").on('input',function(){
                var precio = {{  $publication->price}}; //precio tablar
                var espesor =   dimension * 0.0393701; // espesor en pulgadas
                var superficieCubrir = $(".superficie").val() * 10.7639;
                var calculo = superficieCubrir * precio * espesor //precio final cubriendo todos los m2
                $(".total").hide();
                if (calculo > 0 ){
                    $(".total").show();
                    $("#total").html(calculo.toFixed(2));
                }
            });
        }

        $(".total-cantidad").hide();
        $(".cantidad").on('input',function(){
            var precio = {{  $publication->total}}; //precio tablar
            var calculo = $(".cantidad").val() * precio
            $(".total-cantidad").hide();
            if (calculo > 0 ){
                $(".total-cantidad").show();
                $("#total-cantidad").html(calculo.toFixed(2));
            }
        });

        $("#contacted").on('click', function(){
            $("#contactar").animate({
              scrollTop: 200
          }, 200);
        })

    })
</script>
<script>
    $('.btn-add-cart').on('click', function(){

        if(parseFloat($(".superficie").val()) <= 0 && parseFloat($(".cantidad").val()) <= 0) {
            Swal.fire({
                icon: 'error',
                title: 'Calculo con errores',
                text: "Debe ingresa un número mayor a 0 para el calcular",
            })
            return;
        }

        //if(!$('#optionBuy').is(':checked') && $(".superficie").val() !== 0 || $(".cantidad").val() !== 0) {
        let price = 0;
        let quantity = 0;
        if ($('input[name="optionBuy"]:checked').val()==="1"){
            price =  $("#total").text();
            quantity = $(".superficie").val();

        }else if ($('input[name="optionBuy"]:checked').val()==="2"){
            price = $("#total-cantidad").text();
            quantity =$(".cantidad").val();
        }

        var myData = {
         publication_id: {{ $publication->id }},
         price: price,
         quantity: quantity,
         option_buy: $('input[name="optionBuy"]:checked').val(),
        };

        axios.post(route("shopping_cart.add", myData)).then(res => {
            window.location = res.data.redirect;
        })
        .catch(error => {

               window.location.href= route('login');
        });

    })

    $(document).ready( function () {
        $('#glasscase').glassCase({ 'thumbsPosition': 'bottom', 'widthDisplay' : 560});
    });

</script>
<script>
    $(document).ready( function () {
        $("#rateYo").rateYo({
            readOnly: true,
            rating    :  $("#rateYo").attr('data-rateyo-precision'),
            spacing   : "5px",
            multiColor: {

                "startColor": "#FF0000", //RED
                "endColor"  : "#00FF00"  //GREEN
            }
        });
    });
</script>
@if($publication->address!= null)
    <script>
        $( document ).ready(function(){
            var map = null;
            var canvas = null;
            const address = @json($publication->address);
            //carga el mapa incial con los datos traidos del back
            const changeInitMap = () =>{
                //cargar mapa incial con la posicion si tiene address.longitud,address.latitud
                mapboxgl.accessToken = 'pk.eyJ1IjoicnViZW5tb3YiLCJhIjoiY2tqMGN1Nnk3MGZ3NDJycGh2eWJ4NW5ubSJ9.ac6ojU1TR6GcYSPfV_F0WQ';
                map = new mapboxgl.Map({
                    container: 'map',
                    style: 'mapbox://styles/mapbox/streets-v11',
                    center: [address.longitude,address.latitude],
                    zoom: 6
                });
                canvas = map.getCanvasContainer();
                map.addControl(new mapboxgl.NavigationControl());

                geojson = {
                    'type': 'FeatureCollection',
                    'features': [{
                        'type': 'Feature',
                        'geometry': {
                            'type': 'Point',
                            'coordinates': [address.longitude,address.latitude]
                        }
                    }]
                };

                map.on('load',function(){
                    map.addSource('point', {
                    'type': 'geojson',
                    'data': geojson
                    });

                    map.addLayer({
                        'id': 'point',
                        'type': 'circle',
                        'source': 'point',
                        'paint': {
                            'circle-radius': 10,
                            'circle-color': '#3887be'
                        }
                    });

                    // Set a UI indicator for dragging.
                    canvas.style.cursor = 'grabbing';

                    // Update the Point feature in `geojson` coordinates
                    // and call setData to the source layer `point` on it.
                    geojson.features[0].geometry.coordinates = [address.longitude,address.latitude];

                    map.getSource('point').setData(geojson);
                    map.flyTo({
                        center: geojson.features[0].geometry.coordinates,
                        speed: 0.5,
                        zoom:15
                    });
                })
            }

            changeInitMap();

        })
    </script>
@endif
@endpush
