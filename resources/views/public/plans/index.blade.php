@extends('layouts.public')

@section('content')

<section class="section-pagetop bg-light">
	<div class="container">
		<h1 class="title-page text-center">Nuestros planes</h2>
	</div> <!-- container .// -->
</section>
<section class="section-content padding-y bg">
    <div class="container">
        <!-- ============================ COMPONENT PRICING 1  ================================= -->
        <div class="card-deck mb-3 text-center">
         @foreach ($plans as $plan)
            <div class="card">
                <div class="card-header">
                    <h5 class="my-0">{{ $plan->name }}</h5>
                </div>
                <div class="card-body d-flex flex-column">
                    <h1 class="card-title pricing-card-title">${{ $plan->price  }} <small class="text-muted"></small></h1>
                    <div class="list-unstyled mt-3 mb-4">
                       <small> {!! $plan->description !!}</small>
                    </div>


                    @if($plan->hasTrial())
                        @if(auth()->check())
                            <a href="{{ route('home') }}" class="btn btn-block btn-outline-primary mt-auto" disabled>Ya tienes este plan</a>
                         @else
                            <a href="{{ route('register') }}" class="btn btn-block btn-outline-primary mt-auto">Registrate gratis</a>
                         @endif
                    @else
                        @if(auth()->check())
                            <form class="mt-auto" action="{{ route('pay_suscription.store') }}" method="POST">
                                @csrf
                                <input type="hidden" name="title" value="{{ $plan->name }}">
                                <input type="hidden" name="id_suscription" value="{{ $plan->id }}">
                                <input type="hidden" name="price" value="{{ $plan->price }}">
                                <input type="hidden" name="description" value="{{ $plan->description }}">
                                <input type="hidden" name="date" value="{{ date('Y-m-d H:i:s')}}">
                                <input type="hidden" name="unit" value="1">
                                <button type="submit" class="btn btn-block btn-outline-primary mt-auto">
                                    Cambiarse de plan
                                </button>
                            </form>
                        @else
                            <a href="#" class="btn btn-block btn-outline-primary mt-auto">Contactanos</a>
                        @endif
                    @endif

                </div>
            </div>
         @endforeach
        </div>
    </div>
</section>

<section class="section-content bg-white padding-y bg">
	<div class="container">
        <div class="row row-sm">
            <a href="javascript: history.back()" class="btn btn-light"> &laquo Atras</a>
        </div>
    </div>
</section>

@endsection
