@extends('layouts.public')

@section('content')
    <div class="container">
        <section class="section-main padding-y">
            <div class="d-flex justify-content-center">

                <main class="card" style="max-width: 500px;">
                    <div class="card-body">

                        <h2 class="title-page text-center">Gracias </h2>
                        <div class="text-center">
                            <i class="fa fa-thumbs-o-up fa-5x  fa_custom "></i>
                            <div class="bg-blue text-center text-white mb-0 p-2">
                                <p class="card-title">No tienes mas productos que clasificar</p>
                            </div>
                        </div>

                        <div class="card-body border-top  text-center">
                            <a href="{{ route('public.home') }}" class="btn btn-light"> <i class="fa fa-chevron-left"></i> Volver</a>
                        </div>
                    </div>
                </main>

            </div>

        </section>
    </div>

@endsection
