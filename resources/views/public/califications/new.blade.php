@extends('layouts.empty')

@section('content')



    <div class="container">
        <section class="section-main padding-y">
            <div class="d-flex justify-content-center">

                <main class="card" style="max-width: 600px;">
                    <div class="card-body">

                        <h2 class="title-page text-center">Dejanos tu opinión </h2>
                        <div class="text-center align-middle">
                            <img src="@if (!$publication->hasMedia('images')) http://placehold.it/120 @else {{$publication->getMedia('images')[0]->getUrl('thumb')}}@endif" class="img-fluid rounded-circle" alt="Cinque Terre" width="304" height="236">
                        </div>

                        <div class="text-center mb-0 p-2">
                            <br>
                            <h5 class="card-title">{{$publication->title}}</h5>
                        </div>
                        <form role="form" method="POST"
                              action="{{ route('califications.store',[$buy->id,$item->id]) }}">
                            @csrf
                            <div class="text-center mb-0 p-2">
                                <h4 class="card-title">¿Cuántas estrellas le darías?</h4>
                                <div class="d-flex justify-content-center">
                                    <input type="hidden" id="rating" name="rating"  value="">

                                    <div id="rateYo"></div>
                                </div>

                            </div>
                            <div  id="sdescription" style="display: none;">
                                <div class="row" style="margin-top: 10px;">
                                    <small style="color: #ff6a00;"  >Decinos que te pareció en una sola frase</small>
                                    <input class="form-control" maxlength="30" name="phrase" value="" required>
                                    <small>Escribi al menos 30 caracteres</small>
                                    <br>
                                </div>
                                <div class="row">
                                    <small style="color: #ff6a00;" >Cuentanos más del producto</small>
                                </div>
                                <div class="row">
                                    <textarea class="form-control" maxlength="50" rows="4" name="description" required></textarea>
                                </div>

                                <div class="row">
                                    <small>Escribi al menos 50 caracteres</small>
                                </div>
                                <div class="card-body border-top  text-center">
                                    <button type="submit" class="btn btn-primary"> <i class="fa fa-check-circle"></i> Enviar opinion</button>
                                </div>
                            </div>
                        </form>

                    </div>
                </main>

            </div>

        </section>
    </div>










@endsection
@push('scripts')
<script>

    $(document).ready( function () {
        $(function () {

            $("#rateYo").rateYo({
                rating: 0,
                fullStar: true
            });


            $("#rateYo").click(function () {
                $("html, body").animate({ scrollTop: $(document).height() }, 1000);

                /* set rating */
                var rating =  $("#rateYo").rateYo("rating");
                $('#rating').val(rating);
                $('#sdescription').show();

            });

        });
    });
</script>
@endpush
