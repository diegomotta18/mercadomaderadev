@extends('layouts.empty')

@section('content')



    <div class="container">
        <section class="section-main padding-y">
            <div class="d-flex justify-content-center">
                <div class="card" style="margin-bottom: 10px; max-width: 600px;">
                    <div class="card-body">
                        <div class="align-middle">

                            <div class="alert alert-info"><span class="fa fa-info-circle"></span> Tienes productos que
                                esperan tu opinión luego de haber realizado la compra con el vendedor
                                <b>{{$buy->seller->name}}</b></div>
                            <h2 class="title-page text-center">Dejanos tu opinión </h2>

                        </div>

                    </div>
                </div>
            </div>
            @foreach($items as $item)

                <div class="d-flex justify-content-center">
                    <div class="card" style="margin-bottom: 10px;; max-width: 600px;">
                        <div class="card-body">
                            <div class="text-center align-middle">
                                <img
                                    src="@if (!$item->publication->hasMedia('images')) http://placehold.it/120 @else {{$item->publication->getMedia('images')[0]->getUrl('thumb')}}@endif"
                                    class="img-fluid rounded-circle" alt="Cinque Terre" width="304" height="236">
                            </div>

                            <div class="text-center mb-0 p-2">
                                <br>
                                <h5 class="card-title">{{$item->publication->title}}</h5>
                            </div>
                        </div>
                        <div class="card-body border-top  text-center">
                            <a href="{{route('califications.create',[$item->buy_id,$item->id])}}"
                               class="btn btn-primary"> <i class="fa fa-check-circle"></i> Calificar</a>
                        </div>
                    </div>
                </div>

            @endforeach
        </section>
    </div>


@endsection
