@extends('layouts.public')
@section('content')
    <section class="section-content padding-y">
        <div class="container">
            <!--<div class="card mb-3">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-2"> Your are here: </div>
                        <nav class="col-md-8">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="page-listing-grid.html#">Home</a></li>
                                <li class="breadcrumb-item"><a href="page-listing-grid.html#">Category name</a></li>
                                <li class="breadcrumb-item"><a href="page-listing-grid.html#">Sub category</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Items</li>
                            </ol>
                        </nav>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-2">Filter by</div>
                        <div class="col-md-10">
                            <ul class="list-inline">
                                <li class="list-inline-item mr-3 dropdown"><a href="page-listing-grid.html#"
                                        class="dropdown-toggle" data-toggle="dropdown"> Supplier type </a>
                                    <div class="dropdown-menu p-3" style="max-width:400px;">
                                        <label class="form-check">
                                            <input type="radio" name="myfilter" class="form-check-input"> Good supplier
                                        </label>
                                        <label class="form-check">
                                            <input type="radio" name="myfilter" class="form-check-input"> Best supplier
                                        </label>
                                        <label class="form-check">
                                            <input type="radio" name="myfilter" class="form-check-input"> New supplier
                                        </label>
                                    </div>
                                </li>
                                <li class="list-inline-item mr-3 dropdown">
                                    <a href="page-listing-grid.html#" class="dropdown-toggle" data-toggle="dropdown">
                                        Country </a>
                                    <div class="dropdown-menu p-3">
                                        <label class="form-check"> <input type="checkbox" class="form-check-input"> China
                                        </label>
                                        <label class="form-check"> <input type="checkbox" class="form-check-input"> Japan
                                        </label>
                                        <label class="form-check"> <input type="checkbox" class="form-check-input">
                                            Uzbekistan </label>
                                        <label class="form-check"> <input type="checkbox" class="form-check-input"> Russia
                                        </label>
                                    </div>
                                </li>
                                <li class="list-inline-item mr-3 dropdown">
                                    <a href="page-listing-grid.html#" class="dropdown-toggle"
                                        data-toggle="dropdown">Feature</a>
                                    <div class="dropdown-menu">
                                        <a href="page-listing-grid.html" class="dropdown-item">Anti backterial</a>
                                        <a href="page-listing-grid.html" class="dropdown-item">With buttons</a>
                                        <a href="page-listing-grid.html" class="dropdown-item">Extra safety</a>
                                    </div>
                                </li>
                                <li class="list-inline-item mr-3"><a href="page-listing-grid.html#">Color</a></li>
                                <li class="list-inline-item mr-3"><a href="page-listing-grid.html#">Size</a></li>
                                <li class="list-inline-item mr-3">
                                    <div class="form-inline">
                                        <label class="mr-2">Price</label>
                                        <input class="form-control form-control-sm" placeholder="Min" type="number">
                                        <span class="px-2"> - </span>
                                        <input class="form-control form-control-sm" placeholder="Max" type="number">
                                        <button type="submit" class="btn btn-sm btn-light ml-2">Ok</button>
                                    </div>
                                </li>
                                <li class="list-inline-item mr-3">
                                    <label class="custom-control mt-1 custom-checkbox">
                                        <input type="checkbox" class="custom-control-input">
                                        <div class="custom-control-label">Ready to ship
                                        </div>
                                    </label>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>-->
            @if(!$publications->isEmpty())
                <div class="row">
                    @foreach($publications as $publication)
                        <div class="col-md-3">
                            <figure class="card card-product-grid">
                                <div class="img-wrap">
                                    <!--<span class="badge badge-danger"> NEW </span>-->
                                    <img src="@if (!$publication->hasMedia('images')) http://placehold.it/700x400 @else {{$publication->getMedia('images')[0]->getUrl()}}@endif">
                                </div>
                                <figcaption class="info-wrap">
                                    <a href="{{ route('view.publications.show',$publication->id)}}" class="title mb-2">{{$publication->title}}</a>
                                    <div class="price-wrap">
                                        <span class="price">${{ $publication->total }}</span>
                                        <small class="text-muted"></small>
                                    </div>
                                    <p class="mb-3">

                                        @if(isset($publication->products()->first()->typeWood))
                                        <span class="tag">@if(isset($publication->products()->first()->typeWood->name)){{$publication->products()->first()->typeWood->name}} @endif</span>
                                        @endif

                                        @if(isset($publication->products()->first()->kind))
                                        <span class="tag">@if(isset($publication->products()->first()->kind->name)){{$publication->products()->first()->kind->name}} @endif</span>
                                        @endif

                                        @if(isset($publication->products()->first()->typeProduct))
                                        <span class="tag">@if(isset($publication->products()->first()->typeProduct->name)){{$publication->products()->first()->typeProduct->name}}@endif</span>
                                        @endif

                                        @if(isset($publication->products()->first()->wayPay))
                                            @if(count($publication->products()->first()->wayPay->paymentTypes)> 0)
                                                @foreach ($publication->products()->first()->wayPay->paymentTypes as $item)
                                                <span class="tag">{{$item->paymentType}}</span>
                                                @endforeach
                                            @endif
                                        @endif
                                    </p>
                                </figcaption>
                            </figure>
                        </div>
                    @endforeach
                </div>
                <nav class="mb-4" aria-label="Page navigation sample">
                    <ul class="pagination">
                    {{ $publications->links() }}
                    </ul>
                </nav>
            @else
                <div class="alert alert-info mt-3">
                    <p class="icontext"><i class="icon text-info fa fa-cube"></i> No hay demandas registradas</p>
                </div>
            @endif

        </div>
    </section>
@endsection
