@extends('layouts.app')
@push('styles')
<style>
/* Chrome, Safari, Edge, Opera */
input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
  -webkit-appearance: none;
  margin: 0;
}

/* Firefox */
input[type=number] {
  -moz-appearance: textfield;
}
</style>
@endpush
@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Calculadora</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a>Home</a>

                </li>
                <li class="breadcrumb-item">
                    <a>Calculadora</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>{{ $publication->title }}</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>{{ $publication->title }}<small></small></h5>
                        <div class="ibox-tools">

                            <a type="button" href="{{ url()->previous() }}" class="btn btn-sm btn-default"> <i
                                    class="fa fa-arrow-left"></i> Atras</a>

                        </div>
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="row">
                                    <div class="col-md-12">

                                        <h3>
                                            Medidas
                                        </h3>

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">

                                        <h5>
                                            Espesor real
                                        </h5>
                                        <p>
                                            @if (asset($product->dimension) && asset($product->dimension->actualWidth))
                                                {{ $product->dimension->actualThickness }} mm @endif
                                        </p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">

                                        <h5>
                                            Ancho real
                                        </h5>
                                        <p>
                                            @if (asset($product->dimension) && asset($product->dimension->actualWidth))
                                                {{ $product->dimension->actualWidth }} mm @endif
                                        </p>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-12">

                                        <h5>
                                            Largo real
                                        </h5>
                                        <p>
                                            @if (asset($product->dimension) && asset($product->dimension->actualLength))
                                                {{ $product->dimension->actualLength }} mm @endif
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="row">
                                    <div class="col-md-12">

                                        <h3>
                                            Precios
                                        </h3>

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">

                                        <h5>
                                            Precio x Pie Tablar
                                        </h5>
                                        <p>
                                            @if (asset($publication->price))
                                                ${{ $publication->price }} @endif
                                        </p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">

                                        <h5>
                                            Precio x unidad
                                        </h5>
                                        <p >
                                            @if (asset($publication->total))
                                                ${{ $publication->total }} @endif
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="row">
                                    <div class="col-md-12">

                                        <h3>
                                            Calculo
                                        </h3>

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <h5>
                                            Superficie a cubri en m2
                                        </h5>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-calculator"></i></span>
                                                <input
                                                 placeholder="Superficie"
                                                 id="superficie"
                                                 name="superficie"
                                                 type="number"
                                                 step="0.01"
                                                 class="form-control superficie">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <h5>
                                            Total
                                        </h5>
                                        <h1>$<span id="total">
                                        </span></h1>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
<script>
    $(document).ready(function(){
        $("#total").html("0.00");
        $(".superficie").on('input',function(){
            var precio = {{  $publication->price}}; //precio tablar
            var espesor =   {{  $product->dimension->actualThickness}} * 0.0393701; // espesor en pulgadas
            var superficieCubrir = $(".superficie").val() * 10.7639;
            var calculo = superficieCubrir * precio * espesor //precio final cubriendo todos los m2
            console.log(precio,espesor,superficieCubrir,calculo);
            $("#total").html(calculo.toFixed(2));
        });
    })
</script>
@endpush
