@extends('layouts.app')

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Tipos de productos</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a>Home</a>

            </li>
            <li class="breadcrumb-item">
                <a>Tipos de productos</a>
            </li>
            {{-- <li class="breadcrumb-item active">
                <strong>Basic Form</strong>
            </li> --}}
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5> Tipos de productos <small></small></h5>
                    <div class="ibox-tools">
                        <a type="button" href="{{ route('typeproducts.create') }}" class="btn btn-primary btn-xs">
                            <i class="fa fa-plus"></i>
                            Nuevo
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="m-b-sm m-t-sm">
                        <form role="search" method="GET" action="{{ route('typeproducts.search') }}">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <a href="{{ route('typeproducts.index') }}" tabindex="-1"
                                        class="btn btn-white btn-sm" type="button"><i class="fa fa-refresh"></i>
                                        Refrescar</a>
                                </div>
                                <input type="text" class="form-control form-control-sm" name="name">
                                <div class="input-group-append">
                                    <button tabindex="-1" class="btn btn-white btn-sm" type="submit"><i
                                            class="fa fa-search"></i> Buscar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>#ID</th>
                                    <th>Nombre </th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($typeproducts as $item)
                                <tr class="row-{{$item->id}}">
                                    <td>{{$item->id}}</td>
                                    <td>{{$item->name}}</td>
                                    <td>
                                        <div class="btn-group" role="group" aria-label="Basic example">

                                            <a type="button" class="btn btn-white btn-sm"
                                                href="{{route('typeproducts.edit',$item->id)}}"><i
                                                    class="fa fa-edit"></i> Editar</a>
                                            <button type="button" class="btn btn-white btn-sm btn-danger"
                                                data-id="{{$item->id}}"><i class="fa fa-trash"></i> Eliminar
                                            </button>

                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="text-center">
                            {{$typeproducts->links()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @endsection
    @push('scripts')

    @routes
    <script>
        $(document).ready(function () {

        $('.btn-danger').on('click', function () {
            var id = $(this).data('id');
            var url = route('typeproducts.destroy', {id: id});

            Swal.fire({
                title: 'Confirmar',
                text: "¿Eliminar tipo de producto?",
                type: 'info',
                showCancelButton: true,
                cancelButtonText: "Cancelar",
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Confirmar'
            }).then(function (result) {
                if (result.value) {
                    axios.delete(url)
                        .then(function (res) {

                            if (res.status === 204) {

                                const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 3000,
                                timerProgressBar: true,
                                onOpen: (toast) => {
                                    toast.addEventListener('mouseenter', Swal.stopTimer)
                                    toast.addEventListener('mouseleave', Swal.resumeTimer)
                                    }
                                })

                                Toast.fire({
                                    icon: 'success',
                                    title: 'Tipo de producto elimininado'
                                })

                                $('.row-'+id).remove();
                            }

                        }).catch(function (res) {
                            swal("No tiene los privilegios para realizar esta acción!", '', 'error');
                        });
                }
            });
        });
    });
    </script>
    @endpush
