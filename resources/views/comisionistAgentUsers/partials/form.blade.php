<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label>Nombre de usuario</label>
            <input type="text" placeholder="Nombre de usuario" name="name" class="form-control"
                value="{{isset($comisionistAgentUser) ? $comisionistAgentUser->name : old('name')}}">
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Email</label>
            <input type="text" placeholder="Email" name="email" class="form-control"
                value="{{isset($comisionistAgentUser) ? $comisionistAgentUser->email : old('email')}}">
        </div>
    </div>


{{-- Si se crea un usuario de muestra esta opcion --}}


    @if(!isset($comisionistAgentUser))
    <div class="col-sm-6">
        <div class="form-group">
            <label>Contraseña</label>
            <input type="text" placeholder="Contraseña" name="password" class="form-control"
                value="{{isset($comisionistAgentUser) ? $comisionistAgentUser->password : old('password')}}">
        </div>
    </div>
    @endif
    <div class="col-sm-6">
        <label>Persona</label>
        <select class="form-control m-b" id="person" name="person" placeholder="Persona">
            <option value="{{isset($comisionistAgentUser->person) ? $comisionistAgentUser->person->id :  '' }}" selected="selected">{{isset($comisionistAgentUser->person) ? $comisionistAgentUser->person->nameComplete : old('person')}}</option>

        </select>
    </div>

</div>

<div class="form-group">
    <button class="btn btn-sm btn-primary float-right m-t-n-xs" type="submit"><i class="fa fa-save"></i>
        Guardar</button>
</div>