@extends('layouts.app')
@push('styles')
<link href="{{asset('admin/css/plugins/select2/select2.min.css')}}" rel="stylesheet">
<link href="{{asset('admin/css/plugins/select2/select2-bootstrap4.min.css')}}"rel="stylesheet">
@endpush
@section('content')
<div>
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Usuarios de comisionistas</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a>Home</a>

                </li>
                <li class="breadcrumb-item">
                    <a>Usuarios de comisionistas</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Editar ({{$comisionistAgentUser->name}})</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5> <small></small></h5>
                        <div class="ibox-tools">
                            <a type="button"  href="{{ url()->previous() }}" class="btn btn-xs btn-white"> <i class="fa fa-arrow-left"></i> Atras</a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-sm-12">
                                <form role="form" method="POST" action="{{ route('comisionistAgentUsers.update',$comisionistAgentUser) }}">
                                    @csrf
                                    @method('PUT')
                                    @include('comisionistAgentUsers.partials.form')  
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection
@push('scripts')
@routes
<script src="{{asset('admin/js/plugins/select2/select2.full.min.js')}}"></script>

<script>
    $("#roles").select2({
     theme: 'bootstrap4',
     placeholder: "-- Seleccionar roles --"
    });

    var tagsControl = $('#person').select2({
        placeholder: "-- Seleccionar persona --",
        theme: 'bootstrap4',
        ajax: {
            delay : 1000,
            allowClear: true,
            url: route('comisionistAgentUsers.getpersons'),
            dataType: 'json',
            data: function(params) {

                return {
                    term: params.term || '',
                    page: params.page || 1
                }
            },
            processResults: function (response) {
                return {
                    results: response.results,
                    pagination: {
                        more: response.pagination.more
                    }
                };
            },

            cache: true
        }
    });

</script>



@endpush