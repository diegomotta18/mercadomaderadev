@extends('layouts.app')

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Usuarios de comisionistas</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a>Home</a>

            </li>
            <li class="breadcrumb-item">
                <strong> <a>Usuarios de comisionistas</a></strong>
            </li>

        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5><small></small></h5>
                    <div class="ibox-tools">

                        <a class="btn btn-xs btn-primary" href="{{route('comisionistAgentUsers.new')}}">
                            <i class="fa fa-plus"></i> Nuevo
                        </a>


                    </div>
                </div>
                <div class="ibox-content">
                    <div class="m-b-sm m-t-sm">
                        <form role="search" method="GET" action="{{ route('comisionistAgentUsers.search') }}">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <a href="{{ route('comisionistAgentUsers.index') }}" tabindex="-1"
                                        class="btn btn-white btn-sm" type="button"><i class="fa fa-refresh"></i>
                                        Refrescar</a>
                                </div>
                                <input type="text" class="form-control form-control-sm" name="search">
                                <div class="input-group-append">
                                    <button tabindex="-1" class="btn btn-primary btn-sm" type="submit">Buscar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>

                                    <th>#ID</th>
                                    <th>Nombre de usuario</th>
                                    <th>Email</th>
                                    <th>Nombre y Apellido</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($users as $user)

                                <tr class="row-{{$user->id}}">
                                    
                                    <td><img class="rounded-circle" style="width: 50px;height:50px;" src="@if ($user->hasMedia('profile')) {{$user->getMedia('profile')[0]->getUrl('thumb')}}@endif" /></td>
                                    <td>{{$user->name}}</td>
                                    <td>{{$user->email}}</td>
                                    <td>{{ isset($user->person) ? $user->person->nameComplete: ''}}</td>
                                    <td>
                                        <div class="btn-group" role="group"
                                            aria-label="Basic example">
                                            {{-- <a type="button" class="btn btn-sm btn-info"
                                                href="{{route('comisionistAgentUsers.show',$user)}}"><i
                                                class="fa fa-eye"></i> Ver</a> --}}
                                            <a type="button" class="btn btn-white btn-sm"
                                                href="{{route('comisionistAgentUsers.edit',$user)}}"><i
                                                    class="fa fa-edit"></i>
                                                Editar</a>
                                            <button type="button" class="btn btn-white btn-sm btn-dgr"
                                                data-id="{{$user->id}}"><i class="fa fa-trash"></i> Eliminar</button>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach

                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@push('scripts')

@routes
<script>
    $(document).ready(function () {

        $('.btn-dgr').on('click', function () {
            var id = $(this).data('id');
            var url = route('comisionistAgentUsers.destroy', {id: id});

            Swal.fire({
                title: 'Confirmar',
                text: "¿Eliminar el comisionista?",
                type: 'info',
                showCancelButton: true,
                cancelButtonText: "Cancelar",
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Confirmar'
            }).then(function (result) {
                if (result.value) {
                    axios.delete(url)
                        .then(function (res) {

                            if (res.status === 204) {

                                const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 3000,
                                timerProgressBar: true,
                                onOpen: (toast) => {
                                    toast.addEventListener('mouseenter', Swal.stopTimer)
                                    toast.addEventListener('mouseleave', Swal.resumeTimer)
                                    }
                                })

                                Toast.fire({
                                    icon: 'success',
                                    title: 'Comisionista elimininado'
                                })

                                $('.row-'+id).remove();
                            } 

                        }).catch(function (res) {
                            swal("No tiene los privilegios para realizar esta acción!", '', 'error');
                        });
                }
            });
        });
    });
</script>
@endpush