@extends('layouts.app')

@section('content')

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Plan contratado</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a>Home</a>

            </li>
            <li class="breadcrumb-item">
                <a href="index.html">Plan contratado</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="row animated fadeInRight">
        <div class="col-lg-12">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5></h5>
                    <div class="ibox-tools">
                        <a type="button" href="{{ url()->previous() }}" class="btn btn-sm btn-default"> <i
                                class="fa fa-arrow-left"></i> Atras</a>
                    </div>
                </div>
                <div class="ibox-content border-left-right">

                        <h2>Datos del plan</h2>

                        <div class="row">
                            <div class="col-sm-3">
                                <h5>
                                    Nombre
                                </h5>
                                <p>
                                       <strong> {{$plan->name}} </strong>
                                </p>
                            </div>
                            <div class="col-sm-3">
                                <h5>
                                    Periodo de facturación
                                </h5>
                                <p>
                                    <strong>{{$plan->invoice_period}} {{ $plan->invoice_interval }}</strong>
                                </p>
                            </div>
                            <div class="col-sm-3">
                                <h5>
                                    Estado
                                </h5>
                                <p>
                                    <strong>@if(!$suscription->canceled()) Activo @else Vencido @endif</strong>
                                </p>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <h5>
                                    Fecha de inicio | Fecha de finalización 
                                </h5>
                                <p>
                                    Desde <strong>{{$suscription->startness()}}</strong> hasta <strong>{{$suscription->endness()}} </strong>
                                </p>
                            </div>
                           
                        </div>

                            
                        <br>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    @if($suscription->isEndingPeriodo())
                                    <form action="{{ route('suscriptions.renew', $suscription->id) }}" method="POST">
                                        @csrf
                                        <input type="hidden" name="title" value="{{ $plan->name }}">
                                        <input type="hidden" name="id_suscription" value="{{ $plan->id }}">
                                        <input type="hidden" name="price" value="{{ $plan->price }}">
                                        <input type="hidden" name="description" value="{{ $plan->description }}">
                                        <input type="hidden" name="date" value="{{ date('Y-m-d H:i:s')}}">
                                        <input type="hidden" name="unit" value="1">
                                        <input type="hidden" name="flag" value="true">

                                        <button type="submit" class="btn btn-sm btn-warning float-right m-t-n-xs">
                                            Renovar
                                        </button>
                                    </form>
                                    @endif
                                    <a  style="color: white; " href="{{ route('ours-plans') }}" class="btn btn-sm btn-primary float-right m-t-n-xs"><i
                                            class="fa fa-id-card"></i> Cambiar de plan</a>
                                </div>
                            </div>
                        </div>
                </div>
                <div class="ibox-content border-left-right">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Referencia de pago </th>
                                    <th>Desde</th>
                                    <th>Hasta</th>
                                    <th>Fecha de pago</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($payments as $item)
                                <tr class="row-{{$item->id}}">
                                    <td>{{$item->payment_identification}}</td>
                                    <td>{{$item->suscription_init}}</td>
                                    <td>{{$item->suscription_finish}}</td>
                                    <td>{{$item->created_at}}</td>                                   
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="text-center">
                            {{$payments->links()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')

@routes
<script>
    $(document).ready(function () {


    });
</script>
@endpush
