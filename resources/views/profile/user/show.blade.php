@extends('layouts.app')

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Perfil de usuario</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a>Home</a>

            </li>
            <li class="breadcrumb-item">
                <strong>Perfil de usuario </strong>
            </li>

        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="row animated fadeInRight">
        <div class="col-lg-12">
            <div class="ibox ">
                {{-- <div class="ibox-title">
                    <h5>Perfil de usuario</h5>

                </div> --}}
                <div class="ibox-content border-left-right">
                    <h2>Foto de perfil</h2>

                    <div class="d-flex justify-content-center ">

                        <img src="@if ($profile->user->hasMedia('profile')) {{$profile->user->getMedia('profile')[0]->getUrl('thumb')}}@endif"
                            class="rounded-circle circle-border m-b-md" alt="profile">

                    </div>
                    <div class="d-flex justify-content-center">

                        <a href="{{route('profile.change.photo')}}" type="button" class="btn btn-sm btn-primary "><i
                                class="fa fa-camera"></i> Cambiar foto de perfil</a>
                    </div>
                </div>
                <div class="ibox-title">
                    <h2>Datos personales</h2>
                    <div class="ibox-tools">
                        <a href="{{route('profile.edit')}}" type="button"
                            class="btn btn-sm btn-primary float-right m-t-n-xs"><i class="fa fa-pencil"></i> Editar</a>
                    </div>
                </div>
                <div class="ibox-content profile-content">

                    <div class="row">
                        <div class="col-md-6">

                            <h5>
                                Nombres y Apellidos
                            </h5>
                            <p>
                                <span class="fa fa-id-card-o"> </span>@if(isset($profile->nameComplete))
                                {{$profile->nameComplete}} @endif
                            </p>

                        </div>
                        <div class="col-md-6">

                            <h5>
                                Número de documento
                            </h5>
                            <p>
                                <span class="fa fa-hashtag"> </span>@if(isset($profile->dni)) {{$profile->dni}} @endif
                            </p>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">

                            <h5>
                                Cuil
                            </h5>
                            <p>
                                <span class="fa fa-hashtag"> </span>@if(isset($profile->cuil)) {{$profile->cuil}} @endif
                            </p>

                        </div>
                        <div class="col-md-6">
                            @if(isset($profile->burndate))
                            <h5>
                                Fecha de nacimiento
                            </h5>
                            <p>
                                <span class="fa fa-calendar"> </span> {{$profile->burndate}}
                            </p>
                            @endif
                        </div>
                    </div>
{{--
                    <h2>Datos de dirección</h2>
                    <div class="row">

                        <div class="col-md-6">
                            <h5>
                                Dirección
                            </h5>

                            <p><span class="fa fa-globe"> </span>
                                @if(($profile->addressLocation()) != null)
                                    {{$profile->addressLocation()}}
                                @endif
                            </p>
                            <p><span class="fa fa-map-marker"></span>
                                @if(($profile->addressComplete()) !=null)
                                    {{$profile->addressComplete()}}
                                @endif
                            </p>
                        </div>
                        <div class="col-md-6">
                            <h5>
                                &nbsp;
                            </h5>
                            @map([
                            'lat' => isset($profile->address->latitud) && $profile->address->latitud != null ? $profile->address->latitud: -27.042191,
                            'lng' => isset($profile->address->longitud) && $profile->address->longitud!= null ? $profile->address->longitud :-54.6922327,
                            'zoom' => 7,
                            'markers' => [
                            [
                            'title' => '',
                            'lat' => isset($profile->address->latitud) && $profile->address->latitud!= null ? $profile->address->latitud : -27.042191,
                            'lng' => isset($profile->address->longitud) && $profile->address->longitud != null ? $profile->address->longitud :-54.6922327,
                            ],
                            ],
                            ])
                        </div>

                    </div> --}}

                    <h2>Datos de contacto</h2>
                    <div class="row">
                        <div class="col-md-6">

                            <h5>
                                Telefono
                            </h5>
                            <p>
                                <span class="fa fa-phone"> </span>
                                @if(isset($profile->contact->telphone)){{$profile->contact->telphone}}@endif
                            </p>

                        </div>
                        <div class="col-md-6">

                            <h5>
                                Celular
                            </h5>
                            <p>
                                <span class="fa fa-mobile"> </span>@if(isset($profile->contact->celphone))
                                {{$profile->contact->celphone}}@endif
                            </p>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">

                            <h5>
                                Email
                            </h5>
                            <p>
                                <span class="fa fa-envelope"> </span>
                                @if(isset($profile->user->email)){{$profile->user->email}}@endif
                            </p>

                        </div>

                        {{-- <div class="col-md-6">

                            <h5>
                                Web
                            </h5>
                            <p>
                                <span class="fa fa-globe"> </span>@if(isset($profile->contact->web))
                                {{$profile->contact->web}}@endif
                            </p>
                        </div> --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@push('scripts')

@routes
<script>
    $(document).ready(function () {


    });
</script>
@endpush
