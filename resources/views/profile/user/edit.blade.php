@extends('layouts.app')
@push('styles')
<link href="{{asset('admin/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css')}}" rel="stylesheet">

@endpush
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Perfil de usuario</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a>Home</a>

            </li>
            <li class="breadcrumb-item">
                <a href="index.html">Perfil de usuario</a>
            </li>
            <li class="breadcrumb-item">
                <a>Editar</a>
            </li>

        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="row animated fadeInRight">
        <div class="col-lg-12">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5></h5>
                    <div class="ibox-tools">
                        <a type="button" href="{{ url()->previous() }}" class="btn btn-sm btn-default"> <i
                                class="fa fa-arrow-left"></i> Atras</a>
                    </div>
                </div>
                <div class="ibox-content border-left-right">
                    <form role="form" method="POST" action="{{ route('profile.update') }}">
                        @csrf
                        @method('PUT')
                        <h2>Datos personales</h2>

                        <div class="row">
                            <div class="col-sm-6">
                                <h5>
                                    Nombres
                                </h5>
                                <p>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-font"></i></span>
                                            <input type="text" id="name" placeholder="Nombre" name="name"
                                                class="form-control"
                                                value="{{isset($profile->name) ? $profile->name : old('name')}}">
                                        </div>
                                    </div>
                                </p>
                            </div>
                            <div class="col-sm-6">
                                <h5>
                                    Apellidos
                                </h5>
                                <p>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-font"></i></span>
                                            <input type="text" id="surname" placeholder="Apellido" name="surname"
                                                class="form-control"
                                                value="{{isset($profile->surname) ? $profile->surname : old('surname')}}">
                                        </div>
                                    </div>
                                </p>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <h5>
                                    Número de documento
                                </h5>
                                <p>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-hashtag"></i></span>
                                            <input type="text" id="dni" placeholder="Nº Documento" name="dni"
                                                class="form-control"
                                                value="{{isset($profile->dni) ? $profile->dni : old('dni')}}">
                                        </div>
                                    </div>
                                </p>
                            </div>
                            <div class="col-sm-6">
                                <h5>
                                    Cuil
                                </h5>
                                <p>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-hashtag"></i></span>
                                            <input type="text" id="cuil" placeholder="Nº Documento" name="cuil"
                                                class="form-control"
                                                value="{{isset($profile->cuil) ? $profile->cuil : old('cuil')}}">
                                        </div>
                                    </div>
                                </p>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <h5>
                                    Fecha de nacimiento
                                </h5>
                                <p>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input
                                                placeholder="Fecha de nacimiento" id="burndate" name="burndate"
                                                type="text" class="form-control"
                                                value="{{isset($profile) ? $profile->burndate: old('burndate')}}">
                                        </div>
                                    </div>
                                </p>
                            </div>
                        </div>
                        <h2>Datos de contacto</h2>

                        <div class="row">
                            <div class="col-sm-6">
                                <h5>
                                    Telefono
                                </h5>
                                <p>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                            <input type="number" id="telphone" placeholder="Teléfono" name="telphone"
                                                class="form-control"
                                                value="{{isset($profile->contact) ? $profile->contact->telphone : old('telphone')}}">
                                        </div>
                                    </div>
                                </p>

                            </div>

                            <div class="col-sm-6">
                                <h5>
                                    Celular
                                </h5>
                                <p>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                            <input type="number" id="celphone" placeholder="Celular" name="celphone"
                                                class="form-control"
                                                value="{{isset($profile->contact) ? $profile->contact->celphone : old('celphone')}}">
                                        </div>
                                    </div>
                                </p>

                            </div>
                        </div>

                        {{-- <div class="row">
                            <div class="col-sm-6">
                                <h5>
                                    Email
                                </h5>
                                <p>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                            <input type="email" id="email" placeholder="Email" name="email"
                                                class="form-control"
                                                value="{{isset($profile->contact) ? $profile->contact->email : old('email')}}">
                                        </div>
                                    </div>
                                </p>
                            </div>
                            <div class="col-sm-6">
                                <h5>
                                    Sitio web
                                </h5>
                                <p>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-globe"></i></span>
                                            <input type="text" id="web" placeholder="Sitio web" name="web"
                                                class="form-control"
                                                value="{{isset($profile->contact) ? $profile->contact->web : old('web')}}">
                                        </div>
                                    </div>
                                </p>
                            </div>
                        </div>
                        <h2>Datos de dirección</h2>
                        <div class="row">
                            @if(isset($profile->address))

                            <div class="col-sm-6">

                                <address-map :named="''" :addressj='@json($profile->address)'></address-map>

                            </div>
                            <div class="col-sm-6">

                                @map([
                                'lat' => $profile->address != null ? $profile->address->latitud: -27.042191,
                                'lng' => $profile->address != null ? $profile->address->longitud:-54.6922327,
                                'zoom' => 7,
                                'markers' => [
                                [
                                'title' => '',
                                'lat' => $profile->address != null ? $profile->address->latitud: -27.042191,
                                'lng' => $profile->address != null ? $profile->address->longitud:-54.6922327,
                                ],
                                ],
                                ])
                            </div>

                            @else
                            <div class="col-sm-12">
                                <address-component :named="''"></address-component>
                            </div>
                            @endif

                        </div> --}}
                        <br>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-sm btn-primary float-right m-t-n-xs"><i
                                            class="fa fa-id-card"></i> Guardar</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@push('scripts')
<script src="{{asset('admin/js/plugins/moment/moment.js')}}"></script>
<script src="{{asset('admin/js/plugins/moment/es.js')}}"></script>
<script src="{{asset('admin/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js')}}"></script>
@routes
<script>
    $(document).ready(function () {


    });
    $('#burndate').datetimepicker({
                inline: false,
                locale: 'es',
                format: "L",
        });
</script>
@endpush
