@extends('layouts.app')
@push('styles')
    <style>
        .thumb-post img {
            width: 740px;
            max-height: 460px;
            margin-bottom: 1rem;
            background-size: cover;
        }

        @media (max-width: 768px) {
            .thumb-post img {
                width: 300px;
                max-height: 360px;
                margin-bottom: 1rem;
                background-size: cover;
            }
        }

    </style>

    <style>
        #map {

            width: 100%;
            height: 75vh;
        }

        /* #geocoder-container>div {
            min-width: 50%;
            margin-left: 25%;
        } */
        .coordinates {
            background:rgba(0, 0, 0, 0.5);
            color: #fff;
            position: absolute;
            bottom: 40px;
            left: 10px;
            padding: 5px 10px;
            margin: 0;
            font-size: 11px;
            line-height: 18px;
            border-radius: 3px;
            display: none;
        }
</style>
@endpush
@section('content')

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Perfil de empresa</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a>Home</a>

            </li>
            <li class="breadcrumb-item">
                <a>Perfil de empresa</a>
            </li>

        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="row animated fadeInRight">


        <div class="col-lg-12">
            <div class="ibox ">
{{--
                <div class="ibox-title">
                    <h5>Perfil de empresa</h5>
                    <div class="ibox-tools">

                    </div>
                </div> --}}
                <div class="ibox-content border-left-right">
                    <h2>Logo de la empresa</h2>
                    <div class="d-flex justify-content-center ">

                        <img src="@if ($company->hasMedia('logo')) {{$company->getMedia('logo')[0]->getUrl('thumb')}}@endif"
                            class="rounded-circle circle-border m-b-md" alt="profile">

                    </div>
                    <div class="d-flex justify-content-center">

                        <a href="{{route('profile.company.editLogo')}}" type="button" class="btn btn-sm btn-primary "><i
                                class="fa fa-camera"></i> Cambiar logo</a>
                    </div>
                </div>
                <div class="ibox-content border-left-right">
                    @if(isset($company) && $company->hasMedia('companies'))
                    <div class="d-flex justify-content-center ">
                        <div class="row">
                            <div class="easyzoom easyzoom--overlay easyzoom--with-thumbnails">
                                <a href="{{$company->getMedia('companies')[0]->getUrl()}}">
                                    <div class="thumb-post">
                                        <img src="{{$company->getMedia('companies')[0]->getUrl()}}" />
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="d-flex justify-content-center ">
                        <div class="row">
                            <ul class="thumbnails">
                                @foreach($company->getMedia('companies') as $fotos)
                                {{-- <a href="{{$fotos->getUrl()}}">
                                <img src="{{$fotos->getUrl('thumb')}}" class="img-thumbnail" alt="{{$fotos->name}}">
                                </a> --}}
                                <li>
                                    <a href="{{$fotos->getUrl()}}" data-standard="{{$fotos->getUrl()}}">
                                        <img src="{{$fotos->getUrl('thumb')}}" alt="" />
                                    </a>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>

                    <br>
                    @endif
                    <h2>Fotos de la empresa</h2>
                    <div class="d-flex justify-content-center">

                        <a href="{{route('profile.company.editImgs')}}" type="button" class="btn btn-sm btn-primary "><i
                                class="fa fa-camera"></i> Fotos</a>
                    </div>
                </div>


                <div class="ibox-title">
                    <h2>Datos de la empresa</h2>
                    <div class="ibox-tools">
                        <a href="{{route('profile.company.edit')}}" type="button"
                            class="btn btn-sm btn-primary float-right m-t-n-xs"><i class="fa fa-pencil"></i> Editar</a>
                    </div>
                </div>
                <div class="ibox-content profile-content">
                    <div class="row">
                        <div class="col-sm-6">
                            @if (isset($company->name))
                            <h5>
                                Nombre
                            </h5>

                            <p>
                                <span class="fa fa-bank"> </span> {{$company->name}}
                            </p>
                            @endif
                        </div>
                        <div class="col-sm-6">
                            @if (isset($company->cuit))
                            <h5>
                                Cuit
                            </h5>
                            <p>
                                <span class="fa fa-user"> </span> {{$company->cuit}}
                            </p>
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">


                            <h5>
                                Trayectoria
                            </h5>
                            <p>
                                <span class="fa fa-signal"> </span> @if (isset($company->trajectory))
                                {{$company->trajectory}}@endif
                            </p>

                        </div>
                        <div class="col-sm-6">

                            <h5>
                                Compromiso
                            </h5>
                            <p>
                                <span class="fa fa-handshake-o"> </span>@if (isset($company->compromise))
                                {{$company->compromise}}@endif
                            </p>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">

                            <h5>
                                Tecnologias
                            </h5>
                            <p>
                                <span class="fa fa-cogs"> </span>@if (isset($company->technologies))
                                {{$company->technologies}}@endif
                            </p>

                        </div>
                        <div class="col-sm-6">

                            <h5>
                                Otros
                            </h5>
                            <p>
                                <span class="fa fa-info"> </span>@if (isset($company->others))
                                {{$company->others}}@endif
                            </p>

                        </div>
                    </div>
                    <h2>
                        Datos de dirección
                    </h2>
                    <div class="row">
                        <div class="col-sm-6">

                            <h5>
                                Dirección
                            </h5>

                            <p><span class="fa fa-globe"></span>
                                @if($company->addressLocation()!=null)
                                {{$company->addressLocation()}}
                            @endif</p>

                            <p> <span class="fa fa-map-marker"></span>
                                @if(($company->addressComplete()) !=null)
                                  {{$company->addressComplete()}}
                                @endif
                            </p>
                        </div>
                        <div class="col-md-6">
                            <h5>
                                &nbsp;
                            </h5>



                            {{-- @map([
                                'lat' => $company->address->latitud ?? -27.042191,
                                'lng' => $company->address->longitud ?? -54.6922327,
                                'zoom' => 7,
                                'markers' => [
                                [
                                'title' => '',
                                'lat' => $company->address->latitud ?? -27.042191,
                                'lng' => $company->address->longitud ?? -54.6922327,
                                ],
                                ],
                                ]) --}}
                        </div>


                    </div>
                    <h2>
                        Mapa
                    </h2>
                    <div class="row">
                        <div class="col-md-12">
                            <div id="map"></div>
                            <pre id="coordinates" class="coordinates"></pre>
                        </div>

                    </div>
                    {{-- <h2>
                        Datos de contacto
                    </h2>
                    <div class="row">

                        <div class="col-sm-6">


                            <h5>
                                Celular
                            </h5>
                            <p>
                                <span class="fa fa-mobile"> </span>
                                @if(isset($company->contact->celphone)){{$company->contact->celphone}}@endif
                            </p>

                        </div>

                        <div class="col-sm-6">

                            <h5>
                                Telefono
                            </h5>
                            <p>
                                <span class="fa fa-phone"> </span>@if(isset($company->contact->telephone))
                                {{$company->contact->telephone}} @endif
                            </p>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">

                            <h5>
                                Email
                            </h5>
                            <p>
                                <span class="fa fa-envelope"> </span>
                                @if(isset($company->contact->email)){{$company->contact->email}}@endif
                            </p>

                        </div>

                        <div class="col-sm-6">


                            <h5>
                                Web
                            </h5>
                            <p>
                                <span class="fa fa-globe"> </span> @if(isset($company->contact->web))
                                {{$company->contact->web}}@endif
                            </p>

                        </div>
                    </div> --}}
                </div>
            </div>
        </div>
    </div>

</div>
</div>
@endsection
@push('scripts')

    <script>
        // mapboxgl.accessToken = 'pk.eyJ1IjoicnViZW5tb3YiLCJhIjoiY2tqMGN1Nnk3MGZ3NDJycGh2eWJ4NW5ubSJ9.ac6ojU1TR6GcYSPfV_F0WQ';
        // var map = new mapboxgl.Map({
        //     container: 'map',
        //     style: 'mapbox://styles/mapbox/streets-v11',
        //     center: [-54.6922327,-27.042191],
        //     zoom: 6
        // });

        // var geocoder = new MapboxGeocoder({
        //     accessToken: mapboxgl.accessToken,
        //     marker: {
        //     color: 'green'
        //     },
        //     mapboxgl: mapboxgl
        // });

        // map.addControl(geocoder);


        mapboxgl.accessToken = 'pk.eyJ1IjoicnViZW5tb3YiLCJhIjoiY2tqMGN1Nnk3MGZ3NDJycGh2eWJ4NW5ubSJ9.ac6ojU1TR6GcYSPfV_F0WQ';
        var coordinates = document.getElementById('coordinates');
        var map = new mapboxgl.Map({
            container: 'map',
            style: 'mapbox://styles/mapbox/streets-v11',
            center: [-54.6922327,-27.042191],
            zoom: 6
        });

        var canvas = map.getCanvasContainer();

        var geojson = {
            'type': 'FeatureCollection',
            'features': [{
                'type': 'Feature',
                'geometry': {
                    'type': 'Point',
                    'coordinates': [-54.6922327,-27.042191]
                }
            }]
        };

        function onMove(e) {
            var coords = e.lngLat;

            // Set a UI indicator for dragging.
            canvas.style.cursor = 'grabbing';

            // Update the Point feature in `geojson` coordinates
            // and call setData to the source layer `point` on it.
            geojson.features[0].geometry.coordinates = [coords.lng, coords.lat];
            map.getSource('point').setData(geojson);
        }

        function onUp(e) {
            var coords = e.lngLat;

            // Print the coordinates of where the point had
            // finished being dragged to on the map.
            coordinates.style.display = 'block';
            coordinates.innerHTML =
                'Longitude: ' + coords.lng + '<br />Latitude: ' + coords.lat;
            canvas.style.cursor = '';

            // Unbind mouse/touch events
            map.off('mousemove', onMove);
            map.off('touchmove', onMove);
        }

        map.on('load', function() {
            // Add a single point to the map
            map.addSource('point', {
                'type': 'geojson',
                'data': geojson
            });

            map.addLayer({
                'id': 'point',
                'type': 'circle',
                'source': 'point',
                'paint': {
                    'circle-radius': 10,
                    'circle-color': '#3887be'
                }
            });

            // When the cursor enters a feature in the point layer, prepare for dragging.
            map.on('mouseenter', 'point', function() {
                map.setPaintProperty('point', 'circle-color', '#3bb2d0');
                canvas.style.cursor = 'move';
            });

            map.on('mouseleave', 'point', function() {
                map.setPaintProperty('point', 'circle-color', '#3887be');
                canvas.style.cursor = '';
            });

            map.on('mousedown', 'point', function(e) {
                // Prevent the default map drag behavior.
                e.preventDefault();

                canvas.style.cursor = 'grab';

                map.on('mousemove', onMove);
                map.once('mouseup', onUp);
            });

            map.on('touchstart', 'point', function(e) {
                if (e.points.length !== 1) return;

                // Prevent the default map drag behavior.
                e.preventDefault();

                map.on('touchmove', onMove);
                map.once('touchend', onUp);
            });
        });
        map.addControl(
            new MapboxGeocoder({
            accessToken: mapboxgl.accessToken,
            mapboxgl: mapboxgl,
             // Do not use the default marker style
            placeholder: 'Lugares en Misiones',
            })
        );
        map.addControl(new mapboxgl.NavigationControl());





    </script>
@endpush
