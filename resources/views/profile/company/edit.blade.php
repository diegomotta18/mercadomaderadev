@extends('layouts.app')
@push('styles')

@endpush
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Perfil de empresa</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a>Home</a>

            </li>
            <li class="breadcrumb-item">
                <a>Perfil de empresa</a>
            </li>
            <li class="breadcrumb-item">
                <a>Editar</a>
            </li>

        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="row animated fadeInRight">
        <div class="col-lg-12">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5></h5>
                    <div class="ibox-tools">
                        <a type="button" href="{{ url()->previous() }}" class="btn btn-sm btn-default"> <i
                                class="fa fa-arrow-left"></i> Atras</a>
                    </div>
                </div>
                <div>
                    <form role="form" method="POST" action="{{ route('profile.company.update') }}">
                        @csrf
                        @method('PUT')
                        <div class="ibox-content company-content">
                            <h2>Datos de la empresa</h2>

                            <div class="row">
                                <div class="col-sm-6">
                                    <h5>
                                        Nombre
                                    </h5>
                                    <p>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-font"></i></span>
                                                <input type="text" id="name" placeholder="Nombre" name="name"
                                                    class="form-control"
                                                    value="{{isset($company->name) ? $company->name : old('name')}}">
                                            </div>
                                        </div>
                                    </p>
                                </div>
                                <div class="col-sm-6">
                                    <h5>
                                        CUIT
                                    </h5>
                                    <p>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-font"></i></span>
                                                <input type="text" id="cuit" placeholder="Cuit" name="cuit"
                                                    class="form-control"
                                                    value="{{isset($company->cuit) ? $company->cuit : old('cuit')}}">
                                            </div>
                                        </div>
                                    </p>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <h5>
                                        Trayetoria
                                    </h5>
                                    <p>
                                        <div class="form-group">
                                            <textarea type="text" id="dni" placeholder="Trayectoria" name="trajectory"
                                                class="form-control"
                                                value="{{isset($company->trajectory) ? $company->trajectory : old('trajectory')}}"></textarea>
                                        </div>
                                    </p>
                                </div>
                                <div class="col-sm-6">
                                    <h5>
                                        Compromiso
                                    </h5>
                                    <p>
                                        <div class="form-group">
                                            <textarea type="text" id="compromise" placeholder="Compromiso"
                                                name="compromise" class="form-control"
                                                value="{{isset($company->compromise) ? $company->compromise : old('compromise')}}"></textarea>
                                        </div>
                                    </p>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <h5>
                                        Tecnologías
                                    </h5>
                                    <p>
                                        <div class="form-group">
                                            <textarea placeholder="Tecnologías" id="technologies" name="technologies"
                                                type="text" class="form-control"
                                                value="{{isset($company) ? $company->technologies: old('technologies')}}"></textarea>
                                        </div>
                                    </p>
                                </div>
                            </div>
                            {{-- <h2>Datos de contacto</h2>

                            <div class="row">
                                <div class="col-sm-6">
                                    <h5>
                                        Telefono
                                    </h5>
                                    <p>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                                <input type="number" id="telphone" placeholder="Teléfono"
                                                    name="telphone" class="form-control"
                                                    value="{{isset($company->contact) ? $company->contact->telphone : old('telphone')}}">
                                            </div>
                                        </div>
                                    </p>

                                </div>

                                <div class="col-sm-6">
                                    <h5>
                                        Celular
                                    </h5>
                                    <p>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                                <input type="number" id="celphone" placeholder="Celular" name="celphone"
                                                    class="form-control"
                                                    value="{{isset($company->contact) ? $company->contact->celphone : old('celphone')}}">
                                            </div>
                                        </div>
                                    </p>

                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <h5>
                                        Email
                                    </h5>
                                    <p>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                                <input type="email" id="email" placeholder="Email" name="email"
                                                    class="form-control"
                                                    value="{{isset($company->contact) ? $company->contact->email : old('email')}}">
                                            </div>
                                        </div>
                                    </p>
                                </div>
                                <div class="col-sm-6">
                                    <h5>
                                        Sitio web
                                    </h5>
                                    <p>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-globe"></i></span>
                                                <input type="text" id="web" placeholder="Sitio web" name="web"
                                                    class="form-control"
                                                    value="{{isset($company->contact) ? $company->contact->web : old('web')}}">
                                            </div>
                                        </div>
                                    </p>
                                </div>
                            </div> --}}
                            <h2>Datos de dirección</h2>
                            <div class="row">
                                @if(isset($company->address))

                                <div class="col-sm-6">

                                    <address-map :named="''" :addressj='@json($company->address)'></address-map>

                                </div>
                                <div class="col-sm-6">

                                    @map([
                                    'lat' => $company->address != null ? $company->address->latitud: -27.042191,
                                    'lng' => $company->address != null ?  $company->address->longitud:-54.6922327,
                                    'zoom' => 7,
                                    'markers' => [
                                    [
                                    'title' => '',
                                    'lat' => $company->address != null ? $company->address->latitud: -27.042191,
                                    'lng' => $company->address != null ? $company->address->longitud:-54.6922327,
                                    ],
                                    ],
                                    ])
                                </div>

                                @else
                                <div class="col-sm-12">
                                    <address-component :named="''"></address-component>
                                </div>
                                @endif
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-sm btn-primary float-right m-t-n-xs"><i
                                                class="fa fa-id-card"></i> Guardar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@push('scripts')

@routes
<script>
    $(document).ready(function () {


    });

</script>
@endpush
