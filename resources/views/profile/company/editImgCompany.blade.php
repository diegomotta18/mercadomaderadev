@extends('layouts.app')
@push('styles')
<link href="{{ asset('admin/css/plugins/dropzone/dropzone.css') }}" rel="stylesheet">

@endpush
@section('content')
<div>
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2> Fotos de la empresa</h2>

            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a>Home</a>

                </li>
                <li class="breadcrumb-item">
                    <a>Perfil de empresa</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Fotos de la empresa</strong> <small>({{$company->name}})
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5> Agregar/actualizar fotos de la empresa<small></small></h5>
                        <div class="ibox-tools">
                            <a type="button" href="{{ url()->previous() }}" class="btn btn-sm btn-default"> <i
                                    class="fa fa-arrow-left"></i> Atras</a>
                        </div>
                    </div>

                    <form role="form" method="POST" action="{{ route('profile.company.updateImgs') }}">
                        @csrf
                        @method('PUT')
                        <div class="ibox-content">
                            <div class="col-md-12">
                                <div class="alert alert-info"><h5><i class="fa fa-info-circle"></i>Se puede agregar hasta un máximo de 5 fotos</h5></div>
                                <div class="needsclick dropzone" id="document-dropzone">
                                </div>
                            </div>
                            <br>
                            <div class="d-flex justify-content-center ">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-sm btn-primary "><i class="fa fa-camera"></i>
                                        Guardar</a>

                                </div>

                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>

</div>
@endsection
@push('scripts')
@routes
<script src="{{asset('admin/js/dropz/dropzone.js')}}"></script>

<script>
    var uploadedDocumentMap = {}
    Dropzone.options.documentDropzone = {
            dictDefaultMessage: 'Clic aqui para seleccionar fotos de la empresa',
            dictRemoveFile: 'Eliminar',
            dictCancelUpload : 'Cancelar',
            url: '{{route('admin.archivos.store')}}',
            maxFiles: 5,
            maxFilesize: 100, // MB
            addRemoveLinks: true,
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            success: function(file, response) {
                $('form').append('<input type="hidden" name="document[]" value="' + response.name + '">')
                console.log(response.name)
                uploadedDocumentMap[file.name] = response.name
            },
            removedfile: function(file) {
                    file.previewElement.remove()
                    var name = ''
                    if (typeof file.file_name !== 'undefined') {
                        name = file.file_name
                    } else {
                        name = uploadedDocumentMap[file.name]
                    }
                    var id = "{!!$company->id!!}"
                    $('form').find('input[name="document[]"][value="' + name+ '"]').remove()
                    var url = route('deleteImageCompany',{id: id,file: file.name})//'/admin/archivos/'+ file.name+'/destroyFileEmpresa';
                    axios.delete(url).then(function(response) {
                        console.log(response);
                    })
                    .catch(function(error) {
                        console.log(error);
                    });
            },
            maxfilesexceeded: function(file){
                this.removeFile(file);
            },
        init: function() {

            @if(isset($company) && $company->getMedia('companies'))
                @foreach($company->getMedia('companies') as $fotos)
                    var url = "{!!$fotos->getUrl('thumb')!!}"
                    var name = "{!!$fotos->name!!}"
                    var size = "{!!$fotos->size!!}"
                    var file_name = "{!!$fotos->file_name!!}"
                    var mockFile = {
                        name: name,
                        size: size
                    };
                    this.options.addedfile.call(this, mockFile)
                    this.options.thumbnail.call(this, mockFile, url)
                    this.options.complete.call(this, mockFile)
                    //importante si se quiere conocer que name corresponde a un name.jpg
                    uploadedDocumentMap[name] = file_name
                    $('form').append('<input type="hidden" name="document[]" value="' + file_name + '">')
                @endforeach
            @endif

        }
    }
</script>
@endpush