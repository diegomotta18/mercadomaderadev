@extends('layouts.app')
@push('styles')
<link href="{{ asset('admin/css/plugins/dropzone/dropzone.css') }}" rel="stylesheet">

@endpush
@section('content')
<div>
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2> Logo de la empresa</h2>

            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a>Home</a>

                </li>
                <li class="breadcrumb-item">
                    <a>Perfil de empresa</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Logo de la empresa</strong> <small>({{$company->name}})
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5> Cambiar logo de la empresa<small></small></h5>
                        <div class="ibox-tools">
                            <a type="button" href="{{ url()->previous() }}" class="btn btn-sm btn-default"> <i
                                    class="fa fa-arrow-left"></i> Atras</a>
                        </div>
                    </div>

                    <form role="form" method="POST" action="{{ route('profile.company.changeLogo') }}">
                        @csrf
                        @method('PUT')
                        <div class="ibox-content">

                            <div class="d-flex justify-content-center ">
                                <div class="needsclick dropzone" id="document-dropzone">
                                </div>
                            </div>
                            <br>
                            <div class="d-flex justify-content-center ">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-sm btn-primary "><i class="fa fa-camera"></i>
                                        Guardar</a>

                                </div>

                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>

</div>
@endsection
@push('scripts')
@routes
<script src="{{asset('admin/js/dropz/dropzone.js')}}"></script>

<script>
    var uploadedDocumentMap = {}
    Dropzone.options.documentDropzone = {
            dictDefaultMessage: 'Clic aqui para seleccionar logo de la empresa',
            dictRemoveFile: 'Eliminar',
            dictCancelUpload : 'Cancelar imagen',
            url: '{{route('admin.archivos.store')}}',
            maxFiles: 1,
            maxFilesize: 100, // MB
            addRemoveLinks: true,
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            success: function(file, response) {
                $('form').append('<input type="hidden" name="document[]" value="' + response.name + '">')
                uploadedDocumentMap[file.name] = response.name
            },
            removedfile: function(file) {
                    console.log(file)
                    file.previewElement.remove()
                    var name = ''
                    if (typeof file.file_name !== 'undefined') {
                        name = file.file_name
                    } else {
                        name = uploadedDocumentMap[file.name]
                    }
                    $('form').find('input[name="document[]"][value="' + name+ '"]').remove()
                    var id = "{!!$company->id!!}"
                    var url = route('imgprofile.delete',{id: id,file: file.name})//'/admin/archivos/'+ file.name+'/destroyFileEmpresa';
                    axios.delete(url).then(function(response) {
                        console.log(response)
                    })
                    .catch(function(error) {
                        console.log(error);
                    });
            },
            maxfilesexceeded: function(file){
                this.removeFile(file);
            },
        // init: function() {
        //     @if(isset($company) && $company->getMedia('profile'))
        //         @foreach($company->getMedia('profile') as $fotos)
        //             var url = "{!!$fotos->getUrl('thumb')!!}"
        //             var name = "{!!$fotos->name!!}"
        //             var size = "{!!$fotos->size!!}"
        //             var file_name = "{!!$fotos->file_name!!}"
        //             var mockFile = {
        //                 name: name,
        //                 size: size
        //             };
        //             this.options.addedfile.call(this, mockFile)
        //             this.options.thumbnail.call(this, mockFile, url)
        //             this.options.complete.call(this, mockFile)
        //             $('form').append('<input type="hidden" name="document[]" value="' + file_name + '">')
        //         @endforeach
        //     @endif
        // }
    }
</script>
@endpush