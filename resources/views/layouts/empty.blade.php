<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="pragma" content="no-cache" />
    <meta http-equiv="cache-control" content="max-age=604800" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>{{ config('app.name', 'Mercado Madera') }}</title>
    <link href="{{ asset('main/images/favicon.ico') }}" rel="shortcut icon" type="image/x-icon">
    <link href="{{ asset('main/css/bootstrap.css') }}" rel="stylesheet" type="text/css" />
    {{--    <link href="{{ asset('main/fonts/fontawesome/css/all.min.css') }}" type="text/css" rel="stylesheet">--}}
    <link href="{{ asset('admin/font-awesome/css/font-awesome.css') }}" type="text/css" rel="stylesheet">

    <link href="{{ asset('main/css/ui.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('main/css/responsive.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('admin/css/plugins/sweetalert2/sweetalert2.css') }}" rel="stylesheet">
    <link href="{{ asset('main/css/zoom-box.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.css">

    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body>
@yield('content')
</body>
<script src="{{ asset('main/js/jquery-2.0.0.min.js') }}" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.js"></script>

<script src="{{ asset('main/js/bootstrap.bundle.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('main/js/script.js') }}" type="text/javascript"></script>

<script src="{{ asset('main/js/zoom-box.js') }}" type="text/javascript"></script>
<script src="{{asset('admin/js/plugins/sweetalert2/sweetalert2.all.js')}}"></script>
{{-- <script src="{{ asset('js/app.js') }}"></script> --}}

@include('sweetalert::alert')
@stack('scripts')
