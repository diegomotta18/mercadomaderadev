

<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>{{ config('app.name', 'Mercado Madera') }}</title>
    <link href="{{ asset('main/images/favicon.ico')}}" rel="shortcut icon" type="image/x-icon">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{{ asset('admin/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{ asset('admin/font-awesome/css/font-awesome.css')}}" rel="stylesheet">
    <link href="{{ asset('admin/css/animate.css')}}" rel="stylesheet">
    <link href="{{ asset('admin/css/style.css')}}" rel="stylesheet">
    <!-- Sweet Alert 2  -->
    <link href="{{ asset('admin/css/plugins/sweetalert2/sweetalert2.css') }}" rel="stylesheet">
    <link href="{{ asset('admin/css/plugins/easyzoom/easyzoom.css') }}" rel="stylesheet">
    <!-- Mapbox  -->
    <link href="{{ asset('admin/css/plugins/mapboxgl/mapbox-gl.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}" />
    <!-- Scripts -->
    @stack('styles')
</head>

<body  class="md-skin">

    <div id="wrapper">
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav metismenu" id="side-menu">
                    <li class="nav-header">
                        <div class="dropdown profile-element">
                            <p style="text-align: center;">
                            <img alt="image" class="rounded-circle" src="@if (auth()->user()->hasMedia('profile')) {{auth()->user()->getMedia('profile')[0]->getUrl('thumb')}}@endif" />
                            </p>
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                @if(auth()->user()->hasRole('ROOT'))
                                <span style="text-align: center;" class="block m-t-xs font-bold"><h4><b>Perfil Programador</b></h4></span>
                                @elseif(auth()->user()->hasRole('Premium'))
                                <span style="text-align: center;" class="block m-t-xs font-bold"><h4><b>Perfil de Empresa</b></h4></span>
                                @elseif(auth()->user()->hasRole('Administrador'))
                                <span style="text-align: center;" class="block m-t-xs font-bold"><h4><b>Perfil Administrador</b></h4></span>
                                @elseif(auth()->user()->hasRole('Normal'))
                                <span style="text-align: center;" class="block m-t-xs font-bold"><h4><b>Perfil Normal</b></h4></span>
                                @elseif(auth()->user()->hasRole('Comisionista'))
                                <span style="text-align: center;" class="block m-t-xs font-bold"><h4><b>Perfil Comisionista</b></h4></span>
                                @endif
                                <span style="text-align: center;" class="block m-t-xs font-bold">{{auth()->user()->name}}</span>

                                {{-- <span style="text-align: center;" class="text-muted text-xs block"><b class="caret"></b></span> --}}
                            </a>
                            {{-- <ul class="dropdown-menu animated fadeInRight m-t-xs">
                                <li><a class="dropdown-item" href="profile.html">Profile</a></li>
                                <li><a class="dropdown-item" href="contacts.html">Contacts</a></li>
                                <li><a class="dropdown-item" href="mailbox.html">Mailbox</a></li>
                                <li class="dropdown-divider"></li>



                                <li><a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">
                                        Salir
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                        style="display: none;">
                                        @csrf
                                    </form>
                                </li>
                            </ul> --}}
                        </div>
                        <div class="logo-element">
                            IN+
                        </div>
                    </li>
                    @if(auth()->user()->hasRole('ROOT'))
                        @include('layouts.main.root')
                    @elseif(auth()->user()->hasRole('Premium'))
                        @include('layouts.main.premium')
                    @elseif(auth()->user()->hasRole('Administrador'))
                        @include('layouts.main.administrador')
                    @elseif(auth()->user()->hasRole('Normal'))
                        @include('layouts.main.normal')
                    @elseif(auth()->user()->hasRole('Comisionista'))
                        @include('layouts.main.comisionista')
                    @endif
                </ul>
            </div>
        </nav>

        <div id="page-wrapper" class="gray-bg">
            <div class="row border-bottom">
                <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
                    <div class="navbar-header">
                        <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i
                                class="fa fa-bars"></i> </a>
                        {{-- <form role="search" class="navbar-form-custom" action="search_results.html">
                            <div class="form-group">
                                <input type="text" placeholder="Search for something..." class="form-control"
                                    name="top-search" id="top-search">
                            </div> --}}
                        </form>
                    </div>
                    <ul class="nav navbar-top-links navbar-right">
                        @if(auth()->user()->hasRole('Normal'))
                        <li>
                            <a style="border: solid 5px;background-color: #f8ac59;" href="{{ route('suscriptions.index') }}" id="btnSendPayment">
                                PASATE A PREMIUM
                            </a>
                        </li>
                        @endif
                        {{-- <li class="dropdown">
                            <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                                <i class="fa fa-envelope"></i> <span class="label label-warning">16</span>
                            </a>
                            <ul class="dropdown-menu dropdown-messages">
                                <li>
                                    <div class="dropdown-messages-box">
                                        <a class="dropdown-item float-left" href="profile.html">
                                            <img alt="image" class="rounded-circle" src="{{asset('img/a7.jpg')}}">
                                        </a>
                                        <div>
                                            <small class="float-right">46h ago</small>
                                            <strong>Mike Loreipsum</strong> started following <strong>Monica
                                                Smith</strong>. <br>
                                            <small class="text-muted">3 days ago at 7:58 pm - 10.06.2014</small>
                                        </div>
                                    </div>
                                </li>
                                <li class="dropdown-divider"></li>
                                <li>
                                    <div class="dropdown-messages-box">
                                        <a class="dropdown-item float-left" href="profile.html">
                                            <img alt="image" class="rounded-circle" src="{{asset('img/a4.jpg')}}">
                                        </a>
                                        <div>
                                            <small class="float-right text-navy">5h ago</small>
                                            <strong>Chris Johnatan Overtunk</strong> started following <strong>Monica
                                                Smith</strong>. <br>
                                            <small class="text-muted">Yesterday 1:21 pm - 11.06.2014</small>
                                        </div>
                                    </div>
                                </li>
                                <li class="dropdown-divider"></li>
                                <li>
                                    <div class="dropdown-messages-box">
                                        <a class="dropdown-item float-left" href="profile.html">
                                            <img alt="image" class="rounded-circle" src="{{asset('img/profile.jpg')}}">
                                        </a>
                                        <div>
                                            <small class="float-right">23h ago</small>
                                            <strong>Monica Smith</strong> love <strong>Kim Smith</strong>. <br>
                                            <small class="text-muted">2 days ago at 2:30 am - 11.06.2014</small>
                                        </div>
                                    </div>
                                </li>
                                <li class="dropdown-divider"></li>
                                <li>
                                    <div class="text-center link-block">
                                        <a href="mailbox.html" class="dropdown-item">
                                            <i class="fa fa-envelope"></i> <strong>Read All Messages</strong>
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                                <i class="fa fa-bell"></i> <span class="label label-primary">8</span>
                            </a>
                            <ul class="dropdown-menu dropdown-alerts">
                                <li>
                                    <a href="mailbox.html" class="dropdown-item">
                                        <div>
                                            <i class="fa fa-envelope fa-fw"></i> You have 16 messages
                                            <span class="float-right text-muted small">4 minutes ago</span>
                                        </div>
                                    </a>
                                </li>
                                <li class="dropdown-divider"></li>
                                <li>
                                    <a href="profile.html" class="dropdown-item">
                                        <div>
                                            <i class="fa fa-twitter fa-fw"></i> 3 New Followers
                                            <span class="float-right text-muted small">12 minutes ago</span>
                                        </div>
                                    </a>
                                </li>
                                <li class="dropdown-divider"></li>
                                <li>
                                    <a href="grid_options.html" class="dropdown-item">
                                        <div>
                                            <i class="fa fa-upload fa-fw"></i> Server Rebooted
                                            <span class="float-right text-muted small">4 minutes ago</span>
                                        </div>
                                    </a>
                                </li>
                                <li class="dropdown-divider"></li>
                                <li>
                                    <div class="text-center link-block">
                                        <a href="notifications.html" class="dropdown-item">
                                            <strong>See All Alerts</strong>
                                            <i class="fa fa-angle-right"></i>
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        </li> --}}
                            <li class="dropdown show">
                                <a class="dropdown-toggle count-info" data-toggle="dropdown" aria-expanded="true">
                                    <i class="fa fa-bell"></i>  <span class="label label-warning">{{$items->count()}}</span>
                                </a>

                                @component('renders/itemWithoutCalification',['items' => $items])
                                @endcomponent
                            </li>
                        <li>
                            <a href="{{ route('public.home') }}">
                                <i class="fa fa-home"></i> Home
                            </a>
                        </li>
                        <li>

                            <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                                <i class="fa fa-sign-out"></i> Salir
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>

                        </li>

                    </ul>

                </nav>
            </div>
            <div id="app">

                    @yield('content')

            </div>
            {{-- <div class="footer">
                <div class="float-right">
                    10GB of <strong>250GB</strong> Free.
                </div>
                <div>
                    <strong>Copyright</strong> Example Company &copy; 2014-2018
                </div>
            </div> --}}
        </div>
    </div>

    <script src="{{ asset('admin/js/jquery-2.1.1.js')}}"></script>
    <script src="{{ asset('admin/js/popper.min.js')}}"></script>
    <script src="{{ asset('admin/js/bootstrap.js')}}"></script>
    <script src="{{asset('admin/js/plugins/sweetalert2/sweetalert2.all.js')}}"></script>
    <script src="{{asset('admin/js/plugins/axios/dist/axios.js')}}"></script>
    <script src="{{asset('admin/js/plugins/easyzoom/easyzoom.js')}}"></script>

    <!-- Custom and plugin javascript -->
    <script src="{{asset('admin/js/plugins/metisMenu/jquery.metisMenu.js')}}"></script>
    <script src="{{asset('admin/js/plugins/slimscroll/jquery.slimscroll.js')}}"></script>
    <script src="{{asset('admin/js/inspinia.js')}}"></script>
    <script src="{{asset('admin/js/plugins/pace/pace.min.js')}}"></script>
     <!-- Mapbox javascript -->
     <script src="{{ asset('admin/js/plugins/mapboxgl/mapbox-gl.js') }}" type="text/javascript"></script>
    {{-- <script  src="{{ asset('js/app.js') }}"></script> --}}
    <script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
        });
        var $easyzoom = $('.easyzoom').easyZoom();
		// Setup thumbnails example
		var api1 = $easyzoom.filter('.easyzoom--with-thumbnails').data('easyZoom');

		$('.thumbnails').on('click', 'a', function(e) {
			var $this = $(this);

			e.preventDefault();

			// Use EasyZoom's `swap` method
			api1.swap($this.data('standard'), $this.attr('href'));
		});
    </script>
    <script>
        $(document).on("wheel", "input[type=number]", function (e) {
            $(this).blur();
        });
    </script>
    @include('sweetalert::alert')
    @stack('scripts')
</body>
</html>
