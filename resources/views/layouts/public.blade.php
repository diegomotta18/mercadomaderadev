<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="pragma" content="no-cache" />
    <meta http-equiv="cache-control" content="max-age=604800" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>{{ config('app.name', 'Mercado Madera') }}</title>
    <link href="{{ asset('main/images/favicon.ico') }}" rel="shortcut icon" type="image/x-icon">
    <link href="{{ asset('main/css/bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@100;300&display=swap" rel="stylesheet">
    <link href="{{ asset('admin/font-awesome/css/font-awesome.css') }}" type="text/css" rel="stylesheet">

    <link href="{{ asset('main/css/ui.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('main/css/responsive.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('admin/css/plugins/sweetalert2/sweetalert2.css') }}" rel="stylesheet">
    <link href="{{ asset('main/css/zoom-box.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.css">
    <link href="{{ asset('admin/css/plugins/mapboxgl/mapbox-gl.css') }}" rel="stylesheet">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <style>

        html{
            scroll-behavior: smooth;
        }

        body{
            font-family: 'Raleway', sans-serif;
        }
        .dropdown-submenu {
            position: relative;
        }

        .dropdown-submenu>.dropdown-menu {
            top: 0;
            left: 100%;
            margin-top: -6px;
            margin-left: 0;
            border-radius: 0.25rem;
            padding: 0.0rem 0 !important;
        }

        .dropdown-submenu:hover>.dropdown-menu {
            display: block;
        }

        .dropdown-submenu>a::after {
            border-bottom: 0.3em solid transparent;
            border-left-color: inherit;
            border-left-style: solid;
            border-left-width: 0.3em;
            border-top: 0.3em solid transparent;
            content: " ";
            display: block;
            float: right;
            height: 0;
            margin-right: 0.4em;
            margin-top: 0.4em !important;
            width: 0;
        }

        .dropdown-submenu.pull-left {
            float: none;
        }

        .dropdown-submenu.pull-left>.dropdown-menu {
            left: -75%;
        }

        .dropdown-menu .divider {
            background-color: #e5e5e5;
            height: 1px;
            margin: 9px 0;
            overflow: hidden;
        }
        .vs--loading .vs__spinner {
            opacity: 1;
        }

        .item-menu-fix{
            font-size: 1.25rem ;
            font-weight: bold;
        }
    </style>
    @stack('styles')
</head>
<body>
    @include('layouts.public.header')
    @yield('content')
    @include('layouts.public.footer')
    <script src="{{ asset('main/js/jquery-2.0.0.min.js') }}" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.js"></script>
    <script src="{{ asset('main/js/bootstrap.bundle.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('main/js/zoom-box.js') }}" type="text/javascript"></script>
    <script src="{{asset('admin/js/plugins/sweetalert2/sweetalert2.all.js')}}"></script>
    <script src="{{ asset('admin/js/plugins/mapboxgl/mapbox-gl.js') }}" type="text/javascript"></script>
    {{-- <script src="{{ asset('js/app.js') }}"></script> --}}
    <script>
        $(function() {
            $('[data-toggle="tooltip"]').tooltip();
        })

    </script>
    @include('sweetalert::alert')
    @routes
    @stack('scripts')
</body>
</html>
