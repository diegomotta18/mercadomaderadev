
<li class="{{ (request()->is('profiles/*')) ? 'active' : '' }}">
        <a href="#"><i class="fa fa-user-circle-o"></i> <span class="nav-label">Perfil</span>
        <span class="fa arrow"></span></a>
        <ul class="nav nav-second-level">
            <li class="{{ (request()->is('profiles/user')) ? 'active' : '' }}"><a href="{{route('profile.user')}}"><span class="fa fa-user"></span> Mi perfil de usuario</a></li>

        </ul>
        <ul class="nav nav-second-level">
            <li class="{{ (request()->is('plans/me')) ? 'active' : '' }}"><a href="{{route('plans.me')}}"><span class="fa fa-id-card"></span> Plan contratado</a></li>
        </ul>
    </li>
<li class="{{ (request()->is('simples.index')) ? 'active' : '' }}">
    <a href="{{route('simples.index')}}"><i class="fa fa-cubes"></i> <span class="nav-label">Mis
            Publicaciones</span></a>
</li>

<li class="{{ (request()->is('/simples/categories')) ? 'active' : '' }}"><a
        href="{{ route('simples.categories')}}"><span class="fa fa-cube"></span> Nueva publicación</a><span>
</li>
