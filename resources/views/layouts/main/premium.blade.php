
<li class="{{ (request()->is('profiles/*')) ? 'active' : '' }}">
    <a href="#"><i class="fa fa-user-circle-o"></i> <span class="nav-label">Perfil</span>
    <span class="fa arrow"></span></a>
    <ul class="nav nav-second-level">
        <li class="{{ (request()->is('profiles/user')) ? 'active' : '' }}"><a href="{{route('profile.user')}}"><span class="fa fa-user"></span> Mi perfil de usuario</a></li>

    </ul>
    <ul class="nav nav-second-level">
        <li class="{{ (request()->is('profiles/company')) ? 'active' : '' }}"><a href="{{route('profile.company')}}"><span class="fa fa-bank"></span> Mi perfil de empresa</a></li>
    </ul>
    <ul class="nav nav-second-level">
        <li class="{{ (request()->is('plans/me')) ? 'active' : '' }}"><a href="{{route('plans.me')}}"><span class="fa fa-id-card"></span> Plan contratado</a></li>
    </ul>
</li>
@can('Crear.comisionista')
    <li class="{{ (request()->is('comisionistAgents*')) ? 'active' : '' }}">
        <a  href="{{route('comisionistAgents.index')}}"><i class="fa fa-users"></i> <span class="nav-label">Comisionistas</span></a>
    </li>
@endcan
@can('Crear.usuario.comisionista')
    <li class="{{ (request()->is('comisionistAgentUsers*')) ? 'active' : '' }}">
        <a  href="{{route('comisionistAgentUsers.index')}}"><i class="fa fa-user-plus"></i> <span class="nav-label">Usuarios</span></a>
    </li>
@endcan

@can('Crear.oferta')
    <li class="{{ (request()->is('offerts*')) ? 'active' : '' }}">
        <a href="{{route('offerts.publications.categories')}}"><span class="fa fa-cube"></span> Nueva Oferta</a>
    </li>
@endcan
@can('Crear.demanda')
    <li class="{{ (request()->is('demands*')) ? 'active' : '' }}">
        <a href="{{route('demands.publications.categories')}}"><span class="fa fa-cube"></span> Nueva Demanda</a>
    </li>
@endcan
<li class="{{ (request()->is('publications*')) ? 'active' : '' }}">
    <a href="{{route('publications.index')}}"><span class="fa fa-list"></span> Publicaciones</a>
</li>
@can('Crear.comentario')


    <li class="{{ request()->is('comments/*') ? 'active' : '' }}">
        <a href="#"><i class="fa fa-comments"></i> <span class="nav-label">Comentarios <span class="badge badge-success">{{ count(
            App\Comment::with(['user', 'owner'])->where('owner_id', auth()->user()->id)->whereNull('subcomment')->get(),
        ) }}</span>
          <span class="fa arrow"></span></span>
        </a>

        <ul class="nav nav-second-level">
            <li class="{{ request()->is('profiles/company') ? 'active' : '' }}">
            <a href="{{ route('comments.index') }}"><span class="fa fa-comments"></span> Comentarios</a>
            </li>
        </ul>
        <ul class="nav nav-second-level">
            <li class="{{ request()->is('profiles/company') ? 'active' : '' }}">
            <a href="{{ route('comments.replies') }}"><span class="fa fa-comments"></span> Repuestas</a>
            </li>
        </ul>
    </li>
@endcan