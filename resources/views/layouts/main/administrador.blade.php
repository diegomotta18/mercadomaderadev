<li class="{{ request()->is('profiles/*') ? 'active' : '' }}">
    <a href="#"><i class="fa fa-user-circle-o"></i> <span class="nav-label">Perfil</span>
        <span class="fa arrow"></span></a>
    <ul class="nav nav-second-level">
        <li class="{{ request()->is('profiles/user') ? 'active' : '' }}"><a href="{{ route('profile.user') }}"><span
                    class="fa fa-user"></span> Mi perfil de usuario</a></li>

    </ul>
    <ul class="nav nav-second-level">
        <li class="{{ request()->is('profiles/company') ? 'active' : '' }}"><a
                href="{{ route('profile.company') }}"><span class="fa fa-bank"></span> Mi perfil de empresa</a></li>
    </ul>
</li>
<li class="{{ request()->is('parameters/*') ? 'active' : '' }}">
    <a href="#"><i class="fa fa-cogs"></i> <span class="nav-label">Parámetros</span>
        <span class="fa arrow"></span></a>
    <ul class="nav nav-second-level">
        <li class="{{ request()->is('parameters/categories*') ? 'active' : '' }}"><a
                href="{{ route('categories.index') }}"><span class="fa fa-list"></span> Categorias</a></li>
    </ul>
    <ul class="nav nav-second-level">
        <li class="{{ request()->is('parameters/typewoods*') ? 'active' : '' }}"><a
                href="{{ route('typewoods.index') }}"><span class="fa fa-list"></span> Tipos de maderas</a></li>
    </ul>
    <ul class="nav nav-second-level">
        <li class="{{ request()->is('parameters/typeproducts*') ? 'active' : '' }}"><a
                href="{{ route('typeproducts.index') }}"><span class="fa fa-list"></span> Tipos de productos</a></li>
    </ul>
</li>

@if (auth()
        ->user()
        ->can('Crear.usuario'))
    <li class="{{ request()->is('admin/*') ? 'active' : '' }}">
        <a href="#"><i class="fa fa-th-large"></i> <span class="nav-label">Seguridad</span>
            <span class="fa arrow"></span></a>
        <ul class="nav nav-second-level">
            @can('Crear.usuario')
                <li class="{{ request()->is('admin/users*') ? 'active' : '' }}"><a href="{{ route('users.index') }}"><span
                            class="fa fa-user-plus"></span> Usuarios</a></li>
            @endcan
            @can('Crear.rol')
                <li class="{{ request()->is('admin/roles*') ? 'active' : '' }}"><a href="{{ route('roles.index') }}"><span
                            class="fa fa-id-card"></span> Roles</a></li>
            @endcan
        </ul>
    </li>
    @can('Crear.persona')
        <li class="{{ request()->is('persons*') ? 'active' : '' }}">
            <a href="{{ route('persons.index') }}"><i class="fa fa-users"></i> <span class="nav-label">Personas</span></a>
        </li>
    @endcan
    @can('Crear.oferta')
        <li class="{{ request()->is('offerts*') ? 'active' : '' }}">
            <a href="{{ route('offerts.publications.categories') }}"><span class="fa fa-cube"></span> Nueva Oferta</a>
        </li>
    @endcan
    @can('Crear.demanda')
        <li class="{{ request()->is('demands*') ? 'active' : '' }}">
            <a href="{{ route('demands.publications.categories') }}"><span class="fa fa-cube"></span> Nueva Demanda</a>
        </li>
    @endcan
    <li class="{{ request()->is('publications*') ? 'active' : '' }}">
        <a href="{{ route('publications.index') }}"><span class="fa fa-list"></span> Publicaciones</a>
    </li>
@endif
@can('Crear.comentario')


    <li class="{{ request()->is('comments/*') ? 'active' : '' }}">
        <a href="#"><i class="fa fa-comments"></i> <span class="nav-label">Comentarios <span class="badge badge-success">{{ count(
            App\Comment::with(['user', 'owner'])->where('owner_id', auth()->user()->id)->whereNull('subcomment')->get(),
        ) }}</span>
          <span class="fa arrow"></span></span>
        </a>

        <ul class="nav nav-second-level">
            <li class="{{ request()->is('profiles/company') ? 'active' : '' }}">
            <a href="{{ route('comments.index') }}"><span class="fa fa-comments"></span> Comentarios</a>
            </li>
        </ul>
        <ul class="nav nav-second-level">
            <li class="{{ request()->is('profiles/company') ? 'active' : '' }}">
            <a href="{{ route('comments.replies') }}"><span class="fa fa-comments"></span> Repuestas</a>
            </li>
        </ul>
    </li>
@endcan
