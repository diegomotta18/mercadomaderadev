<header class="section-header">

    <section class="header-main border-bottom">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-xl-2 col-lg-3 col-md-12">
                    <a href="{{ route('public.home') }}" class="brand-wrap">
                        <h3 class="text-center">Mercado Madera</h3>
{{--                        <img class="logo" src="{{ asset('main/images/logo.png') }}">--}}
                    </a>
                </div>
                <div class="col-xl-6 col-lg-5 col-md-6">
                    <form role="form" method="GET" action="{{ route('filter.byProducts') }}" class="search-header">
                        <div class="input-group w-100">
                            <form action="">
                                <input type="text" class="form-control" id="title" name="title" value="{{request()->input('title')}}" placeholder="Buscar">
                                <div class="input-group-append">
                                    <button class="btn btn-primary" id="title_filter_btn" type="submit">
                                        <i class="fa fa-search"></i> Buscar
                                    </button>
                                </div>
                            </form>

                        </div>
                    </form>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6">
                    <div class="widgets-wrap float-md-right">

                        <div class="widget-header mr-3">
                            @if (!auth()->check())
                                <a href="{{ route('login') }}" class="widget-view">
                                    <div class="icon-area">
                                        <i class="fa fa-sign-in"></i>
                                    </div>
                                    <small class="text"> Ingreso
                                    </small>
                                </a>
                            @else
                                <a href="{{ route('home') }}" class="widget-view">
                                    <div class="icon-area">
                                        <i class="fa fa-sign-in"></i>
                                    </div>
                                    <small class="text"> Mi sesión
                                    </small>
                                </a>
                            @endif
                        </div>
                        @if (!auth()->check())
                            <div class="widget-header mr-3">
                                <a href="{{ route('register') }}" class="widget-view">
                                    <div class="icon-area">
                                        <i class="fa fa-user-plus"></i>
                                    </div>
                                    <small class="text"> Registrarme </small>
                                </a>
                            </div>
                        @else
                            <div class="widget-header mr-3">

                                <a href="{{ route('logout') }}" class="widget-view" onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                                    <div class="icon-area">
                                        <i class="fa fa-window-close"></i>
                                    </div>
                                    <small class="text"> Salir </small>
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                    style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
    <nav class="navbar navbar-main navbar-expand-lg border-bottom">
        <div class="container">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main_nav"
                aria-controls="main_nav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="main_nav">

                <ul class="navbar-nav  nav-fill w-100">
                    @component('renders/categories-nav',['categories' => $categories])
                    @endcomponent
                    <li class="nav-item">
                        <a class="nav-link item-menu-fix" href="{{ route('offerts.index') }}">Ofertas</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link item-menu-fix" href="{{ route('demands.index') }}">Demandas</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link item-menu-fix" href="{{ route('auctions.index') }}">Subastas</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link item-menu-fix" href="{{ route('ours-plans') }}">Planes</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

</header>
