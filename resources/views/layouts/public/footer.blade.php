@if (!auth()->check())
<section class="section-subscribe padding-y-lg">
    <div class="container">
        <p class="pb-2 text-center text-white">Buscar, vender y recibir actualizaciones de los últimos productos de la industria directamente en tu correo electrónico</p>
        <div class="row justify-content-md-center">
            <div class="col-lg-5 col-md-6">
                <div class="form-row d-flex justify-content-center">
                        <a href="{{ route('register') }}" type="button" class="btn btn-warning"> <i class="fa fa-envelope"></i>
                            Registrarme </a>
                            <small class="form-text text-white-50">
                                Nunca compartiremos su dirección de correo electrónico con un tercero.
                            </small>
                        </div>

            </div>
        </div>
</section>
@endif
<footer class="section-footer bg-secondary">
    <div class="container">
        <section class="footer-top padding-y-lg text-white">
            <div class="row">
                <!--<aside class="col-md col-6">
                    <h6 class="title">Empresas</h6>
                    <ul class="list-unstyled">
                        <li> <a href="#">Adidas</a></li>
                        <li> <a href="#">Puma</a></li>
                        <li> <a href="#">Reebok</a></li>
                        <li> <a href="#">Nike</a></li>
                    </ul>
                </aside>-->
                <aside class="col-md col-6">
                    <h6 class="title">Mercado Madera</h6>
                    <ul class="list-unstyled">
                        <li> <a href="#">Sobre nosotros</a></li>
                        <li> <a href="#">Contactanos</a></li>
                        <li> <a href="#">Servicios</a></li>
                        <li> <a href="#">Anuncie con nostros</a></li>

                    </ul>
                </aside>

                <aside class="col-md col-6">
                    <h6 class="title">Cuenta</h6>
                    <ul class="list-unstyled">
                        <li> <a href="{{ route('login') }}"> Ingreso </a></li>
                        <li> <a href="{{ route('register') }}"> Registrarme</a></li>
                        <li> <a href="{{ route('home') }}"> Mi sesión</a></li>
                        <li> <a href="{{ route('publications.index') }}"> Mis publicaciones </a></li>
                        <li> <a href="{{ route('offerts.publications.categories') }}"> Publicar ofertas </a></li>
                        <li> <a href="{{ route('demands.publications.categories') }}"> Publicar demadas </a></li>
                        <li> <a href="{{ route('offerts.index') }}"> Ofertas </a></li>
                        <li> <a href="{{ route('demands.index') }}"> Demandas </a></li>
                        <li> <a href="{{ route('auctions.index') }}"> Subastas </a></li>

                    </ul>
                </aside>
                <aside class="col-md col-6">
                    <h6 class="title">Seguridad y soporte</h6>
                    <ul class="list-unstyled">
                        <li> <a href="#"> Condiciones generales </a></li>
                        <li> <a href="#"> Politicas de cookies</a></li>
                        <li> <a href="#"> Declaración de Privacidad de Mercado Madera</a></li>
                        <li> <a href="#"> Preguntas frecuentas </a></li>
                        <li> <a href="#"> Guia del usuario </a></li>

                    </ul>
                </aside>
                <aside class="col-md">
                    <h6 class="title">Redes sociales</h6>
                    <ul class="list-unstyled">
                        <li><a href="#"> <i class="fab fa-facebook"></i> Facebook </a></li>
                        <li><a href="#"> <i class="fab fa-twitter"></i> Twitter </a></li>
                        <li><a href="#"> <i class="fab fa-instagram"></i> Instagram </a></li>
                        <li><a href="#"> <i class="fab fa-youtube"></i> Youtube </a></li>
                    </ul>
                </aside>
            </div>
        </section>
        <section class="footer-bottom text-center">
            <p class="text-white"> Política de privacidad - Condiciones de uso - Información del usuario Guía de consultas legales</p>
            <p class="text-muted"> &copy @current Mercado Madera, todos los derechos reservados. </p>
            <br>
        </section>
    </div>
</footer>
