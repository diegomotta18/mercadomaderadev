<div class="row">
    <div class="col-sm-12">
        <div class="form-group">
            <label>Titulo</label>
            <input type="text" placeholder="Titulo" name="title" class="form-control"
                value="{{isset($product) ? $product->title : old('title')}}">
        </div>
    </div>
    <div class="col-sm-12">
        <div class="form-group">
            <label>Descripción</label>
            <textarea id="summernote" class="summernote form-control"
                name="description">{{isset($product) ? $product->description : old('description')}}</textarea>
        </div>
    </div>

</div>
@hasanyrole('ROOT|Administrador|Premium')
<p> Puedo ver esta seccion porque soy un usuario {{auth()->user()->getRoleNames()}}</p>
@endhasanyrole
<div class="form-group">
    <button class="btn btn-sm btn-primary float-right m-t-n-xs" type="submit"><i
            class="fa fa-save"></i> Guardar</button>
</div>