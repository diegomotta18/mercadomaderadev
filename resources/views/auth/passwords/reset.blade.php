@extends('layouts.authoritation')

@section('content')
<div class="loginColumns animated fadeInDown">
    <div class="row">
        <div class="col-md-6">
            <h2 class="font-bold" style="text-align: center;">Restablecimiento de contraseña</h2>
            <div class="text-center">
                <div style="margin-top: 20px">
                    <i class="fa fa-unlock-alt" style="font-size: 180px;color: #e5e5e5 "></i>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="ibox-content">
                <form method="POST" action="{{ route('password.update') }}">
                    @csrf
                    <p>
                        Escriba su dirección de correo electrónico registrado en el espacio a continuación y
                        la nueva contraseña
                    </p>
                    <input type="hidden" name="token" value="{{ $token }}">

                    <div class="form-group">
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                            name="email" placeholder="E-mail" value="{{ $email ?? old('email') }}" required
                            autocomplete="email" autofocus>

                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <input id="password" type="password" placeholder="Contraseña"
                            class="form-control @error('password')  is-invalid @enderror" name="password" required
                            autocomplete="new-password">

                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror

                    </div>

                    <div class="form-group ">
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation"
                            placeholder="Confirmar contraseña" required autocomplete="new-password">
                    </div>


                    <button type="submit" class="btn btn-primary block full-width m-b">
                        Restablecer contraseña {{-- {{ __('Reset Password') }} --}}
                    </button>

            </div>
            </form>
        </div>
    </div>
    <hr />
    <div class="row">
        <div class="col-md-6">
            Copyright Mercado Madera
        </div>
        <div class="col-md-6 text-right">
            <small>© {{now()->year}}</small>
        </div>
    </div>
</div>

@endsection