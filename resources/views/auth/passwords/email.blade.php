@extends('layouts.authoritation')

@section('content')
<div class="loginColumns animated fadeInDown">
    <div class="row">
        <div class="col-md-6">
            <h2 class="font-bold" style="text-align: center;">Solicitud para restablecer la contraseña</h2>
            <div class="text-center">
                <div style="margin-top: 20px">
                    <i class="fa fa-unlock" style="font-size: 180px;color: #e5e5e5 "></i>
                </div>
            </div>
        </div>
        
        <div class="col-md-6">
            <p>&nbsp;</p>
            <div class="ibox-content">

                @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
                @endif
                <p style="text-align:justify;">
                    Escriba su dirección de correo electrónico registrado en el espacio a continuación y le
                    enviaremos las instrucciones de restablecimiento de contraseña
                </p>

            
            <form class="m-t" method="POST" action="{{ route('password.email') }}">
                @csrf

                <div class="form-group">
                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                        name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                    @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror

                </div>

                <button type="submit" class="btn btn-primary block full-width m-b">
                    Enviar solicitud de restablecimiento de contraseña{{-- {{ __('Send Password Reset Link') }} --}}
                </button>
                <p>&nbsp;</p>

            </form>
        </div>
        </div>
    </div>
    <hr />
    <div class="row">
        <div class="col-md-6">
            Copyright Mercado Madera
        </div>
        <div class="col-md-6 text-right">
            <small>© {{now()->year}}</small>
        </div>
    </div>
</div>

@endsection