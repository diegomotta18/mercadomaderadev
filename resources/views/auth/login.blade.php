@extends('layouts.authoritation')

@section('content')
<div class="loginColumns animated fadeInDown">
    <div class="row">
        <div class="col-md-6">
            <h2 class="font-bold">Bievenido a Mercado Madera</h2>
            <div class="text-center">
                <div style="margin-top: 20px">
                    <i class="fa fa-unlock-alt" style="font-size: 180px;color: #e5e5e5 "></i>
                </div>
            </div>
            {{-- <p>
                Perfectly designed and precisely prepared admin theme with over 50 pages with extra new web app views.
            </p>

            <p>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the
                industry's standard dummy text ever since the 1500s.
            </p>

            <p>
                When an unknown printer took a galley of type and scrambled it to make a type specimen book.
            </p>

            <p>
                <small>It has survived not only five centuries, but also the leap into electronic typesetting, remaining
                    essentially unchanged.</small>
            </p> --}}

        </div>
        <div class="col-md-6">
            <div class="ibox-content">
                <form class="m-t" method="POST" action="{{ route('login') }}">
                    @csrf
                    <div class="form-group">
                        <input id="email" placeholder="Email/Nombre de usuario/Cuil" type="email"
                            class="form-control @error('email') is-invalid @enderror" name="email"
                            value="{{ old('email') }}" required autocomplete="email" autofocus>
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input id="password" placeholder="Contraseña" type="password"
                            class="form-control @error('password') is-invalid @enderror" name="password" required
                            autocomplete="current-password">

                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="remember" id="remember"
                                    {{ old('remember') ? 'checked' : '' }}>

                                <label class="form-check-label" for="remember">
                                    {{-- {{ __('Remember Me') }} --}}
                                    Recordarme
                                </label>
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary block full-width m-b">
                        {{ __('Login') }}
                    </button>

                    @if (Route::has('password.request'))
                    <a href="{{ route('password.request') }}">
                        {{-- <small> {{ __('Forgot Your Password?') }}</small> --}}
                        <small> ¿Olvido la contraseña?</small>
                    </a>
                    @endif

                    <p class="text-muted text-center">
                        <small>¿No tienes una cuenta?</small>
                    </p>
                    <a class="btn btn-sm btn-white btn-block" href="{{ route('register') }}">Registrar una cuenta</a>
                </form>
                {{-- <p class="m-t">
                    <small>Inspinia we app framework base on Bootstrap 3 &copy; 2014</small>
                </p> --}}
            </div>
        </div>
    </div>
    <hr />
    <div class="row">
        <div class="col-md-6">
            Copyright Mercado Madera
        </div>
        <div class="col-md-6 text-right">
            <small>© {{now()->year}}</small>
        </div>
    </div>
</div>

@endsection