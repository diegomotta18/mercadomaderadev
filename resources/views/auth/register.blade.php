@extends('layouts.authoritation')

@push('styles')
<link href="{{asset ('admin/css/plugins/steps/jquery.steps.css')}}" rel="stylesheet">
<link href="{{asset('admin/css/plugins/select2/select2.min.css')}}" rel="stylesheet">
<link href="{{asset('admin/css/plugins/select2/select2-bootstrap4.min.css')}}" rel="stylesheet">
<link href="{{asset('admin/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css')}}" rel="stylesheet">
<style>
    .invalidfeedback {
        /* display: none; */
        width: 100%;
        margin-top: .25rem;
        font-size: 80%;
        color: #dc3545;
    }
</style>
@endpush
@section('content')

<div class="row">
    <div class="col-lg-12">
        <div class="ibox">
            <div class="ibox-title">
                <h5>Registro <small></small></h5>
            </div>
            <div class="ibox-content">

                <div class="alert alert-info">
                    <p><i class="fa fa-info"></i> Para completar el registro, es necesario completar los formularios de <b>Cuenta</b>, <b>Perfil</b>, <b>Empresa</b>, <b>Aceptación de términos y condiciones</b>. Para avanzar en cada formulario se realiza click en el botón <b>Siguiente</b>, en caso de ser necesario y volver a un formulario anterior click en el boton <b>Atrás</b>. Estos botones se encuentra en la parte inferior (abajo) de esta página.</p>
                </div>

                <form method="POST" action="{{ route('register') }}" id="register" role="form" class="wizard-big">
                    {{--   --}}
                    @csrf
                    <h1>Cuenta</h1>
                    <section>
                        <div class="ibox-content">
                            <h2>Información de la cuenta</h2>

                            @if ($errors->any())
                            <div class="alert alert-danger">
                                <i class="fa fa-info"></i> <b>Revisar el formulario para corregir los campos faltantes o
                                    erroneos.</b>
                                <br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif

                            <div class="row">
                                <div class="col-lg-8">
                                    <div class="form-group">
                                        <label>Nombre de usuario*</label>
                                        <input id="nameUser" name="nameUser" type="text"
                                            class="form-control @error('userName') is-invalid @enderror" autofocus
                                            required autocomplete="userName" autofocus value="{{ old('nameUser')}}">
                                        @error('nameUser')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label>Email*</label>
                                        <input id="email" name="email" type="email"
                                            class="form-control @error('email') is-invalid @enderror" autofocus required
                                            value="{{ old('email')}}">
                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label>Contraseña *</label>
                                        <input name="password" type="password"
                                            class="form-control  @error('password') is-invalid @enderror" autofocus
                                            required value="{{ old('password')}}">
                                        @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label>Confirmar contraseña *</label>
                                        <input name="password_confirmation" type="password"
                                            class="form-control @error('password_confirmation') is-invalid @enderror"
                                            value="{{ old('password_confirmation')}}" autofocus required>
                                        @error('password_confirmation')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="text-center">
                                        <div style="margin-top: 20px">
                                            <i class="fa fa-sign-in" style="font-size: 180px;color: #e5e5e5 "></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <h1>Perfil</h1>
                    <section>
                        <div class="ibox-content">
                            <h2>Información de perfil</h2>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Nombre</label>
                                        <input type="text" placeholder="Nombre" name="name"
                                            class="form-control @error('name') is-invalid @enderror" autofocus required
                                            value="{{old('name')}}">
                                        @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Apellido</label>
                                        <input type="text" placeholder="Apellido" name="surname"
                                            class="form-control @error('surname') is-invalid @enderror"
                                            value="{{old('surname')}}" autofocus required>
                                        @error('surname')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                <input type="hidden" name="nameComplete" class="form-control" value="">

                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>DNI</label>
                                        <input type="number" placeholder="DNI" name="dni"
                                            class="form-control @error('dni') is-invalid @enderror"
                                            value="{{ old('dni')}}" autofocus required>
                                        @error('dni')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>CUIL</label>
                                        <input type="number" placeholder="CUIL" name="cuil"
                                            class="form-control @error('cuil') is-invalid @enderror"
                                            value="{{old('cuil')}}" autofocus required>
                                        @error('cuil')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Fecha de nacimiento</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input
                                                placeholder="Fecha de nacimiento" id="burndate" name="burndate"
                                                type="text" class="form-control" value="{{old('burndate')}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <h2>Datos de contacto</h2>
                            <div class="row">
                                <!--<div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-envelope"></i></span><input
                                                placeholder="Email" id="emailContact" name="emailContact" type="email"
                                                class="form-control" value="{{ old('email')}}">
                                        </div>
                                    </div>
                                </div>-->
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Telefono</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-phone"></i></span><input
                                                placeholder="Teléfono" id="telphone" name="telphone" type="number"
                                                class="form-control" value="{{old('telphone')}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Celular/Whatsapp</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-whatsapp"></i></span><input
                                                placeholder="Celular/Whatsapp" id="celphone" name="celphone" type="number"
                                                class="form-control" value="{{ old('celphone')}}">
                                        </div>

                                    </div>

                                </div>

                            </div>
                            {{-- <h2>Datos de dirección</h2>
                            <div class="row">
                                <div class="col-sm-3">
                                    <label class="control-label">País</label>
                                    <select class="form-control" name="country" id="country">
                                        <option value=""> -- Seleccionar país --</option>
                                        @foreach ($countries as $country)
                                        <option value="{{$country->id}}"
                                            {{ old('country') == 1 ? 'selected' : '' }}>
                                            {{$country->name}}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col-sm-3">
                                    <label class="control-label">Provincia</label>
                                    <select class="form-control" name="province" id="province">
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <label class="control-label">Departamento</label>
                                    <select class="form-control" name="city" id="city">
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <label class="control-label">Localidad</label>
                                    <select class="form-control" name="town" id="town">
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="control-label">Calle</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-home"></i></span><input
                                                placeholder="Calle" id="address" name="address" type="text"
                                        class="form-control" value="{{old('address')}}" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="control-label">Altura</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-home"></i></span><input
                                                placeholder="Altura" id="number" name="number" type="number"
                                                class="form-control" value="{{old('number')}}"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="control-label">Dpto</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-home"></i></span><input
                                                placeholder="Dpto" id="dpto" name="dpto" type="text"
                                                class="form-control" value="{{old('dpto')}}"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="control-label">Piso</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-home"></i></span><input
                                                placeholder="Piso" id="floor" name="floor" type="text"
                                                class="form-control" value="{{old('floor')}}"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> --}}
                    </section>

                    <h1>Empresa</h1>
                    <section>
                        <div class="ibox-content">
                            <h2>Información de empresa</h2>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Nombre</label>
                                        <input type="text" placeholder="Nombre" name="nameCompany" id="nameCompany"
                                            class="form-control @error('nameCompany') is-invalid @enderror"
                                            value="{{old('nameCompany')}}" autofocus required>
                                        @error('nameCompany')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Cuit</label>
                                        <input type="number" placeholder="Cuit" name="cuit"
                                            class="form-control @error('cuit') is-invalid @enderror"
                                            value="{{old('cuit')}}" autofocus required>
                                        @error('cuit')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            {{--<h2>Datos de contacto de la empresa</h2>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Email empresarial</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-envelope"></i></span><input
                                                placeholder="Email" id="emailCompany" name="emailCompany" type="email"
                                                class="form-control" value="{{old('emailCompany')}}">
                                        </div>

                                    </div>

                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                                       <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Celular/Whatsapp</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-whatsapp"></i></span><input
                                                placeholder="Celular/Whatsapp" id="celphoneCompany" name="celphoneCompany"
                                                type="number" class="form-control" value="{{old('celphoneCompany')}}">
                                        </div>

                                    </div>

                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Telefono</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-phone"></i></span><input
                                                placeholder="number" id="telphoneCompany" name="telphoneCompany"
                                                type="text" class="form-control" value="{{ old('telphoneCompany')}}">
                                        </div>
                                    </div>
                                </div>
                            </div>              <label>Sitio web</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-feed"></i></span><input
                                                placeholder="Sitio web" id="web" name="web" type="text"
                                                class="form-control"
                                                value="{{isset($person->contact) ? $person->contact->web : old('web')}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
 --}}
                            <h2>Datos de dirección de la empresa</h2>
                            <div class="row">
                                <div class="col-sm-3">
                                    <label class="control-label">Paise</label>
                                    <select class="form-control  m-b" name="countryCompany" id="countryCompany">
                                        <option value=""> -- Seleccionar país --</option>
                                        @foreach ($countries as $country)
                                        <option value="{{$country->id}}"
                                            {{ old('countryCompany') == 1 ? 'selected' : '' }}>
                                            {{$country->name}}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col-sm-3">
                                    <label class="control-label">Provincia</label>
                                    <select class="form-control" name="provinceCompany" id="provinceCompany" >
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <label class="control-label">Departamento</label>
                                    <select class="form-control" name="cityCompany" id="cityCompany">
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <label class="control-label">Localidad</label>
                                    <select class="form-control" name="townCompany" id="townCompany">
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="control-label">Calle</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-home"></i></span><input
                                                placeholder="Calle" id="address" name="companyaddress" type="text"
                                                class="form-control" value="{{ old('companyaddress')}}"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="control-label">Altura</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-home"></i></span><input
                                                placeholder="Altura" id="number" name="companynumber" type="number"
                                                class="form-control" value="{{ old('companynumber')}}"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="control-label">Dpto</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-home"></i></span><input
                                                placeholder="Dpto" id="dpto" name="companydpto" type="text"
                                                class="form-control" value="{{ old('companydpto')}}"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="control-label">Piso</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-home"></i></span><input
                                                placeholder="Piso" id="floor" name="companyfloor" type="text"
                                                class="form-control" value="{{ old('companyfloor')}}"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>

                    <h1>Aceptación de términos y condiciones</h1>
                    <section>
                        <div class="ibox-content">
                            <h2>Aceptación de términos y condiciones</h2>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">

                                        <input name="aceptarTerminosYCondiciones" type="checkbox" value="accept"
                                            {{(old('aceptarTerminosYCondiciones') == "accept") ? 'checked': ''}}
                                            @error('aceptarTerminosYCondiciones') is-invalid @enderror>
                                        <label for="aceptarTerminosYCondiciones">Acepto los términos y
                                            condiciones</label>

                                        @if ($errors->has('aceptarTerminosYCondiciones'))
                                        <br>
                                        <span class="invalidfeedback" role="alert">
                                            <strong>{{ $errors->first('aceptarTerminosYCondiciones') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        &nbsp;
                                    </div>
                                    <div class="form-group">
                                        &nbsp;
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </form>
            </div>
        </div>
    </div>

</div>
</div>


@endsection
@routes
@push('scripts')


<!-- Steps -->
<script src="{{asset('admin/js/plugins/steps/jquery.steps.min.js')}}"></script>
<script src="{{asset('admin/js/plugins/select2/select2.full.min.js')}}"></script>
<!-- Jquery Validate -->
<script src="{{asset('admin/js/plugins/validate/jquery.validate.min.js')}}"></script>
<script src="{{asset('admin/js/plugins/moment/moment.js')}}"></script>
<script src="{{asset('admin/js/plugins/moment/es.js')}}"></script>
<script src="{{asset('admin/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js')}}"></script>
<script>
    $(document).ready(function(){
        $("#register").steps({
            labels:{
                finish: "Finalizar",
                next: "Siguiente <i class='fa fa-arrow-right'></i>",
                previous: "<i class='fa fa-arrow-left'></i> Atras ",
            },
            bodyTag: "section",
            onStepChanging: function (event, currentIndex, newIndex)
            {

                // Always allow going backward even if the current step contains invalid fields!
                if (currentIndex > newIndex)
                {
                    return true;
                }

                var form = $(this);

                // Clean up if user went backward before
                if (currentIndex < newIndex)
                {
                    // To remove error styles
                    $(".body:eq(" + newIndex + ") label.error", form).remove();
                    $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                }

                // Disable validation on fields that are disabled or hidden.
                form.validate().settings.ignore = ":disabled,:hidden";

                // Start validation; Prevent going forward if false
                return form.valid();
            },
            onStepChanged: function (event, currentIndex, newIndex)
            {

                if (currentIndex > newIndex)
                {
                    return true;
                }

                var form = $(this);

                // Clean up if user went backward before
                if (currentIndex < newIndex)
                {
                    // To remove error styles
                    $(".body:eq(" + newIndex + ") label.error", form).remove();
                    $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                }

                // Disable validation on fields that are disabled or hidden.
                form.validate().settings.ignore = ":disabled,:hidden";

                // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                if (currentIndex === 2 && priorIndex === 3)
                {
                    $(this).steps("previous");
                }
                // Start validation; Prevent going forward if false
                return form.valid();

            },
            onStepChanged: function (event, currentIndex, newIndex)
            {
                $('#burndate').datetimepicker({
                inline: false,
                locale: 'es',
                format: "L",
                });
                $("#country").select2({
                    theme: 'bootstrap4',
                });
                $("#province").select2({
                    theme: 'bootstrap4',
                });
                $("#city").select2({
                    theme: 'bootstrap4',
                });
                $("#town").select2({
                    theme: 'bootstrap4',
                });

                var country_id = "{{ old('country') }}";
                var province_id = "{{ old('province')}}";
                var city_id = "{{ old('city') }}";
                var town_id = "{{ old('town') }}";

                if(country_id!== null){
                    $.ajax({
                        type:"get",
                        url: route('provinces', {id: country_id}),
                        success:function(res)
                        {
                            if(res)
                            {
                                $("#province").empty();
                                $("#city").empty();
                                $("#town").empty();
                                $("#province").append('<option value="">-- Seleccionar provincia --</option>');
                                $.each(res,function(key,value){
                                    $("#province").append('<option value="'+value.id+'">'+value.name+'</option>');
                                });
                                $('#province').val(province_id)
                                if(province_id!== null){
                                    $.ajax({
                                        type:"get",
                                        url: route('cities', {id: province_id}),
                                        success:function(res)
                                        {
                                            if(res)
                                            {
                                                $("#city").empty();
                                                $("#town").empty();
                                                $("#city").append('<option value="">-- Seleccionar departamento --</option>');
                                                $.each(res,function(key,value){
                                                    $("#city").append('<option value="'+value.id+'">'+value.name+'</option>');
                                                });
                                                $('#city').val(city_id)
                                                if(city_id!== null){
                                                    $.ajax({
                                                        type:"get",
                                                        url: route('towns', {id: city_id}),
                                                        success:function(res)
                                                        {
                                                            if(res)
                                                            {
                                                                $("#town").empty();
                                                                $("#town").append('<option value="">-- Seleccionar localidad --</option>');
                                                                $.each(res,function(key,value){
                                                                    $("#town").append('<option value="'+value.id+'">'+value.name+'</option>');
                                                                });
                                                                if(town_id!=null){
                                                                    $('#town').val(town_id)
                                                                }

                                                            }
                                                        }
                                                    });
                                                }
                                            }
                                        }
                                    });
                                }
                            }
                        }
                    });
                }

                $('#country').on('change',function(){
                    var id = $(this).val();
                    if(id){
                    $.ajax({
                        type:"get",
                        url: route('provinces', {id: id}),
                        success:function(res)
                        {
                            if(res)
                            {
                                $("#province").empty();
                                $("#city").empty();
                                $("#town").empty();
                                $("#province").append('<option value="">-- Seleccionar provincia --</option>');
                                $.each(res,function(key,value){
                                    $("#province").append('<option value="'+value.id+'">'+value.name+'</option>');
                                });
                            }
                        }

                        });
                    }
                });
                $('#province').change(function(){
                    var id = $(this).val();
                    if(id){
                    $.ajax({
                        type:"get",
                        url:route('cities', {id: id}),
                        success:function(res)
                        {
                            if(res)
                            {
                                $("#city").empty();
                                $("#town").empty();
                                $("#city").append('<option value="">-- Seleccionar departamento --</option>');
                                $.each(res,function(key,value){
                                    $("#city").append('<option value="'+value.id+'">'+value.name+'</option>');
                                });
                            }
                        }
                    });
                    }
                });
                $('#city').change(function(){
                    var id = $(this).val();
                    if(id){
                    $.ajax({
                        type:"get",
                        url:route('towns', {id: id}),
                        success:function(res)
                        {
                            if(res)
                            {
                                $("#town").empty();
                                $("#town").append('<option value="">-- Seleccionar localidad --</option>');
                                $.each(res,function(key,value){
                                    $("#town").append('<option value="'+value.id+'">'+value.name+'</option>');
                                });
                            }
                        }
                    });
                    }
                });
                $("#countryCompany").select2({
                    theme: 'bootstrap4',
                });
                $("#provinceCompany").select2({
                    theme: 'bootstrap4',
                });
                $("#cityCompany").select2({
                    theme: 'bootstrap4',
                });
                $("#townCompany").select2({
                    theme: 'bootstrap4',
                });

                var countryCompany_id = "{{ old('countryCompany') }}";
                var provinceCompany_id = "{{ old('provinceCompany')}}";
                var cityCompany_id = "{{ old('cityCompany') }}";
                var townCompany_id = "{{ old('townCompany') }}";

                if(countryCompany_id!== null && $('#countryCompany').val()!==null){
                    console.log(countryCompany_id,$('#countryCompany').val());  
                    $.ajax({
                        type:"get",
                        url: route('provinces', {id: countryCompany_id}),
                        success:function(res)
                        {
                            if(res)
                            {
                                $("#provinceCompany").empty();
                                $("#cityCompany").empty();
                                $("#townCompany").empty();
                                $("#provinceCompany").append('<option value="">-- Seleccionar provincia --</option>');
                                $.each(res,function(key,value){
                                    $("#provinceCompany").append('<option value="'+value.id+'">'+value.name+'</option>');
                                });
                                $('#provinceCompany').val(provinceCompany_id)
                                if(provinceCompany_id!== null){
                                    $.ajax({
                                        type:"get",
                                        url: route('cities', {id: provinceCompany_id}),
                                        success:function(res)
                                        {
                                            if(res)
                                            {
                                                $("#cityCompany").empty();
                                                $("#townCompany").empty();
                                                $("#cityCompany").append('<option value="">-- Seleccionar departamento --</option>');
                                                $.each(res,function(key,value){
                                                    $("#cityCompany").append('<option value="'+value.id+'">'+value.name+'</option>');
                                                });
                                                $('#cityCompany').val(cityCompany_id)
                                                if(cityCompany_id!== null){
                                                    $.ajax({
                                                        type:"get",
                                                        url: route('towns', {id: cityCompany_id}),
                                                        success:function(res)
                                                        {
                                                            if(res)
                                                            {
                                                                $("#townCompany").empty();
                                                                $("#townCompany").append('<option value="">-- Seleccionar localidad --</option>');
                                                                $.each(res,function(key,value){
                                                                    $("#townCompany").append('<option value="'+value.id+'">'+value.name+'</option>');
                                                                });
                                                                if(townCompany_id!=null){
                                                                    $('#townCompany').val(townCompany_id)
                                                                }
                                                            }
                                                        }
                                                    });
                                                }
                                            }
                                        }
                                    });
                                }
                            }
                        }
                    });
                }
                $('#countryCompany').on('change',function(){
                    var id = $(this).val();
                    if(id){
                    $.ajax({
                        type:"get",
                        url: route('provinces', {id: id}),
                        success:function(res)
                        {
                            if(res)
                            {
                                $("#provinceCompany").empty();
                                $("#cityCompany").empty();
                                $("#townCompany").empty();
                                $("#provinceCompany").append('<option value="">  -- Seleccionar provincia --</option>');
                                $.each(res,function(key,value){
                                    $("#provinceCompany").append('<option value="'+value.id+'">'+value.name+'</option>');
                                });
                            }
                        }

                        });
                    }
                });
                $('#provinceCompany').change(function(){
                    var id = $(this).val();
                    if(id){
                    $.ajax({
                        type:"get",
                        url:route('cities', {id: id}),
                        success:function(res)
                        {
                            if(res)
                            {
                                $("#cityCompany").empty();
                                $("#townCompany").empty();
                                $("#cityCompany").append('<option value=""> -- Seleccionar departamento --</option>');
                                $.each(res,function(key,value){
                                    $("#cityCompany").append('<option value="'+value.id+'">'+value.name+'</option>');
                                });
                            }
                        }
                    });
                    }
                });
                $('#cityCompany').change(function(){
                    var id = $(this).val();
                    if(id){
                    $.ajax({
                        type:"get",
                        url:route('towns', {id: id}),
                        success:function(res)
                        {
                            if(res)
                            {
                                $("#townCompany").empty();
                                $("#townCompany").append('<option value=""> -- Seleccionar localidad --</option>');
                                $.each(res,function(key,value){
                                    $("#townCompany").append('<option value="'+value.id+'">'+value.name+'</option>');
                                });
                            }
                        }
                    });
                    }
                });
                if (currentIndex === 3 && priorIndex === 4)
                {
                    $(this).steps("previous");
                }
                // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                if (currentIndex > newIndex)
                {
                    return true;
                }

                var form = $(this);

                // Clean up if user went backward before
                if (currentIndex < newIndex)
                {
                    // To remove error styles
                    $(".body:eq(" + newIndex + ") label.error", form).remove();
                    $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                }

                // Disable validation on fields that are disabled or hidden.
                form.validate().settings.ignore = ":disabled,:hidden";

                // Start validation; Prevent going forward if false
                return form.valid();

            },
            onFinishing: function (event, currentIndex)
            {
                var form = $(this);

                // Disable validation on fields that are disabled.
                // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                form.validate().settings.ignore = ":disabled";

                // Start validation; Prevent form submission if false
                return form.valid();
            },
            onFinished: function (event, currentIndex)
            {
                var form = $(this);

                // Submit form input
                form.submit();
            }
        })
        .validate({
                    errorPlacement: function (error, element)
                    {
                        element.before(error);
                    },
                    rules: {
                        confirm: {
                            equalTo: "#password"
                        },
                    },
        });


   });
</script>
@endpush
