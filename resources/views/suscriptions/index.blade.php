@extends('layouts.app')

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Suscripciones</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a>Home</a>

            </li>
            <li class="breadcrumb-item">
                <a>Suscripciones</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>Pago de suscripciones<small></small></h5>
                    <div class="ibox-tools">

                    </div>
                </div>
                <div class="ibox-content">
                    <section class="pricing py-5">
                      <div class="container">
                        <div class="row">
                          <!-- Free Tier -->
                            @foreach ($suscriptions as $suscription)
                                <div class="col-lg-4">
                                    <div class="card mb-5 mb-lg-0">
                                        <div class="card-body">
                                            <h5 class="card-title text-muted text-uppercase text-center">
                                                {{ $suscription->title }}
                                            </h5>
                                            <h1 class="card-price text-center">
                                                ${{ $suscription->amount }}
                                                {{-- <span class="period">/mes</span> --}}
                                            </h1>
                                            <hr>
                                            <ul class="fa-ul">
                                                @foreach ($itemSuscriptions as $item)
                                                    @if ($suscription->items->contains('id',$item->id))
                                                        <li>
                                                            <span class="fa-li">
                                                                <i class="fa fa-check"></i>
                                                            </span>
                                                            {{ $item->description }}
                                                        </li>
                                                    @else
                                                        <li class="text-muted">
                                                            <span class="fa-li">
                                                                <i class="fa fa-times"></i>
                                                            </span>
                                                            {{ $item->description }}
                                                        </li>
                                                    @endif
                                                @endforeach
                                            </ul>
                                            <form action="{{ route('pay_suscription.store') }}" method="POST">
                                                @csrf
                                                <input type="hidden" name="title" value="{{ $suscription->title }}">
                                                <input type="hidden" name="id_suscription" value="{{ $suscription->id }}">
                                                <input type="hidden" name="price" value="{{ $suscription->amount }}">
                                                <input type="hidden" name="description" value="{{ $suscription->description }}">
                                                <input type="hidden" name="date" value="{{ date('Y-m-d H:i:s')}}">
                                                <input type="hidden" name="unit" value="1">

                                                <button type="submit" class="btn btn-block btn-primary text-uppercase">
                                                    Suscribirse
                                                </button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                      </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
