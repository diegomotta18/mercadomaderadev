<div class="row">
    <div class="col-sm-12">
        <div class="form-group">
            <label for="name">Nombre</label>
            <input type="text" placeholder="Nombre" name="name" class="form-control"
                value="{{isset($plan) ? $plan->name : old('name')}}">
        </div>
    </div>
    <div class="col-sm-12">
        <div class="form-group">
            <label for="description">Descripción</label>
            <textarea id="summernote" rows="3" placeholder="Descripción" name="description" class="summernote form-control"
                >{{isset($plan) ? $plan->description : old('description')}}</textarea>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Precio</label>
            <input placeholder="Precio" name="price" type="number" step="0.01" class="form-control"
            value="{{isset($plan) ? $plan->price : old('price')}}">
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Periodo facturado</label>
            <select class="form-control" name="invoice_period">
                <option {{ isset($plan->invoice_period) &&  $plan->invoice_period  == 0? 'selected="selected"' : '' }} type="hidden" value="0"> -- Seleccionar periodo--</option>
                <option {{ isset($plan->invoice_period) &&  $plan->invoice_period  == 1? 'selected="selected"' : '' }} value="1"> 1 mes</option>
                <option {{ isset($plan->invoice_period) &&  $plan->invoice_period  == 2 ? 'selected="selected"' : '' }} value="2"> 2 mes</option>
                <option  {{ isset($plan->invoice_period) &&  $plan->invoice_period == 3? 'selected="selected"' : '' }} value="3"> 3 mes</option>
                <option  {{ isset($plan->invoice_period) &&  $plan->invoice_period == 4 ? 'selected="selected"' : '' }} value="4"> 4 mes</option>
                <option  {{ isset($plan->invoice_period) &&  $plan->invoice_period == 5 ? 'selected="selected"' : '' }} value="5"> 5 mes</option>
                <option  {{ isset($plan->invoice_period) &&  $plan->invoice_period == 6 ? 'selected="selected"' : '' }} value="6"> 6 mes</option>
                <option  {{ isset($plan->invoice_period) &&  $plan->invoice_period == 7 ? 'selected="selected"' : '' }} value="7"> 7 mes</option>
                <option  {{ isset($plan->invoice_period) &&  $plan->invoice_period == 8 ? 'selected="selected"' : '' }} value="8"> 8 mes</option>
                <option  {{ isset($plan->invoice_period) &&  $plan->invoice_period == 9 ? 'selected="selected"' : '' }} value="9"> 9 mes</option>
                <option  {{ isset($plan->invoice_period) &&  $plan->invoice_period == 10 ? 'selected="selected"' : '' }} value="10"> 10 meses</option>
                <option  {{ isset($plan->invoice_period) &&  $plan->invoice_period == 11 ? 'selected="selected"' : '' }} value="11"> 11 meses</option>
                <option  {{ isset($plan->invoice_period) &&  $plan->invoice_period == 12 ? 'selected="selected"' : '' }} value="12"> 12 meses</option>
            </select>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Periodo de prueba</label>
            <select class="form-control" name="trial_period">
                <option type="hidden" {{ isset($plan->trial_period) &&  $plan->trial_period  == 0? 'selected="selected"' : '' }} value="0"> -- Seleccionar periodo de prueba--</option>
                <option {{ isset($plan->trial_period) &&  $plan->trial_period  == 1? 'selected="selected"' : '' }} value="1"> 1 dia</option>
                <option {{ isset($plan->trial_period) &&  $plan->trial_period  == 15 ? 'selected="selected"' : '' }} value="15"> 15 dias</option>
                <option  {{ isset($plan->trial_period) &&  $plan->trial_period == 30? 'selected="selected"' : '' }} value="30"> 1 meses</option>
                <option  {{ isset($plan->trial_period) &&  $plan->trial_period == 90? 'selected="selected"' : '' }} value="90"> 3 meses</option>
                <option  {{ isset($plan->trial_period) &&  $plan->trial_period == 180? 'selected="selected"' : '' }} value="180"> 6 meses</option>
                <option  {{ isset($plan->trial_period) &&  $plan->trial_period == 365? 'selected="selected"' : '' }} value="365"> 1 año</option>
            </select>
        </div>
    </div>


</div>

<div class="form-group">
    <button class="btn btn-sm btn-primary float-right m-t-n-xs" type="submit"><i
            class="fa fa-save"></i> Guardar</button>
</div>
