<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Availability extends Model
{
    //
    use SoftDeletes;

    protected $table = 'availabilities';

    protected $fillable = [
        'id',
        'cantitude',
        'availability',
        'unitMeasurement',
        'measurementTerm'
    ];
    
    protected $dates = ['created_at','updated_at','deleted_at'];

}
