<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Condition extends Model
{
    //
    use SoftDeletes;

    protected $table = 'conditions';

    protected $fillable = [
        'id',
        'piece',
        'weight',
        'length',
        'tall',
        'width',
        'zunchos',
        'wrapping'
    ];
    
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    public function attribute()
    {
        return $this->belongsTo('App\Attribute');
    }
}
