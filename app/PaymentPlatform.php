<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PaymentPlatform extends Model
{
    use SoftDeletes;

    protected $table = 'payment_platforms';

    protected $fillable = [
        'id',
        'name',
        'state'
    ];

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
}
