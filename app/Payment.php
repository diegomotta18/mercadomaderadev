<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;
use App\Suscription;

class Payment extends Model
{
    use SoftDeletes;

    protected $table = 'payments';

    protected $fillable = [
        'id',
        'payment_identification',
        'payment_state',
        'payment_type',
        'user_id',
        'plan_id',
        'pagado',
        'preference_id',
        'suscription_init',
        'suscription_finish'

    ];

    protected $dates = ['created_at','updated_at','deleted_at'];


    public function user(){
        return $this->belongsTo(User::class);
    }

    public function suscription(){
        return $this->belongsTo(Suscription::class);
    }
}
