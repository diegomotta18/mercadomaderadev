<?php

namespace App;

use Spatie\Permission\Models\Role;

class Rol extends Role
{
    //
    public function scopeName($query,$search){
        if(trim($search)!=null){
            $query->where('name', 'LIKE','%' . $search . '%');
        }
    }
}
