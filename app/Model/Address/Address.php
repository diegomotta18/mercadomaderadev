<?php

namespace App\Model\Address;

use App\Transformers\AddressTransformer;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Model\Address\Department;
use App\Model\Address\Country;
use App\Model\Address\Province;
use App\Model\Address\City;
class Address extends Model
{
    //

    public $transformer = AddressTransformer::class;
    use SoftDeletes;

    protected $table = 'addresses';

    protected $dates = ['created_at', 'updated_at','deleted_at'];

    protected $fillable = ['id',
                            'country_id',
                            'province_id',
                            'department_id',
                            'city_id',
                            'street',
                            'number',
                            'dpto',
                            'floor' ,
                            'latitude',
                            'longitude'
                        ];

    public function completeAddress(){
        return $this->province->name. " ".$this->department->name. " ". $this->city->name;
    }

    public function country(){
        return $this->belongsTo(Country::class);
    }

    public function province(){
        return $this->belongsTo(Province::class);
    }
    public function department(){
        return $this->belongsTo(Department::class);

    }
    public function city(){
        return $this->belongsTo(City::class);

    }


}
