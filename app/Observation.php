<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Observation extends Model
{
    //
    use SoftDeletes;

    protected $table = 'observations';

    protected $fillable = [
        'id',
        'description',
        'user_id',
        'publication_id'
    ];

    protected $dates = ['created_at','updated_at','deleted_at'];

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function publication(){
        return $this->belongsTo('App\Publication');
    }
    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('d/m/Y H:s ');
    }

}
