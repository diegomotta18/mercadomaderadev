<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemSuscription extends Model
{
    protected $table = 'items_suscriptions';

    protected $dates = ['created_at', 'updated_at'];

    protected $fillable = [
        'item_id',
        'suscription_id',
    ];
}
