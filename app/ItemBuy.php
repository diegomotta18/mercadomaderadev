<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ItemBuy extends Model
{
    //
    use SoftDeletes;

    protected $table = 'item_buys';

    protected $fillable = [
        'id',
        'publication_id',
        'buyer_id',
        'seller_id',
        'quantity',
        'price',
        'description',
        'buy_id',
        'is_calificated'
    ];

    protected $dates = ['created_at','updated_at','deleted_at'];

    public function seller()
    {
        return $this->belongsTo('App\User');
    }

    public function buyer()
    {
        return $this->belongsTo('App\User');
    }

    public function publication()
    {
        return $this->belongsTo('App\Publication');
    }

    public function buy()
    {
        return $this->belongsTo('App\Buy');
    }

}
