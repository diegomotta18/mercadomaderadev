<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Subcategory extends Model
{
    //
    use SoftDeletes;

    protected $table = 'subcategories';

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    protected $fillable = [
        'id',
        'name',
        'description',
        'category_id',
        'subcategory_id'
    ];

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function padre()
    {
        return $this->belongsTo('App\Subcategory');
    }
}
