<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PaymentType extends Model
{
    //
    use SoftDeletes;

    protected $table = 'payment_types';

    protected $fillable = [
        'id',
        'paymentType',
        'description'
    ];

    protected $dates = ['created_at','updated_at','deleted_at','burndate'];



}
