<?php

namespace App;

use App\Model\Address\Address;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;

class Person extends Model
{
    //

    use SoftDeletes;

    protected $table = 'people';

    protected $fillable = [
        'id',
        'name',
        'surname',
        'nameComplete',
        'cuil',
        'dni',
        'user_id'];

    protected $dates = ['created_at','updated_at','deleted_at','burndate'];

    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }

    public function contact()
    {
        return $this->belongsTo('App\Contact');
    }

    public function address()
    {
        return $this->belongsTo(Address::class);
    }

    public function company()
    {
        return $this->belongsTo('App\Company');
    }

    public function setBurndateAttribute($value)
    {
        $this->attributes['burndate'] = Carbon::createFromFormat('d/m/Y', $value->format('d/m/Y'));
    }

    public function getBurndateAttribute($value)
    {
        return Carbon::parse($value)->format('d/m/Y');
    }

    public function setNameCompleteAttribute($value)
    {
        $this->attributes['nameComplete'] = $this->attributes['name']." ".$this->attributes['surname'];
    }

    public function addressLocation(){
        $address = $this->address;
        if ($address!=null){
            $country =  $this->address->country!= null ? $this->address->country->name : null;
            $province = $this->address->province!= null ? $this->address->province->name : null;
            $town =   $this->address->town!= null ? $this->address->town->name : null;
            $city =   $this->address->city!= null ? $this->address->city->name : null;

            $address = $country . "-". $province . "-". $town . "-". $city;
        }
        return  $address ;
    }

    public function addressComplete(){
        $addressComplete = $this->address;
        if($addressComplete != null){
            $addressComplete = $this->address->address != null? $this->address->address: null ;
            if ( $this->address->floor != null && $this->address->dpto!= null){
                $addressComplete =  $addressComplete . " - Piso  ". $this->address->floor." - Dpto ".$this->address->dpto ;
            }
        }

        return $addressComplete;
    }

    public function scopeNameComplete($query,$nameComplete){
        if(trim($nameComplete)!=null){
            $query->where('nameComplete', 'LIKE','%' . $nameComplete . '%');

        }
    }
}
