<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Dimension extends Model
{

    use SoftDeletes;

    protected $table = 'dimensions';

    protected $fillable = [
        'id',
        'nominalThickness',
        'nominalWidth',
        'nominalLength',
        'actualThickness',
        'actualWidth',
        'actualLength',
        'isMoreThickness',
        'isLessThickness',
        'isMoreWidth',
        'isLessWidth',
        'isMoreLength',
        'isLessLength',
    ];
    
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    public function attribute()
    {
        return $this->belongsTo('App\Attribute');
    }

}
