<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ComisionistAgent extends Person
{
    //
    public function companies()
    {
        return $this->belongsToMany(Company::class, 'company_comisionist_agent','company_id','person_id')->withTimestamps();
    }
}
