<?php

namespace App\Jobs;

use App\Mail\EmailConfirmationMailable;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class EmailConfirmationJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var \App\User
     */
    public $user;

    /**
     * @var
     */
    public $password;
    public $message;

    public $tries = 5;

    /**
     * Create a new message instance.
     *
     * @param \App\User $user
     */
    public function __construct($user, $message)
    {
        $this->user = $user;
        $this->message = $message;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
            Mail::to($this->user->email)
                ->queue(new EmailConfirmationMailable($this->message, $this->user));

    }
}


