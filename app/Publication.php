<?php

namespace App;

use App\Model\Address\Address;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\File;
use Spatie\MediaLibrary\Models\Media;
use Spatie\Image\Manipulations;
use Illuminate\Database\Eloquent\SoftDeletes;


class Publication extends Model implements HasMedia
{
    //
    use SoftDeletes;
    use HasMediaTrait;

    protected $table = 'publications';

    protected $fillable = [
        'id',
        'title',
        'description',
        'company_id',
        'user_id',
        'contact_id',
        'address_id',
        'typePublication',
        'price',
        'slug',
        'cantitude',
        'product_id',
        'status',
        'total',
        'unit',
        'calification'
    ];



	/* protected static function boot()
	{
		parent::boot();

		static::addGlobalScope(new AddressScope);
	} */

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    public function observation(){
        return $this->hasOne('App\Observation');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function address()
    {
        return $this->belongsTo(Address::class);
    }

    public function company()
    {
        return $this->belongsTo('App\Company');
    }

    public function contact()
    {
        return $this->belongsTo('App\Contact');
    }

    public function products()
    {
        return $this->belongsToMany('App\Product', 'publication_product','publication_id','product_id')
                    ->withPivot('product_id')
                    ->withTimestamps();

    }

    public function comments(){
        return $this->hasMany('App\Comment');
    }

    public function auction (){
        return $this->hasOne('App\Auction');
    }

    public function unit(){
        return $this->belongsTo('App\Unit');
    }

    public function isOffert(){
        return $this->typePublication == TypePublication::OFFERT;
    }

    public function isDemand(){
        return $this->typePublication == TypePublication::DEMAND;
    }

    public function isSimple(){
        return $this->typePublication == TypePublication::SIMPLE;
    }

    public function isAuction(){
        return $this->typePublication == TypePublication::AUCTION;
    }


    public function isActived(){
        return $this->status == PublicationStatus::ACTIVED;
    }

    public function isPaused(){
        return $this->status == PublicationStatus::PAUSED;
    }

    public function isPending(){
        return $this->status == PublicationStatus::PENDING;
    }

    public function isInactived(){
        return $this->status == PublicationStatus::INACTIVED;
    }

    public function isApproved(){
        return $this->status == PublicationStatus::APPROVED;
    }

    public function isDisapproved(){
        return $this->status == PublicationStatus::DISAPPROVED;
    }

    public function isReview(){
        return $this->status == PublicationStatus::REVIEW;
    }

    public function isObservation(){
        return $this->status == PublicationStatus::OBSERVATION;
    }

    public function scopeTitle($query,$title){
        if(trim($title)!=null){
            $query->where('title', 'LIKE','%' . $title . '%');
        }
    }

    public function scopeEstatus($query,$status){
        if(trim($status)!=null){
            $query->where('status',  $status );
        }
    }

    public function scopeOrderByActived($query){
        $query->where('approved',true)->where('status',  PublicationStatus::ACTIVED )->orWhere('status',  PublicationStatus::APPROVED );
    }

    public function registerMediaCollections()
    {
        $this->addMediaCollection('images')
            ->acceptsFile(function (File $file) {
                return $file->mimeType === 'image/jpeg' || $file->mimeType === 'image/png' || $file->mimeType === 'image/jpg' ||  $file->mimeType === 'image/webp';
            })
            ->registerMediaConversions(function (Media $media) {
                $this->addMediaConversion('thumb')
                    ->fit(Manipulations::FIT_CROP, 120, 120)->nonQueued();

            });
    }

    public function scopeByCategory($query, Product $product){
        return $query->whereHas('products',function($query) use ($product){
            return $query->where('subcategory_id',$product->subcategory_id);
        })->whereNotIn("id",[$product->id]);

    }

    public function scopeOfferts($query){
        return $query->where('typePublication',TypePublication::OFFERT);
    }

    public function scopeDemands($query){
        return $query->where('typePublication',TypePublication::DEMAND);
    }

    public function scopeSimples($query){
        return $query->where('typePublication',TypePublication::SIMPLE);
    }

    public function scopeAuctions($query){
        return $query->where('typePublication',TypePublication::AUCTION);
    }

    public function scopeGetTypePublication($query,$value){
        return $query->where('typePublication',$value);
    }

    public function califications(){
        return $this->hasMany(Calification::class);
    }

    public function  scopeGetOrderPublication($query,$value){

        if($value == PublicationOrder::HIGHER_PRICE){
            return $query->orderBy('total', 'DESC');

        }
        else if($value == PublicationOrder::LOWER_PRICE){
            return $query->orderBy('total', 'ASC');

        }
        else if($value == PublicationOrder::MOST_RELEVANT){
            return $query->orderBy('calification','DESC');

        }
        else if($value == PublicationOrder::MOST_SCORE){
            return $query->orderBy('calification','DESC');

        }
        else if($value == PublicationOrder::LOWER_SCORE){
            return $query->orderBy('calification','ASC');
        }
    }


    public function scopeGetByPriceRange($query, $values){
        return $query->whereBetween('total',$values);
    }


    public function scopeGetByTypeWood($query,$value){
        return $query->whereHas('products', function($query) use ($value) {
           return $query->whereHas('typeWood',function ($query) use ($value){
                return $query->whereIn('id',$value);
           });
        });
    }


    public function scopeGetByTypeProduct($query,$value){
        return $query->whereHas('products', function($query) use ($value) {
            return $query->whereHas('typeProduct',function ($query) use ($value){
                return $query->whereIn('id',$value);
            });
        });
    }

    public function scopeGetByWayPay($query,$value){
        return $query->whereHas('products', function($query) use ($value) {
            return $query->whereHas('wayPay',function ($query) use ($value){
                return $query->whereHas('paymentTypes',function ($query) use ($value){
                    return $query->where('payment_type_id',$value);
                });
            });
        });
    }

    public function scopeGetByDimension($query,$wide,$long,$thick){
        return $query->whereHas('products', function($query) use ($wide,$long,$thick) {
            return $query->whereHas('dimension',function ($query) use ($wide,$long,$thick){
                    return $query->where('actualThickness',$thick)
                                 ->where('actualWidth',$wide)
                                 ->where('actualLength',$long);
            });
        });
    }

    public function scopeGetByAvailability($query,$value){
        return $query->whereHas('products', function($query) use ($value) {
            return $query->whereHas('availability',function ($query) use ($value){
                return $query->where('availability',$value);
            });
        });
    }

    public function scopeGetByShippingType($query,$value){
   
        return $query->whereHas('products', function($query) use ($value) {
            return $query->whereHas('shippingType',function ($query) use ($value){
                return $query->where('id',$value);
            });
        });
    }

    public function scopeGetByTitle($query,$value){
        return $query->whereLike('title',$value);
    }



    public function calculateCalification(){

        $calification = $this->califications->sum('calification');
        $total = $this->califications->count();

        if($total>0){
            $this->calification =   round($calification/$total);
            $this->save();
        }
        return 0;
    }
}
