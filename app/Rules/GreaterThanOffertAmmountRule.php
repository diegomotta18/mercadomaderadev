<?php

namespace App\Rules;

use App\Auction;
use Illuminate\Contracts\Validation\Rule;

class GreaterThanOffertAmmountRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    private $lastOffertAmmout;
    public function __construct($lastOffertAmmout)
    {
        //
        $this->lastOffertAmmout =$lastOffertAmmout;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        //
        if($this->lastOffertAmmout==null){
            return $value > Auction::first()->ammount;
        }
        return $value > $this->lastOffertAmmout->ammount;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'La :attribute debe ser mayor al precio de la ultima oferta realizada';
    }
}
