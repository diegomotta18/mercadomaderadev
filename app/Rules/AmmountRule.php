<?php

namespace App\Rules;

use App\TypePublication;
use Illuminate\Contracts\Validation\Rule;

class AmmountRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    private $typePublication;
    public function __construct($typePublication)
    {
        $this->typePublication = $typePublication;
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        //
        if($value == TypePublication::DEMAND){
            return true;
        }
        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The validation error message.';
    }
}
