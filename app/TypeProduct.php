<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TypeProduct extends Model
{
    //
    use SoftDeletes;

    protected $table = 'type_products';

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    protected $fillable = [
        'id',
        'name',
    ];

    public function scopeName($query,$name){
        if(trim($name)!=null){
            $query->where('name', 'LIKE','%' . $name . '%');
        }
    }
}
