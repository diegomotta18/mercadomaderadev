<?php

namespace App\Providers;

use App\Category;
use App\ComisionistAgent;
use App\Comment;
use App\Demand;
use App\Observation;
use App\Offert;
use App\ProfileCompany;
use App\Policies\CategoryPolicy;
use App\Policies\DemandPolicy;
use App\Policies\OffertPolicy;
use App\Policies\PublicationPolicy;
use App\Policies\ComisionistAgentPolicy;
use App\Policies\ComisionistAgentUserPolicy;
use App\Policies\CommentPolicy;
use App\Policies\ObservationPolicy;
use App\Policies\ProfileCompanyPolicy;
use App\Publication;
use App\TypeWood;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [

            Offert::class => OffertPolicy::class,
            Demand::class => DemandPolicy::class,
            Publication::class => PublicationPolicy::class,
            TypeProduct::class => TypeProductPolicy::class,
            TypeWood::class => TypeWoodPolicy::class,
            ComisionistAgent::class => ComisionistAgentPolicy::class,
            Category::class => CategoryPolicy::class,
            ComisionistAgentUser::class => ComisionistAgentUserPolicy::class,
            Observation::class => ObservationPolicy::class,
            Comment::class => CommentPolicy::class,
            ProfileCompany::class => ProfileCompanyPolicy::class,

    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        //
        // Implicitly grant "Super Admin" role all permissions
        // This works in the app by using gate-related functions like auth()->user->can() and @can()
        Gate::before(function ($user, $ability) {
            return $user->hasRole('ROOT') || $user->hasRole('Administrador')  ? true : null;
        });
    }
}
