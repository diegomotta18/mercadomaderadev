<?php

namespace App\Providers;

use App\Category;
use App\ItemBuy;
use Illuminate\Support\ServiceProvider;
use Blade;
use Illuminate\Support\Facades\Schema;
use Auth;
use Illuminate\Support\Facades\View;

class BladeServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {


        if (!Schema::hasTable('categories'))
        {
            return;
        }
//        Blade::directive('categories', function ($expression) {
//
//            $categories = Category::all();
//            return view('renders/categories-nav',['categories'=> $categories]);
//        });

        $categories = Category::query()->with('subcategories')->get(); //Change this to the code you would use to get the categories

        view()->share('categories', $categories);

        View::composer('*', function($view)
        {
            if (Auth::check()){

                $items = ItemBuy::where('buyer_id', auth()->id())->where('is_calificated', false)->get();
                view()->share('items', $items);
            }
        });

    }
}
