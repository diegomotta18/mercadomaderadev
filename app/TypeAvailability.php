<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TypeAvailability extends Model
{
    //
    use SoftDeletes;

    protected $table = 'type_availabilities';

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    protected $fillable = [
        'id',
        'name',
    ];
}
