<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use App\Notifications\ResetPasswordNotification;
use Rinvex\Subscriptions\Traits\HasSubscriptions;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\File;
use Spatie\MediaLibrary\Models\Media;

class User extends Authenticatable implements HasMedia
{
    //
    use HasMediaTrait;
    use HasRoles;
    use Notifiable;
    use SoftDeletes;
    use HasSubscriptions;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'id','name', 'email', 'password','is_verified'
    ];


    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token','verificacion_code'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function person()
    {
        return $this->hasOne(Person::class);
    }

    public function registerMediaCollections()
    {
        $this->addMediaCollection('profile')
            ->acceptsFile(function (File $file) {
                return $file->mimeType === 'image/jpeg' || $file->mimeType === 'image/png' || $file->mimeType === 'image/jpg';
            })
            ->registerMediaConversions(function (Media $media) {
                $this->addMediaConversion('thumb')
                    ->fit(Manipulations::FIT_CROP, 120, 120)->nonQueued();
            });;
    }

    public function companies()
    {
        return $this->belongsToMany('App\Company','company_user');
    }

    public function isRoot(){
        return $this->hasRole('ROOT');
    }

    public function isAdmin(){
        return $this->hasRole('Administrador');
    }

    public function isPremium(){
        return $this->hasRole('Premium');
    }

    public function isNormal(){
        return $this->hasRole('Normal');
    }

    public function isComisionista(){
        return $this->hasRole('Comisionista');
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }


    public function scopeNameOrEmail($query,$search){
        if(trim($search)!=null){
            $query->where('name', 'LIKE','%' . $search . '%')
            ->orWhere('email', 'LIKE','%' . $search . '%');
        }
    }

    public function scopeStatus($query,$status = "active"){
        switch($status){
            case "active":
                return $query->withoutTrashed();
            case "delete":
                return $query->onlyTrashed();
            case "all":
                return $query->withTrashed();
            default:
                return $query->withoutTrashed();
            }

    }

    public function buyConfirms(){
        return $this->hasMany('App\BuyConfirm','seller_id','id');
    }

    public function publications(){
        return $this->hasMany(Publication::class);

    }
    public function publicationsTrasheds(){
        return $this->hasMany(Publication::class)->onlyTrashed();

    }


}
