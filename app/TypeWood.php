<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TypeWood extends Model
{
    //
    use SoftDeletes;

    protected $table = 'type_woods';

    protected $fillable = [
        'id',
        'name',
        'description'
    ];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    public function kinds(){
        return $this->hasMany('App\Kind');
    }

    public function scopeName($query,$name){
        if(trim($name)!=null){
            $query->where('name', 'LIKE','%' . $name . '%');
        }
    }
}


