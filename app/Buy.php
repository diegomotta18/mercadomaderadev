<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Buy extends Model
{
    //
    use SoftDeletes;

    protected $table = 'buys';

    protected $fillable = [
        'id',
        'buyer_id',
        'seller_id',
        'publication_id',
        'total',
    ];

    protected $dates = ['created_at','updated_at','deleted_at'];

    public function seller()
    {
        return $this->belongsTo('App\User');
    }

    public function buyer()
    {
        return $this->belongsTo('App\User');
    }

    public function items()
    {
        return $this->hasMany(ItemBuy::class);
    }


    public  function hasByCalificated(){


        return $this->items->where('is_calificated',false)->count() > 0 ? true : false;
    }
}
