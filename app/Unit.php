<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Unit  extends Model
{
    //
    use SoftDeletes;

    //medida para conversion
    const ESPESOR = 25.4;
    const LARGO =  304.8;
    const ANCHO =  304.8;


    //unidades de medida

    const PIETABLAR = "pie tablar";

    public function isPieTablar(){
        return  $this->name = Unit::PIETABLAR;
    }

}
