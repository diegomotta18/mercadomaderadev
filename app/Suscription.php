<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Suscription extends Model
{

    protected $table = 'suscriptions';

    protected $dates = ['created_at', 'updated_at'];

    protected $fillable = [
        'id',
        'amount',
        'description',
        'mes'
    ];

    public function items()
    {
        return $this->belongsToMany('App\Item', 'items_suscriptions');
    }
}
