<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Rinvex\Subscriptions\Models\PlanSubscription as PlanSuscriptionRinvex;

class PlanSuscription extends PlanSuscriptionRinvex
{
   
    public function endness()
    {
        return Carbon::parse($this->attributes['ends_at'])->format('d/m/Y');

    }

    
    public function startness()
    {
        return Carbon::parse( $this->attributes['starts_at'])->format('d/m/Y');
    }


    public function isEndingPeriodo()
    {
        return  (
                    $this->ends_at->diffInDays(Carbon::now())== 3 ||
                    $this->ends_at->diffInDays(Carbon::now())== 2 ||
                    $this->ends_at->diffInDays(Carbon::now())== 1 
                ); 
                // &&
                // $this->ended();
    }

    public function daysToFinish()
    {
        return  $this->ends_at->diffInDays(Carbon::now());
    }
}
