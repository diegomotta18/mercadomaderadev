<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    //
    use SoftDeletes;

    protected $table = 'categories';

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    protected $fillable = [
        'id',
        'name',
        'description'
    ];

    public function subcategories()
    {
        return $this->hasMany('App\Subcategory');
    }

    public function scopeName($query,$search){
        if(trim($search)!=null){
            $query->where('name', 'LIKE','%' . $search . '%');
        }
    }
}
