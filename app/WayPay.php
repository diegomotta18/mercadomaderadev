<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WayPay extends Model
{
    //
    use SoftDeletes;

    protected $table = 'way_pays';

    protected $fillable = [
        'id',
        'money',
        'ammount',
        'iva',
    ];

    protected $dates = ['created_at', 'updated_at', 'deleted_at', 'burndate'];

    public function paymentTypes()
    {
        return $this->belongsToMany(PaymentType::class, 'way_pay_payment_type')->withTimestamps();
    }
}
