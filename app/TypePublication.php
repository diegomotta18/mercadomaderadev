<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypePublication extends Model
{
    //

    const OFFERT = 'offert';
    const DEMAND = 'demand';
    const SIMPLE = 'simple';
    const AUCTION = 'auction';

    public static function str($type) {
        switch ($type) {
            case TypePublication::OFFERT:
                return "Oferta";
            case TypePublication::DEMAND:
                return "Demanda";
            case TypePublication::SIMPLE:
                return "Simple";
            case TypePublication::AUCTION:
                return "Subasta";
          default:
                return null;
        }
    }
}
