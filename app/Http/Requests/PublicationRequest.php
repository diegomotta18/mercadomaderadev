<?php

namespace App\Http\Requests;

use App\Rules\AmmountRule;
use App\TypePublication;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class PublicationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //dd($this->request->all());
        return [
            //
            'title' => 'required|max:100',
            'description' => 'required|max:25000',
            'actualThickness' => 'required',
            'actualWidth' => 'required',
            'actualLength' => 'required',

            'ammount' => Rule::requiredIf($this->request->get('typePublicacion') != TypePublication::DEMAND),


        ];
    }
}
