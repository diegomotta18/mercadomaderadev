<?php

namespace App\Http\Services;

class PaymentService
{
    public static function create($data)
    {

        try {
                $items = [
                    [
                        "title" => $data['title'],
                        "quantity" => intval($data['unit']),
                        "description" => $data['description'],
                        "currency_id" => "ARS",
                        "unit_price" => (double)$data['price']
                    ],
                ];
    
                //Luego cambiar estos datos por los que esta registrado el usuario autorizado
                $payer = [
                    "email" =>  'test_user_63274575@testuser.com',
                    "name" => 'Lalo',
                    "surname" => 'Landa',
                    "phone" => [
                        "area_code"=>"11",
                        "number"=> "4444-4444"
                    ],
    
                    "identification"=> [
                        "type"=> "DNI",
                        "number"=> "12345678"
                    ],
                    "address"=> [
                        "street_name"=> "Street",
                        "street_number"=> 123,
                        "zip_code"=> "5700"
                    ]
                ];
    
                $external_reference = $data['id_suscription'];
                $mp = resolve(MercadoPagoService::class);
                $flag = isset($data['flag']) ? boolval($data['flag']): false;
                $preference = $mp->createPreference($items, $payer,$external_reference,$flag);
                return $preference->init_point;
        } catch (\Exception $e) {
            throw ($e);
        }
    }

}