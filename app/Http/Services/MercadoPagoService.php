<?php

namespace App\Http\Services;
use App\Traits\ConsumesExternalServices;
use Illuminate\Support\Facades\Http;

class MercadoPagoService
{
    use ConsumesExternalServices;

    protected $baseUri;

    protected $key;

    protected $secret;

    protected $baseCurrency;

    public static $id_suscription;
    public function __construct()
    {
        $this->baseUri = config('services.mercadopago.base_uri');
        $this->key = config('services.mercadopago.key');
        $this->secret = config('services.mercadopago.secret');
        $this->baseCurrency = config('services.mercadopago.base_currency');
    }

    public function resolveAuthorization(&$queryParams, &$formParams, &$headers)
    {
        $queryParams['access_token'] = $this->resolveAccessToken();
    }

    public function decodeResponse($response)
    {
        return json_decode($response);
    }

    public function resolveAccessToken()
    {
        return $this->secret;
    }

    public function createPreference($items, $payer,$external_reference, $flag = false)
    {

        return $this->makeRequest(
            'POST',
            '/checkout/preferences',
            [],
            [
                'items'=>$items,
                'payer'=>$payer,
                'payment_methods' => [
                    'excluded_payment_types' => [
                        ['id'=>'atm']
                    ],
                    'installments'=>12
                ],
                'back_urls' => [
                    'success' => $flag  ? route('plans.me'):route('success') ,
                    'pending' => route('pending'),
                    'failure' => route('failure')
                ],
                'auto_return' => 'approved',
                'marketplace' => "mercadomadera.com.ar",
                "binary_mode"=>true,
                "statement_descriptor" => "mercadomadera.com.ar",
                "external_reference"=> $external_reference,
            ],
            [],
            $isJsonRequest = true
        );
    }

    public function capturePayment($id)
    {
        $response = Http::get('https://api.mercadopago.com/checkout/preferences/'.$id.'?access_token='. $this->secret);
        return $response->json();
    }
}
