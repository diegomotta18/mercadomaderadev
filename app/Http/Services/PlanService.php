<?php

namespace App\Http\Services;

use App\Plan;
use Illuminate\Database\Eloquent\Collection;

class PlanService
{

    /**
     * Filtrado de cliente
     */
    public static function getByFilter($filter)
    {
        try {
            // filtrado
            $plans = Plan::search($filter)->paginate(10);
            return $plans;
        } catch (\Exception $e) {
            throw ($e);
        }
    }

    /**
     * Alta de cliente
     */
    public static function create($data)
    {
        try {
            $plan = new Plan();
            $request = array_merge($data,[
             'signup_fee' => 0,
             'invoice_interval' => 'month',
             'currency' => 'USD',
             'sort_order' => 1,
             'trial_period' => intval($data['trial_period']),
             'trial_interval' => "day"]);
            $plan->fill($request);
            $plan->save();
            $new_plan = PlanService::getById($plan->id);
            return $new_plan;
        } catch (\Exception $e) {
            throw ($e);
        }
    }

    /**
     * Edición de cliente
     */
    public static function update($data, $id)
    {
        try {
            $plan = Plan::findOrFail($id);
            $plan->fill($data);
            $plan->save();
            $new_plan = PlanService::getById($plan->id);

            return $new_plan;
        } catch (\Exception $e) {
            throw ($e);
        }
    }


    /**
     * Baja de cliente
     */
    public static function delete($id)
    {
        try {
            $plan = Plan::findOrFail($id);
            $plan->delete();

            return ['message' => 'Se eliminó el cliente correctamente'];
        } catch (\Exception $e) {
            throw ($e);
        }
    }

    /**
     * Retorna Fila Editada
     */
    public static function getById($id)
    {
        try {
            $plan = Plan::findOrFail($id);
            return $plan;
        } catch (\Exception $e) {
            throw ($e);
        }
    }

    /**
     * combo de clientes
     */
    public static function getAll()
    {
        try {
            $plan = Plan::orderBy('name', 'ASC')->get();

            return $plan;
        } catch (\Exception $e) {
            throw ($e);
        }
    }

}
