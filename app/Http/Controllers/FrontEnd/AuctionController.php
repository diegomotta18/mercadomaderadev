<?php

namespace App\Http\Controllers\FrontEnd;
use App\Http\Controllers\Controller;

use App\Bidder;
use App\Category;
use App\Publication;
use App\Rules\GreaterThanOffertAmmountRule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AuctionController extends Controller
{

    /*public function __construct()
    {
        $this->middleware('auth');
    }*/

    public function index(){
        $publications = Publication::with('media','typePublication')->auctions()->orderByActived()->paginate(20);
        return view('public/auctions/index',compact('publications'));
    }

    public function auction($id)
    {
        # code...
        $categories = Category::all();
        $publication = Publication::with(['company','user','auction'])->findOrFail($id);
        return view('public/publications/auction', compact('publication','categories'));

    }

    public function createATender($publication_id)
    {
        $publication = Publication::with('auction')->find($publication_id);
        $product = $publication->products->first();
        return view('public/publications/makeatinder',compact('publication','product'));
    }

    public function makeATender($publication_id, Request $request)
    {

        $publication = Publication::with('auction')->find($publication_id);
        if (!$publication->has('auction')) {
            return redirect()->route('view.publications.show', $publication->id)->with('toast_error', 'Operación no permitida');
        }
        $request->validate(['ammount'=> ['required',new GreaterThanOffertAmmountRule($publication->auction->bidders->last())]]);

        $auction = $publication->auction;
        $bidder =  Bidder::create([
            'user_id' => auth()->user()->id,
            'auction_id' => $auction->id,
            'ammount' => $request->get('ammount')
        ]);
        $auction->bidders()->save($bidder);

        return redirect()->route('view.publications.auction', $publication->id)->with('toast_success', 'Oferta enviada');
    }
}
