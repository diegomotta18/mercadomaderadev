<?php

namespace App\Http\Controllers\FrontEnd;

use App\Http\Controllers\Controller;
use App\Publication;

class OffertController extends Controller
{
    //
    public function index(){
        $publications = Publication::with('media','products')->offerts()->orderByActived()->paginate(20);
        return view('public/offerts/index',compact('publications'));
    }

}
