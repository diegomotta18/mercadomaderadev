<?php

namespace App\Http\Controllers\FrontEnd;

use App\Http\Controllers\Controller;
use App\Publication;

class DemandController extends Controller
{
    //
    public function index(){
        $publications = Publication::with('media','typePublication')->demands()->orderByActived()->paginate(20);
        return view('public/demands/index',compact('publications'));
    }

}
