<?php

namespace App\Http\Controllers\FrontEnd;

use App\Http\Controllers\Controller;
use App\Product;
use App\Publication;
use App\Subcategory;
use GuzzleHttp\Psr7\Request;

class CategoryController extends Controller
{
    //
    public function index(Subcategory $subcategory){
        $products = Product::with('subcategory')->where('subcategory_id', $subcategory->id)->pluck('id');
        $publications = Publication::with('media','products')->whereHas('products', function($query) use ($products){
            return $query->whereIn('product_id',$products);
        })->paginate(10);
        return view('public.categories.index',compact('publications'));
    }

}
