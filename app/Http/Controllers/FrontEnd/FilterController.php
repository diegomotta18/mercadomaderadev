<?php

namespace App\Http\Controllers\FrontEnd;

use App\Category;
use App\Http\Controllers\Controller;
use App\PaymentType;
use App\Publication;
use App\PublicationStatus;
use App\ShippingType;
use App\TypeAvailability;
use App\TypeProduct;
use App\TypeWood;
use Illuminate\Http\Request;


class FilterController extends Controller
{
    public function productsBySearch(Request $request){

        //dd($request->all());
        $publications = Publication::query()
                                    ->with([
                                        'media',
                                        'address',
                                        'califications',
                                        'products',
                                        'products.typeWood',
                                        'products.typeProduct',
                                        'products.dimension',
                                        'products.condition',
                                        'products.availability',
                                        'products.shippingType',
                                        'products.kind',
                                        'products.wayPay.paymentTypes'])
                                    ->where('status',PublicationStatus::APPROVED);

        if ($request->get('title') != null){
            $publications =  $publications->getByTitle($request->get('title'));
        }

        if ($request->get('type') != null){
            $publications =  $publications->getTypePublication($request->get('type'));
        }


        if ($request->get('order')!= null){
            $publications =  $publications->getOrderPublication($request->get('order'));
        }


        if ($request->get('max_price')!= null && $request->get('min_price')){
            $publications =  $publications->getByPriceRange([ (float) $request->get('min_price'), (float) $request->get('max_price')]);
        }

        if ($request->get('type_wood')!= null ){
            $publications =  $publications->getByTypeWood($request->get('type_wood'));
        }

        if ($request->get('type_product') != [] ){
            $publications =  $publications->getByTypeProduct($request->get('type_product'));
        }

        if ($request->get('way_pay')!= null ){
            $publications =  $publications->getByWayPay($request->get('way_pay'));
        }

        if (($request->get('wide')!= null && $request->get('long')!= null && $request->get('thick')!= null )
        &&($request->get('wide')>= 0 && $request->get('long')>= 0 && $request->get('thick')>= 0 )){
            $publications =  $publications->getByDimension($request->get('wide'),$request->get('long'),$request->get('thick'));
        }

        if ($request->get('shipping_type')!= null ){
            $publications =  $publications->getByShippingType($request->get('shipping_type'));
        }


        if ($request->get('availability')!= null ){
            $publications =  $publications->getByAvailability($request->get('availability'));
        }

        $publications = $publications->paginate(9);
        if ($request->ajax()) {
            return view('renders/_filterList', compact('publications'));
        }

        $types_woods = TypeProduct::query()->get();
        $way_pays = PaymentType::query()->get();
        $shipping_types = ShippingType::query()->get();
        $type_availabilities = TypeAvailability::query()->get();
        return view('public/publications/index',compact('publications','types_woods','way_pays','shipping_types','type_availabilities'));
    }



}
