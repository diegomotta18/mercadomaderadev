<?php

namespace App\Http\Controllers\FrontEnd;
use App\Buy;
use App\Calification;
use App\Http\Controllers\Controller;
use App\Item;
use App\ItemBuy;
use App\Publication;
use Illuminate\Http\Request;


class CalificationController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Buy $buy){
        $items = $buy->items->where('is_calificated',false);
        return view('public/califications/index',compact('items','buy'));
    }

    public function create(Buy $buy,ItemBuy $item){
        $publication = $item->publication;
        return view('public/califications/new',compact('publication','buy','item'));
    }

    public  function  store(Request $request, Buy $buy,ItemBuy $item){

        Calification::create([
            'publication_id' =>  $item->publication->id,
            'description'=> $request->get('description'),
            'calification'=> $request->get('rating'),
            'phrase'=> $request->get('phrase'),
        ]);

        $item->is_calificated = true;
        $item->save();

        $publication = Publication::find($item->publication->id);
        $publication->calculateCalification();

        if ($buy->hasByCalificated()){
            return redirect()->route('califications.index',$buy->id)->with('toast_info', 'Opinión enviada');
        }

        return redirect()->route('califications.thanks');
    }

}
