<?php

namespace App\Http\Controllers\FrontEnd;

use App\Buy;
use App\BuyConfirm;
use App\Http\Controllers\ApiController;
use App\Mail\CalificationEmail;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ItemBuy;
use App\Mail\NotifyBuyToBuyerEmail;
use App\Mail\NotifyBuyToSellerEmail;
use App\Publication;
use DateTime;
use Illuminate\Support\Facades\Mail;
use PDF;

class CartController extends ApiController
{


    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        //\Cart::session(auth()->id())->clear();
        $contents =  $this->getContentsCart();;
        $total = 0;
        foreach ($contents as $item){
            $total += $item->price;
        }
        return view('public/cart/shopping_cart',compact('contents','total'));
    }

    public function clear(){
        \Cart::session(auth()->id())->clear();
        $contents =  $this->getContentsCart();;
        return view('public/cart/shopping_cart',compact('contents'));
    }

    public function delete(Request $request){
        \Cart::session(auth()->id())->remove($request->get('item_id'));
        return response()->json(['redirect' => route('shopping_cart.index')]);
    }

    public function productsToBeConfirmed(Request $request)
    {

        $publication = Publication::find($request->get('publication_id'));
        $image = !$publication->hasMedia('images') ? 'https://place-hold.it/700x400'  : $publication->getMedia('images')[0]->getUrl();

        $option_buy_select =  $this->getOptionBuy($request->get('option_buy'));

        \Cart::session(auth()->id())->add(array(
            'id' => uniqid($publication->id),
            'name' => $publication->title,
            'price' =>  $request->get('price'),
            'quantity' => $request->get('quantity'),
            'attributes' => ['image' => $image,
                             'option_buy' => $option_buy_select,
                             'price' => $publication->price,
                             'publication_id' =>$publication->id
                            ],
            'associatedModel' => $publication->products()->first()
        ));
        //return  \Cart::getContent()->toJson();
        return response()->json(['redirect' => route('shopping_cart.index')]);
    }

    public function confirmCart(){

        $contents =  $this->getContentsCart();

        $buy = Buy::create([
            'buyer_id' => auth()->id(),
        ]);

        foreach ($contents as $item){
            $publication = Publication::find($item->attributes->publication_id);
            $item = ItemBuy::create([
                'publication_id' => $item->attributes->publication_id,
                'buyer_id' => auth()->id(),
                'seller_id' =>$publication->user_id,
                'quantity' =>$item->quantity,
                'price' => floatval($item->price),
                'description' =>$item->attributes->option_buy,
            ]);
            $buy->seller_id = $publication->user_id;
            $buy->total += $item->price;
            $buy->save();
            $buy->items()->save($item);
        }
        \Cart::session(auth()->id())->clear();

        //$data["buy"] = $buy;
        //setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true]
        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
                    ->loadView('_pdf/notifyBuyToBuyerPdf', compact('buy'));

        Mail::to(auth()->user()->email)->send(new NotifyBuyToBuyerEmail($buy,$pdf));
        Mail::to(auth()->user()->email)->send(new CalificationEmail($buy));
        Mail::to($buy->seller->email)->send(new NotifyBuyToSellerEmail($buy,$pdf));

        return response()->json(['redirect' => route('shopping_cart.thanks')]);

    }

    public function thanks(){
        return view('public/cart/thanks');
    }

    public function getOptionBuy($option){
        switch ($option) {
            case '1':
                # code...
                return "m²";
            case '2':
                return "unidad";

            default:
                # code...
                return null;
        }
    }

    public function getContentsCart(){
        return \Cart::session(auth()->id())->getContent();
    }
}
