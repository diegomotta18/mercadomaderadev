<?php

namespace App\Http\Controllers\FrontEnd;

use App\Calification;
use App\Category;
use App\Company;
use App\Demand;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Offert;
use App\Person;
use App\Publication;
use App\Subcategory;

class PublicationController extends Controller
{


    public  function show($id){
        $categories = Category::all();
        $publication = Publication::with(['address','user','products','products.subcategory'])->findOrFail($id);
        $product = $publication->products->first();
       // $product = Product::with('subcategory')->where('id',$product_id->id)->get();
        $publicationRandom =  Publication::orderByActived()->count() >= 4 ? Publication::orderByActived()->byCategory($product)->inRandomOrder()->limit(4)->get() : null;
        $profile = Person::where('user_id',$publication->user->id)->select('nameComplete')->first();
        $calification = $publication->calification;//$this->calculatedStars($publication);

        return view('public/publications/show', compact('publication','product','categories','publicationRandom','profile','calification'));
    }


    public function showCompany(Publication $publication,Company $company){
        $categories = Category::all();
        $profile = Person::with(['contact','address','user'])->where('user_id',$publication->user->id)->first();
        $publications = Publication::where('company_id', $company->id)->whereNotIn('id',[$publication->id])->get();
        return view('public/company/show', compact('company','categories','profile','publications'));
    }

}
