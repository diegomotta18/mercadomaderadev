<?php

namespace App\Http\Controllers\FrontEnd;

use App\Category;
use App\Http\Controllers\FrontEnd\PublicationController;
use App\Http\Controllers\Controller;
use App\Publication;
use App\TypeWood;

class HomeController extends Controller
{
    //
    public function index(){
        $categoriesLimits = Category::query()->with('subcategories')->get()->random(2);
        $publications = Publication::query()
            ->with([
                'media',
                'address',
                'califications',
                'products',
                'products.typeWood',
                'products.typeProduct',
                'products.dimension',
                'products.condition',
                'products.availability',
                'products.shippingType',
                'products.kind',
                'products.wayPay.paymentTypes'])
            ->orderByActived()->orderby('id','ASC')->paginate(12);

        return view('public/home',compact('publications','categoriesLimits'));
    }

}
