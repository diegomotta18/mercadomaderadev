<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use App\Model\Address\Country;
use App\Model\Address\Department;
use App\Model\Address\Province;

class DeparmentController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Country $country, Province $province)
    {
        //
        $department = $province->departments;
        return $this->showAll($department);

    }

    public function show(Country $country, Province $province,Department $department)
    {
        //
        return $this->showOne($department);
    }

}
