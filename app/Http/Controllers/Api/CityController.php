<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use App\Model\Address\City;
use App\Model\Address\Country;
use App\Model\Address\Department;
use App\Model\Address\Province;

class CityController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Country $country, Province $province,Department $department)
    {
        //
        $cities = $department->cities;
        return $this->showAll($cities);

    }

    public function show(Country $country, Province $province,Department $department, City $city)
    {
        //
        return $this->showOne($city);
    }
}
