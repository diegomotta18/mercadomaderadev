<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use App\Model\Address\Country;
use Illuminate\Http\Request;

class CountryController extends ApiController
{
    public function index()
    {
        //
        $countries = Country::all();
        return $this->showAll($countries);

    }

    public function show(Country $country)
    {
        //
        return $this->showOne($country);
    }
}
