<?php

namespace App\Http\Controllers;

use App\Http\Services\MercadoPagoService ;
use App\Payment;
use App\Plan;
use App\PlanSuscription;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(MercadoPagoService $mercado)
    {
        $this->mercado = $mercado;
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function index(Request $request)
    {
        if ($request->preference_id) {
            $mercadopago = resolve(MercadoPagoService::class);
            $id = $request->preference_id;
            $preference = $mercadopago->capturePayment($id);
            if ($request->collection_status == "approved") {

               $payment = new Payment();
               //$payment->suscription_id => $preference['external_reference'],
               $payment->user_id=  Auth::user()->id;
               $payment->payment_type =  $request->payment_type;
               $payment->payment_state =  $request->collection_status;
               $payment->payment_identification =  $request->collection_id;
               $payment->pagado =  true;
               $payment->preference_id = $request->preference_id;
               

                $user = User::findOrFail(Auth::user()->id);
                $plan = Plan::find($preference['external_reference']);
                $payment->plan_id = $plan->id;
                $user->newSubscription($plan->name, $plan);
                $suscription = PlanSuscription::where('user_id', auth()->id())->latest()->first();
                $payment->suscription_finish = $suscription->ends_at;
                $payment->suscription_init =  $suscription->starts_at;
                $payment->suscription_id = $suscription->id;
                $payment->save();
                $user->removeRole('Normal');
                $user->assignRole('Premium');
            }
            alert()->success('Felicidades','Se ha suscripto como usuario Premium')->persistent(true,false);
        }
        return view('home', compact('request'));
    }
}
