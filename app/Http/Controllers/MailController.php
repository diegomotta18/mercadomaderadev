<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\EmailConfirmationMailable;
use App\Mail\EmailResetPasswordMailable;

class MailController extends Controller
{
    //
    public static function sendConfirmationEmail(User $user){
        Mail::to($user->email)->send(new EmailConfirmationMailable("Confirmación de cuenta ", $user));
    }

    public static function sendResetPasswordEmail($email,$url){
        Mail::to($email)->send(new EmailResetPasswordMailable("Restablecimiento de contraseña ",$url));
    }

    public function verify($verification_code){
        $user = User::where('verification_code',$verification_code)->first();
        if($user !=null){
            $user->is_verified = 1;
            $user->save();
            alert()->success('Tu cuenta ha sido autenticada','Por favor el ingreso al sistema ingresado email y contraseña')->showConfirmButton('Ok', '#3085d6');
            return redirect()->route('login');
        }
    }
}
