<?php

namespace App\Http\Controllers\BackEnd;

use App\Model\Address\Address;
use App\Demand;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class DemandController extends Controller
{
    //
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $this->authorize('index', Demand::class);
        $demands = Demand::with(['address','user'])->where('user_id', auth()->user()->id)->paginate(10);

        if (auth()->user()->isRoot() || auth()->user()->isAdmin() ){
            $demands= Demand::with(['address','user'])->paginate(10);
        }

        return view('demands/index', compact('demands'));
    }

    public function show($id){
        $this->authorize('view', Demand::class);
        $demand = Demand::findOrFail($id);
        return view('demands/show', compact('demand'));
    }

    public function new(){
        $this->authorize('create', Demand::class);
        return view('demands/new');
    }

    public function edit(Demand $demand){
        $this->authorize('edit', $demand);
        return view('demands/edit',compact('demand'));
    }

    public function store(Request $request){
        $this->authorize('create', Demand::class);
        $demand = Demand::create($request->all());
        //direccion
        $address = Address::create($request->all());
        $demand->address()->associate($address);
        //usuario del producto
        $demand->user()->associate(auth()->user());
        //tipo de publicacion
        $demand->typePublication = $this->typePublication();
        //activar productos
        $demand->status = 'actived';

        $demand->save();
        return redirect()->route('demands.index')->with('toast_success', 'Demanda ' . $demand->name . ' creado');
    }

    public function update(Request $request, Demand $demand){
        //$demand  = Demand::with(['person','address'])->findOrFail($id);
        $this->authorize('update', $demand);
        $demand->update($request->all());
        $address = $demand->address;
        $address->update($request->all());
        return redirect()->route('demands.index')->with('toast_success', 'Demanda ' . $demand->name . ' modificado');

    }

    public function destroy($id)
    {
        $demand = Demand::findOrFail($id);
        $this->authorize('delete',  $demand);
        $demand->delete();
        return response()->noContent();
    }

    //tipo de publicacion a asociar a la demanda
    private function typePublication(){
        $typePublication = 'demand';
        if (auth()->user()->hasRole('Normal')){
            $typePublication = 'simple';
        }
        return $typePublication ;
    }
}
