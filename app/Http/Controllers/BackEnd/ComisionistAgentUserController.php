<?php

namespace App\Http\Controllers\BackEnd;

use App\ComisionistAgent;
use App\ComisionistAgentUser;
use App\Company;
use App\Http\Controllers\Controller;
use App\Person;
use App\User;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;

class ComisionistAgentUserController extends Controller
{
    //
        /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {   
        $this->authorize('index',ComisionistAgentUser::class);
        $company = Company::where('user_id',auth()->user()->id)->first();
        $users = $company->comisionistAgentUsers()->paginate(10);
        return view('comisionistAgentUsers/index', compact('users'));
    }

    public function search(Request $request){
        $this->authorize('index',ComisionistAgentUser::class);
        $company = Company::where('user_id',auth()->user()->id)->first();
        $users = $company->comisionistAgentUsers()->nameOrEmail($request->get('search'))->orderBy('created_at','desc')->paginate(10);
        return view('comisionistAgentUsers/index', compact('users'));
    }

    public function new()
    {
        $this->authorize('create',ComisionistAgentUser::class);
        return view('comisionistAgentUsers/create');
    }


    public function show($id)
    {        
        $user  = User::findOrFail($id);
        $this->authorize('show',$user);
        return view('comisionistAgentUsers/show', compact('user'));
    }

    public function store(Request $request)
    {
        $this->authorize('create',ComisionistAgentUser::class);

        $user = User::create([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'is_verified' => 1,
            'password' => bcrypt($request->get('password'))
        ]);

        if ($request->get('person')!=null){
            $person = Person::findOrFail($request->get('person'));
            $person->user_id = $user->id;
            $person->save();
        }

        $userComisionist = ComisionistAgentUser::create([
            'user_id' => $user->id
        ]);
        $company = Company::where('user_id',auth()->user()->id)->first();
        //lo asocia al nuevo comisionista

        $company->comisionistAgentUsers()->save($user);
        $userComisionist->companies()->save($company);
        
        $user->assignRole('Comisionista');
        //obtiene la compania del usuario premium

        $file = asset('img/default.jpeg');
        $user->addMediaFromUrl($file)->preservingOriginal()->toMediaCollection('profile');
        
        //Mejorar
        // $user = User::find($user->id);
        // $user->addMediaFromUrl($file)->preservingOriginal()->toMediaCollection('profile');
        return redirect()->route('comisionistAgentUsers.index')->with('toast_success', 'Usuario ' . $user->name . ' creada');
    }


    public function edit($id)
    {
        $user  = User::findOrFail($id);
        $this->authorize('edit',$user );
        return view('comisionistAgentUsers/edit', compact('user'));
    }

    public function update(Request $request,  $id)
    {
        $user  = User::findOrFail($id);
        $this->authorize('edit',$user );
        $user->name = $request->get('name');
        $user->email = $request->get('email');
        $user->syncRoles($request->get('roles'));


        if ($request->get('person')!=null){
            $person = Person::findOrFail($request->get('person'));
            $person->user_id = $user->id;
            $person->save();
        }

        $user->save();

        return redirect()->route('comisionistAgentUsers.index')->with('toast_success', 'Usuario ' . $user->name . ' modificada');
    }

    public function destroy($id)
    {
        $user  = User::findOrFail($id);
        $this->authorize('delete',$user );
        $user->delete();
        return response()->noContent();
    }

    public function getPersons(Request $request)
    {
            $page = $request->page;
            $resultCount = 10;
            $offset = ($page - 1) * $resultCount;
            $company = Company::where('user_id',auth()->user()->id)->first();
            $person = Person::where('user_id',auth()->user()->id)->first();
            $persons = Person::orderBy('name')->where('company_id',$company->id)->whereNotIn('id',[$person->id])
            ->skip($offset)
            ->take($resultCount)
            ->selectRaw('id,nameComplete as text')
            ->get();
            $count = Count(Person::orderBy('name')
            ->selectRaw('id,nameComplete as text')
            ->get());

            if ( $request->term!= null){
                $persons = Person::where('nameComplete', 'LIKE', '%' . $request->term . '%')
                ->orderBy('name')
                ->skip($offset)
                ->take($resultCount)
                ->selectRaw('id, nameComplete as text')
                ->get();

                $count = Count(Person::where('nameComplete', 'LIKE', '%' . $request->term . '%')
                ->orderBy('name')
                ->selectRaw('id,nameComplete as text')
                ->get());
            }

            $endCount = $offset + $resultCount;
            $morePages = $count > $endCount;
            $results = array(
                "results" => $persons,
                "pagination" => array(
                    "more" => $morePages
                )
            );

            return response()->json($results);
    }

    public function getCompanies(){
      $comisionistUserCurrent =  ComisionistAgentUser::with('companies')->where('user_id',auth()->user());
      $companies = $comisionistUserCurrent->companies()->get();
      dd($companies);
      return $companies;
    }
}
