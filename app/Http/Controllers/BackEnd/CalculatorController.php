<?php

namespace App\Http\Controllers\BackEnd;

use App\Http\Controllers\Controller;
use App\Publication;
use Illuminate\Http\Request;

class CalculatorController extends Controller
{
    //
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Publication $publication){
        $product = $publication->products()->first();
        return view('calculator/index',compact('publication','product'));
    }



}
