<?php

namespace App\Http\Controllers\BackEnd;

use App\Model\Address\Address;
use App\Company;
use App\Contact;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Person;
use App\Town;
use App\User;

class ProfileController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show()
    {
        $profile = Person::with(['contact', 'address', 'user'])->where('user_id', auth()->user()->id)->first();
        return view('profile/user/show', compact('profile'));
    }

    public function edit()
    {
        $profile = Person::with(['contact', 'address', 'user'])->where('user_id', auth()->user()->id)->first();
        return view('profile/user/edit', compact('profile'));
    }

    public function update(Request $request)
    {
        $profile = Person::with(['contact', 'address', 'user'])->where('user_id', auth()->user()->id)->first();
        $profile->update($request->all());
        $this->createOrUpdateAddress($profile, $request);
        $this->createOrUpdateContact($profile, $request);
        return redirect()->route('profile.user')->with('toast_success', 'Perfil ' . $profile->nameComplete . ' modificada');
    }

    public function createOrUpdateContact(Person $profile, Request $request)
    {
        if (!$profile->contact) {
            $contact = Contact::create($request->all());
            $profile->contact()->associate($contact);
            $profile->save();
        } else {
            $contact = $profile->contact;
            $profile->contact()->associate($contact);
        }
    }

    public function createOrUpdateAddress(Person $profile, Request $request)
    {

        if ($profile->address == null) {
            $address = Address::create($request->all());
            $this->addLocation($request,$address);
            $profile->address()->associate($address);
            $profile->save();
        } else {
            $address = $profile->address;
            $this->addLocation($request,$address);
            $address->update($request->all());
        }
    }

    public function changePhotoProfile(Request $request)
    {
        $user = User::find(auth()->user()->id);
        return view('profile.user.editImgProfile', compact('user'));
    }
    public function updatePhotoProfile(Request $request)
    {
        //agregar foto de perfil
        $user = User::find(auth()->user()->id);
        $user->clearMediaCollection('profile');

        if (!$request->has('document')) {
            $file = asset('img/default.jpeg');
            $user->addMediaFromUrl($file)->preservingOriginal()->toMediaCollection('profile');
        } else {
            foreach ($request->get('document', []) as $file) {
                $user->addMedia(storage_path('tmp/uploads/' . $file))->toMediaCollection('profile');
            }
        }

        return redirect()->route('profile.user')->with('toast_success', 'Foto de perfil actualizada');
    }

}
