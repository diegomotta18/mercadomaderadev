<?php

namespace App\Http\Controllers\BackEnd;

use App\Model\Address\Address;
use App\Contact;
use App\Http\Controllers\Controller;
use App\Person;
use App\Town;
use Illuminate\Http\Request;

class PersonController extends Controller
{
    //
        /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function search(Request $request){
        $persons = Person::orderBy('updated_at', 'desc')->nameComplete($request->get('search'))->paginate(10);
        return view('persons/index', compact('persons'));
    }

    public function index()
    {
        $persons = Person::orderBy('updated_at', 'desc')->paginate(10);
        return view('persons/index', compact('persons'));
    }

    public function new()
    {
        return view('persons/new');
    }

    public function show($id)
    {
        $person  = Person::findOrFail($id);
        return view('persons/show', compact('person'));
    }

    public function store(Request $request)
    {
        $person = Person::create($request->all());
        $contact = Contact::create($request->all());
        $address = Address::create($request->all());

        $this->addLocation($request,$address);

        $person->contact()->associate($contact);
        $person->address()->associate($address);
        $person->save();

        return redirect()->route('persons.index')->with('toast_success', 'Persona ' . $person->nameComplete . ' creada');
    }
    public function edit($id)
    {
        $person = Person::with(['contact','address'])->findOrFail($id);
        return view('persons/edit', compact('person'));
    }

    public function update(Request $request, $id)
    {

        $person  = Person::with(['contact','address'])->findOrFail($id);
        $person->update($request->all());
        $contact = $person->contact;
        $contact->update($request->all());
        $address = $person->address;
        $address->update($request->all());
        $this->addLocation($request,$address);


        return redirect()->route('persons.index')->with('toast_success', 'Persona ' . $person->nameComplete . ' modificada');
    }

    public function destroy($id)
    {
        $person = Person::findOrFail($id);
        $person->delete();
        return response()->noContent();
    }

    private function addLocation(Request $request, Address $address){
        if($request->get('address')  !=null  && $request->get('number') !=null && $request->get('town') !=null ){
            $town = Town::find($request->get('town'));
            $nomitetim = $request->get('address')."+".$request->get('number')."+".$town->name;
            $getLongLat = "https://nominatim.openstreetmap.org/?format=json&addressdetails=1&q={".$nomitetim."}&format=json&limit=1";
            $client = new \GuzzleHttp\Client();
            $response = $client->request('GET', $getLongLat);
            if($response->getStatusCode() == 200){
                $resp = json_decode($response->getBody());
                if ($resp !=null){
                    $address->latitud =$resp[0]->lat;
                    $address->longitud = $resp[0]->lon;
                    $address->save();
                }
            }

        }
    }
}
