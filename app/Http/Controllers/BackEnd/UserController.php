<?php

namespace App\Http\Controllers\BackEnd;

use App\Http\Controllers\Controller;
use App\Person;
use App\Publication;
use App\User;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
    //
        /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function search(Request $request){
        $users = User::with(['person','roles','companies'])->nameOrEmail($request->get('search'))->status($request->get('status'))->orderBy('created_at','desc')->paginate(10);
        return view('admin/security/users/index', compact('users'));
    }

    public function index(Request $request)
    {
        $users = User::with(['person','roles','companies'])->status($request->get('status'))->orderBy('created_at','desc')->paginate(10);

        return view('admin/security/users/index', compact('users'));
    }

    public function new()
    {
        $roles = $this->getRoles();
        return view('admin/security/users/new', compact('roles'));
    }

    public function show($id)
    {
        $user  = User::with('person','roles')->findOrFail($id);
        return view('admin/security/users/show', compact('user'));
    }

    public function store(Request $request)
    {

        $user = User::create([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'password' => bcrypt($request->get('password'))

        ]);

        if ($request->get('person')!=null){
            $person = Person::findOrFail($request->get('person'));
            $person->user_id = $user->id;
            $person->save();
        }

        $user->assignRole($request->get('roles'));

        return redirect()->route('users.index')->with('toast_success', 'Usuario ' . $user->name . ' creada');
    }

    public function edit($id)
    {
        $roles = $this->getRoles();
        $user = User::with(['roles','person'])->findOrFail($id);
        return view('admin/security/users/edit', compact('user', 'roles'));
    }

    public function update(Request $request, $id)
    {

        $user  = User::findOrFail($id);
        $user->name = $request->get('name');
        $user->email = $request->get('email');
        $user->syncRoles($request->get('roles'));


        if ($request->get('person')!=null){
            $person = Person::findOrFail($request->get('person'));
            $person->user_id = $user->id;
            $person->save();
        }

        $user->save();
        return redirect()->route('users.index')->with('toast_success', 'Usuario ' . $user->name . ' modificado');
    }

    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->is_verified = 0;
        $user->save();
        $user->publications()->delete();
        $user->delete();
        return response()->noContent();
    }

    private function getRoles()
    {

        $tmp = Role::all();

        return $tmp;
    }

    public function getPersons(Request $request)
    {
            $page = $request->page;
            $resultCount = 10;
            $offset = ($page - 1) * $resultCount;

            $persons = Person::orderBy('name')
            ->skip($offset)
            ->take($resultCount)
            ->selectRaw('id,nameComplete as text')
            ->get();
            $count = Count(Person::orderBy('name')
            ->selectRaw('id,nameComplete as text')
            ->get());

            if ( $request->term!= null){
                $persons = Person::where('nameComplete', 'LIKE', '%' . $request->term . '%')
                ->orderBy('name')
                ->skip($offset)
                ->take($resultCount)
                ->selectRaw('id, nameComplete as text')
                ->get();

                $count = Count(Person::where('nameComplete', 'LIKE', '%' . $request->term . '%')
                ->orderBy('name')
                ->selectRaw('id,nameComplete as text')
                ->get());
            }

            $endCount = $offset + $resultCount;
            $morePages = $count > $endCount;
            $results = array(
                "results" => $persons,
                "pagination" => array(
                    "more" => $morePages
                )
            );

            return response()->json($results);
    }

    public function stop(User $user){

        $user->publications()->delete();
        $user->syncRoles('Normal');
        $user->is_verified = 0;
        $user->save();
        return response()->json($user);
    }

    public function active(User $user){

        Publication::onlyTrashed()->where('user_id',$user->id)->restore();;
        $user->is_verified = 1;
        $user->save();

        return response()->json($user);
    }

    public function restore(User $userDelete){
        $userDelete->is_verified = 1;
        $userDelete->save();
        $userDelete->restore();
        Publication::onlyTrashed()->where('user_id',$userDelete->id)->restore();

        return redirect()->route('users.index')->with('toast_success', 'Usuario restaurado');
    }


}
