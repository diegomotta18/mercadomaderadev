<?php

namespace App\Http\Controllers\BackEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Mail\ObservationEmail;
use App\Mail\ObservationOkEmail;
use App\Observation;
use App\Publication;
use App\PublicationStatus;
use Illuminate\Support\Facades\Mail;

class ObservationController extends Controller
{
    //

    public function show(Publication $publication,Observation $observation){
        $this->authorize('show', Observation::class);
        return view('observations/show',compact('observation','publication'));
    }

    public function create(Publication $publication, Request $request){
        $this->authorize('create', Observation::class);
        return view('observations/create',compact('publication'));

    }

    public function edit(Publication $publication,Observation $observation){
        $this->authorize('edit', Observation::class);
        return view('observations/edit',compact('observation'));

    }

    public function store(Request $request, Publication $publication){
        $this->authorize('store', Observation::class);
        $request->validate([
            'description' => 'required|max:25000',
        ]);
        $request->request->add(['publication_id' => $publication->id, 'user_id'=>auth()->user()->id]);

        if(isset($publication->observation)){
            $publication->observation()->delete();
        }
        $publication->status= PublicationStatus::OBSERVATION;
        $publication->save();
        $observation = Observation::create($request->all());
        Mail::to($publication->user->email)->send(new ObservationEmail(route("observations.show",[$publication,$observation])));
        return redirect()->route('publications.index')->with('toast_success', 'Observación creada');;
    }

    public function update(Publication $publication,Observation $observation, Request $request){
          $this->authorize('update', Observation::class);
          $request->validate([
            'description' => 'required|max:25000',
          ]);
          $observation->update($request->all());
          return redirect()->route('publications.index')->with('toast_success', 'Observación modificada');;
    }

    public function destroy(Publication $publication,Observation $observation){
        $this->authorize('destroy', Observation::class);
        $observation->delete();
        return redirect()->route('publications.index')->with('toast_success', 'Observación eliminada');;
    }

    public function check(Publication $publication,Observation $observation){
        $this->authorize('check', Observation::class);
        $observation->check = true;
        $observation->save();
        $publication->status= PublicationStatus::REVIEW;
        $publication->save();
        //Mail::to($publication->user->email)->send(new ObservationOkEmail(route("observations.show",[$publication,$observation])));
        return redirect()->route('publications.index')->with('toast_success', 'Observación completada');;

    }
}
