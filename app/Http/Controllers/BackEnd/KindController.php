<?php

namespace App\Http\Controllers\BackEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Kind;

class KindController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public  function getKinds($id)
    {
        # code...
        $kinds  = Kind::where('type_wood_id',$id)->get();
        return $kinds;
    }

    public function search(Request $request,$typewood_id){
        $this->authorize('index',  Kind::class);
        $kinds  = Kind::where('type_wood_id',$typewood_id)->orderBy('updated_at','desc')->name($request->get('name'))->paginate(10);
        return view('parameters/typewoods/kinds/index',compact('kinds','typewood_id'));
    }


    public  function index($typewood_id)
    {
        $this->authorize('index',  Kind::class);
        $kinds  = Kind::where('type_wood_id',$typewood_id)->orderBy('updated_at','desc')->paginate(10);
        return view('parameters/typewoods/kinds/index',compact('kinds','typewood_id'));

    }


    public function create($typewood_id)
    {
        //
        $this->authorize('create',  Kind::class);
        return view('parameters/typewoods/kinds/create',compact('typewood_id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($typewood_id,Request $request)
    {
        //
        $this->authorize('create',  Kind::class);
        $kind = Kind::create($request->all());
        $kind->type_wood_id = $typewood_id;
        $kind->save();

        return redirect()->route('kinds.index',$typewood_id)->with('toast_success', 'Especie ' . $kind->name . ' creada');
    }


    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
        $this->authorize('edit',  Kind::class);
        $kind = Kind::findOrFail($id);
        return view('parameters/typewoods/kinds/edit',compact('kind'));
    }

    public function update(Request $request, $id)
    {
        //
        $this->authorize('update',  Kind::class);
        $kind = Kind::find($id);
        $kind->update($request->all());
        return redirect()->route('kinds.index',$kind->type_wood_id)->with('toast_success', 'Especie ' . $kind->name . ' actualizada');
    }

    public function destroy($id)
    {
        //
        $kind = Kind::findOrFail($id);
        $this->authorize('delete',  $kind);
        $kind->delete();
        return response()->noContent();
    }
}
