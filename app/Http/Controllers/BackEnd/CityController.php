<?php

namespace App\Http\Controllers\BackEnd;

use App\Http\Controllers\Controller;
use App\Model\Address\Department;
use App\Model\Address\Province;

class CityController extends Controller
{
    //
    public function getCities($id){
        $province = Province::findOrFail($id);
        $cities = Department::where('province_id',$province->id)->selectRaw('id,name')->get();;
        return response()->json($cities);
    }

    public function getCity($id){
        $city = Department::findOrFail($id);
        return response()->json($city);
    }
}
