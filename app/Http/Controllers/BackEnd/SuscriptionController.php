<?php

namespace App\Http\Controllers\BackEnd;

use App\Http\Controllers\Controller;
use App\Item;
use App\Suscription;
use Illuminate\Http\Request;

class SuscriptionController extends Controller
{

    public function index()
    {
        try {
            $suscriptions = Suscription::with(['items'])->get();
            $itemSuscriptions = Item::all();
            return view('suscriptions/index', compact('suscriptions', 'itemSuscriptions'));
        } catch (\Exception $e) {
            throw $e;
        }
    }

}
