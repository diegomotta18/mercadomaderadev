<?php
namespace App\Http\Controllers\BackEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Publication;
use App\PublicationStatus;
use App\Subcategory;
use App\Auction;
use App\Availability;
use App\Condition;
use App\Dimension;
use App\ShippingType;
use App\Product;
use App\QualitativeParameter;
use App\TypePublication;
use App\Unit;
use App\WayPay;

class PublicationController extends Controller
{
    //
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $this->authorize('index', Publication::class);
        $publications = Publication::with(['media','address','user','products','products.subcategory','observation'])->where('user_id', auth()->user()->id)->paginate(10);

        if (auth()->user()->isRoot() || auth()->user()->isAdmin() ){
            $publications= Publication::with([
                'media',
                'address',
                'user',
                'products',
                'products.subcategory',
                'observation'])
                ->orderBy('id','DESC')
                ->paginate(10);
        }

        return view('publications/index', compact('publications'));
    }

    public function search(Request $request){

        $this->authorize('index', Publication::class);
        $publications = Publication::query();

        if (!auth()->user()->isRoot() || !auth()->user()->isAdmin() ){
            $publications = $publications->where('user_id', auth()->user()->id);
        }

        if ($request->has('search')){
            $publications=  $publications->title($request->get('search'));

        }
        if ($request->has('status')){
            $publications=  $publications->estatus($request->get('status'));
        }

        $publications=  $publications->paginate(10);



        return view('publications/index', compact('publications'));
    }

    public function show($id){
        $this->authorize('show', Publication::class);
        $publication = Publication::with(['address','user','products'])->findOrFail($id);
        $this->authorize('view', $publication );

        return view('publications/show', compact('publication'));
    }

    public function destroy($id)
    {
        $publication = Publication::findOrFail($id);
        $this->authorize('delete',  $publication);
        $publication->delete();
        return response()->noContent();
    }

    public function getCategoriesToOffert()
    {
        $categories = CategoryController::getCategories();
        return view('publications/categories/offert', compact('categories'));
    }

    public function getCategoriesToDemand()
    {
        $categories = CategoryController::getCategories();
        return view('publications/categories/demand', compact('categories'));
    }


    public function editPublicationStatus(Request $request, Publication $publication){
        return view('publications/status/edit',compact('publication'));
    }

    public function updatePublicationStatus(Request $request, Publication $publication){
        $this->authorize('updateStatus', $publication);

        $publication->status = PublicationStatus::getStatus($request->get('status'));
        if ($publication->status == 'approved'){
            $publication->approved = true;
        }

        $publication->save();
        return redirect()->route('publications.index')->with('toast_success',  'El estado de la publicación ' . $publication->name . ' modificado');;
    }

    private function addAvailability(Request $request)
    {
        $availability =  Availability::create([
            'cantitude' => $request->get('cantitude'),
            'availability' => $request->get('availability'),
            'unitMeasurement' => $request->get('unitMeasurement'),
            'measurementTerm'=> $request->get('measurementTerm'),
        ]);
        return $availability;
    }

    private function addDimension(Request $request)
    {

        $dimension =  Dimension::create([
            'nominalThickness'=> $request->get('nominalThickness'),
            'nominalWidth'=> $request->get('nominalWidth'),
            'nominalLength'=> $request->get('nominalLength'),
            'actualThickness'=> $request->get('actualThickness'),
            'actualWidth'=> $request->get('actualWidth'),
            'actualLength'=>   $request->get('actualLength'),
            'isMoreThickness'=> $request->get('isMoreThickness'),
            'isLessThickness'=> $request->get('isLessThickness'),
            'isMoreWidth'=>  $request->get('isMoreWidth'),
            'isLessWidth'=> $request->get('isLessWidth'),
            'isMoreLength'=>  $request->get('isMoreLength'),
            'isLessLength'=>   $request->get('isLessLength'),
        ]);
        return $dimension;
    }

    private function addParameter(Request $request)
    {
        $parametroCualitativo=  QualitativeParameter::create([
            'calidad' => $request->get('calidad'),
            'nudo' => $request->get('nudo'),
            'manchaHongo' => $request->get('manchaHongo'),
            'humedadMax' => $request->get('humedadMax'),
            'secado' => $request->get('secado'),
            'humedadMin' => $request->get('humedadMin'),
            'ubicacionTronco' => $request->get('ubicacionTronco'),
            'grieta' => $request->get('grieta'),
            'rajaduras'=> $request->get('rajaduras'),
            'cantoMuerto' => $request->get('cantoMuerto'),
            'alaveos' => $request->get('alaveos'),
            'combas' => $request->get('combas'),
            'torceduras' => $request->get('torceduras'),
        ]);
        return $parametroCualitativo;
    }

    private function addShippingType(Request $request)
    {
        $shippingType = ShippingType::create([
            'send' => $request->get('send'),
            'description' => $request->get('description_send')
        ]);

        return  $shippingType;
    }

    private function addWayPay(Request $request)
    {
        $wayPay = new WayPay();
        $wayPay->money = $request->get('money');
        $wayPay->iva = $request->has('iva') ? true : false;;
        $wayPay->ammount = $request->get('ammount');
        $wayPay->save();
        if ($request->has('paymentType')) {
            $wayPay->paymentTypes()->attach($request->get('paymentType'));
            $wayPay->save();
        }
        return $wayPay;
    }

    private function addCondition(Request $request)
    {
        $condition = Condition::create(
            [
               'piece' => $request->get('piece'),
               'weight' => $request->get('weight'),
               'length' => $request->get('length'),
               'tall' => $request->get('tall'),
               'width' => $request->get('width'),
               'zunchos' => $request->get('zunchos'),
               'wrapping' => $request->get('wrapping'),
            ]
        );

        return $condition;
    }

    public function newProduct(Request $request,Subcategory $subcategory)
    {
        $dimension = $this->addDimension($request);
        $condition = $this->addCondition($request);
        $availability = $this->addAvailability($request);
        $qualitativeParameter = $this->addParameter($request);
        $shippingType = $this->addShippingType($request);
        $wayPay = $this->addWayPay($request);
        $product = Product::create(
            [
                'condition_id' => $condition->id,
                'dimension_id' => $dimension->id,
                'availability_id' => $availability->id,
                'qualitative_parameter_id' => $qualitativeParameter->id,
                'shipping_type_id' => $shippingType->id,
                'subcategory_id' => $subcategory->id,
                'way_pay_id' => $wayPay->id,
                'kind_id' => $request->get("kind"),
                'type_wood_id' => $request->get("typeWood"),
                'type_product_id' => $request->get("typeProduct"),
                'price' => $request->get('ammount'),
                'total' => $request->get('ammount'),
            ]
        );

        return $product;
    }

    public function updateProduct(Product $product, Request $request)
    {
        $condition = $product->condition;
        $condition->update($request->all());

        $availability = $product->availability;
        $availability->cantitude = $request->cantitude;
        $availability->availability = $request->availability;
        $availability->unitMeasurement = $request->unitMeasurement;
        $availability->measurementTerm = $request->measurementTerm;
        $availability->save();

        $dimension = $product->dimension;
        $dimension->update($request->all());

        $qualitativeParameter = $product->qualitativeParameter;
        $qualitativeParameter->update($request->all());

        $wayPay = $product->wayPay;
        $wayPay->money = $request->get('money');
        $wayPay->iva = $request->has('iva') ? true : false;
        $wayPay->ammount = $request->get('ammount');
        if ($request->has('paymentType')) {
            $wayPay->paymentTypes()->sync($request->get('paymentType'));
        }
        $wayPay->save();

        $shippingType = $product->shippingType;
        $shippingType->send = $request->send;
        $shippingType->save();

        $product->kind_id = $request->get("kind");
        $product->type_wood_id = $request->get("typeWood");
        $product->type_product_id = $request->get("typeProduct");

        $product->save();
    }

    public function addAuction(Request $request){
        if ($request->has('auction')) {
            $auction = new Auction([
                "price_init" => $request->get('price_init'),
                'finish_date' => $request->get('finish_date'),
                'quantity' => $request->get('quantity'),
                'status' => 'actived'
            ]);
           return $auction;
        }
        return;
    }

    public function updateOrCreateAuction(Request $request, Publication $publication, Product $product){
        if ($request->has('auction')) {
            $action = Auction::updateOrCreate(
            [ 'publication_id' => $publication->id],
            [
                "price_init" => $request->get('price_init'),
                'finish_date' => $request->get('finish_date'),
                'quantity' => $request->get('quantity'),
                'status' => 'actived'
            ]);
            $publication->typePublication = TypePublication::AUCTION;
            $publication->price = $action->price_init;
            $publication->total = $action->price_init;
        }else{
            if($publication->auction!=null){
                $publication->auction->delete();
                $publication->price = $request->get('ammount');

                $publication->total = $product->calculatePriceTotal(Unit::PIETABLAR) * $request->get('ammount');
                $publication->typePublication = TypePublication::OFFERT;

            }
        }
        $publication->save();
    }

}
