<?php

namespace App\Http\Controllers\BackEnd;

use App\Http\Controllers\Controller;
use App\Offert;



class OffertController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $this->authorize('index', Offert::class);
        $offerts = Offert::with([
            'address',
            'user',
            'products',
            'products.condition',
            'products.dimension',
            'products.typeWood',
            'products.kind',
            'products.subcategory',
            'products.qualitativeParameter',
            'products.shippingType',
            'products.wayPay',
            'products.wayPay.paymentTypes'
        ])->where('user_id', auth()->user()->id)->paginate(10);

        if (auth()->user()->isRoot() || auth()->user()->isAdmin()) {

            $offerts =  Offert::with([
                'address',
                'user',
                'product',
                'product.condition',
                'product.dimension',
                'product.typeWood',
                'product.kind',
                'product.subcategory',
                'product.qualitativeParameter',
                'product.shippingType',
                'product.wayPay',
                'product.wayPay.paymentTypes'
            ])->paginate(10);
        }

        return view('publications/offerts/index', compact('offerts'));
    }
}
