<?php

namespace App\Http\Controllers\BackEnd\Suscription;

use App\Http\Controllers\ApiController;
use App\Http\Services\MercadoPagoService;
use App\Http\Services\PlanService;
use App\Payment;
use App\Plan;
use App\PlanSuscription;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class PlanController extends ApiController
{
    //
    /**
     * Create a new controller instance.
     *
     *
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        try {

            $plans = PlanService::getByFilter($request);
            return view('suscriptions/plans/index', compact('plans'));
        } catch (\Exception $e) {
            Log::critical('Exception: ' . $e);
            return $this->errorResponse($e->getMessage(), 500);
        }
    }


    /**
     *
     *
     *
     */
    public function getAll()
    {
        try {
            $result = PlanService::getAll();

            return $this->showAll(collect($result));
        } catch (\Exception $e) {
            Log::critical('Expection: ' . $e);
            return $this->errorResponse($e->getMessage(), 500);
        }
    }

    public function create()
    {

        return view('suscriptions/plans/create');
    }

    public function edit($id)
    {
        $plan = PlanService::getById($id);
        return view('suscriptions/plans/edit', compact('plan'));
    }


    /**
     * Store a newly created resource in storage.
     *
     *
     *
     */
    public function store(Request $request)
    {
        try {
            $data = $request->all();
            DB::beginTransaction();
            $result = PlanService::create($data);
            DB::commit();
            return redirect()->route('plans.index')->with('toast_success', 'Plan creada');
        } catch (\Exception $e) {
            DB::rollBack();
            Log::critical('Exception: ' . $e->getMessage());
            return redirect()->route('plans.create')->withInput()->with('toast_error', 'Error al crear plan');
        }
    }

    /**
     * Display the specified resource.
     *
     *
     *
     */
    public function show($id)
    {
        try {
            $plan = PlanService::getById($id);
            return view('suscriptions/plans/show', compact('plan'));
        } catch (\Exception $e) {
            Log::critical('Exception: ' . $e);
            return redirect()->route('plans.index')->with('toast_error', 'Error al mostrar plan');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     *
     *
     *
     */
    public function update(Request $request, $id)
    {
        try {
            $data = $request->all();

            DB::beginTransaction();
            $result = PlanService::update($data, $id);
            DB::commit();

            return redirect()->route('plans.index')->with('toast_success', 'Plan actualizado');
        } catch (\Exception $e) {
            DB::rollBack();
            Log::critical('Exception: ' . $e);
            return redirect()->route('plans.index')->with('toast_error', 'Error al actualizar plan');
        }
    }

    public function me(Request $request)
    {
        try {
            if ($request->preference_id) {
                if(!Payment::where('preference_id', $request->preference_id)->exists()){
                    $mercadopago = resolve(MercadoPagoService::class);
                    $id = $request->preference_id;
                    $preference = $mercadopago->capturePayment($id);
                    if ($request->collection_status == "approved") {
    
                        $payment = new Payment();
                        $payment->user_id=  Auth::user()->id;
                        $payment->payment_type =  $request->payment_type;
                        $payment->payment_state =  $request->collection_status;
                        $payment->payment_identification =  $request->collection_id;
                        $payment->pagado =  true;
                        $payment->preference_id = $request->preference_id;
                        $user = User::findOrFail(Auth::user()->id);
                        $plan = Plan::find($preference['external_reference']);
                        $payment->plan_id = $plan->id;
                        $user->newSubscription($plan->name, $plan);
                        $suscription = PlanSuscription::where('user_id', auth()->id())->latest()->first();
                        $payment->suscription_finish = $suscription->ends_at;
                        $payment->suscription_init =  $suscription->starts_at;
                        $payment->suscription_id = $suscription->id;
                        $payment->save();
                        if($suscription->active()){
                            $suscription->renew();
                        }else{
                            return redirect()->route('home')->with('toast_error', 'La suscripcion esta inactiva');
                        }
                    }        
                    alert()->success('Felicidades', 'Se ha renovado su membresia Premium')->persistent(true, false);
                }
            }
            $suscription = PlanSuscription::where('user_id', auth()->id())->first();
            $plan = PlanService::getById($suscription->plan_id);
            $payments = Payment::where('user_id',Auth::user()->id)->orderBy('created_at','DESC')->paginate(10);
            return view('profile/plan/index', compact('plan', 'suscription','payments'));
         } catch (\Exception $e) {
            Log::critical('Exception: ' . $e);
            return redirect()->route('home')->with('toast_error', 'Error al mostrar plan');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     *
     *
     */
    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            $result = PlanService::delete($id);
            DB::commit();
            return response()->noContent();
        } catch (\Exception $e) {
            DB::rollBack();
            Log::critical('Exception: ' . $e);
            return redirect()->route('plans.index')->with('toast_error', 'Error al eliminar plan');
        }
    }
}
