<?php

namespace App\Http\Controllers\BackEnd\Suscription;

use App\Http\Controllers\Controller;
use App\Http\Services\PaymentService;
use Illuminate\Http\Request;

class SuscriptionController extends Controller
{
        //
        /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    //post
    public function renewBoard(Request $request,$id){
        $data = $request->all();
        $result = PaymentService::create($data);
        return redirect()->to($result);

    }
  
}
