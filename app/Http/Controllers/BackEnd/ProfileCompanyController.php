<?php

namespace App\Http\Controllers\BackEnd;

use App\Model\Address\Address;
use App\Company;
use App\Contact;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Person;
use App\Town;
use App\ProfileCompany;
class ProfileCompanyController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show(){
        $this->authorize('show', ProfileCompany::class);
        $company = Company::with(['contact','address','user'])->where('user_id',auth()->user()->id)->first();
        return view('profile/company/show',compact('company'));
    }

    public function edit(){
        $this->authorize('update', ProfileCompany::class);
        $company = Company::with(['contact','address','user'])->where('user_id',auth()->user()->id)->first();
        return view('profile/company/edit',compact('company'));
    }

    public function update(Request $request){
        $this->authorize('update', ProfileCompany::class);
        $company = Company::with(['contact','address','user'])->where('user_id',auth()->user()->id)->first();
        $company->update($request->all());
        $this->createOrUpdateAddress($company,$request);
        $this->createOrUpdateContact($company,$request);
        return redirect()->route('profile.company')->with('toast_success', 'Empresa ' . $company->nameComplete . ' modificada');
    }

    public function createOrUpdateContact(Company $company, Request $request){

        if(!$company->contact){
            $contact = Contact::create($request->all());
            $company->contact()->associate($contact);
            $company->save();
        }else{
            $contact = $company->contact;
            $company->contact()->associate($contact);
        }
    }

    public function createOrUpdateAddress(Company $company, Request $request){

        if($company->address == null){
            $address = Address::create($request->all());
            $this->addLocation($request,$address);
            $company->address()->associate($address);
            $company->save();
        }else{
            $address = $company->address;
            $this->addLocation($request,$address);
            $address->update($request->all());
        }
    }

    public function updateLogo(Request $request){
        $this->authorize('update', ProfileCompany::class);
        $company = Company::with(['contact','address','user'])->where('user_id',auth()->user()->id)->first();
        $company->clearMediaCollection('logo');
        if (!$request->has('document')) {
            $file = asset('img/logo_default.jpeg');
            $company->addMediaFromUrl($file)->preservingOriginal()->toMediaCollection('logo');
        } else {
            foreach ($request->get('document', []) as $file) {
                $company->addMedia(storage_path('tmp/uploads/' . $file))->toMediaCollection('logo');
            }
        }
        return redirect()->route('profile.company')->with('toast_success', 'Logo de la empresa ' . $company->nameComplete . ' modificada');

    }

    public function editLogo(Request $request){
        $this->authorize('update', ProfileCompany::class);
        $company = Company::with(['contact','address','user'])->where('user_id',auth()->user()->id)->first();
        return view('profile.company.editLogoCompany', compact('company'));
    }

    public function editImgs(Request $request){
        $this->authorize('update', ProfileCompany::class);
        $company = Company::with(['contact','address','user'])->where('user_id',auth()->user()->id)->first();
        return view('profile.company.editImgCompany', compact('company'));

    }

    public function updateImgs(Request $request){
        $this->authorize('update', ProfileCompany::class);

        $company = Company::with(['contact','address','user'])->where('user_id',auth()->user()->id)->first();

        if (count($company->getMedia('companies')) > 0) {
            foreach ($company->getMedia('companies') as $media) {
                if (!in_array($media->file_name, $request->input('document', []))) {
                    $media->delete();
                }
            }
        }
        $media = $company->getMedia('companies')->pluck('file_name')->toArray();
        foreach ($request->input('document', []) as $file) {
            if (count($media) === 0 || !in_array($file, $media)) {
                $company->addMedia(storage_path('tmp/uploads/' . $file))->toMediaCollection('companies');
            }
        }

        return redirect()->route('profile.company')->with('toast_success', 'Imagenes de la empresa ' . $company->nameComplete . ' modificada');

    }

}
