<?php

namespace App\Http\Controllers\BackEnd;

use App\Company;
use App\Http\Controllers\Controller;
use App\Product;
use App\Publication;
use App\PublicationStatus;
use App\Subcategory;
use App\TypePublication;
use Illuminate\Http\Request;

class SimpleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $this->authorize('index', Publication::class);
        $simples = Publication::where('typePublication',"simple")->where('user_id', auth()->user()->id)->get();
        return view('publications/simples/index', compact('simples'));
    }


    public function search(Request $request){
        $this->authorize('index', Publication::class);
        $simples = Publication::where('user_id', auth()->user()->id)
        ->title($request->get('title'))
        ->paginate(10);
        return view('publications/simples/index', compact('simples'));
    }

    public function show($id)
    {
        $this->authorize('view', Publication::class);
        $simple = Publication::findOrFail($id);
        return view('publications/simples/show', compact('simple'));
    }

    public function new($subcategory_id)
    {
        $this->authorize('create', Publication::class);

        $subcategory = Subcategory::findOrFail($subcategory_id);

        return view('publications/simples/create', compact('subcategory'));
    }

    public function edit($id)
    {
        $simple = Publication::with([
            'user',
        ])->findOrFail($id);

        return view('publications/simples/edit', compact('simple'));
    }

    public function store(Request $request,$subcategory_id)
    {
        $this->authorize('create', Publication::class);

        $request->validate([
            'title' => 'required|max:100',
            'description' => 'required|max:25000',
            'price' => 'required'
        ]);
        //crea la oferta con datos simples
        $simple = Publication::create($request->all());
        $simple->total = request()->get('price');
        //asociar usuario
        $simple->user()->associate(auth()->user());
        //obtener compania
        $company = Company::where('user_id',auth()->user()->id)->first();
        //asociar compania a la oferta
        if($company != null){
            $simple->company_id = $company->id;
        }
        //asociar oferta con el usuario activo
        $simple->user()->associate(auth()->user());
       //publicacion activa
        $simple->status = PublicationStatus::REVIEW;
        //agregar producto
        $product = new Product();
        //asocar categoria del producto
        $product->subcategory_id = $subcategory_id;
        $product->save();
        //asocia al producto
        $simple->products()->attach($product->id);
        //guarda los datos del producto en la oferta
        $simple->typePublication =  TypePublication::SIMPLE;
        $simple->save();

        //agregar imagenes
        foreach ($request->input('document', []) as $file) {
            $simple->addMedia(storage_path('tmp/uploads/' .$file))->toMediaCollection('images');
        }

        return redirect()->route('simples.index')->with('toast_success', 'Publicación ' . $simple->name . ' creado');
    }

    public function update(Request $request, $id)
    {
        $simple = Publication::findOrFail($id);

        $this->authorize('update', $simple);

        $request->validate([
            'title' => 'required|max:100',
            'description' => 'required|max:25000',
            'price' => 'required'
        ]);

        $simple->update($request->all());
        $simple->total = request()->get('price');
        $simple->save();
        //actualizar imagenes
        if (count($simple->getMedia('images')) > 0) {
            foreach ($simple->getMedia('images') as $media) {
                if (!in_array($media->file_name, $request->input('document', []))) {
                    $media->delete();
                }
            }
        }
        $media = $simple->getMedia('images')->pluck('file_name')->toArray();
        foreach ($request->input('document', []) as $file) {
            if (count($media) === 0 || !in_array($file, $media)) {
                $simple->addMedia(storage_path('tmp/uploads/' . $file))->toMediaCollection('images');
            }
        }

        return redirect()->route('simples.index')->with('toast_success', 'Publicación ' . $simple->name . ' actualizada');

    }

    public function destroy($id)
    {
        $simple = Publication::findOrFail($id);
        $this->authorize('delete',  $simple);
        $simple->delete();
        return response()->noContent();
    }

    public function getCategoriesToSimple()
    {
        $categories = CategoryController::getCategories();
        return view('publications/simples/categories/categories', compact('categories'));
    }
}
