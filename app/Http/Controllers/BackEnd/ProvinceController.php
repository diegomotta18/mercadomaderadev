<?php
namespace App\Http\Controllers\BackEnd;

use App\Http\Controllers\Controller;
use App\Model\Address\Country;
use App\Model\Address\Province;

class ProvinceController extends Controller
{
    //
    public function getProvinces($id){
        $country = Country::findOrFail($id);
        $provinces = Province::where('country_id',$country->id)->selectRaw('id,name')->get();;
        return response()->json($provinces);
    }

    public function getProvince($id){
        $province = Province::findOrFail($id);
        return response()->json($province);
    }
}
