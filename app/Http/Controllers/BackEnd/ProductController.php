<?php
namespace App\Http\Controllers\BackEnd;


use App\Subcategory;
use App\Company;
use App\Product;
use App\Http\Controllers\BackEnd\TypeWoodController;
use App\Http\Controllers\BackEnd\TypeProductController;
use App\Http\Controllers\BackEnd\PaymentTypeController;
use App\Http\Controllers\BackEnd\PublicationController;
use App\Http\Requests\PublicationRequest;
use App\Model\Address\Address;
use App\Publication;
use App\PublicationStatus;
use App\TypePublication;
use App\Unit;

class ProductController extends PublicationController
{
    //
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show($id)
    {
        $this->authorize('view', Publication::class);
        $publication = Publication::findOrFail($id);
        return view('publications/offerts/show', compact('offert'));
    }

    public function create($type_publication, $id)
    {
        $this->authorize('create', Publication::class);

        $typeWoods = TypeWoodController::getTypeWoods();

        $typeProducts = TypeProductController::getTypeProducts();

        $paymentTypes = PaymentTypeController::getPaymentTypes();

        $subcategory = Subcategory::find($id);

        return view('publications/offerts/new', compact('typeWoods', 'typeProducts', 'paymentTypes', 'subcategory', 'type_publication'));
    }

    public function edit($type_publication,Publication $publication, Product $product)
    {

        $this->authorize('edit', $publication);

        $typeWoods = TypeWoodController::getTypeWoods();

        $typeProducts = TypeProductController::getTypeProducts();

        $paymentTypes = PaymentTypeController::getPaymentTypes();

        return view('publications/offerts/edit', compact('publication', 'product', 'typeWoods', 'typeProducts', 'paymentTypes', 'type_publication'));
    }

    public function store($type_publication,Subcategory $subcategory_id, PublicationRequest $request)
    {

        $this->authorize('create', Publication::class);

        $publication = Publication::create([
                'title'=> $request->get('title'),
                'description' => $request->get('description'),
                'status'=>PublicationStatus::REVIEW,
                'typePublication' => $type_publication,
                'unit' => UNIT::PIETABLAR,
                'user_id' => auth()->user()->id,
        ]);

        $address = Address::create([
            'country_id' => $request->country,
            'province_id' => $request->province,
            'department_id' => $request->department,
            'city_id' => $request->city,
            'street' => $request->street,
            'number' => $request->number,
            'dpto' => $request->dpto,
            'floor'  => $request->floor,
            'latitude' => $request->lat,
            'longitude'=> $request->lon,
        ]);

        $publication->address()->associate($address);

        //Asocia si tiene la empresa del usuario a la publicacion
        $company = Company::where('user_id', auth()->user()->id)->first();
        if ($company != null) {
            $publication->company_id = $company->id;
        }

        //crea el producto
        $product = $this->newProduct($request,$subcategory_id);
        $auction = $this->addAuction($request);

        if ($type_publication != TypePublication::DEMAND){
            $publication->total = $product->total;
            $publication->price = $product->price;
        }

        $publication->products()->attach($product->id);
        $publication->save();

        //Verifica si la opcion de subasta se encuentra seleccionada
        if($auction!= null){
            $publication->typePublication = TypePublication::AUCTION;
            $publication->auction()->save($auction);
            $publication->price = $auction->price_init;
            $publication->total = $auction->price_init;
        }
        $publication->save();

        //Agrega fotos si las tiene
        foreach ($request->input('document', []) as $file) {
            $publication->addMedia(storage_path('tmp/uploads/' . $file))->toMediaCollection('images');
        }

        return redirect()->route('publications.index')->with('toast_success', __($type_publication) . ' ' . $publication->name . ' creado');
    }

    public function update(PublicationRequest $request, Publication $publication, Product $product)
    {

        $this->authorize('update', $publication);
        $publication->update($request->all());
        $publication->address->update($request->all());
        $this->updateProduct($product, $request);

        if ($publication->typePublication != TypePublication::DEMAND){
            $product->price = $request->get('ammount');
            $product->total = $product->price;
            $product->save();
            $publication->total = $product->total;
            $publication->price =$product->price ;
            $publication->save();
        }

        $this->updateOrCreateAuction($request,$publication,$product);

        $address =  $publication->address;
        $address->country_id =$request->country;
        $address->province_id = $request->province;
        $address->department_id = $request->department;
        $address->city_id = $request->city;
        $address->street = $request->street;
        $address->number = $request->number;
        $address->dpto = $request->dpto;
        $address->floor = $request->floor;
        $address->latitude = $request->lat;
        $address->longitude = $request->lon;
        $address->save();
        $publication->save();
        //actualizar imagenes
        if (count($publication->getMedia('images')) > 0) {
            foreach ($publication->getMedia('images') as $media) {
                if (!in_array($media->file_name, $request->input('document', []))) {
                    $media->delete();
                }
            }
        }
        $media = $publication->getMedia('images')->pluck('file_name')->toArray();
        foreach ($request->input('document', []) as $file) {
            if (count($media) === 0 || !in_array($file, $media)) {
                $publication->addMedia(storage_path('tmp/uploads/' . $file))->toMediaCollection('images');
            }
        }
        return redirect()->route('publications.index')->with('toast_success', __($publication->typePublication) . ' ' . $publication->name . ' actualizada');
    }

    public function destroy($id)
    {
        $publication = Publication::findOrFail($id);
        $this->authorize('delete',  $publication);
        $publication->delete();
        return response()->noContent();
    }
}
