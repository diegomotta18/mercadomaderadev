<?php

namespace App\Http\Controllers\BackEnd;

use App\Model\Address\Address;
use App\ComisionistAgent;
use App\Contact;
use App\Http\Controllers\Controller;
use App\Company;
use App\Person;
use Illuminate\Http\Request;

class ComisionistAgentController extends Controller
{
    //
        /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $this->authorize('index', ComisionistAgent::class);
        $company = Company::where('user_id',auth()->user()->id)->first();
        $comisionistAgents = $company->comisionistAgents()->paginate(10);
        return view('comisionistAgents/index', compact('comisionistAgents'));
    }

    public function search(Request $request){
        $this->authorize('index', ComisionistAgent::class);
        $company = Company::where('user_id',auth()->user()->id)->first();
        $comisionistAgents = $company->comisionistAgents()->nameComplete($request->get('search'))->paginate(10);
        return view('comisionistAgents/index', compact('comisionistAgents'));
    }

    public function new()
    {
        $this->authorize('create', ComisionistAgent::class);
        return view('comisionistAgents/new');
    }

    public function show($id)
    {
        $comisionistAgent  = Person::findOrFail($id);
        $this->authorize('show',$comisionistAgent);

        return view('comisionistAgents/show', compact('comisionistAgent'));
    }

    public function store(Request $request)
    {
        $this->authorize('create',ComisionistAgent::class);
        $comisionistAgent = Person::create($request->all());
        $contact = Contact::create($request->all());
        $address = Address::create($request->all());
        $comisionistAgent->contact()->associate($contact);
        $comisionistAgent->address()->associate($address);
        $company = Company::where('user_id',auth()->user()->id)->first();
        $comisionistAgent->company_id = $company->id;
        $comisionistAgent->save();
        $company->comisionistAgents()->save($comisionistAgent);
        return redirect()->route('comisionistAgents.index')->with('toast_success', 'Comisionista ' . $comisionistAgent->nameComplete . ' creada');
    }
    public function edit($id)
    {
        $comisionistAgent = Person::with(['contact','address'])->findOrFail($id);
        $this->authorize('edit',$comisionistAgent );
        return view('comisionistAgents/edit', compact('comisionistAgent'));
    }

    public function update(Request $request, $id)
    {

        $comisionistAgent  = Person::with(['contact','address'])->findOrFail($id);
        $this->authorize('update',$comisionistAgent );

        $comisionistAgent->update($request->all());
        $contact = $comisionistAgent->contact;
        $contact->update($request->all());
        $address = $comisionistAgent->address;
        $address->update($request->all());

        return redirect()->route('comisionistAgents.index')->with('toast_success', 'Comisionista ' . $comisionistAgent->nameComplete . ' modificada');
    }

    public function destroy($id)
    {
        $comisionistAgent = Person::findOrFail($id);
        $this->authorize('delete',$comisionistAgent );
        $comisionistAgent->delete();
        return response()->noContent();
    }
}
