<?php

namespace App\Http\Controllers\BackEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\TypeWood;

class TypeWoodController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $this->authorize('index',  TypeWood::class);
        $typewoods = TypeWood::with(['kinds'])
        ->orderBy('updated_at', 'desc')
        ->paginate(20);

        return view('parameters/typewoods/index',compact('typewoods'));
    }

    public function search(Request $request){
        $this->authorize('index',  TypeWood::class);
        $typewoods = TypeWood::with(['kinds'])
        ->orderBy('updated_at', 'desc')
        ->name($request->get('name'))
        ->paginate(20);
        
        return view('parameters/typewoods/index',compact('typewoods'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $this->authorize('create',  TypeWood::class);
        return view('parameters/typewoods/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->authorize('create',  TypeWood::class);
        $typewood = TypeWood::create($request->all());
        return redirect()->route('typewoods.index')->with('toast_success', 'Tipo de madera ' . $typewood->name . ' creada');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $this->authorize('edit',  TypeWood::class);
        $typewood = TypeWood::findOrFail($id);
        return view('parameters/typewoods/edit',compact('typewood'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->authorize('update',  TypeWood::class);

        $typewood = TypeWood::find($id);
        $typewood->update($request->all());
        return redirect()->route('typewoods.index')->with('toast_success', 'Tipo de madera ' . $typewood->name . ' actualizada');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $typeWood = TypeWood::findOrFail($id);
        $this->authorize('delete',  $typeWood);
        $typeWood->delete();
        return response()->noContent();
    }

    public static function getTypeWoods(){
        $typeWoods = TypeWood::all();
        return $typeWoods;
    }
}
