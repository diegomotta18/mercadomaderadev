<?php

namespace App\Http\Controllers\BackEnd;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\TypeProduct;

class TypeProductController extends Controller
{
    //
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $this->authorize('index',  TypeProduct::class);
        $typeproducts = TypeProduct::orderBy('updated_at', 'desc')
        ->paginate(10);
        return view('parameters/typeproducts/index', compact('typeproducts'));

    }

    public function search(Request $request){
        $this->authorize('index',  TypeProduct::class);
        $typeproducts = TypeProduct::orderBy('updated_at', 'desc')
        ->name($request->get('name'))
        ->paginate(20);
        return view('parameters/typeproducts/index', compact('typeproducts'));
    }

    public function create(){
        $this->authorize('create',  TypeProduct::class);
        return view('parameters/typeproducts/create');

    }

    public function store(Request $request){
        $this->authorize('create',  TypeProduct::class);
        $typeproduct = TypeProduct::create(['name' => $request->get('name')]);
        return redirect()->route('typeproducts.index')->with('toast_success', 'Tipo de producto '.$typeproduct->name.' creado');
    }

    public function edit($id){
        $this->authorize('update',  TypeProduct::class);
        $typeproduct = TypeProduct::findOrFail($id);
        return view('parameters/typeproducts/edit',compact('typeproduct'));
    }

    public function show($id){
        
    }

    public function update($id,Request $request){
        $typeproduct = TypeProduct::findOrFail($id);
        $this->authorize('update',  TypeProduct::class);
        $typeproduct->name = $request->get('name');
        $typeproduct->save();
        return redirect()->route('typeproducts.index')->with('toast_success', 'Tipo de producto '.$typeproduct->name.' modificado');
    }

    public function destroy($id){
        $this->authorize('delete',  TypeProduct::class);
        $typeproduct = TypeProduct::findOrFail($id);

        $typeproduct->delete();
        return response()->noContent();

    }

    public static function getTypeProducts(){
        $typeProduct = TypeProduct::all();
        return $typeProduct;
    }
}
