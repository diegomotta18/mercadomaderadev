<?php
namespace App\Http\Controllers\BackEnd;

use App\Http\Controllers\Controller;
use App\Model\Address\City;
use App\Model\Address\Department;

class TownController extends Controller
{
    //
    public function getTowns($id){
        $city = Department::findOrFail($id);
        $towns = City::where('department_id',$city->id)->selectRaw('id,name')->get();
        return response()->json($towns);
    }

    public function getTown($id){
        $town = City::findOrFail($id);
        return response()->json($town);
    }
}
