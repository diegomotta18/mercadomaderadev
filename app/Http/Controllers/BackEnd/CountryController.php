<?php
namespace App\Http\Controllers\BackEnd;

use App\Http\Controllers\Controller;
use App\Model\Address\Country as AddressCountry;

class CountryController extends Controller
{

    public function getCountries(){
        $countries = AddressCountry::selectRaw('id,name')->get();;
        return response()->json($countries);
    }

    public function getCountry($id){
        $country = AddressCountry::findOrFail($id);
        return response()->json($country);
    }
}
