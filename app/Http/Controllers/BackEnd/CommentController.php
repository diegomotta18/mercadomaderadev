<?php

namespace App\Http\Controllers\BackEnd;

use App\Comment;
use App\Http\Controllers\Controller;
use App\Mail\CommentBuyerEmail;
use App\Mail\CommentSellerEmail;
use App\Publication;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class CommentController extends Controller
{
    //
    //
        /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $this->authorize('index', Comment::class);
        $comments = Comment::with(['user','owner','publication'])
        ->where('owner_id',auth()->user()->id)
        ->whereNull("subcomment")->get();
        return view('comments/index',compact('comments'));
    }

    public function replies(){
        $this->authorize('index', Comment::class);
        $comments = Comment::with(['user','owner'])
        ->where('owner_id',auth()->user()->id)
        ->whereNotNull("subcomment")->get();
        return view('comments/replies',compact('comments'));
    }

    public function store(Request $request, Publication $publication){
        $this->authorize('store', Comment::class);

        $request->validate([
            'comment' => 'required|max:25000',
        ]);
        $comment = Comment::create([
            'comment' =>$request->get('comment'),
            'publication_id' => $publication->id,
            'owner_id' => $publication->user->id
        ]);
        $comment->user()->associate(auth()->user());
        $comment->save();
        Mail::to($publication->user->email)->send(new CommentSellerEmail($publication->title, route('comments.show',$comment),auth()->user()->name));

        return redirect()->back();
    }

    public function show(Comment $comment){
        return view('comments/show',compact('comment'));

    }

    public function update(Request $request, Comment $comment){

        $this->authorize('update', Comment::class);

        $request->validate([
            'subcomment' => 'required|max:25000',
        ]);

        $comment->update([
            'subcomment' =>$request->get('subcomment'),
        ]);

        $publication = Publication::find($comment->publication_id);
        Mail::to($comment->user->email)->send(new CommentBuyerEmail($publication->title, route('view.publications.show',$comment->publication_id),auth()->user()->name,$comment->id));

        return redirect()->route('comments.index')->with('toast_success', 'Comentario enviado');
    }
}
