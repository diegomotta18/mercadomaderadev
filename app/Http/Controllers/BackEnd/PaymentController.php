<?php

namespace App\Http\Controllers\BackEnd;

use App\Http\Controllers\Controller;
use App\Http\Services\MercadoPagoService;
use App\Http\Services\PaymentService;
use App\Payment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class PaymentController extends Controller
{

    public function store(Request $request)
    {
        try {
            // $items = [
            //     [
            //         "title" => $request->title,
            //         "quantity" => intval($request->unit),
            //         "description" => $request->description,
            //         "currency_id" => "ARS",
            //         "unit_price" => (double) $request->price

            //     ],
            // ];

            // $payer = [
            //     "email" =>  'test_user_63274575@testuser.com',
            //     "name" => 'Lalo',
            //     "surname" => 'Landa',
            //     "phone" => [
            //         "area_code"=>"11",
            //         "number"=> "4444-4444"
            //     ],

            //     "identification"=> [
            //         "type"=> "DNI",
            //         "number"=> "12345678"
            //     ],
            //     "address"=> [
            //         "street_name"=> "Street",
            //         "street_number"=> 123,
            //         "zip_code"=> "5700"
            //     ]
            // ];

            // $external_reference = $request->id_suscription;


            // $mp = resolve(MercadoPagoService::class);
            // $preference = $mp->createPreference($items, $payer,$external_reference);
            // return redirect()->to($preference->init_point);
            $data = $request->all();
            $result = PaymentService::create($data);
            return redirect()->to($result);
        } catch (\Exception $e) {
            Log::critical('Exception :' .$e);
            throw $e;
        }
    }
}
