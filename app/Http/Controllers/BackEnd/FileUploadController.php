<?php

namespace App\Http\Controllers\BackEnd;

use App\Company;
use App\Http\Controllers\Controller;
use App\Publication;
use App\User;
use Illuminate\Http\Request;

class FileUploadController extends Controller
{
    //
    public function store(Request $request)
    {
        $path = storage_path('tmp/uploads');

        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }
    
        $file = $request->file('file');
    
        $name = uniqid() . '_' . trim($file->getClientOriginalName());
    
        $file->move($path, $name);
    
        return response()->json([
            'name'          => $name,
            'original_name' => $file->getClientOriginalName(),
        ]);
    }

    public function deleteImage($id,$file){

        $publication = Publication::find($id);
        foreach($publication->getMedia('images') as $fotos){
            dd($fotos->name, $file);
            if ($fotos->name == $file){
                $fotos->delete();
            }
        }
       return response()->noContent();;
    }

    public function deleteImageProfile($id,$file){

        $profile = User::find($id);

        foreach($profile->getMedia('profile') as $fotos){
            if ($fotos->name == $file){
                $fotos->delete();
            }
        }
       return response()->noContent();;
    }

    public function deleteImageCompany($id,$file){

        $company = Company::find($id);

        foreach($company->getMedia('companies') as $fotos){
            if ($fotos->name == $file){
                $fotos->delete();
            }
        }
       return response()->noContent();;
    }

    public function deleteLogoCompany($id,$file){

        $company = Company::find($id);

        foreach($company->getMedia('logo') as $fotos){
            if ($fotos->name == $file){
                $fotos->delete();
            }
        }
       return response()->noContent();;
    }

}
