<?php

namespace App\Http\Controllers\Auth;

use App\Company;
use App\Contact;
use App\Model\Address\Address;
use App\Model\Address\Country;
use App\Http\Controllers\Controller;
use App\Http\Controllers\MailController;
use App\Person;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [

            'nameUser' => ['required', 'string', 'max:100'],
            'nameCompany' => ['required', 'string', 'max:100'],
            'name' => ['required', 'string', 'max:100'],
            'surname' => ['required', 'string', 'max:100'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'dni' => ['required', 'max:8', 'unique:people'],
            'cuit' => ['required', 'max:11', 'unique:companies'],
            'cuil' => ['required', 'max:11', 'unique:people'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'aceptarTerminosYCondiciones' => ['required','in:accept'],
            'telphone' =>[ 'max:13'],
            'celphone' =>[ 'max:13'],
            'celphoneCompany' =>[ 'max:13'],
            'telphoneCompany' =>[ 'max:13'],

        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user =  User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);


    }

    public function showRegistrationForm()
    {
        $countries = Country::all();
        return view('auth.register',compact('countries'));
    }


    public function register(Request $request)
    {
        $this->validator($request->all())->validate();
        $user = new User();
        $file = asset('img/default.jpeg');
        $user->name = $request->get('nameUser');
        $user->email = $request->get('email');
        $user->password = Hash::make($request->get('password'));
        $user->verification_code = Str::random(32);
        //$user->is_verified = 1;
        $user->save();
        app()['cache']->forget('spatie.permission.cache');
        $user->addMediaFromUrl($file)->preservingOriginal()->toMediaCollection('profile');
        $user->assignRole('Normal');

        //event(new Registered($user));

        $person = Person::create($request->all());
        $contact = Contact::create($request->all());
        $address = Address::create($request->all());
        $this->addLocation($request,$address);
        $person->contact()->associate($contact);
        $person->address()->associate($address);
        $person->save();
        $user->person()->save($person);

        $company = new Company();

        $company->name = $request->get('nameCompany');
        $company->cuit = $request->get('cuit');
        $company->trajectory = $request->get('trajectory');
        $company->compromise= $request->get('compromise');
        $company->technologies= $request->get('technologies');
        $company->others = $request->get('others');
        $company->user_id = $user->id;

        $contactocompany = Contact::create([
            "email"=> $request->get("emailCompany"),
            "web"=> $request->get("webCompany"),
            "celphone"=> $request->get("celphoneCompany"),
            "telphone"=> $request->get("telphoneCompany"),
        ]);

        $addresscompany = Address::create([
            "address"=> $request->get("companyaddress"),
            "dpto"=> $request->get("companydpto"),
            "floor"=> $request->get("companyfloor"),
            "number"=> $request->get("companynumber"),
            "country"=> $request->get("countryCompany"),
            "province"=> $request->get("provinceCompany"),
            "city"=> $request->get("cityCompany"),
            "town"=> $request->get("townCompany"),
            "user_id"=>$user->id
        ]);
        $this->addLocationcompany($request,$addresscompany);
        $company->address()->associate($addresscompany);
        $company->contact()->associate($contactocompany);
        $company->save();

        $logo = asset('img/logo_default.jpeg');
        $company->addMediaFromUrl($logo)->preservingOriginal()->toMediaCollection('logo');

        //
        //$person->company()->associate($company);

        if($user->exists){
            MailController::sendConfirmationEmail($user);
            alert()->success('Tu cuenta ha sido creada','Por favor revisar tu correo electrónico para confirmar tu email')->showConfirmButton('Ok', '#3085d6');
            return redirect()->route('login');
        }
        return redirect()->back()->with('error', 'Reintentar crear la cuenta nuevamente');

        //dispatch(new EmailConfirmationJob($user, 'Ha sido dado de alta a '));

        //$this->guard()->login($user);

        // if ($response = $this->registered($request, $user)) {
        //     return $response;
        // }

        // return $request->wantsJson()
        //             ? new Response('', 201)
        //             : redirect($this->redirectPath());
    }

    public function addLocation(Request $request, Address $address){
        if($request->get('address')  !=null  && $request->get('number') !=null && $request->get('town') !=null ){
            $town = Town::find($request->get('town'));
            $nomitetim = $request->get('address')."+".$request->get('number')."+".$town->name;
            $getLongLat = "https://nominatim.openstreetmap.org/?format=json&addressdetails=1&q={".$nomitetim."}&format=json&limit=1";
            $client = new \GuzzleHttp\Client();
            $response = $client->request('GET', $getLongLat);
            if($response->getStatusCode() == 200){
                $resp = json_decode($response->getBody());
                if ($resp !=null){
                    $address->latitud =$resp[0]->lat;
                    $address->longitud = $resp[0]->lon;
                    $address->save();
                }
            }
        }
    }

    public function addLocationcompany(Request $request, Address $address){
        if($request->get('companyaddress')  !=null  && $request->get('companynumber') !=null && $request->get('townCompany') !=null ){
            $town = Town::find($request->get('townCompany'));
            $nomitetim = $request->get('companyaddress')."+".$request->get('companynumber')."+".$town->name;
            $getLongLat = "https://nominatim.openstreetmap.org/?format=json&addressdetails=1&q={".$nomitetim."}&format=json&limit=1";
            $client = new \GuzzleHttp\Client();
            $response = $client->request('GET', $getLongLat);
            if($response->getStatusCode() == 200){
                $resp = json_decode($response->getBody());
                if ($resp !=null){
                    $address->latitud =$resp[0]->lat;
                    $address->longitud = $resp[0]->lon;
                    $address->save();
                }
            }
        }
    }
}
