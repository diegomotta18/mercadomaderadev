<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NotifyWinnerAuctionSellerEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $auction_url;
    public $publication_url;
    public function __construct($auction_url, $publication_url)
    {
        //
        $this->auction_url = $auction_url;
        $this->publication_url = $publication_url;
    }


    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Subasta finalizada de un producto que publicaste')
                    ->view('_emails.notifyWinnerAuctionSellerEmail');
    }
}
