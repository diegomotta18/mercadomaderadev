<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class CommentBuyerEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $publication_title;
    public $comment_url;
    public $seller_name;
    public $comment_id;
    public function __construct($publication_title, $comment_url,$seller_name,$comment_id)
    {
        //
        $this->publication_title = $publication_title;
        $this->comment_url = $comment_url;
        $this->seller_name = $seller_name;
        $this->comment_id = $comment_id;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject("Tienes un mensaje nuevo por el producto que consultaste")
        ->view('_emails.commentBuyerEmail');
        }
}
