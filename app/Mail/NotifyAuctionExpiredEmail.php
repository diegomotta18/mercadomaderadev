<?php

namespace App\Mail;

use App\Auction;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NotifyAuctionExpiredEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $auction_url;
    public $publication_url;
    public function __construct($auction_url, $publication_url)
    {
        //
        $this->auction_url = $auction_url;
        $this->publication_url = $publication_url;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Finalizacion de la subasta a la que ofertaste')
                    ->view('_emails.notifyAuctionExpiredEmail');
    }
}
