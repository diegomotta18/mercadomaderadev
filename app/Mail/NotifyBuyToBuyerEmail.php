<?php

namespace App\Mail;

use App\Buy;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NotifyBuyToBuyerEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $buy;
    private $pdf;
    public function __construct($buy,$pdf)
    {
        //
        $this->buy = $buy;
        $this->pdf = $pdf;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Comprobante de compromiso de compra')
                    ->attachData($this->pdf->output(), "recibo-mercadomadera-".$this->buy->created_at->format('d-m-Y').".pdf")
                    ->view('_emails.notifyBuyToBuyerEmail');
    }
}
