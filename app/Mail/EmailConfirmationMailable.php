<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class EmailConfirmationMailable extends Mailable
{
    use Queueable, SerializesModels;


    /**
     * @var \App\User
     */
    public $user;

    /**
     * @var
     */
    public $message;

    /**
     * Create a new message instance.
     *
     * @param \App\User $user
     */
    public function __construct($message, $user)
    {
        $this->user = $user;
        $this->message = $message;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->message .config('app.name'))
        ->view('_emails.emailConfirmation');
    }
}


