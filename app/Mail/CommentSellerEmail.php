<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class CommentSellerEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $publication_title;
    public $comment_url;
    public $buyer_name;

    public function __construct($publication_title, $comment_url,$buyer_name)
    {
        //
        $this->publication_title = $publication_title;
        $this->comment_url = $comment_url;
        $this->buyer_name = $buyer_name;
    }


    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject("Tienes un mensaje nuevo por un producto que tienes en venta")
        ->view('_emails.commentSellerEmail');
     }
}
