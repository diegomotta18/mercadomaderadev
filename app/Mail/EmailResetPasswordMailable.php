<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class EmailResetPasswordMailable extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var
     */
    public $message;
    public $url;
    /**
     * Create a new message instance.
     *
     * @param \App\User $user
     */
    public function __construct($message, $url)
    {
        $this->message = $message;
        $this->url = $url;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->message .config('app.name'))
        ->view('_emails.emailResetPassword');
    }
}
