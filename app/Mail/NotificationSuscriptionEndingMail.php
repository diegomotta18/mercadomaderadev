<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NotificationSuscriptionEndingMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $restingDay;
    public $refplan;
    public $ends;
    public function __construct($restingDay,$refplan,$ends)
    {
        //
        $this->ends = $ends;
        $this->restingDay = $restingDay;
        $this->refplan = $refplan;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Suscripción premium cerca de finalizar')
                    ->view('_emails.notificationSuscriptionEndingMail');    }
}
