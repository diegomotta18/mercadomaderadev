<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kind extends Model
{
    //
    protected $table = 'kinds';

    protected $fillable = [
        'id',
        'name',
        'description',
        'type_wood_id'

    ];

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    public function attribute()
    {
        return $this->belongsTo('App\Attribute');
    }
    
    public function scopeName($query,$name){
        if(trim($name)!=null){
            $query->where('name', 'LIKE','%' . $name . '%');
        }
    }
}
