<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Attribute extends Model
{
    //
    use SoftDeletes;

    protected $table = 'attributes';

    protected $fillable = [
        'id',
        'name',
        'description'
    ];
    
    protected $dates = ['created_at','updated_at','deleted_at'];

    //disponibilidad
    public function availabity()
    {
        return $this->hasOne('App\Availability');
    }

    public function condition()
    {
        return $this->hasOne('App\Condition');
    }

    public function dimension()
    {
        return $this->hasOne('App\Dimension');
    }

    public function kind()
    {
        return $this->hasOne('App\Kind');
    }

    public function typeWood()
    {
        return $this->hasOne('App\TypeWood');
    }
}
