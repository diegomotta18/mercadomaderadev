<?php

namespace App\Console\Commands;

use App\Mail\NotificationSuscriptionEndingMail;
use App\PlanSuscription;
use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class SuscriptionEndingCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'suscription:ending';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command verify the suscriptions ending';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $suscriptions = PlanSuscription::all();
        if ($suscriptions != null) {
            foreach ($suscriptions->chunk(10) as $suscription_chuck) {
                foreach ($suscription_chuck as $suscription) {
                    if ($suscription->isEndingPeriodo()) {
                        $user_id = $suscription->user_id;
                        $user = User::find($user_id);
                        $days = $suscription->daysToFinish();
                        Mail::to($user->email)->send(new NotificationSuscriptionEndingMail($days, route("plans.me"),$suscription->ends_at));
                    }
                }
            }
        }
        echo "SuscriptionEnding was exec" . "\n";

    }
}
