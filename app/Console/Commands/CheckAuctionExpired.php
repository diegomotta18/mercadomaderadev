<?php

namespace App\Console\Commands;

use App\Auction;
use App\Bidder;
use App\Mail\NotifyAuctionExpiredEmail;
use App\Mail\NotifyWinnerAuctionEmail;
use App\Mail\NotifyWinnerAuctionSellerEmail;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class CheckAuctionExpired extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:auction_finished';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command stop in the finish date of the auction';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $auctions = Auction::all();
        if ($auctions != null) {
            foreach ($auctions->chunk(10) as $t) {
                foreach ($t as $auction) {
                    if ($auction->isExpired()) {
                        foreach ($auction->bidders->unique("user_id") as $bidder) {
                            Mail::to($bidder->user->email)->send(new NotifyAuctionExpiredEmail(route("view.publications.show", $auction->publication->id), route('view.publications.auction', $auction->publication->id)));
                        }
                        $bidder = Bidder::where('auction_id', $auction->id)->orderBy('ammount', 'desc')->first();
                        $auction->user_id = $bidder->user_id;
                        Mail::to($auction->user->email)->send(new NotifyWinnerAuctionEmail(route("view.publications.show", $auction->publication->id), route('view.publications.auction', $auction->publication->id)));
                        Mail::to($auction->publication->user->email)->send(new NotifyWinnerAuctionSellerEmail(route("view.publications.show", $auction->publication->id), route('view.publications.auction', $auction->publication->id)));
                        $auction->save();
                    }
                }
            }
        }
        echo "Auction was updated" . "\n";
    }
}
