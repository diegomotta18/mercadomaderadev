<?php

namespace App\Console;

use App\Console\Commands\CheckAuctionExpired;
use App\Console\Commands\SuscriptionEndingCommand;
use App\Console\Commands\SuscriptionExpiredCommand;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
        CheckAuctionExpired::class,
        SuscriptionEndingCommand::class,
        SuscriptionExpiredCommand::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('check:auction_finished')->dailyAt('07:45');
        $schedule->command('suscription:ending')->dailyAt('00:00');
        $schedule->command('suscription:expired')->dailyAt('00:05');

    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
