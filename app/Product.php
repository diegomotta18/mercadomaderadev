<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Unit;

class Product extends Model
{
    //
    use SoftDeletes;

    protected $table = 'products';

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    protected $fillable = [
        'subcategory_id',
        'dimension_id',
        'condition_id',
        'availability_id',
        'type_wood_id',
        'type_product_id',
        'qualitative_parameter_id',
        'way_pay_id',
        'shipping_type_id',
        'total',
        'price',
    ];

    public function subcategory()
    {
        return $this->belongsTo('App\Subcategory','subcategory_id');
    }

    public function dimension()
    {
        return $this->belongsTo('App\Dimension','dimension_id');
    }

    public function condition()
    {
        return $this->belongsTo('App\Condition','condition_id');
    }

    public function availability()
    {
        return $this->belongsTo('App\Availability','availability_id');
    }

    public function publication()
    {
        return $this->belongsTo('App\Publication','publication_id');
    }

    public function kind()
    {
        return $this->belongsTo('App\Kind','kind_id');
    }

    public function typeWood()
    {
        return $this->belongsTo('App\TypeWood','type_wood_id');
    }

    public function typeProduct()
    {
        return $this->belongsTo('App\TypeProduct','type_product_id');
    }

    public function qualitativeParameter()
    {
        return $this->belongsTo('App\QualitativeParameter','qualitative_parameter_id');
    }

    public function shippingType()
    {
        return $this->belongsTo('App\ShippingType','shipping_type_id');
    }


    public function wayPay()
    {
        return $this->belongsTo('App\WayPay','way_pay_id');
    }

    public function  setTotalAttribute($value)
    {

        $this->attributes['total'] =  floatval(($this->calculatePriceTotal(UNIT::PIETABLAR) *$value));
    }

    public function  calculatePriceTotal($unit): float
    {

        $dimension = $this->dimension()->first();

        if (
            $dimension->actualThickness == null ||
            $dimension->actualWidth == null ||
            $dimension->actualLength === null
        ) {
            return 0;
        }

        switch ($unit) {
            case Unit::PIETABLAR:
                return (($dimension->actualThickness / Unit::ESPESOR) * ($dimension->actualLength /   Unit::LARGO) * ($dimension->actualWidth / Unit::ANCHO));
        }
    }
}
