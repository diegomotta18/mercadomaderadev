<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class QualitativeParameter extends Model
{
    //
    //
    use SoftDeletes;

    protected $table = 'qualitative_parameters';
    protected $fillable = [
        'id', 
        'calidad',
        'nudoMuerto', 
        'manchaHongo', 
        'secado',
        'humedadMax',
        'humedadMin', 
        'ubicacionTronco', 
        'grieta',
        'rajaduras',
        'cantoMuerto',
        'alaveos', 
        'combas', 
        'torceduras'
    ];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
}
