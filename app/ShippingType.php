<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShippingType extends Model
{
    //
    protected $table = 'shipping_types';

    protected $fillable = ['id','send','description'];
    
    protected $dates = ['created_at','updated_at','deleted_at'];
}
