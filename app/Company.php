<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\File;
use Spatie\MediaLibrary\Models\Media;
use App\ComisionistAgent;
use App\ComisionistAgentUser;
use App\Model\Address\Address;

class Company extends Model implements HasMedia
{
    //
    use SoftDeletes;
    use HasMediaTrait;

    protected $table = 'companies';

    protected $fillable = [
        'id',
        'name',
        'cuit',
        'compromise',
        'trajectory',
        'technologies',
        'others'
    ];
    protected $dates = ['created_at','updated_at','deleted_at'];


    public function contact()
    {
        return $this->belongsTo('App\Contact');
    }

    public function address()
    {
        return $this->belongsTo(Address::class);
    }

    //comisionistas
    public function users()
    {
        return $this->belongsToMany('App\User','company_user');
    }

    //usuario principal
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function addressLocation(){
        $address = $this->address;
        if ($address!=null){
            $country =  $this->address->country!= null ? $this->address->country->name : null;
            $province = $this->address->province!= null ? $this->address->province->name : null;
            $department =   $this->address->department!= null ? $this->address->department->name : null;
            $city =   $this->address->city!= null ? $this->address->city->name : null;
            $address = $country . " ". $province . " ". $department. " ". $city;
        }
        return  $address ;
    }

    public function addressComplete(){
        $addressComplete = $this->address;
        if($addressComplete != null){
            $addressComplete = $this->address->street != null? $this->address->street .  " ".$this->address->number: null ;
            if ( $this->address->floor != null && $this->address->dpto!= null){
                $addressComplete =  $addressComplete . " ". $this->address->floor." ".$this->address->dpto ;
            }
        }

        return $addressComplete;
    }


    public function comisionistAgentUser(){
        return $this->belongsTo('App\ComisionistAgentUser');
    }

    public function comisionistAgents()
    {
        return $this->belongsToMany(ComisionistAgent::class, 'company_comisionist_agent','company_id','person_id')->withTimestamps();
    }

    public function comisionistAgentUsers()
    {
        return $this->belongsToMany(ComisionistAgentUser::class, 'company_user','company_id','user_id')->withTimestamps();
    }

    public function registerMediaCollections()
    {
       $this->addMediaCollection('logo')->acceptsFile(function (File $file) {
                return $file->mimeType === 'image/jpeg' || $file->mimeType === 'image/png' || $file->mimeType === 'image/jpg';
            })
            ->registerMediaConversions(function (Media $media) {
                $this->addMediaConversion('thumb')
                    ->fit(Manipulations::FIT_CROP, 120, 120)->nonQueued();
            });

        $this->addMediaCollection('companies')->acceptsFile(function (File $file) {
                return $file->mimeType === 'image/jpeg' || $file->mimeType === 'image/png' || $file->mimeType === 'image/jpg';
            })
            ->registerMediaConversions(function (Media $media) {
                $this->addMediaConversion('thumb')
                    ->fit(Manipulations::FIT_CROP, 120, 120)->nonQueued();
            });
    }
}
