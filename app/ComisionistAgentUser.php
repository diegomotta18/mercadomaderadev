<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ComisionistAgentUser extends Model
{
    //
    use SoftDeletes;
    protected $table = 'comisionist_users';

    protected $fillable = [
        'id',
        'user_id'
    ];

    protected $dates = ['created_at','updated_at','deleted_at'];

    public function companies()
    {
        return $this->hasMany('App\Company');
    }

    public function user(){
        return $this->hasOne('App\User');
    }
}
