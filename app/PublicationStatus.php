<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PublicationStatus extends Model
{

    const ACTIVED = 'actived';
    const PAUSED = 'paused';
    const PENDING = 'pending';
    const INACTIVED = 'inactived';
    const APPROVED = 'approved';
    const DISAPPROVED = 'disapproved';
    const REVIEW = 'review';
    const OBSERVATION = 'observation';

    public static function str($status) {
        switch ($status) {
            case PublicationStatus::ACTIVED:
                return "Activo";
            case PublicationStatus::PAUSED:
                return "Pausado";
            case PublicationStatus::PENDING:
                return "Pendiente";
            case PublicationStatus::INACTIVED:
                return "Inactivo";
            case PublicationStatus::APPROVED:
                return "Aprobado";
            case PublicationStatus::DISAPPROVED:
                return "No aprobado";
            case PublicationStatus::REVIEW:
                return "Revisión";
            case PublicationStatus::OBSERVATION:
                    return "En observación";
          default:
                return null;
        }
    }

    public static function getStatus($status) {
        switch ($status) {
            case PublicationStatus::ACTIVED:
                return "actived";
            case PublicationStatus::PAUSED:
                return "paused";
            case PublicationStatus::PENDING:
                return "pending";
            case PublicationStatus::INACTIVED:
                return "inactived";
            case PublicationStatus::APPROVED:
                return "approved";
            case PublicationStatus::DISAPPROVED:
                return "disapproved";
            case PublicationStatus::REVIEW:
                return "review";
            case PublicationStatus::OBSERVATION:
                    return "observation";
          default:
                return null;
        }
    }
}
