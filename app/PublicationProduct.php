<?php

namespace App;



class PublicationProduct  extends \Illuminate\Database\Eloquent\Relations\Pivot
{
    //
    protected $table = 'publication_product';

    public function product()
    {
        return $this->belongsTo('App\Product','product_id');
    }




}
