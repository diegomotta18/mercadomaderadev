<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PublicationOrder extends Model
{
    //
    const HIGHER_PRICE = 'higer_price';
    const LOWER_PRICE = 'lower_price';
    const MOST_RELEVANT = 'most_relevant';
    const MOST_SCORE = 'most_score';
    const LOWER_SCORE = 'lower_score';

}
