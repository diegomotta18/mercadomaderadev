<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Calification extends Model
{

    protected $table = 'califications';

    protected $fillable = [
        'id',
        'publication_id',
        'description',
        'calification',
        'phrase',
        'buy_id'
    ];

    protected $dates = ['created_at','updated_at','deleted_at'];


    public function buy()
    {
        return $this->belongsTo('App\Buy');
    }
}
