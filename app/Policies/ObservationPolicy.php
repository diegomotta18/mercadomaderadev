<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ObservationPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }


    public function show(User $user)
    {
        //
        if ($user->can('Ver.observación')) {
            return true;
        }

        return false;
    }


    public function create(User $user)
    {
        //
        if ($user->can('Crear.observación')) {
            return true;
        }

        return false;
    }

    public function store(User $user)
    {
        //
        if ($user->can('Crear.observación')) {
            return true;
        }

        return false;
    }

    public function update(User $user)
    {
        //
        if ($user->can('Modificar.observación')) {
            return true;
        }

        return false;
    }

    public function destroy(User $user)
    {
        //
        if ($user->can('Eliminar.observación')) {
            return true;
        }

        return false;
    }

    public function check(User $user)
    {
        //
        if ($user->can('Aceptar.observación')) {
            return true;
        }

        return false;
    }
}
