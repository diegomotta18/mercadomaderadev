<?php

namespace App\Policies;

use App\TypeProduct;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class TypeProductPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\User  $user
     * @param  \App\TypeProduct  $typeProduct
     * @return mixed
     */
    public function view(User $user)
    {
        //
    }
    public function index(User $user)
    {
        //
        if ($user->can('Listar.tipos.de.productos')) {
            return true;
        }

        return false;
    }



    /**
     * Determine whether the user can create models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
        if ($user->can('Crear.tipo.de.producto')) {
            return true;
        }

        return false;
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\User  $user
     * @param  \App\TypeProduct  $typeProduct
     * @return mixed
     */
    public function update(User $user)
    {
        if ($user->can('Modificar.tipo.de.producto')) {
            return true;
        }

        return false;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\TypeProduct  $typeProduct
     * @return mixed
     */
    public function delete(User $user)
    {
        if ($user->can('Eliminar.tipo.de.producto')) {
            return true;
        }

        return false;
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\User  $user
     * @param  \App\TypeProduct  $typeProduct
     * @return mixed
     */
    public function restore(User $user)
    {
        //
        if ($user->can('Restaurar.tipo.de.producto')) {
            return true;
        }

        return false;
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\TypeProduct  $typeProduct
     * @return mixed
     */
    public function forceDelete(User $user)
    {
        //
    }
}
