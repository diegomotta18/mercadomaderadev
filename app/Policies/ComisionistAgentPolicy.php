<?php

namespace App\Policies;

use App\User;
use  App\ComisionistAgent;
use App\Company;
use Illuminate\Auth\Access\HandlesAuthorization;

class ComisionistAgentPolicy
{
    use HandlesAuthorization;

    public function index(User $user)
    {
        //
        if ($user === null) {
            return false;
        }
        if ($user->can('Listar.comisionistas')) {
            return true;
        }

        return false;
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\User  $user

     * @return mixed
     */
    public function view(User $user,ComisionistAgent $comisionistAgent)
    {
        //
        if ($user === null) {
            return false;
        }
        
        if ($user->can('Ver.comisionista')) {
            $company = Company::where('user_id',auth()->user()->id)->first();
            return $comisionistAgent->company_id == $company->id;
        }

        return false;
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
        if ($user === null) {
            return false;
        }
        
        if ($user->can('Crear.comisionista')) {
            return true;
        }

        return false;
    }

     /**
     * Determine whether the user can create models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function edit(User $user,ComisionistAgent $comisionistAgent)
    {
        if ($user === null) {
            return false;
        }

        if ($user->can('Modificar.comisionista')) {
            $company = Company::where('user_id',auth()->user()->id)->first();
                        
            return  $company->comisionistAgents->contains($comisionistAgent->id);
        }

        return false;
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\User  $user
     * @param  \App\ComisionistAgent  $comisionistAgent
     * @return mixed
     */
    public function update(User $user,ComisionistAgent $comisionistAgent)
    {
        if ($user === null) {
            return false;
        }
        if ($user->can('Modificar.comisionista')) {
            $company = Company::where('user_id',auth()->user()->id)->first();
            return  $company->comisionistAgents->contains($comisionistAgent->id);
        }
        return false;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\ComisionistAgent  $comisionistAgent
     * @return mixed
     */
    public function delete(User $user,ComisionistAgent  $comisionistAgent)
    {
        if ($user === null) {
            return false;
        }
        if ($user->can('Eliminar.comisionista')) {
            $company = Company::where('user_id',auth()->user()->id)->first();
            return  $company->comisionistAgents->contains($comisionistAgent->id);
        }
        return false;
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function restore(User $user)
    {
        //
        if ($user === null) {
            return false;
        }
        if ($user->can('Restaurar.comisionista')) {
            return true;
        }

        return false;
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\ComisionistAgent  $comisionistAgent
     * @return mixed
     */
    public function forceDelete(User $user, ComisionistAgent $comisionistAgent)
    {
        //
        if ($user === null) {
            return false;
        }
        if ($user->can('Eliminar.comisionista')) {
            return true;
        }

        return false;
    }
}
