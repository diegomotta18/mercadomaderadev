<?php

namespace App\Policies;

use App\TypeWood;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class TypeWoodPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\User  $user
     * @param  \App\TypeWood  $TypeWood
     * @return mixed
     */
    public function view(User $user, TypeWood $TypeWood)
    {
        //
    }
    public function index(User $user)
    {
        //
        if ($user->can('Listar.tipos.de.maderas')) {
            return true;
        }

        return false;
    }



    /**
     * Determine whether the user can create models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
        if ($user->can('Crear.tipo.de.madera')) {
            return true;
        }

        return false;
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\User  $user
     * @param  \App\TypeWood  $TypeWood
     * @return mixed
     */
    public function update(User $user)
    {
        if ($user->can('Modificar.tipo.de.madera')) {
            return true;
        }

        return false;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\TypeWood  $TypeWood
     * @return mixed
     */
    public function delete(User $user, TypeWood $TypeWood)
    {
        if ($user->can('Eliminar.tipo.de.madera')) {
            return true;
        }

        return false;
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\User  $user
     * @param  \App\TypeWood  $TypeWood
     * @return mixed
     */
    public function restore(User $user, TypeWood $TypeWood)
    {
        //
        if ($user->can('Restaurar.tipo.de.madera')) {
            return true;
        }

        return false;
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\TypeWood  $TypeWood
     * @return mixed
     */
    public function forceDelete(User $user, TypeWood $TypeWood)
    {
        //
    }
}
