<?php

namespace App\Policies;

use App\User;

use Illuminate\Auth\Access\HandlesAuthorization;

class ProfileCompanyPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function show(User $user)
    {
        //
        if ($user->can('Ver.perfil.empresa')) {
            return true;
        }

        return false;
    }

    public function update(User $user)
    {
        //
        if ($user->can('Modificar.perfil.empresa')) {
            return true;
        }

        return false;
    }

    
}
