<?php

namespace App\Policies;

use App\Offert;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class OffertPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function index(User $user)
    {
        //
        if ($user->can('Listar.ofertas')) {
            return true;
        }

        return false;
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\User  $user
     * @param  \App\Offert  $offert
     * @return mixed
     */
    public function view(User $user, Offert $offert)
    {
        //
        
        if ($user->can('Ver.oferta')) {
            return true;
        }

        return false;
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
        if ($user === null) {
            return false;
        }
        
        if ($user->can('Crear.oferta')) {
            return true;
        }

        return false;
    }

     /**
     * Determine whether the user can create models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function edit(User $user,Offert $offert)
    {
        //
        if ($user === null) {
            return false;
        }

        if ($user->can('Modificar.oferta')) {
            return $user->id == $offert->user_id;
        }

        return false;
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\User  $user
     * @param  \App\Offert  $offert
     * @return mixed
     */
    public function update(User $user, Offert $offert)
    {
        //
        if ($user === null) {
            return false;
        }

        if ($user->can('Modificar.oferta')) {
            return $user->id == $offert->user_id;
        }

        return false;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\Offert  $offert
     * @return mixed
     */
    public function delete(User $user, Offert $offert)
    {
        //
        if ($user->can('Eliminar.oferta')) {
            return true;
        }

        return false;
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\User  $user
     * @param  \App\Offert  $offert
     * @return mixed
     */
    public function restore(User $user, Offert $offert)
    {
        //
        if ($user->can('Restaurar.oferta')) {
            return true;
        }

        return false;
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\Offert  $offert
     * @return mixed
     */
    public function forceDelete(User $user, Offert $offert)
    {
        //
        if ($user->can('Eliminar.oferta')) {
            return true;
        }

        return false;
    }
}
