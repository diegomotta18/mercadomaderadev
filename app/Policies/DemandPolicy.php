<?php

namespace App\Policies;

use App\Demand;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class DemandPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function index(User $user)
    {
        //
        if ($user->can('Listar.productos')) {
            return true;
        }

        return false;
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\User  $user
     * @param  \App\Demand  $demands
     * @return mixed
     */
    public function view(User $user, Demand $demands)
    {
        //
        if ($user->can('Ver.demanda')) {
            return true;
        }

        return false;
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
        if ($user === null) {
            return false;
        }

        if ($user->can('Crear.demanda')) {
            return true;
        }

        return false;
    }

     /**
     * Determine whether the user can create models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function edit(User $user,Demand $demands)
    {
        //
        if ($user === null) {
            return false;
        }

        if ($user->can('Modificar.demanda')) {
            return $user->id == $demands->user_id;
        }

        return false;
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\User  $user
     * @param  \App\Demand  $demands
     * @return mixed
     */
    public function update(User $user, Demand $demands)
    {
        //
        if ($user === null) {
            return false;
        }

        if ($user->can('Modificar.demanda')) {
            return $user->id == $demands->user_id;
        }

        return false;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\Demand  $demands
     * @return mixed
     */
    public function delete(User $user, Demand $demands)
    {
        //
        if ($user->can('Eliminar.demanda')) {
            return true;
        }

        return false;
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\User  $user
     * @param  \App\Demand  $demands
     * @return mixed
     */
    public function restore(User $user, Demand $demands)
    {
        //
        if ($user->can('Restaurar.demanda')) {
            return true;
        }

        return false;
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\Demand  $demands
     * @return mixed
     */
    public function forceDelete(User $user, Demand $demands)
    {
        //
        if ($user->can('Eliminar.demanda')) {
            return true;
        }

        return false;
    }
}
