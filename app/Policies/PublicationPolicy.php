<?php

namespace App\Policies;

use App\Publication;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PublicationPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function index(User $user)
    {
        //
        if ($user->can('Listar.publicaciones')) {
            return true;
        }

        return false;
    }
    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\User  $user
     * @param  \App\Publication  $publication
     * @return mixed
     */
    public function view(User $user, Publication $publication)
    {
        //
        if ($user->can('Ver.publicacion')) {
            return $publication->user_id == $user->id;
        }

        return false;
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
        if ($user === null) {
            return false;
        }

        if ($user->can('Crear.publicacion')) {
            return true;
        }

        return false;
    }
    /**
     * Determine whether the user can create models.
     *
     * @param  \App\User  $user
     * @return mixed
     */

    public function edit(User $user,Publication $publication)
    {
        //
        if ($user === null) {
            return false;
        }

        if ($user->can('Modificar.publicacion')) {
            return $publication->user_id == $user->id;
        }

        return false;
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\User  $user
     * @param  \App\Publication  $publication
     * @return mixed
     */
    public function update(User $user,Publication $publication)
    {
        //
        if ($user === null) {
            return false;
        }

        if ($user->can('Modificar.publicacion')) {
            return $publication->user_id == $user->id;
        }

        return false;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\Publication  $publication
     * @return mixed
     */
    public function delete(User $user,Publication $publication)
    {
        //
        if ($user->can('Eliminar.publicacion')) {
            return $publication->user_id == $user->id;
        }

        return false;
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\User  $user
     * @param  \App\Publication  $publication
     * @return mixed
     */
    public function restore(User $user,Publication $publication)
    {
        //
        if ($user->can('Restaurar.publicacion')) {
            return $publication->user_id == $user->id;
        }

        return false;
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\Publication  $publication
     * @return mixed
     */
    public function forceDelete(User $user)
    {
        //
        if ($user->can('Eliminar.publicacion.permantemente')) {
            return true;
        }

        return false;
    }

    public function updateStatus(User $user,Publication $publication)
    {
        //
        if ($user === null) {
            return false;
        }

        if ($user->can('Cambiar.estado.publicacion')) {
            return $publication->user_id == $user->id;
        }

        return false;
    }
}
