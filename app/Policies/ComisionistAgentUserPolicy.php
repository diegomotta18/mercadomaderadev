<?php

namespace App\Policies;

use App\User;
use  App\ComisionistAgentUser;
use App\Company;
use Illuminate\Auth\Access\HandlesAuthorization;

class ComisionistAgentUserPolicy
{
    use HandlesAuthorization;

    public function index(User $user)
    {
        //
        if ($user === null) {
            return false;
        }
        if ($user->can('Listar.usuarios.comisionistas')) {
            return true;
        }

        return false;
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\User  $user

     * @return mixed
     */
    public function view(User $user,ComisionistAgentUser $comisionistAgentUser)
    {
        //
        if ($user === null) {
            return false;
        }
        
        if ($user->can('Ver.usuario.comisionista')) {
            $company = Company::where('user_id',auth()->user()->id)->first();
            return $comisionistAgentUser->company_id == $company->id;
        }

        return false;
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
        if ($user === null) {
            return false;
        }
        
        if ($user->can('Crear.usuario.comisionista')) {
            return true;
        }

        return false;
    }

     /**
     * Determine whether the user can create models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function edit(User $user,ComisionistAgentUser $comisionistAgentUser)
    {
        if ($user === null) {
            return false;
        }

        if ($user->can('Modificar.usuario.comisionista')) {
            $company = Company::where('user_id',auth()->user()->id)->first();
                        
            return  $company->users->contains($comisionistAgentUser->id);
        }

        return false;
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\User  $user
     * @param  \App\ComisionistAgentUser  $comisionistAgentUser
     * @return mixed
     */
    public function update(User $user,ComisionistAgentUser $comisionistAgentUser)
    {
        if ($user === null) {
            return false;
        }
        if ($user->can('Modificar.usuario.comisionista')) {
            $company = Company::where('user_id',auth()->user()->id)->first();
            return  $company->users->contains($comisionistAgentUser->id);
        }
        return false;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\ComisionistAgentUser  $comisionistAgentUser
     * @return mixed
     */
    public function delete(User $user,ComisionistAgentUser  $comisionistAgentUser)
    {
        if ($user === null) {
            return false;
        }
        if ($user->can('Eliminar.usuario.comisionista')) {
            $company = Company::where('user_id',auth()->user()->id)->first();
            return  $company->users->contains($comisionistAgentUser->id);
        }
        return false;
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function restore(User $user)
    {
        //
        if ($user === null) {
            return false;
        }
        if ($user->can('Restaurar.usuario.comisionista')) {
            return true;
        }

        return false;
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\ComisionistAgentUser  $comisionistAgentUser
     * @return mixed
     */
    public function forceDelete(User $user, ComisionistAgentUser $comisionistAgentUser)
    {
        //
        if ($user === null) {
            return false;
        }
        if ($user->can('Eliminar.usuario.comisionista')) {
            return true;
        }

        return false;
    }
}
