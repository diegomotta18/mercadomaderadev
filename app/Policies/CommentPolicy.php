<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CommentPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    public function index(User $user)
    {
        //
        if ($user->can('Listar.comentarios')) {
            return true;
        }

        return false;
    }
    
    public function show(User $user)
    {
        //
        if ($user->can('Ver.comentario')) {
            return true;
        }

        return false;
    }


    public function create(User $user)
    {
        //
        if ($user->can('Crear.comentario')) {
            return true;
        }

        return false;
    }

    public function store(User $user)
    {
        //
        if ($user->can('Crear.comentario')) {
            return true;
        }

        return false;
    }

    public function update(User $user)
    {
        //
        if ($user->can('Modificar.comentario')) {
            return true;
        }

        return false;
    }

    public function destroy(User $user)
    {
        //
        if ($user->can('Eliminar.comentario')) {
            return true;
        }

        return false;
    }

    
}
