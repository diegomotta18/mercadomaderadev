<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Auction extends Model
{
    //
    use SoftDeletes;

    protected $table = 'auctions';

    protected $fillable = [
        'id',
        'publication_id',
        'price_init',
        'quantity',
        'status',
        'finish_date',
        'user_id'
    ];
    protected $dates = ['created_at','updated_at','deleted_at','finish_date'];

    public function bidders(){
        return $this->hasMany('App\Bidder');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function publication(){
        return $this->belongsTo('App\Publication');
    }

    public function getFinishDateAttribute($value)
    {
        return Carbon::parse($value)->format('d/m/Y');
    }

    public function setFinishDateAttribute($value)
    {

        $this->attributes['finish_date'] = Carbon::createFromFormat('d/m/Y', $value);

    }
    public function isActived(){
        return $this->status == "actived";
    }
    public function isPaused(){
        return $this->status == "paused";
    }
    public function isFinished(){
        return $this->status == "finished";
    }

    public function isExpired()
    {
            if ((Carbon::today()->timestamp > Carbon::parse($this->finish_date)->timestamp) && $this->isActived()) {
                $this->status = "finished";
                $this->save();
                return true;
            }

            return false;

    }


    public function isWinner($id){
        if(($this->user_id != null)){
            if ($id == Bidder::latest()->first()->id) {
                return true;
            }
        }
        return false;
    }
}
