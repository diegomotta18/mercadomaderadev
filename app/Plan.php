<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Rinvex\Subscriptions\Models\Plan as ModelsPlan;

class Plan extends ModelsPlan
{
     public function scopeSearch($query, $search)
    {
        if (!isset($$search->name) && $search->name!= "") {
            $query->whereRaw("(
              UPPER(name) LIKE UPPER('%$search->name%')
           )");
        }
        return $query;
    }

}
