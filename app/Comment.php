<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comment extends Model
{
    //
    use SoftDeletes;
    protected $table = 'comments';

    protected $fillable = [
        'id',
        'comment',
        'subcomment',
        'user_id,',
        'publication_id',
        'owner_id'
    ];
    //usuario que realizo el comentario
    public function user(){
        return $this->belongsTo('App\User');
    }

    public function publication(){
        return $this->belongsTo('App\Publication');
    }
    //dueño del producto
    public function owner(){
        return $this->belongsTo('App\User');
    }

    protected $dates = ['created_at','updated_at','deleted_at'];
}
