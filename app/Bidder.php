<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Bidder extends Model
{
    //
    use SoftDeletes;

    protected $table = 'bidders';

    protected $fillable = [
        'id',
        'user_id',
        'auction_id',
        'ammount'
    ];

    protected $dates = ['created_at','updated_at','deleted_at'];


    public function user(){
        return $this->belongsTo('App\User');
    }

    public function auction(){
        return $this->belongsTo('App\Auction');
    }

    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('d/m/Y H:s a');
    }
}
