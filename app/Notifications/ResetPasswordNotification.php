<?php

namespace App\Notifications;

use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Lang;

class ResetPasswordNotification extends Notification
{
    /**
     * The password reset token.
     *
     * @var string
     */
    public $token;

    /**
     * Create a notification instance.
     *
     * @param  string  $token
     * @return void
     */
    public function __construct($token)
    {
        $this->token = $token;
    }

    /**
     * Get the notification's channels.
     *
     * @param  mixed  $notifiable
     * @return array|string
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Build the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {

            $url = url(route('password.reset', [
                'token' => $this->token,
                'email' => $notifiable->getEmailForPasswordReset(),
            ], false));
     

        //return MailController::sendResetPasswordEmail($notifiable->getEmailForPasswordReset(),$url);
        // alert()->success('Solicitud de restablecimiento de contraseña','Por favor revisa tu correo electrónico para restablecer la contraseña')->showConfirmButton('Ok', '#3085d6');
        // return redirect()->back();
        return (new MailMessage)
            ->subject(Lang::get('Restablecimiento de contraseña en Mercado Madera'))
            ->greeting('Solicitud para restablecer la contraseña')
            ->line(Lang::get('Estas recibiendo este email porque recibimos una solicitud para restablecer la contraseña de tu cuenta.'))
            ->action(Lang::get('Restablecer contraseña'), $url)
            ->line(Lang::get('Esta solicitud de restablecimiento de contraseña expira en :count minutos.', ['count' => config('auth.passwords.'.config('auth.defaults.passwords').'.expire')]))
            ->line(Lang::get('Si no realizaste una solicitud de restablecimiento de contraseña, no se requiere ninguna acción.'))
            ->salutation('Saludos, '. config('app.name'));
        }

}
