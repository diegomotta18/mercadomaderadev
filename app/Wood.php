<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wood extends  Product
{
    //
    public function dimension()
    {
        return $this->belongsTo('App\Dimension');
    }

    public function condition()
    {
        return $this->belongsTo('App\Condition');
    }

    public function availability()
    {
        return $this->belongsTo('App\Availability');
    }

    public function publication()
    {
        return $this->belongsTo('App\Publication');
    }

    public function kind()
    {
        return $this->belongsTo('App\Kind');
    }

    public function typeWood()
    {
        return $this->belongsTo('App\TypeWood');
    }

    public function typeProduct()
    {
        return $this->belongsTo('App\TypeProduct');
    }

    public function qualitativeParameter()
    {
        return $this->belongsTo('App\QualitativeParameter');
    }

}
