<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(App\Unit::class, function (Faker $faker,$params) {
    return [
        'name' => $faker->name(),
        'description' => $faker->paragraph(),
    ];
});