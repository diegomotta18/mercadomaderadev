<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Country;
use Faker\Generator as Faker;

$factory->define(App\Model\Address\Country::class, function (Faker $faker) {
    return [
        'name' => $faker->country,
    ];
});
