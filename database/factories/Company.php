<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(App\Company::class, function (Faker $faker,$params) {
    return [
        'name' => $faker->company,
        'cuit' => strval(rand(1, 9999999999)),
    ];
});