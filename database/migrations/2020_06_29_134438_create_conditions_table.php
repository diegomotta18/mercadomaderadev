<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConditionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('conditions', function (Blueprint $table) {
            $table->id();
            $table->integer('piece')->nullable();
            $table->float('weight')->nullable();
            $table->float('length')->nullable();
            $table->float('tall')->nullable();
            $table->float('width')->nullable();
            $table->enum('zunchos',['No','Yes'])->nullable();
            //envoltorio
            $table->enum('wrapping',['No','Yes'])->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('conditions');
    }
}
