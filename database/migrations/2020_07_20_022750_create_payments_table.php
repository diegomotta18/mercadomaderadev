<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->id();
            $table->string('payment_identification')->nullable();
            $table->string('payment_state')->nullable();
            $table->string('payment_type')->nullable();
            $table->boolean('pagado');
            $table->string('preference_id')->nullable();
            $table->foreignId('user_id')->nullable()->constrained();
            $table->integer('plan_id')->unsigned();
            $table->foreign('plan_id')->references('id')->on(config('rinvex.subscriptions.tables.plans'))
                  ->onDelete('cascade')->onUpdate('cascade'); 
            $table->integer('suscription_id')->unsigned();
            $table->foreign('suscription_id')->references('id')->on(config('rinvex.subscriptions.tables.plan_subscriptions'))
                  ->onDelete('cascade')->onUpdate('cascade');


            $table->timestamp('suscription_init')->nullable();
            $table->timestamp('suscription_finish')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
