<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->foreignId('subcategory_id')->nullable()->constrained();
            $table->foreignId('availability_id')->nullable()->constrained();
            $table->foreignId('condition_id')->nullable()->constrained();
            $table->foreignId('kind_id')->nullable()->constrained();
            $table->foreignId('dimension_id')->nullable()->constrained();
            $table->foreignId('type_wood_id')->nullable()->constrained();
            $table->foreignId('type_product_id')->nullable()->constrained();
            $table->foreignId('qualitative_parameter_id')->nullable()->constrained();
            $table->decimal('price',12,2)->nullable();
            $table->decimal('total',12,2)->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
