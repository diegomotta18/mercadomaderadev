<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDimensionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dimensions', function (Blueprint $table) {
            $table->id();
            //nominal
            $table->float('nominalThickness')->nullable();
            $table->float('nominalWidth')->nullable();
            $table->float('nominalLength')->nullable();
            //real
            $table->float('actualThickness')->nullable();
            $table->float('actualWidth')->nullable();
            $table->float('actualLength')->nullable();
            //es mas o menos
            $table->float('isMoreThickness')->nullable();
            $table->float('isLessThickness')->nullable();
            $table->float('isMoreWidth')->nullable();
            $table->float('isLessWidth')->nullable();
            $table->float('isMoreLength')->nullable();
            $table->float('isLessLength')->nullable();
            

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dimensions');
    }
}
