<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemBuysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_buys', function (Blueprint $table) {
            $table->id();
            $table->foreignId('publication_id')->nullable()->constrained();
            $table->decimal('price',11,2)->nullable();
            $table->string('quantity')->nullable();
            $table->string('description')->nullable();
            $table->foreignId('buyer_id')->nullable()->constrained('users','id');
            $table->foreignId('seller_id')->nullable()->constrained('users','id');
            $table->foreignId('buy_id')->nullable()->constrained('buys','id');
            $table->boolean('is_calificated')->default(false);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_buys');
    }
}
