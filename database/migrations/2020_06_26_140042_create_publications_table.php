<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePublicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('publications', function (Blueprint $table) {
            $table->id();
            $table->string('title')->nullable();
            $table->longText('description',25000)->nullable();
            $table->foreignId('company_id')->nullable()->constrained();
            $table->foreignId('user_id')->nullable()->constrained();
            $table->foreignId('contact_id')->nullable()->constrained();
            $table->foreignId('address_id')->nullable()->constrained();
            $table->enum('typePublication',['simple','offert','demand','auction']);
            $table->double('price')->nullable();
            $table->string('slug')->nullable();
            $table->enum('status',['actived','paused','pending','review','inactived','approved','disapproved','observation']);
            $table->integer('cantitude')->nullable();
            $table->string('unit')->nullable();
            $table->decimal('calification', 12,2)->default(0);

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('publications');
    }
}
