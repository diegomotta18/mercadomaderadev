<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQualitativeParametersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('qualitative_parameters', function (Blueprint $table) {
            $table->id();
            $table->enum('calidad', ['Primera', 'Segunda', 'Tercera'])->nullable();
            $table->enum('nudoMuerto',['Si','No','No a simple vista'])->nullable();
            $table->enum('nudo',['Si','No'])->nullable();
            $table->enum('manchaHongo',['No se nota','Se nota','Sin'])->nullable();
            $table->enum('secado',['Natural','Artificial','Sin especificar'])->nullable();
            $table->enum('humedadMax',['Indiferente',1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,'Verde'])->nullable();
            $table->enum('humedadMin',['Indiferente',1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,'Verde'])->nullable();
            $table->enum('ubicacionTronco',['Indiferente','Central','Lateral','Ambas','Sin especificar'])->nullable();
            $table->enum('grieta',['Indiferente','Si','No'])->nullable();
            $table->enum('rajaduras',['Indiferente','Si','No','No a simple vista'])->nullable();
            $table->enum('cantoMuerto',['Indiferente','Si','No'])->nullable();
            $table->float('alaveos')->nullable();
            $table->float('combas')->nullable();
            $table->float('torceduras')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('qualitative_parameters');
    }
}
