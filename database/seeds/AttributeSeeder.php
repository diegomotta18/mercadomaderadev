<?php

use App\Attribute;
use Illuminate\Database\Seeder;


class AttributeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        Attribute::create([
            "name"=>"Tipo de madera",
            "description"=>"",
            "model"=>"typeWood"
        ]);

        Attribute::create([
            "name"=>"Especie",
            "description"=>"",
            "model"=>"kind"
        ]);

        Attribute::create([
            "name"=>"Dimension",
            "description"=>"",
            "model"=>"dimension"
        ]);

        Attribute::create([
            "name"=>"Disponibilidad",
            "description"=>"",
            "model"=>"availabilty"
        ]);

        Attribute::create([
            "name"=>"Condición",
            "description"=>"",
            "model"=>"condition"
        ]);
    }
}