<?php

use App\Product;
use App\Publication;
use App\QualitativeParameter;
use App\ShippingType;
use App\Unit;
use Illuminate\Database\Seeder;

class PublicationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createPublicationWithOffertUser1();
        $this->createPublicationWithOffertUser2();
        $this->createPublicationWithOffertUser3();
        $this->createPublicationWithOffertUser4();
        $this->createPublicationWithOffertUser5();
        $this->createPublicationWithOffertUser6();
        $this->shippingTypeAddPublications();
    }

    private function createPublicationWithOffertUser1()
    {

        \Illuminate\Support\Facades\DB::table('SET FOREIGN_KEY_CHECKS=0;');
        \Illuminate\Support\Facades\DB::table('publication_product')->delete();
        \Illuminate\Support\Facades\DB::table('products')->delete();
        \Illuminate\Support\Facades\DB::table('publications')->delete();
        \Illuminate\Support\Facades\DB::table('SET FOREIGN_KEY_CHECKS=1;');
        //Disponibilidad

        $availibity_1 = \App\Availability::create([
            'cantitude' => 10,
            'availability' => 'limited',
            'unitMeasurement' => Unit::PIETABLAR,
            'measurementTerm' => null,
        ]);

        $availibity_2 = \App\Availability::create([
            'cantitude' => 10,
            'availability' => 'continuous',
            'unitMeasurement' => Unit::PIETABLAR,
            'measurementTerm' => null,
        ]);

        //Condiciones

        $condition = \App\Condition::create([
            'piece' =>1,
            'weight' =>1,
            'length' =>1,
            'tall' =>11,
            'zunchos'=> 'No',
            //envoltorio
            'wrapping' =>'Yes'
        ]);



        //dimensiones
        $dimension_1 = \App\Dimension::create([
            'actualThickness' => 50,
            'actualWidth' => 100,
            'actualLength' => 4000,
        ]);


        $dimension_2 = \App\Dimension::create([
            'actualThickness' => 30,
            'actualWidth' => 200,
            'actualLength' => 3000,
        ]);

        //kind
        $kind_1 = \App\Kind::find(1);
        $kind_2 = \App\Kind::find(2);
        $kind_3 = \App\Kind::find(3);
        $kind_4 = \App\Kind::find(4);
        $kind_5 = \App\Kind::find(5);
        $kind_6 = \App\Kind::find(6);
        $kind_7 = \App\Kind::find(7);
        $kind_8 = \App\Kind::find(8);
        $kind_9 = \App\Kind::find(9);
        $kind_10 = \App\Kind::find(10);
        $kind_11 = \App\Kind::find(11);
        $kind_12 = \App\Kind::find(12);
        $kind_13 = \App\Kind::find(13);
        $kind_14 = \App\Kind::find(14);

        //nativa 1 implantada 2
        $typeWood_1 = \App\TypeWood::find(1);
        $typeWood_2 = \App\TypeWood::find(2);

        $typeProduct_1 = \App\TypeProduct::find(1);

        $typeProduct_2 = \App\TypeProduct::find(2);
        $typeProduct_3 = \App\TypeProduct::find(3);
        $typeProduct_4 = \App\TypeProduct::find(4);
        $typeProduct_5 = \App\TypeProduct::find(5);
        $typeProduct_6 = \App\TypeProduct::find(6);
        $typeProduct_7 = \App\TypeProduct::find(7);
        $typeProduct_8 = \App\TypeProduct::find(8);
        $typeProduct_9 = \App\TypeProduct::find(9);
        $typeProduct_10 = \App\TypeProduct::find(10);
        $typeProduct_11 = \App\TypeProduct::find(11);

        //forma de pago

        $WayPay = \App\WayPay::create([
            'money' => '$',
            'ammount' => 100,
            'iva' => true,
        ]);

        $WayPay->paymentTypes()->attach([1,2,3]);
        $WayPay->save();


        $person_premium_1 = \App\Person::find(4);

        $qualitativeParameter = new QualitativeParameter();
        $qualitativeParameter->save();

        $product = new \App\Product();
        $product->subcategory_id = 1;
        $product->availability_id = $availibity_2->id;
        $product->condition_id = $condition->id;
        $product->kind_id = $kind_1->id;
        $product->dimension_id = $dimension_1->id;
        $product->type_wood_id = $typeWood_1->id;
        $product->type_product_id = $typeProduct_2->id;
        $product->way_pay_id = $WayPay->id;
        $product->price = 100;

        $product->total =  $product->price;
        $product->qualitative_parameter_id = $qualitativeParameter->id;
        $product->save();

        $publication = new \App\Publication;
        $publication->title ="Pino Creado por Seed product 1";
        $publication->description="Descripcion creado por el seed";
        $publication->company_id =$person_premium_1->company_id;
        $publication->user_id=$person_premium_1->user_id;
        $publication->contact_id=$person_premium_1->contact_id;
        $publication->address_id=$person_premium_1->address_id;
        $publication->typePublication = 'offert';
        $publication->status = 'approved';
        $publication->price = $product->price;
        $publication->total =  $product->total;
        $publication->save();
        $publication->products()->attach([$product->id]);
        $publication->save();


        $WayPay_2 = \App\WayPay::create([
            'money' => '$',
            'ammount' => 110,
            'iva' => true,
        ]);

        $WayPay_2->paymentTypes()->attach([1,3,4]);
        $WayPay_2->save();

        $qualitativeParameter = new QualitativeParameter();
        $qualitativeParameter->save();

        $product_2 = new \App\Product();
        $product_2->subcategory_id = 1;
        $product_2->availability_id = $availibity_1->id;
        $product_2->condition_id = $condition->id;
        $product_2->kind_id = $kind_2->id;
        $product_2->dimension_id = $dimension_1->id;
        $product_2->type_wood_id = $typeWood_2->id;
        $product_2->type_product_id = $typeProduct_3->id;
        $product_2->way_pay_id = $WayPay_2->id;

        $product_2->price = 110;
        $product_2->total =  $product_2->price;
        $product_2->qualitative_parameter_id = $qualitativeParameter->id;

        $product_2->save();

        $publication_2 = new \App\Publication;
        $publication_2->title ="Pino Creado por Seed product 2";
        $publication_2->description="Descripcion 2 creado por el seed";
        $publication_2->company_id =$person_premium_1->company_id;
        $publication_2->user_id=$person_premium_1->user_id;
        $publication_2->contact_id=$person_premium_1->contact_id;
        $publication_2->address_id=$person_premium_1->address_id;
        $publication_2->typePublication = 'offert';
        $publication_2->status = 'approved';
        $publication_2->price = $product_2->price;
        $publication_2->total =  $product_2->total;
        $publication_2->save();
        $publication_2->products()->attach([$product_2->id]);
        $publication_2->save();


        $WayPay_3 = \App\WayPay::create([
            'money' => '$',
            'ammount' => 200,
            'iva' => true,
        ]);

        $WayPay_3->paymentTypes()->attach([1]);
        $WayPay_3->save();

        $qualitativeParameter = new QualitativeParameter();
        $qualitativeParameter->save();

        $product_3 = new \App\Product();
        $product_3->subcategory_id = 1;
        $product_3->availability_id = $availibity_2->id;
        $product_3->condition_id = $condition->id;
        $product_3->kind_id = $kind_2->id;
        $product_3->dimension_id = $dimension_1->id;
        $product_3->type_wood_id = $typeWood_1->id;
        $product_3->type_product_id = $typeProduct_3->id;
        $product_3->way_pay_id = $WayPay_3->id;
        $product_3->price = 200;
        $product_3->total =  $product_3->price;
        $product_3->qualitative_parameter_id = $qualitativeParameter->id;

        $product_3->save();

        $publication_3 = new \App\Publication;
        $publication_3->title ="Pino Creado por Seed product 3";
        $publication_3->description="Descripcion 3 creado por el seed";
        $publication_3->company_id =$person_premium_1->company_id;
        $publication_3->user_id=$person_premium_1->user_id;
        $publication_3->contact_id=$person_premium_1->contact_id;
        $publication_3->address_id=$person_premium_1->address_id;
        $publication_3->typePublication = 'offert';
        $publication_3->status = 'approved';
        $publication_3->price = $product_3->price;
        $publication_3->total =  $product_3->total;
        $publication_3->save();
        $publication_3->products()->attach([$product_3->id]);
        $publication_3->save();


        $WayPay_4 = \App\WayPay::create([
            'money' => '$',
            'ammount' => 250,
            'iva' => true,
        ]);

        $WayPay_4->paymentTypes()->attach([2]);
        $WayPay_4->save();

        $qualitativeParameter = new QualitativeParameter();
        $qualitativeParameter->save();

        $product_4 = new \App\Product();
        $product_4->subcategory_id = 1;
        $product_4->availability_id = $availibity_1->id;
        $product_4->condition_id = $condition->id;
        $product_4->kind_id = $kind_2->id;
        $product_4->dimension_id = $dimension_1->id;
        $product_4->type_wood_id = $typeWood_1->id;
        $product_4->type_product_id = $typeProduct_4->id;
        $product_4->way_pay_id = $WayPay_4->id;
        $product_4->price = 250;
        $product_4->total =  $product_4->price;
        $product_4->qualitative_parameter_id = $qualitativeParameter->id;
        $product_4->save();

        $publication_4 = new \App\Publication;
        $publication_4->title ="Algarrobo Creado por Seed product 4";
        $publication_4->description="Descripcion 4 creado por el seed";
        $publication_4->company_id =$person_premium_1->company_id;
        $publication_4->user_id=$person_premium_1->user_id;
        $publication_4->contact_id=$person_premium_1->contact_id;
        $publication_4->address_id=$person_premium_1->address_id;
        $publication_4->typePublication = 'offert';
        $publication_4->status = 'approved';
        $publication_4->price = $product_4->price;
        $publication_4->total =  $product_4->total;
        $publication_4->save();
        $publication_4->products()->attach([$product_4->id]);
        $publication_4->save();


        $WayPay_5 = \App\WayPay::create([
            'money' => '$',
            'ammount' => 450,
            'iva' => true,
        ]);

        $WayPay_5->paymentTypes()->attach([2]);
        $WayPay_5->save();

        $qualitativeParameter = new QualitativeParameter();
        $qualitativeParameter->save();

        $product_5 = new \App\Product();
        $product_5->subcategory_id = 1;
        $product_5->availability_id = $availibity_2->id;
        $product_5->condition_id = $condition->id;
        $product_5->kind_id = $kind_2->id;
        $product_5->dimension_id = $dimension_1->id;
        $product_5->type_wood_id = $typeWood_1->id;
        $product_5->type_product_id = $typeProduct_6->id;
        $product_5->way_pay_id = $WayPay_5->id;
        $product_5->price = 450;
        $product_5->total =  $product_5->price;
        $product_5->qualitative_parameter_id = $qualitativeParameter->id;
        $product_5->save();

        $publication_5 = new \App\Publication;
        $publication_5->title ="Algarrobo Creado por Seed product 5";
        $publication_5->description="Descripcion 5 creado por el seed";
        $publication_5->company_id =$person_premium_1->company_id;
        $publication_5->user_id=$person_premium_1->user_id;
        $publication_5->contact_id=$person_premium_1->contact_id;
        $publication_5->address_id=$person_premium_1->address_id;
        $publication_5->typePublication = 'offert';
        $publication_5->status = 'approved';
        $publication_5->price = $product_5->price;
        $publication_5->total =  $product_5->total;
        $publication_5->save();
        $publication_5->products()->attach([$product_5->id]);
        $publication_5->save();

        $WayPay_6 = \App\WayPay::create([
            'money' => '$',
            'ammount' => 402,
            'iva' => true,
        ]);

        $WayPay_6->paymentTypes()->attach([1,2]);
        $WayPay_6->save();

        $qualitativeParameter = new QualitativeParameter();
        $qualitativeParameter->save();

        $product_6 = new \App\Product();
        $product_6->subcategory_id = 1;
        $product_6->availability_id = $availibity_1->id;
        $product_6->condition_id = $condition->id;
        $product_6->kind_id = $kind_2->id;
        $product_6->dimension_id = $dimension_1->id;
        $product_6->type_wood_id = $typeWood_1->id;
        $product_6->type_product_id = $typeProduct_7->id;
        $product_6->way_pay_id = $WayPay_6->id;
        $product_6->qualitative_parameter_id = $qualitativeParameter->id;
        $product_6->price = 402;
        $product_6->total =  $product_6->price;
        $product_6->save();

        $publication_6 = new \App\Publication;
        $publication_6->title ="Tirante Creado por Seed product 6";
        $publication_6->description="Descripcion 6 creado por el seed";
        $publication_6->company_id =$person_premium_1->company_id;
        $publication_6->user_id=$person_premium_1->user_id;
        $publication_6->contact_id=$person_premium_1->contact_id;
        $publication_6->address_id=$person_premium_1->address_id;
        $publication_6->typePublication = 'offert';
        $publication_6->status = 'approved';
        $publication_6->price = $product_6->price;
        $publication_6->total =  $product_6->total;
        $publication_6->save();
        $publication_6->products()->attach([$product_6->id]);
        $publication_6->save();


        $WayPay_8 = \App\WayPay::create([
            'money' => '$',
            'ammount' => 375,
            'iva' => true,
        ]);

        $WayPay_8->paymentTypes()->attach([2]);
        $WayPay_8->save();

        $qualitativeParameter = new QualitativeParameter();
        $qualitativeParameter->save();

        $product_8 = new \App\Product();
        $product_8->subcategory_id = 1;
        $product_8->availability_id = $availibity_1->id;
        $product_8->condition_id = $condition->id;
        $product_8->kind_id = $kind_2->id;
        $product_8->dimension_id = $dimension_1->id;
        $product_8->type_wood_id = $typeWood_1->id;
        $product_8->type_product_id = $typeProduct_2->id;
        $product_8->way_pay_id = $WayPay_8->id;
        $product_8->qualitative_parameter_id = $qualitativeParameter->id;
        $product_8->price = 375;
        $product_8->total =  $product_8->price;
        $product_8->save();

        $publication_8 = new \App\Publication;
        $publication_8->title ="Tirantes Creado por Seed product 7";
        $publication_8->description="Descripcion 7 creado por el seed";
        $publication_8->company_id =$person_premium_1->company_id;
        $publication_8->user_id=$person_premium_1->user_id;
        $publication_8->contact_id=$person_premium_1->contact_id;
        $publication_8->address_id=$person_premium_1->address_id;
        $publication_8->typePublication = 'offert';
        $publication_8->status = 'approved';
        $publication_8->price = $product_8->price;
        $publication_8->total =  $product_8->total;
        $publication_8->save();
        $publication_8->products()->attach([$product_8->id]);
        $publication_8->save();

        $WayPay_7 = \App\WayPay::create([
            'money' => '$',
            'ammount' => 175,
            'iva' => true,
        ]);

        $WayPay_7->paymentTypes()->attach([2]);
        $WayPay_7->save();

        $qualitativeParameter = new QualitativeParameter();
        $qualitativeParameter->save();

        $product_7 = new \App\Product();
        $product_7->subcategory_id = 1;
        $product_7->availability_id = $availibity_2->id;
        $product_7->condition_id = $condition->id;
        $product_7->kind_id = $kind_2->id;
        $product_7->dimension_id = $dimension_1->id;
        $product_7->type_wood_id = $typeWood_1->id;
        $product_7->type_product_id = $typeProduct_3->id;
        $product_7->way_pay_id = $WayPay_7->id;
        $product_7->qualitative_parameter_id = $qualitativeParameter->id;
        $product_7->qualitative_parameter_id = $qualitativeParameter->id;

        $product_7->price = 175;
        $product_7->total =  $product_7->price;
        $product_7->save();

        $publication_7 = new \App\Publication;
        $publication_7->title ="Paraiso Creado por Seed product 8";
        $publication_7->description="Descripcion 8 creado por el seed";
        $publication_7->company_id =$person_premium_1->company_id;
        $publication_7->user_id=$person_premium_1->user_id;
        $publication_7->contact_id=$person_premium_1->contact_id;
        $publication_7->address_id=$person_premium_1->address_id;
        $publication_7->typePublication = 'offert';
        $publication_7->status = 'approved';
        $publication_7->price = $product_7->price;
        $publication_7->total =  $product_7->total;
        $publication_7->save();
        $publication_7->products()->attach([$product_7->id]);
        $publication_7->save();


        $WayPay_9 = \App\WayPay::create([
            'money' => '$',
            'ammount' => 850,
            'iva' => true,
        ]);

        $WayPay_9->paymentTypes()->attach([2,4]);
        $WayPay_9->save();

        $qualitativeParameter = new QualitativeParameter();
        $qualitativeParameter->save();

        $product_9 = new \App\Product();
        $product_9->subcategory_id = 1;
        $product_9->availability_id = $availibity_1->id;
        $product_9->condition_id = $condition->id;
        $product_9->kind_id = $kind_2->id;
        $product_9->dimension_id = $dimension_1->id;
        $product_9->type_wood_id = $typeWood_1->id;
        $product_9->type_product_id = $typeProduct_7->id;
        $product_9->way_pay_id = $WayPay_9->id;
        $product_9->price = 850;
        $product_9->total =  $product_9->price;
        $product_9->qualitative_parameter_id = $qualitativeParameter->id;
        $product_9->save();

        $publication_9 = new \App\Publication;
        $publication_9->title ="Tablas de paraiso Creado por Seed product 9";
        $publication_9->description="Descripcion 9 creado por el seed";
        $publication_9->company_id =$person_premium_1->company_id;
        $publication_9->user_id=$person_premium_1->user_id;
        $publication_9->contact_id=$person_premium_1->contact_id;
        $publication_9->address_id=$person_premium_1->address_id;
        $publication_9->typePublication = 'offert';
        $publication_9->status = 'approved';
        $publication_9->price = $product_9->price;
        $publication_9->total =  $product_7->total;
        $publication_9->save();
        $publication_9->products()->attach([$product_9->id]);
        $publication_9->save();

        $WayPay_10 = \App\WayPay::create([
            'money' => '$',
            'ammount' => 950,
            'iva' => true,
        ]);

        $WayPay_10->paymentTypes()->attach([2,4]);
        $WayPay_10->save();

        $qualitativeParameter = new QualitativeParameter();
        $qualitativeParameter->save();

        $product_10 = new \App\Product();
        $product_10->subcategory_id = 1;
        $product_10->availability_id = $availibity_2->id;
        $product_10->condition_id = $condition->id;
        $product_10->kind_id = $kind_2->id;
        $product_10->dimension_id = $dimension_1->id;
        $product_10->type_wood_id = $typeWood_1->id;
        $product_10->type_product_id = $typeProduct_8->id;
        $product_10->way_pay_id = $WayPay_10->id;
        $product_10->qualitative_parameter_id = $qualitativeParameter->id;
        $product_10->price = 950;
        $product_10->total =  $product_10->price;
        $product_10->qualitative_parameter_id = $qualitativeParameter->id;
        $product_10->save();

        $publication_10 = new \App\Publication;
        $publication_10->title ="Tablas de araucaria Creado por Seed product 10";
        $publication_10->description="Descripcion 10 creado por el seed";
        $publication_10->company_id =$person_premium_1->company_id;
        $publication_10->user_id=$person_premium_1->user_id;
        $publication_10->contact_id=$person_premium_1->contact_id;
        $publication_10->address_id=$person_premium_1->address_id;
        $publication_10->typePublication = 'offert';
        $publication_10->status = 'approved';
        $publication_10->price = $product_10->price;
        $publication_10->total =  $product_7->total;
        $publication_10->save();
        $publication_10->products()->attach([$product_10->id]);
        $publication_10->save();

        $WayPay_11 = \App\WayPay::create([
            'money' => '$',
            'ammount' => 980,
            'iva' => true,
        ]);

        $WayPay_11->paymentTypes()->attach([1,2]);
        $WayPay_11->save();

        $qualitativeParameter = new QualitativeParameter();
        $qualitativeParameter->save();

        $product_11 = new \App\Product();
        $product_11->subcategory_id = 1;
        $product_11->availability_id = $availibity_1->id;
        $product_11->condition_id = $condition->id;
        $product_11->kind_id = $kind_1->id;
        $product_11->dimension_id = $dimension_1->id;
        $product_11->type_wood_id = $typeWood_1->id;
        $product_11->type_product_id = $typeProduct_8->id;
        $product_11->way_pay_id = $WayPay_11->id;
        $product_11->qualitative_parameter_id = $qualitativeParameter->id;
        $product_11->price = 980;
        $product_11->total =  $product_11->price;
        $product_11->save();


        $publication_11 = new \App\Publication;
        $publication_11->title ="Vigas  Creado por Seed product 10";
        $publication_11->description="Descripcion creado por el seed";
        $publication_11->company_id =$person_premium_1->company_id;
        $publication_11->user_id=$person_premium_1->user_id;
        $publication_11->contact_id=$person_premium_1->contact_id;
        $publication_11->address_id=$person_premium_1->address_id;
        $publication_11->typePublication = 'offert';
        $publication_11->status = 'approved';
        $publication_11->price = $product_11->price;
        $publication_11->total =  $product_11->total;
        $publication_11->save();
        $publication_11->products()->attach([$product_11->id]);
        $publication_11->save();

        $WayPay_12 = \App\WayPay::create([
            'money' => '$',
            'ammount' => 700,
            'iva' => true,
        ]);

        $WayPay_12->paymentTypes()->attach([1,2,3,4]);
        $WayPay_12->save();

        $qualitativeParameter = new QualitativeParameter();
        $qualitativeParameter->save();

        $product_12 = new \App\Product();
        $product_12->subcategory_id = 1;
        $product_12->availability_id = $availibity_1->id;
        $product_12->condition_id = $condition->id;
        $product_12->kind_id = $kind_2->id;
        $product_12->dimension_id = $dimension_1->id;
        $product_12->type_wood_id = $typeWood_1->id;
        $product_12->type_product_id = $typeProduct_9->id;
        $product_12->way_pay_id = $WayPay_12->id;
        $product_12->qualitative_parameter_id = $qualitativeParameter->id;
        $product_12->price = 700;
        $product_12->total =  $product_12->price;
        $product_12->save();

        $publication_12 = new \App\Publication;
        $publication_12->title ="tablas de cedro Creado por Seed product 11";
        $publication_12->description="Descripcion 12 creado por el seed";
        $publication_12->company_id =$person_premium_1->company_id;
        $publication_12->user_id=$person_premium_1->user_id;
        $publication_12->contact_id=$person_premium_1->contact_id;
        $publication_12->address_id=$person_premium_1->address_id;
        $publication_12->typePublication = 'offert';
        $publication_12->status = 'approved';
        $publication_12->price = $product_12->price;
        $publication_12->total =  $product_12->total;
        $publication_12->save();
        $publication_12->products()->attach([$product_12->id]);
        $publication_12->save();


        $WayPay_13 = \App\WayPay::create([
            'money' => '$',
            'ammount' => 320,
            'iva' => true,
        ]);

        $WayPay_13->paymentTypes()->attach([1,2,3,4]);
        $WayPay_13->save();

        $qualitativeParameter = new QualitativeParameter();
        $qualitativeParameter->save();

        $product_13 = new \App\Product();
        $product_13->subcategory_id = 1;
        $product_13->availability_id = $availibity_1->id;
        $product_13->condition_id = $condition->id;
        $product_13->kind_id = $kind_2->id;
        $product_13->dimension_id = $dimension_1->id;
        $product_13->type_wood_id = $typeWood_1->id;
        $product_13->type_product_id = $typeProduct_5->id;
        $product_13->way_pay_id = $WayPay_13->id;
        $product_13->qualitative_parameter_id = $qualitativeParameter->id;
        $product_13->price = 320;
        $product_13->total =  $product_13->price;
        $product_13->save();

        $publication_13 = new \App\Publication;
        $publication_13->title ="Machimbe de guatambú Creado por Seed product 13";
        $publication_13->description="Descripcion 13 creado por el seed";
        $publication_13->company_id =$person_premium_1->company_id;
        $publication_13->user_id=$person_premium_1->user_id;
        $publication_13->contact_id=$person_premium_1->contact_id;
        $publication_13->address_id=$person_premium_1->address_id;
        $publication_13->typePublication = 'offert';
        $publication_13->status = 'approved';
        $publication_13->price = $product_13->price;
        $publication_13->total =  $product_13->total;
        $publication_13->save();
        $publication_13->products()->attach([$product_13->id]);
        $publication_13->save();


        $WayPay_14 = \App\WayPay::create([
            'money' => '$',
            'ammount' => 147,
            'iva' => true,
        ]);

        $WayPay_14->paymentTypes()->attach([1]);
        $WayPay_14->save();

        $qualitativeParameter = new QualitativeParameter();
        $qualitativeParameter->save();


        $product_14 = new \App\Product();
        $product_14->subcategory_id = 1;
        $product_14->availability_id = $availibity_2->id;
        $product_14->condition_id = $condition->id;
        $product_14->kind_id = $kind_2->id;
        $product_14->dimension_id = $dimension_1->id;
        $product_14->type_wood_id = $typeWood_1->id;
        $product_14->type_product_id = $typeProduct_9->id;
        $product_14->way_pay_id = $WayPay_14->id;
        $product_14->qualitative_parameter_id = $qualitativeParameter->id;
        $product_14->price = 147;
        $product_14->total =  $product_14->price;
        $product_14->save();

        $publication_14 = new \App\Publication;
        $publication_14->title ="Vigas de timbó Creado por Seed product 14";
        $publication_14->description="Descripcion 14 creado por el seed";
        $publication_14->company_id =$person_premium_1->company_id;
        $publication_14->user_id=$person_premium_1->user_id;
        $publication_14->contact_id=$person_premium_1->contact_id;
        $publication_14->address_id=$person_premium_1->address_id;
        $publication_14->typePublication = 'offert';
        $publication_14->status = 'approved';
        $publication_14->price = $product_14->price;
        $publication_14->total =  $product_14->total;
        $publication_14->save();
        $publication_14->products()->attach([$product_14->id]);
        $publication_14->save();

        $WayPay_15 = \App\WayPay::create([
            'money' => '$',
            'ammount' => 660,
            'iva' => true,
        ]);

        $WayPay_15->paymentTypes()->attach([1,2,3,4]);
        $WayPay_15->save();

        $qualitativeParameter = new QualitativeParameter();
        $qualitativeParameter->save();

        $product_15 = new \App\Product();
        $product_15->subcategory_id = 1;
        $product_15->availability_id = $availibity_2->id;
        $product_15->condition_id = $condition->id;
        $product_15->kind_id = $kind_2->id;
        $product_15->dimension_id = $dimension_1->id;
        $product_15->type_wood_id = $typeWood_1->id;
        $product_15->type_product_id = $typeProduct_9->id;
        $product_15->way_pay_id = $WayPay_15->id;
        $product_15->qualitative_parameter_id = $qualitativeParameter->id;
        $product_15->price = 660;
        $product_15->total =  $product_15->price;
        $product_15->save();

        $publication_15 = new \App\Publication;
        $publication_15->title ="Laurel Negro Creado por Seed product 15";
        $publication_15->description="Descripcion 15 creado por el seed";
        $publication_15->company_id =$person_premium_1->company_id;
        $publication_15->user_id=$person_premium_1->user_id;
        $publication_15->contact_id=$person_premium_1->contact_id;
        $publication_15->address_id=$person_premium_1->address_id;
        $publication_15->typePublication = 'offert';
        $publication_15->status = 'approved';
        $publication_15->price = $product_15->price;
        $publication_15->total =  $product_15->total;
        $publication_15->save();
        $publication_15->products()->attach([$product_15->id]);
        $publication_15->save();


        $WayPay_16 = \App\WayPay::create([
            'money' => '$',
            'ammount' => 520,
            'iva' => true,
        ]);

        $WayPay_16->paymentTypes()->attach([3,4,5]);
        $WayPay_16->save();

        $qualitativeParameter = new QualitativeParameter();
        $qualitativeParameter->save();

        $product_16 = new \App\Product();
        $product_16->subcategory_id = 1;
        $product_16->availability_id = $availibity_2->id;
        $product_16->condition_id = $condition->id;
        $product_16->kind_id = $kind_2->id;
        $product_16->dimension_id = $dimension_1->id;
        $product_16->type_wood_id = $typeWood_1->id;
        $product_16->type_product_id = $typeProduct_9->id;
        $product_16->way_pay_id = $WayPay_16->id;
        $product_16->qualitative_parameter_id = $qualitativeParameter->id;
        $product_16->price = 520;
        $product_16->total =  $product_16->price;
        $product_16->save();

        $publication_16 = new \App\Publication;
        $publication_16->title ="Listones de pino Creado por Seed product 16";
        $publication_16->description="Descripcion 16 creado por el seed";
        $publication_16->company_id =$person_premium_1->company_id;
        $publication_16->user_id=$person_premium_1->user_id;
        $publication_16->contact_id=$person_premium_1->contact_id;
        $publication_16->address_id=$person_premium_1->address_id;
        $publication_16->typePublication = 'offert';
        $publication_16->status = 'approved';
        $publication_16->price = $product_16->price;
        $publication_16->total =  $product_16->total;
        $publication_16->save();
        $publication_16->products()->attach([$product_16->id]);
        $publication_16->save();

        $WayPay_18 = \App\WayPay::create([
            'money' => '$',
            'ammount' => 320,
            'iva' => true,
        ]);

        $WayPay_18->paymentTypes()->attach([3,4,5]);
        $WayPay_18->save();

        $qualitativeParameter = new QualitativeParameter();
        $qualitativeParameter->save();

        $product_18 = new \App\Product();
        $product_18->subcategory_id = 1;
        $product_18->availability_id = $availibity_1->id;
        $product_18->condition_id = $condition->id;
        $product_18->kind_id = $kind_2->id;
        $product_18->dimension_id = $dimension_1->id;
        $product_18->type_wood_id = $typeWood_1->id;
        $product_18->type_product_id = $typeProduct_2->id;
        $product_18->way_pay_id = $WayPay_18->id;
        $product_18->qualitative_parameter_id = $qualitativeParameter->id;
        $product_18->price = 825;
        $product_18->total =  $product_18->price;
        $product_18->save();

        $publication_18 = new \App\Publication;
        $publication_18->title ="Varillas de pino Creado por Seed product 17";
        $publication_18->description="Descripcion 17 creado por el seed";
        $publication_18->company_id =$person_premium_1->company_id;
        $publication_18->user_id=$person_premium_1->user_id;
        $publication_18->contact_id=$person_premium_1->contact_id;
        $publication_18->address_id=$person_premium_1->address_id;
        $publication_18->typePublication = 'offert';
        $publication_18->status = 'approved';
        $publication_18->price = $product_18->price;
        $publication_18->total =  $product_18->total;
        $publication_18->save();
        $publication_18->products()->attach([$product_18->id]);
        $publication_18->save();

        $WayPay_17 = \App\WayPay::create([
            'money' => '$',
            'ammount' => 415.75,
            'iva' => true,
        ]);

        $WayPay_17->paymentTypes()->attach([3,4,5]);
        $WayPay_17->save();

        $qualitativeParameter = new QualitativeParameter();
        $qualitativeParameter->save();

        $product_17 = new \App\Product();
        $product_17->subcategory_id = 1;
        $product_17->availability_id = $availibity_1->id;
        $product_17->condition_id = $condition->id;
        $product_17->kind_id = $kind_2->id;
        $product_17->dimension_id = $dimension_1->id;
        $product_17->type_wood_id = $typeWood_1->id;
        $product_17->type_product_id = $typeProduct_3->id;
        $product_17->way_pay_id = $WayPay_17->id;
        $product_17->qualitative_parameter_id = $qualitativeParameter->id;
        $product_17->price = 415.75;
        $product_17->total =  $product_17->price;
        $product_17->save();

        $publication_17 = new \App\Publication;
        $publication_17->title ="Retazos de loro negro Creado por Seed product 18";
        $publication_17->description="Descripcion 18 creado por el seed";
        $publication_17->company_id =$person_premium_1->company_id;
        $publication_17->user_id=$person_premium_1->user_id;
        $publication_17->contact_id=$person_premium_1->contact_id;
        $publication_17->address_id=$person_premium_1->address_id;
        $publication_17->typePublication = 'offert';
        $publication_17->status = 'approved';
        $publication_17->price = $product_17->price;
        $publication_17->total =  $product_17->total;
        $publication_17->save();
        $publication_17->products()->attach([$product_17->id]);
        $publication_17->save();

        $WayPay_19 = \App\WayPay::create([
            'money' => '$',
            'ammount' => 780.99,
            'iva' => true,
        ]);

        $WayPay_19->paymentTypes()->attach([3,4,5]);
        $WayPay_19->save();

        $qualitativeParameter = new QualitativeParameter();
        $qualitativeParameter->save();

        $product_19 = new \App\Product();
        $product_19->subcategory_id = 1;
        $product_19->availability_id = $availibity_1->id;
        $product_19->condition_id = $condition->id;
        $product_19->kind_id = $kind_2->id;
        $product_19->dimension_id = $dimension_1->id;
        $product_19->type_wood_id = $typeWood_1->id;
        $product_19->type_product_id = $typeProduct_4->id;
        $product_19->way_pay_id = $WayPay_19->id;
        $product_19->qualitative_parameter_id = $qualitativeParameter->id;
        $product_19->price = 780.99;
        $product_19->total =  $product_19->price;
        $product_19->save();

        $publication_19 = new \App\Publication;
        $publication_19->title ="Pino Creado por Seed product 19";
        $publication_19->description="Descripcion 19 creado por el seed";
        $publication_19->company_id =$person_premium_1->company_id;
        $publication_19->user_id=$person_premium_1->user_id;
        $publication_19->contact_id=$person_premium_1->contact_id;
        $publication_19->address_id=$person_premium_1->address_id;
        $publication_19->typePublication = 'offert';
        $publication_19->status = 'approved';
        $publication_19->price = $product_19->price;
        $publication_19->total =  $product_7->total;
        $publication_19->save();
        $publication_19->products()->attach([$product_19->id]);
        $publication_19->save();

        $WayPay_20 = \App\WayPay::create([
            'money' => '$',
            'ammount' => 412.89,
            'iva' => true,
        ]);

        $WayPay_20->paymentTypes()->attach([1]);
        $WayPay_20->save();

        $qualitativeParameter = new QualitativeParameter();
        $qualitativeParameter->save();

        $product_20 = new \App\Product();
        $product_20->subcategory_id = 1;
        $product_20->availability_id = $availibity_2->id;
        $product_20->condition_id = $condition->id;
        $product_20->kind_id = $kind_2->id;
        $product_20->dimension_id = $dimension_1->id;
        $product_20->type_wood_id = $typeWood_1->id;
        $product_20->type_product_id = $typeProduct_7->id;
        $product_20->way_pay_id = $product_20->id;
        $product_20->qualitative_parameter_id = $qualitativeParameter->id;
        $product_20->price = 412.89;
        $product_20->total =  $product_20->price;
        $product_20->save();

        $publication_20 = new \App\Publication;
        $publication_20->title ="Listones de algarrobo Creado por Seed product 20";
        $publication_20->description="Descripcion 20 creado por el seed";
        $publication_20->company_id =$person_premium_1->company_id;
        $publication_20->user_id=$person_premium_1->user_id;
        $publication_20->contact_id=$person_premium_1->contact_id;
        $publication_20->address_id=$person_premium_1->address_id;
        $publication_20->typePublication = 'offert';
        $publication_20->status = 'approved';
        $publication_20->price = $product_20->price;
        $publication_20->total =  $product_7->total;
        $publication_20->save();
        $publication_20->products()->attach([$product_20->id]);
        $publication_20->save();
    }


    private function createPublicationWithOffertUser2()
    {
        $availibity_1 = \App\Availability::create([
            'cantitude' => 30,
            'availability' => 'limited',
            'unitMeasurement' => Unit::PIETABLAR,
            'measurementTerm' => null,
        ]);

        $availibity_2 = \App\Availability::create([
            'cantitude' => 30,
            'availability' => 'continuous',
            'unitMeasurement' => Unit::PIETABLAR,
            'measurementTerm' => null,
        ]);

        //Condiciones

        $condition = \App\Condition::create([
            'piece' =>1,
            'weight' =>1,
            'length' =>1,
            'tall' =>11,
            'zunchos'=> 'No',
            //envoltorio
            'wrapping' =>'Yes'
        ]);



        //dimensiones
        $dimension_1 = \App\Dimension::create([
            'actualThickness' => 30,
            'actualWidth' => 50,
            'actualLength' => 2000,
        ]);


        $dimension_2 = \App\Dimension::create([
            'actualThickness' => 60,
            'actualWidth' => 250,
            'actualLength' => 3000,
        ]);

        $dimension_3 = \App\Dimension::create([
            'actualThickness' => 80,
            'actualWidth' => 200,
            'actualLength' => 1000,
        ]);

        $dimension_4 = \App\Dimension::create([
            'actualThickness' => 70,
            'actualWidth' => 200,
            'actualLength' => 800,
        ]);
        //kind
        $kind_1 = \App\Kind::find(1);
        $kind_2 = \App\Kind::find(2);
        $kind_3 = \App\Kind::find(3);
        $kind_4 = \App\Kind::find(4);
        $kind_5 = \App\Kind::find(5);
        $kind_6 = \App\Kind::find(6);
        $kind_7 = \App\Kind::find(7);
        $kind_8 = \App\Kind::find(8);
        $kind_9 = \App\Kind::find(9);
        $kind_10 = \App\Kind::find(10);
        $kind_11 = \App\Kind::find(11);
        $kind_12 = \App\Kind::find(12);
        $kind_13 = \App\Kind::find(13);
        $kind_14 = \App\Kind::find(14);

        //nativa 1 implantada 2
        $typeWood_1 = \App\TypeWood::find(1);
        $typeWood_2 = \App\TypeWood::find(2);

        $typeProduct_1 = \App\TypeProduct::find(1);

        $typeProduct_2 = \App\TypeProduct::find(2);
        $typeProduct_3 = \App\TypeProduct::find(3);
        $typeProduct_4 = \App\TypeProduct::find(4);
        $typeProduct_5 = \App\TypeProduct::find(5);
        $typeProduct_6 = \App\TypeProduct::find(6);
        $typeProduct_7 = \App\TypeProduct::find(7);
        $typeProduct_8 = \App\TypeProduct::find(8);
        $typeProduct_9 = \App\TypeProduct::find(9);
        $typeProduct_10 = \App\TypeProduct::find(10);
        $typeProduct_11 = \App\TypeProduct::find(11);

        //forma de pago

        $WayPay = \App\WayPay::create([
            'money' => '$',
            'ammount' => 100,
            'iva' => true,
        ]);

        $WayPay->paymentTypes()->attach([1,2,3]);
        $WayPay->save();

        $qualitativeParameter = new QualitativeParameter();
        $qualitativeParameter->save();


        $person_premium_8 = \App\Person::find(5);

        $qualitativeParameter = new QualitativeParameter();
        $qualitativeParameter->save();

        $product = new \App\Product();
        $product->subcategory_id = 1;
        $product->availability_id = $availibity_2->id;
        $product->condition_id = $condition->id;
        $product->kind_id = $kind_1->id;
        $product->dimension_id = $dimension_2->id;
        $product->type_wood_id = $typeWood_1->id;
        $product->type_product_id = $typeProduct_2->id;
        $product->way_pay_id = $WayPay->id;
        $product->price = 100;
        $product->qualitative_parameter_id = $qualitativeParameter->id;
        $product->total =  $product->price;
        $product->save();

        $product->qualitative_parameter_id = $qualitativeParameter->id;$publication
        = new \App\Publication;
        $publication->title ="Pino Creado por Seed product 1";
        $publication->description="Descripcion creado por el seed";
        $publication->company_id =$person_premium_8->company_id;
        $publication->user_id=$person_premium_8->user_id;
        $publication->contact_id=$person_premium_8->contact_id;
        $publication->address_id=$person_premium_8->address_id;
        $publication->typePublication = 'offert';
        $publication->status = 'approved';
        $publication->price = $product->price;
        $publication->total =  $product->total;
        $publication->save();
        $publication->products()->attach([$product->id]);
        $publication->save();


        $WayPay_2 = \App\WayPay::create([
            'money' => '$',
            'ammount' => 110,
            'iva' => true,
        ]);

        $WayPay_2->paymentTypes()->attach([1,3,4]);
        $WayPay_2->save();

        $qualitativeParameter = new QualitativeParameter();
        $qualitativeParameter->save();

        $product_2 = new \App\Product();
        $product_2->subcategory_id = 1;
        $product_2->availability_id = $availibity_1->id;
        $product_2->condition_id = $condition->id;
        $product_2->kind_id = $kind_2->id;
        $product_2->dimension_id = $dimension_2->id;
        $product_2->type_wood_id = $typeWood_2->id;
        $product_2->type_product_id = $typeProduct_3->id;
        $product_2->way_pay_id = $WayPay_2->id;
        $product_2->qualitative_parameter_id = $qualitativeParameter->id;
        $product_2->price = 110;
        $product_2->total =  $product_2->price;
        $product_2->save();

        $publication_2 = new \App\Publication;
        $publication_2->title ="Pino Creado por Seed product 2";
        $publication_2->description="Descripcion 2 creado por el seed";
        $publication_2->company_id =$person_premium_8->company_id;
        $publication_2->user_id=$person_premium_8->user_id;
        $publication_2->contact_id=$person_premium_8->contact_id;
        $publication_2->address_id=$person_premium_8->address_id;
        $publication_2->typePublication = 'offert';
        $publication_2->status = 'approved';
        $publication_2->price = $product_2->price;
        $publication_2->total =  $product_2->total;
        $publication_2->save();
        $publication_2->products()->attach([$product_2->id]);
        $publication_2->save();


        $WayPay_3 = \App\WayPay::create([
            'money' => '$',
            'ammount' => 200,
            'iva' => true,
        ]);

        $WayPay_3->paymentTypes()->attach([1]);
        $WayPay_3->save();

        $qualitativeParameter = new QualitativeParameter();
        $qualitativeParameter->save();

        $product_3 = new \App\Product();
        $product_3->subcategory_id = 1;
        $product_3->availability_id = $availibity_2->id;
        $product_3->condition_id = $condition->id;
        $product_3->kind_id = $kind_2->id;
        $product_3->dimension_id = $dimension_2->id;
        $product_3->type_wood_id = $typeWood_1->id;
        $product_3->type_product_id = $typeProduct_3->id;
        $product_3->way_pay_id = $WayPay_3->id;
        $product_3->qualitative_parameter_id = $qualitativeParameter->id;
        $product_3->price = 200;
        $product_3->total =  $product_3->price;
        $product_3->save();

        $publication_3 = new \App\Publication;
        $publication_3->title ="Pino Creado por Seed product 3";
        $publication_3->description="Descripcion 3 creado por el seed";
        $publication_3->company_id =$person_premium_8->company_id;
        $publication_3->user_id=$person_premium_8->user_id;
        $publication_3->contact_id=$person_premium_8->contact_id;
        $publication_3->address_id=$person_premium_8->address_id;
        $publication_3->typePublication = 'offert';
        $publication_3->status = 'approved';
        $publication_3->price = $product_3->price;
        $publication_3->total =  $product_3->total;
        $publication_3->save();
        $publication_3->products()->attach([$product_3->id]);
        $publication_3->save();


        $WayPay_4 = \App\WayPay::create([
            'money' => '$',
            'ammount' => 250,
            'iva' => true,
        ]);

        $WayPay_4->paymentTypes()->attach([2]);
        $WayPay_4->save();

        $qualitativeParameter = new QualitativeParameter();
        $qualitativeParameter->save();

        $product_4 = new \App\Product();
        $product_4->subcategory_id = 1;
        $product_4->availability_id = $availibity_1->id;
        $product_4->condition_id = $condition->id;
        $product_4->kind_id = $kind_2->id;
        $product_4->dimension_id = $dimension_2->id;
        $product_4->type_wood_id = $typeWood_1->id;
        $product_4->type_product_id = $typeProduct_4->id;
        $product_4->way_pay_id = $WayPay_4->id;
        $product_4->qualitative_parameter_id = $qualitativeParameter->id;
        $product_4->price = 250;
        $product_4->total =  $product_4->price;
        $product_4->save();

        $publication_4 = new \App\Publication;
        $publication_4->title ="Algarrobo Creado por Seed product 4";
        $publication_4->description="Descripcion 4 creado por el seed";
        $publication_4->company_id =$person_premium_8->company_id;
        $publication_4->user_id=$person_premium_8->user_id;
        $publication_4->contact_id=$person_premium_8->contact_id;
        $publication_4->address_id=$person_premium_8->address_id;
        $publication_4->typePublication = 'offert';
        $publication_4->status = 'approved';
        $publication_4->price = $product_4->price;
        $publication_4->total =  $product_4->total;
        $publication_4->save();
        $publication_4->products()->attach([$product_4->id]);
        $publication_4->save();


        $WayPay_5 = \App\WayPay::create([
            'money' => '$',
            'ammount' => 450,
            'iva' => true,
        ]);

        $WayPay_5->paymentTypes()->attach([2]);
        $WayPay_5->save();

        $qualitativeParameter = new QualitativeParameter();
        $qualitativeParameter->save();

        $product_5 = new \App\Product();
        $product_5->subcategory_id = 1;
        $product_5->availability_id = $availibity_2->id;
        $product_5->condition_id = $condition->id;
        $product_5->kind_id = $kind_2->id;
        $product_5->dimension_id = $dimension_3->id;
        $product_5->type_wood_id = $typeWood_1->id;
        $product_5->type_product_id = $typeProduct_6->id;
        $product_5->way_pay_id = $WayPay_5->id;
        $product_5->qualitative_parameter_id = $qualitativeParameter->id;
        $product_5->price = 450;
        $product_5->total =  $product_5->price;
        $product_5->save();

        $publication_5 = new \App\Publication;
        $publication_5->title ="Algarrobo Creado por Seed product 5";
        $publication_5->description="Descripcion 5 creado por el seed";
        $publication_5->company_id =$person_premium_8->company_id;
        $publication_5->user_id=$person_premium_8->user_id;
        $publication_5->contact_id=$person_premium_8->contact_id;
        $publication_5->address_id=$person_premium_8->address_id;
        $publication_5->typePublication = 'offert';
        $publication_5->status = 'approved';
        $publication_5->price = $product_5->price;
        $publication_5->total =  $product_5->total;
        $publication_5->save();
        $publication_5->products()->attach([$product_5->id]);
        $publication_5->save();

        $WayPay_6 = \App\WayPay::create([
            'money' => '$',
            'ammount' => 402,
            'iva' => true,
        ]);

        $WayPay_6->paymentTypes()->attach([1,2]);
        $WayPay_6->save();

        $qualitativeParameter = new QualitativeParameter();
        $qualitativeParameter->save();

        $product_6 = new \App\Product();
        $product_6->subcategory_id = 1;
        $product_6->availability_id = $availibity_1->id;
        $product_6->condition_id = $condition->id;
        $product_6->kind_id = $kind_2->id;
        $product_6->dimension_id = $dimension_4->id;
        $product_6->type_wood_id = $typeWood_1->id;
        $product_6->type_product_id = $typeProduct_7->id;
        $product_6->way_pay_id = $WayPay_6->id;
        $product_6->qualitative_parameter_id = $qualitativeParameter->id;
        $product_6->price = 402;
        $product_6->total =  $product_6->price;
        $product_6->save();

        $publication_6 = new \App\Publication;
        $publication_6->title ="Tirante Creado por Seed product 6";
        $publication_6->description="Descripcion 6 creado por el seed";
        $publication_6->company_id =$person_premium_8->company_id;
        $publication_6->user_id=$person_premium_8->user_id;
        $publication_6->contact_id=$person_premium_8->contact_id;
        $publication_6->address_id=$person_premium_8->address_id;
        $publication_6->typePublication = 'offert';
        $publication_6->status = 'approved';
        $publication_6->price = $product_6->price;
        $publication_6->total =  $product_6->total;
        $publication_6->save();
        $publication_6->products()->attach([$product_6->id]);
        $publication_6->save();



        $WayPay_8 = \App\WayPay::create([
            'money' => '$',
            'ammount' => 375,
            'iva' => true,
        ]);

        $WayPay_8->paymentTypes()->attach([2]);
        $WayPay_8->save();

        $qualitativeParameter = new QualitativeParameter();
        $qualitativeParameter->save();

        $product_8 = new \App\Product();
        $product_8->subcategory_id = 1;
        $product_8->availability_id = $availibity_1->id;
        $product_8->condition_id = $condition->id;
        $product_8->kind_id = $kind_2->id;
        $product_8->dimension_id = $dimension_2->id;
        $product_8->type_wood_id = $typeWood_1->id;
        $product_8->type_product_id = $typeProduct_2->id;
        $product_8->way_pay_id = $WayPay_8->id;
        $product_8->qualitative_parameter_id = $qualitativeParameter->id;
        $product_8->price = 375;
        $product_8->total =  $product_8->price;
        $product_8->save();

        $publication_8 = new \App\Publication;
        $publication_8->title ="Tirantes Creado por Seed product 7";
        $publication_8->description="Descripcion 7 creado por el seed";
        $publication_8->company_id =$person_premium_8->company_id;
        $publication_8->user_id=$person_premium_8->user_id;
        $publication_8->contact_id=$person_premium_8->contact_id;
        $publication_8->address_id=$person_premium_8->address_id;
        $publication_8->typePublication = 'offert';
        $publication_8->status = 'approved';
        $publication_8->price = $product_8->price;
        $publication_8->total =  $product_8->total;
        $publication_8->save();
        $publication_8->products()->attach([$product_8->id]);
        $publication_8->save();

        $WayPay_7 = \App\WayPay::create([
            'money' => '$',
            'ammount' => 175,
            'iva' => true,
        ]);

        $WayPay_7->paymentTypes()->attach([2]);
        $WayPay_7->save();

        $qualitativeParameter = new QualitativeParameter();
        $qualitativeParameter->save();

        $product_7 = new \App\Product();
        $product_7->subcategory_id = 1;
        $product_7->availability_id = $availibity_2->id;
        $product_7->condition_id = $condition->id;
        $product_7->kind_id = $kind_2->id;
        $product_7->dimension_id = $dimension_3->id;
        $product_7->type_wood_id = $typeWood_1->id;
        $product_7->type_product_id = $typeProduct_3->id;
        $product_7->way_pay_id = $WayPay_7->id;
        $product_7->qualitative_parameter_id = $qualitativeParameter->id;
        $product_7->price = 175;
        $product_7->total =  $product_7->price;
        $product_7->save();

        $publication_7 = new \App\Publication;
        $publication_7->title ="Paraiso Creado por Seed product 8";
        $publication_7->description="Descripcion 8 creado por el seed";
        $publication_7->company_id =$person_premium_8->company_id;
        $publication_7->user_id=$person_premium_8->user_id;
        $publication_7->contact_id=$person_premium_8->contact_id;
        $publication_7->address_id=$person_premium_8->address_id;
        $publication_7->typePublication = 'offert';
        $publication_7->status = 'approved';
        $publication_7->price = $product_7->price;
        $publication_7->total =  $product_7->total;
        $publication_7->save();
        $publication_7->products()->attach([$product_7->id]);
        $publication_7->save();


        $WayPay_9 = \App\WayPay::create([
            'money' => '$',
            'ammount' => 850,
            'iva' => true,
        ]);

        $WayPay_9->paymentTypes()->attach([2,4]);
        $WayPay_9->save();

        $qualitativeParameter = new QualitativeParameter();
        $qualitativeParameter->save();

        $product_9 = new \App\Product();
        $product_9->subcategory_id = 1;
        $product_9->availability_id = $availibity_1->id;
        $product_9->condition_id = $condition->id;
        $product_9->kind_id = $kind_2->id;
        $product_9->dimension_id = $dimension_4->id;
        $product_9->type_wood_id = $typeWood_1->id;
        $product_9->type_product_id = $typeProduct_7->id;
        $product_9->way_pay_id = $WayPay_9->id;
        $product_9->qualitative_parameter_id = $qualitativeParameter->id;
        $product_9->price = 850;
        $product_9->total =  $product_9->price;
        $product_9->save();

        $publication_9 = new \App\Publication;
        $publication_9->title ="Tablas de paraiso Creado por Seed product 9";
        $publication_9->description="Descripcion 9 creado por el seed";
        $publication_9->company_id =$person_premium_8->company_id;
        $publication_9->user_id=$person_premium_8->user_id;
        $publication_9->contact_id=$person_premium_8->contact_id;
        $publication_9->address_id=$person_premium_8->address_id;
        $publication_9->typePublication = 'offert';
        $publication_9->status = 'approved';
        $publication_9->price = $product_9->price;
        $publication_9->total =  $product_7->total;
        $publication_9->save();
        $publication_9->products()->attach([$product_9->id]);
        $publication_9->save();

        $WayPay_10 = \App\WayPay::create([
            'money' => '$',
            'ammount' => 950,
            'iva' => true,
        ]);

        $WayPay_10->paymentTypes()->attach([2,4]);
        $WayPay_10->save();

        $qualitativeParameter = new QualitativeParameter();
        $qualitativeParameter->save();

        $product_10 = new \App\Product();
        $product_10->subcategory_id = 1;
        $product_10->availability_id = $availibity_2->id;
        $product_10->condition_id = $condition->id;
        $product_10->kind_id = $kind_2->id;
        $product_10->dimension_id = $dimension_1->id;
        $product_10->type_wood_id = $typeWood_1->id;
        $product_10->type_product_id = $typeProduct_8->id;
        $product_10->way_pay_id = $WayPay_10->id;
        $product_10->qualitative_parameter_id = $qualitativeParameter->id;
        $product_10->price = 950;
        $product_10->total =  $product_10->price;
        $product_10->save();

        $publication_10 = new \App\Publication;
        $publication_10->title ="Tablas de araucaria Creado por Seed product 10";
        $publication_10->description="Descripcion 10 creado por el seed";
        $publication_10->company_id =$person_premium_8->company_id;
        $publication_10->user_id=$person_premium_8->user_id;
        $publication_10->contact_id=$person_premium_8->contact_id;
        $publication_10->address_id=$person_premium_8->address_id;
        $publication_10->typePublication = 'offert';
        $publication_10->status = 'approved';
        $publication_10->price = $product_10->price;
        $publication_10->total =  $product_7->total;
        $publication_10->save();
        $publication_10->products()->attach([$product_10->id]);
        $publication_10->save();

        $WayPay_11 = \App\WayPay::create([
            'money' => '$',
            'ammount' => 980,
            'iva' => true,
        ]);

        $WayPay_11->paymentTypes()->attach([1,2]);
        $WayPay_11->save();

        $qualitativeParameter = new QualitativeParameter();
        $qualitativeParameter->save();

        $product_11 = new \App\Product();
        $product_11->subcategory_id = 1;
        $product_11->availability_id = $availibity_1->id;
        $product_11->condition_id = $condition->id;
        $product_11->kind_id = $kind_1->id;
        $product_11->dimension_id = $dimension_2->id;
        $product_11->type_wood_id = $typeWood_1->id;
        $product_11->type_product_id = $typeProduct_8->id;
        $product_11->way_pay_id = $WayPay_11->id;
        $product_11->qualitative_parameter_id = $qualitativeParameter->id;
        $product_11->price = 980;
        $product_11->total =  $product_11->price;
        $product_11->save();


        $publication_11 = new \App\Publication;
        $publication_11->title ="Vigas  Creado por Seed product 10";
        $publication_11->description="Descripcion creado por el seed";
        $publication_11->company_id =$person_premium_8->company_id;
        $publication_11->user_id=$person_premium_8->user_id;
        $publication_11->contact_id=$person_premium_8->contact_id;
        $publication_11->address_id=$person_premium_8->address_id;
        $publication_11->typePublication = 'offert';
        $publication_11->status = 'approved';
        $publication_11->price = $product_11->price;
        $publication_11->total =  $product_11->total;
        $publication_11->save();
        $publication_11->products()->attach([$product_11->id]);
        $publication_11->save();

        $WayPay_12 = \App\WayPay::create([
            'money' => '$',
            'ammount' => 700,
            'iva' => true,
        ]);

        $WayPay_12->paymentTypes()->attach([1,2,3,4]);
        $WayPay_12->save();

        $qualitativeParameter = new QualitativeParameter();
        $qualitativeParameter->save();

        $product_12 = new \App\Product();
        $product_12->subcategory_id = 1;
        $product_12->availability_id = $availibity_1->id;
        $product_12->condition_id = $condition->id;
        $product_12->kind_id = $kind_2->id;
        $product_12->dimension_id = $dimension_4->id;
        $product_12->type_wood_id = $typeWood_1->id;
        $product_12->type_product_id = $typeProduct_9->id;
        $product_12->way_pay_id = $WayPay_12->id;
        $product_12->qualitative_parameter_id = $qualitativeParameter->id;
        $product_12->price = 700;
        $product_12->total =  $product_12->price;
        $product_12->save();

        $publication_12 = new \App\Publication;
        $publication_12->title ="tablas de cedro Creado por Seed product 11";
        $publication_12->description="Descripcion 12 creado por el seed";
        $publication_12->company_id =$person_premium_8->company_id;
        $publication_12->user_id=$person_premium_8->user_id;
        $publication_12->contact_id=$person_premium_8->contact_id;
        $publication_12->address_id=$person_premium_8->address_id;
        $publication_12->typePublication = 'offert';
        $publication_12->status = 'approved';
        $publication_12->price = $product_12->price;
        $publication_12->total =  $product_12->total;
        $publication_12->save();
        $publication_12->products()->attach([$product_12->id]);
        $publication_12->save();


        $WayPay_13 = \App\WayPay::create([
            'money' => '$',
            'ammount' => 320,
            'iva' => true,
        ]);

        $WayPay_13->paymentTypes()->attach([1,2,3,4]);
        $WayPay_13->save();

        $qualitativeParameter = new QualitativeParameter();
        $qualitativeParameter->save();

        $product_13 = new \App\Product();
        $product_13->subcategory_id = 1;
        $product_13->availability_id = $availibity_1->id;
        $product_13->condition_id = $condition->id;
        $product_13->kind_id = $kind_2->id;
        $product_13->dimension_id = $dimension_3->id;
        $product_13->type_wood_id = $typeWood_1->id;
        $product_13->type_product_id = $typeProduct_5->id;
        $product_13->way_pay_id = $WayPay_13->id;
        $product_13->qualitative_parameter_id = $qualitativeParameter->id;
        $product_13->price = 320;
        $product_13->total =  $product_13->price;
        $product_13->save();

        $publication_13 = new \App\Publication;
        $publication_13->title ="Machimbe de guatambú Creado por Seed product 13";
        $publication_13->description="Descripcion 13 creado por el seed";
        $publication_13->company_id =$person_premium_8->company_id;
        $publication_13->user_id=$person_premium_8->user_id;
        $publication_13->contact_id=$person_premium_8->contact_id;
        $publication_13->address_id=$person_premium_8->address_id;
        $publication_13->typePublication = 'offert';
        $publication_13->status = 'approved';
        $publication_13->price = $product_13->price;
        $publication_13->total =  $product_13->total;
        $publication_13->save();
        $publication_13->products()->attach([$product_13->id]);
        $publication_13->save();


        $WayPay_14 = \App\WayPay::create([
            'money' => '$',
            'ammount' => 147,
            'iva' => true,
        ]);

        $WayPay_14->paymentTypes()->attach([1]);
        $WayPay_14->save();

        $qualitativeParameter = new QualitativeParameter();
        $qualitativeParameter->save();

        $product_14 = new \App\Product();
        $product_14->subcategory_id = 1;
        $product_14->availability_id = $availibity_2->id;
        $product_14->condition_id = $condition->id;
        $product_14->kind_id = $kind_2->id;
        $product_14->dimension_id = $dimension_4->id;
        $product_14->type_wood_id = $typeWood_1->id;
        $product_14->type_product_id = $typeProduct_9->id;
        $product_14->way_pay_id = $WayPay_14->id;
        $product_14->qualitative_parameter_id = $qualitativeParameter->id;
        $product_14->price = 147;
        $product_14->total =  $product_14->price;
        $product_14->save();

        $publication_14 = new \App\Publication;
        $publication_14->title ="Vigas de timbó Creado por Seed product 14";
        $publication_14->description="Descripcion 14 creado por el seed";
        $publication_14->company_id =$person_premium_8->company_id;
        $publication_14->user_id=$person_premium_8->user_id;
        $publication_14->contact_id=$person_premium_8->contact_id;
        $publication_14->address_id=$person_premium_8->address_id;
        $publication_14->typePublication = 'offert';
        $publication_14->status = 'approved';
        $publication_14->price = $product_14->price;
        $publication_14->total =  $product_14->total;
        $publication_14->save();
        $publication_14->products()->attach([$product_14->id]);
        $publication_14->save();

        $WayPay_15 = \App\WayPay::create([
            'money' => '$',
            'ammount' => 660,
            'iva' => true,
        ]);

        $WayPay_15->paymentTypes()->attach([1,2,3,4]);
        $WayPay_15->save();

        $qualitativeParameter = new QualitativeParameter();
        $qualitativeParameter->save();


        $product_15 = new \App\Product();
        $product_15->subcategory_id = 1;
        $product_15->availability_id = $availibity_2->id;
        $product_15->condition_id = $condition->id;
        $product_15->kind_id = $kind_2->id;
        $product_15->dimension_id = $dimension_3->id;
        $product_15->type_wood_id = $typeWood_1->id;
        $product_15->type_product_id = $typeProduct_9->id;
        $product_15->way_pay_id = $WayPay_15->id;
        $product_15->qualitative_parameter_id = $qualitativeParameter->id;
        $product_15->price = 660;
        $product_15->total =  $product_15->price;
        $product_15->save();

        $publication_15 = new \App\Publication;
        $publication_15->title ="Laurel Negro Creado por Seed product 15";
        $publication_15->description="Descripcion 15 creado por el seed";
        $publication_15->company_id =$person_premium_8->company_id;
        $publication_15->user_id=$person_premium_8->user_id;
        $publication_15->contact_id=$person_premium_8->contact_id;
        $publication_15->address_id=$person_premium_8->address_id;
        $publication_15->typePublication = 'offert';
        $publication_15->status = 'approved';
        $publication_15->price = $product_15->price;
        $publication_15->total =  $product_15->total;
        $publication_15->save();
        $publication_15->products()->attach([$product_15->id]);
        $publication_15->save();


        $WayPay_16 = \App\WayPay::create([
            'money' => '$',
            'ammount' => 520,
            'iva' => true,
        ]);

        $WayPay_16->paymentTypes()->attach([3,4,5]);
        $WayPay_16->save();

        $qualitativeParameter = new QualitativeParameter();
        $qualitativeParameter->save();

        $product_16 = new \App\Product();
        $product_16->subcategory_id = 1;
        $product_16->availability_id = $availibity_2->id;
        $product_16->condition_id = $condition->id;
        $product_16->kind_id = $kind_2->id;
        $product_16->dimension_id = $dimension_2->id;
        $product_16->type_wood_id = $typeWood_1->id;
        $product_16->type_product_id = $typeProduct_9->id;
        $product_16->way_pay_id = $WayPay_16->id;
        $product_16->qualitative_parameter_id = $qualitativeParameter->id;
        $product_16->price = 520;
        $product_16->total =  $product_16->price;
        $product_16->save();

        $publication_16 = new \App\Publication;
        $publication_16->title ="Listones de pino Creado por Seed product 16";
        $publication_16->description="Descripcion 16 creado por el seed";
        $publication_16->company_id =$person_premium_8->company_id;
        $publication_16->user_id=$person_premium_8->user_id;
        $publication_16->contact_id=$person_premium_8->contact_id;
        $publication_16->address_id=$person_premium_8->address_id;
        $publication_16->typePublication = 'offert';
        $publication_16->status = 'approved';
        $publication_16->price = $product_16->price;
        $publication_16->total =  $product_16->total;
        $publication_16->save();
        $publication_16->products()->attach([$product_16->id]);
        $publication_16->save();

        $WayPay_18 = \App\WayPay::create([
            'money' => '$',
            'ammount' => 320,
            'iva' => true,
        ]);

        $WayPay_18->paymentTypes()->attach([3,4,5]);
        $WayPay_18->save();

        $qualitativeParameter = new QualitativeParameter();
        $qualitativeParameter->save();

        $product_18 = new \App\Product();
        $product_18->subcategory_id = 1;
        $product_18->availability_id = $availibity_1->id;
        $product_18->condition_id = $condition->id;
        $product_18->kind_id = $kind_2->id;
        $product_18->dimension_id = $dimension_1->id;
        $product_18->type_wood_id = $typeWood_1->id;
        $product_18->type_product_id = $typeProduct_2->id;
        $product_18->way_pay_id = $WayPay_18->id;
        $product_18->qualitative_parameter_id = $qualitativeParameter->id;
        $product_18->price = 825;
        $product_18->total =  $product_18->price;
        $product_18->save();

        $publication_18 = new \App\Publication;
        $publication_18->title ="Varillas de pino Creado por Seed product 17";
        $publication_18->description="Descripcion 17 creado por el seed";
        $publication_18->company_id =$person_premium_8->company_id;
        $publication_18->user_id=$person_premium_8->user_id;
        $publication_18->contact_id=$person_premium_8->contact_id;
        $publication_18->address_id=$person_premium_8->address_id;
        $publication_18->typePublication = 'offert';
        $publication_18->status = 'approved';
        $publication_18->price = $product_18->price;
        $publication_18->total =  $product_18->total;
        $publication_18->save();
        $publication_18->products()->attach([$product_18->id]);
        $publication_18->save();



        $WayPay_17 = \App\WayPay::create([
            'money' => '$',
            'ammount' => 415.75,
            'iva' => true,
        ]);

        $WayPay_17->paymentTypes()->attach([3,4,5]);
        $WayPay_17->save();

        $qualitativeParameter = new QualitativeParameter();
        $qualitativeParameter->save();

        $product_17 = new \App\Product();
        $product_17->subcategory_id = 1;
        $product_17->availability_id = $availibity_1->id;
        $product_17->condition_id = $condition->id;
        $product_17->kind_id = $kind_2->id;
        $product_17->dimension_id = $dimension_4->id;
        $product_17->type_wood_id = $typeWood_1->id;
        $product_17->type_product_id = $typeProduct_3->id;
        $product_17->way_pay_id = $WayPay_17->id;
        $product_17->qualitative_parameter_id = $qualitativeParameter->id;
        $product_17->price = 415.75;
        $product_17->total =  $product_17->price;
        $product_17->save();

        $publication_17 = new \App\Publication;
        $publication_17->title ="Retazos de loro negro Creado por Seed product 18";
        $publication_17->description="Descripcion 18 creado por el seed";
        $publication_17->company_id =$person_premium_8->company_id;
        $publication_17->user_id=$person_premium_8->user_id;
        $publication_17->contact_id=$person_premium_8->contact_id;
        $publication_17->address_id=$person_premium_8->address_id;
        $publication_17->typePublication = 'offert';
        $publication_17->status = 'approved';
        $publication_17->price = $product_17->price;
        $publication_17->total =  $product_17->total;
        $publication_17->save();
        $publication_17->products()->attach([$product_17->id]);
        $publication_17->save();

        $WayPay_19 = \App\WayPay::create([
            'money' => '$',
            'ammount' => 780.99,
            'iva' => true,
        ]);

        $WayPay_19->paymentTypes()->attach([3,4,5]);
        $WayPay_19->save();

        $qualitativeParameter = new QualitativeParameter();
        $qualitativeParameter->save();

        $product_19 = new \App\Product();
        $product_19->subcategory_id = 1;
        $product_19->availability_id = $availibity_1->id;
        $product_19->condition_id = $condition->id;
        $product_19->kind_id = $kind_2->id;
        $product_19->dimension_id = $dimension_2->id;
        $product_19->type_wood_id = $typeWood_1->id;
        $product_19->type_product_id = $typeProduct_4->id;
        $product_19->way_pay_id = $WayPay_19->id;
        $product_19->qualitative_parameter_id = $qualitativeParameter->id;

        $product_19->price = 780.99;
        $product_19->total =  $product_19->price;
        $product_19->save();

        $publication_19 = new \App\Publication;
        $publication_19->title ="Pino Creado por Seed product 19";
        $publication_19->description="Descripcion 19 creado por el seed";
        $publication_19->company_id =$person_premium_8->company_id;
        $publication_19->user_id=$person_premium_8->user_id;
        $publication_19->contact_id=$person_premium_8->contact_id;
        $publication_19->address_id=$person_premium_8->address_id;
        $publication_19->typePublication = 'offert';
        $publication_19->status = 'approved';
        $publication_19->price = $product_19->price;
        $publication_19->total =  $product_7->total;
        $publication_19->save();
        $publication_19->products()->attach([$product_19->id]);
        $publication_19->save();

        $WayPay_20 = \App\WayPay::create([
            'money' => '$',
            'ammount' => 412.89,
            'iva' => true,
        ]);

        $WayPay_20->paymentTypes()->attach([1]);
        $WayPay_20->save();

        $qualitativeParameter = new QualitativeParameter();
        $qualitativeParameter->save();

        $product_20 = new \App\Product();
        $product_20->subcategory_id = 1;
        $product_20->availability_id = $availibity_2->id;
        $product_20->condition_id = $condition->id;
        $product_20->kind_id = $kind_2->id;
        $product_20->dimension_id = $dimension_2->id;
        $product_20->type_wood_id = $typeWood_1->id;
        $product_20->type_product_id = $typeProduct_7->id;
        $product_20->way_pay_id = $product_20->id;
        $product_20->qualitative_parameter_id = $qualitativeParameter->id;
        $product_20->price = 412.89;
        $product_20->total =  $product_20->price;
        $product_20->save();

        $publication_20 = new \App\Publication;
        $publication_20->title ="Listones de algarrobo Creado por Seed product 20";
        $publication_20->description="Descripcion 20 creado por el seed";
        $publication_20->company_id =$person_premium_8->company_id;
        $publication_20->user_id=$person_premium_8->user_id;
        $publication_20->contact_id=$person_premium_8->contact_id;
        $publication_20->address_id=$person_premium_8->address_id;
        $publication_20->typePublication = 'offert';
        $publication_20->status = 'approved';
        $publication_20->price = $product_20->price;
        $publication_20->total =  $product_7->total;
        $publication_20->save();
        $publication_20->products()->attach([$product_20->id]);
        $publication_20->save();
    }


    private function createPublicationWithOffertUser3()
    {
        $availibity_1 = \App\Availability::create([
            'cantitude' => 30,
            'availability' => 'limited',
            'unitMeasurement' => Unit::PIETABLAR,
            'measurementTerm' => null,
        ]);

        $availibity_2 = \App\Availability::create([
            'cantitude' => 30,
            'availability' => 'continuous',
            'unitMeasurement' => Unit::PIETABLAR,
            'measurementTerm' => null,
        ]);

        //Condiciones

        $condition = \App\Condition::create([
            'piece' => 1,
            'weight' => 1,
            'length' => 1,
            'tall' => 11,
            'zunchos' => 'No',
            //envoltorio
            'wrapping' => 'Yes'
        ]);


        //dimensiones
        $dimension_1 = \App\Dimension::create([
            'actualThickness' => 35,
            'actualWidth' => 55,
            'actualLength' => 2500,
        ]);


        $dimension_2 = \App\Dimension::create([
            'actualThickness' => 65,
            'actualWidth' => 255,
            'actualLength' => 3500,
        ]);

        $dimension_3 = \App\Dimension::create([
            'actualThickness' => 85,
            'actualWidth' => 255,
            'actualLength' => 1555,
        ]);

        $dimension_4 = \App\Dimension::create([
            'actualThickness' => 75,
            'actualWidth' => 255,
            'actualLength' => 850,
        ]);
        //kind
        $kind_1 = \App\Kind::find(1);
        $kind_2 = \App\Kind::find(2);
        $kind_3 = \App\Kind::find(3);
        $kind_4 = \App\Kind::find(4);
        $kind_5 = \App\Kind::find(5);
        $kind_6 = \App\Kind::find(6);
        $kind_7 = \App\Kind::find(7);
        $kind_8 = \App\Kind::find(8);
        $kind_9 = \App\Kind::find(9);
        $kind_10 = \App\Kind::find(10);
        $kind_11 = \App\Kind::find(11);
        $kind_12 = \App\Kind::find(12);
        $kind_13 = \App\Kind::find(13);
        $kind_14 = \App\Kind::find(14);

        //nativa 1 implantada 2
        $typeWood_1 = \App\TypeWood::find(1);
        $typeWood_2 = \App\TypeWood::find(2);

        $typeProduct_1 = \App\TypeProduct::find(1);

        $typeProduct_2 = \App\TypeProduct::find(2);
        $typeProduct_3 = \App\TypeProduct::find(3);
        $typeProduct_4 = \App\TypeProduct::find(4);
        $typeProduct_5 = \App\TypeProduct::find(5);
        $typeProduct_6 = \App\TypeProduct::find(6);
        $typeProduct_7 = \App\TypeProduct::find(7);
        $typeProduct_8 = \App\TypeProduct::find(8);
        $typeProduct_9 = \App\TypeProduct::find(9);
        $typeProduct_10 = \App\TypeProduct::find(10);
        $typeProduct_11 = \App\TypeProduct::find(11);

        //forma de pago

        $WayPay = \App\WayPay::create([
            'money' => '$',
            'ammount' => 100,
            'iva' => true,
        ]);

        $WayPay->paymentTypes()->attach([1, 2, 3]);
        $WayPay->save();

        $qualitativeParameter = new QualitativeParameter();
        $qualitativeParameter->save();

        $person_premium_8 = \App\Person::find(6);
        $qualitativeParameter = new QualitativeParameter();
        $qualitativeParameter->save();
        $product = new \App\Product();
        $product->subcategory_id = 1;
        $product->availability_id = $availibity_2->id;
        $product->condition_id = $condition->id;
        $product->kind_id = $kind_1->id;
        $product->dimension_id = $dimension_2->id;
        $product->type_wood_id = $typeWood_1->id;
        $product->type_product_id = $typeProduct_2->id;
        $product->way_pay_id = $WayPay->id;
        $product->qualitative_parameter_id = $qualitativeParameter->id;
        $product->price = 100;
        $product->total = $product->price;
        $product->save();

        $product->qualitative_parameter_id = $qualitativeParameter->id;$publication
        = new \App\Publication;
        $publication->title = "Pino Creado por Seed product 1";
        $publication->description = "Descripcion creado por el seed";
        $publication->company_id = $person_premium_8->company_id;
        $publication->user_id = $person_premium_8->user_id;
        $publication->contact_id = $person_premium_8->contact_id;
        $publication->address_id = $person_premium_8->address_id;
        $publication->typePublication = 'offert';
        $publication->status = 'approved';
        $publication->price = $product->price;
        $publication->total = $product->total;
        $publication->save();
        $publication->products()->attach([$product->id]);
        $publication->save();


        $WayPay_2 = \App\WayPay::create([
            'money' => '$',
            'ammount' => 110,
            'iva' => true,
        ]);

        $WayPay_2->paymentTypes()->attach([1, 3, 4]);
        $WayPay_2->save();

        $qualitativeParameter = new QualitativeParameter();
        $qualitativeParameter->save();

        $product_2 = new \App\Product();
        $product_2->subcategory_id = 1;
        $product_2->availability_id = $availibity_1->id;
        $product_2->condition_id = $condition->id;
        $product_2->kind_id = $kind_2->id;
        $product_2->dimension_id = $dimension_2->id;
        $product_2->type_wood_id = $typeWood_2->id;
        $product_2->type_product_id = $typeProduct_3->id;
        $product_2->way_pay_id = $WayPay_2->id;
        $product_2->qualitative_parameter_id = $qualitativeParameter->id;
        $product_2->price = 110;
        $product_2->total = $product_2->price;
        $product_2->save();

        $publication_2 = new \App\Publication;
        $publication_2->title = "Pino Creado por Seed product 2";
        $publication_2->description = "Descripcion 2 creado por el seed";
        $publication_2->company_id = $person_premium_8->company_id;
        $publication_2->user_id = $person_premium_8->user_id;
        $publication_2->contact_id = $person_premium_8->contact_id;
        $publication_2->address_id = $person_premium_8->address_id;
        $publication_2->typePublication = 'offert';
        $publication_2->status = 'approved';
        $publication_2->price = $product_2->price;
        $publication_2->total = $product_2->total;
        $publication_2->save();
        $publication_2->products()->attach([$product_2->id]);
        $publication_2->save();


        $WayPay_3 = \App\WayPay::create([
            'money' => '$',
            'ammount' => 200,
            'iva' => true,
        ]);

        $WayPay_3->paymentTypes()->attach([1]);
        $WayPay_3->save();

        $qualitativeParameter = new QualitativeParameter();
        $qualitativeParameter->save();

        $product_3 = new \App\Product();
        $product_3->subcategory_id = 1;
        $product_3->availability_id = $availibity_2->id;
        $product_3->condition_id = $condition->id;
        $product_3->kind_id = $kind_2->id;
        $product_3->dimension_id = $dimension_2->id;
        $product_3->type_wood_id = $typeWood_1->id;
        $product_3->type_product_id = $typeProduct_3->id;
        $product_3->way_pay_id = $WayPay_3->id;
        $product_3->qualitative_parameter_id = $qualitativeParameter->id;
        $product_3->price = 200;
        $product_3->total = $product_3->price;
        $product_3->save();

        $publication_3 = new \App\Publication;
        $publication_3->title = "Pino Creado por Seed product 3";
        $publication_3->description = "Descripcion 3 creado por el seed";
        $publication_3->company_id = $person_premium_8->company_id;
        $publication_3->user_id = $person_premium_8->user_id;
        $publication_3->contact_id = $person_premium_8->contact_id;
        $publication_3->address_id = $person_premium_8->address_id;
        $publication_3->typePublication = 'offert';
        $publication_3->status = 'approved';
        $publication_3->price = $product_3->price;
        $publication_3->total = $product_3->total;
        $publication_3->save();
        $publication_3->products()->attach([$product_3->id]);
        $publication_3->save();


        $WayPay_4 = \App\WayPay::create([
            'money' => '$',
            'ammount' => 250,
            'iva' => true,
        ]);

        $WayPay_4->paymentTypes()->attach([2]);
        $WayPay_4->save();

        $qualitativeParameter = new QualitativeParameter();
        $qualitativeParameter->save();

        $product_4 = new \App\Product();
        $product_4->subcategory_id = 1;
        $product_4->availability_id = $availibity_1->id;
        $product_4->condition_id = $condition->id;
        $product_4->kind_id = $kind_2->id;
        $product_4->dimension_id = $dimension_2->id;
        $product_4->type_wood_id = $typeWood_1->id;
        $product_4->type_product_id = $typeProduct_4->id;
        $product_4->way_pay_id = $WayPay_4->id;
        $product_4->qualitative_parameter_id = $qualitativeParameter->id;
        $product_4->price = 250;
        $product_4->total = $product_4->price;
        $product_4->save();

        $publication_4 = new \App\Publication;
        $publication_4->title = "Algarrobo Creado por Seed product 4";
        $publication_4->description = "Descripcion 4 creado por el seed";
        $publication_4->company_id = $person_premium_8->company_id;
        $publication_4->user_id = $person_premium_8->user_id;
        $publication_4->contact_id = $person_premium_8->contact_id;
        $publication_4->address_id = $person_premium_8->address_id;
        $publication_4->typePublication = 'offert';
        $publication_4->status = 'approved';
        $publication_4->price = $product_4->price;
        $publication_4->total = $product_4->total;
        $publication_4->save();
        $publication_4->products()->attach([$product_4->id]);
        $publication_4->save();


        $WayPay_5 = \App\WayPay::create([
            'money' => '$',
            'ammount' => 450,
            'iva' => true,
        ]);

        $WayPay_5->paymentTypes()->attach([2]);
        $WayPay_5->save();

        $qualitativeParameter = new QualitativeParameter();
        $qualitativeParameter->save();

        $product_5 = new \App\Product();
        $product_5->subcategory_id = 1;
        $product_5->availability_id = $availibity_2->id;
        $product_5->condition_id = $condition->id;
        $product_5->kind_id = $kind_2->id;
        $product_5->dimension_id = $dimension_3->id;
        $product_5->type_wood_id = $typeWood_1->id;
        $product_5->type_product_id = $typeProduct_6->id;
        $product_5->way_pay_id = $WayPay_5->id;
        $product_5->qualitative_parameter_id = $qualitativeParameter->id;

        $product_5->price = 450;
        $product_5->total = $product_5->price;
        $product_5->save();

        $publication_5 = new \App\Publication;
        $publication_5->title = "Algarrobo Creado por Seed product 5";
        $publication_5->description = "Descripcion 5 creado por el seed";
        $publication_5->company_id = $person_premium_8->company_id;
        $publication_5->user_id = $person_premium_8->user_id;
        $publication_5->contact_id = $person_premium_8->contact_id;
        $publication_5->address_id = $person_premium_8->address_id;
        $publication_5->typePublication = 'offert';
        $publication_5->status = 'approved';
        $publication_5->price = $product_5->price;
        $publication_5->total = $product_5->total;
        $publication_5->save();
        $publication_5->products()->attach([$product_5->id]);
        $publication_5->save();

        $WayPay_6 = \App\WayPay::create([
            'money' => '$',
            'ammount' => 402,
            'iva' => true,
        ]);

        $WayPay_6->paymentTypes()->attach([1, 2]);
        $WayPay_6->save();

        $qualitativeParameter = new QualitativeParameter();
        $qualitativeParameter->save();

        $product_6 = new \App\Product();
        $product_6->subcategory_id = 1;
        $product_6->availability_id = $availibity_1->id;
        $product_6->condition_id = $condition->id;
        $product_6->kind_id = $kind_2->id;
        $product_6->dimension_id = $dimension_4->id;
        $product_6->type_wood_id = $typeWood_1->id;
        $product_6->type_product_id = $typeProduct_7->id;
        $product_6->way_pay_id = $WayPay_6->id;
        $product_6->qualitative_parameter_id = $qualitativeParameter->id;
        $product_6->price = 402;
        $product_6->total = $product_6->price;
        $product_6->save();

        $publication_6 = new \App\Publication;
        $publication_6->title = "Tirante Creado por Seed product 6";
        $publication_6->description = "Descripcion 6 creado por el seed";
        $publication_6->company_id = $person_premium_8->company_id;
        $publication_6->user_id = $person_premium_8->user_id;
        $publication_6->contact_id = $person_premium_8->contact_id;
        $publication_6->address_id = $person_premium_8->address_id;
        $publication_6->typePublication = 'offert';
        $publication_6->status = 'approved';
        $publication_6->price = $product_6->price;
        $publication_6->total = $product_6->total;
        $publication_6->save();
        $publication_6->products()->attach([$product_6->id]);
        $publication_6->save();


        $WayPay_8 = \App\WayPay::create([
            'money' => '$',
            'ammount' => 375,
            'iva' => true,
        ]);

        $WayPay_8->paymentTypes()->attach([2]);
        $WayPay_8->save();

        $qualitativeParameter = new QualitativeParameter();
        $qualitativeParameter->save();

        $product_8 = new \App\Product();
        $product_8->subcategory_id = 1;
        $product_8->availability_id = $availibity_1->id;
        $product_8->condition_id = $condition->id;
        $product_8->kind_id = $kind_2->id;
        $product_8->dimension_id = $dimension_2->id;
        $product_8->type_wood_id = $typeWood_1->id;
        $product_8->type_product_id = $typeProduct_2->id;
        $product_8->way_pay_id = $WayPay_8->id;
        $product_8->qualitative_parameter_id = $qualitativeParameter->id;
        $product_8->price = 375;
        $product_8->total = $product_8->price;
        $product_8->save();

        $publication_8 = new \App\Publication;
        $publication_8->title = "Tirantes Creado por Seed product 7";
        $publication_8->description = "Descripcion 7 creado por el seed";
        $publication_8->company_id = $person_premium_8->company_id;
        $publication_8->user_id = $person_premium_8->user_id;
        $publication_8->contact_id = $person_premium_8->contact_id;
        $publication_8->address_id = $person_premium_8->address_id;
        $publication_8->typePublication = 'offert';
        $publication_8->status = 'approved';
        $publication_8->price = $product_8->price;
        $publication_8->total = $product_8->total;
        $publication_8->save();
        $publication_8->products()->attach([$product_8->id]);
        $publication_8->save();

        $WayPay_7 = \App\WayPay::create([
            'money' => '$',
            'ammount' => 175,
            'iva' => true,
        ]);

        $WayPay_7->paymentTypes()->attach([2]);
        $WayPay_7->save();

        $qualitativeParameter = new QualitativeParameter();
        $qualitativeParameter->save();

        $product_7 = new \App\Product();
        $product_7->subcategory_id = 1;
        $product_7->availability_id = $availibity_2->id;
        $product_7->condition_id = $condition->id;
        $product_7->kind_id = $kind_2->id;
        $product_7->dimension_id = $dimension_3->id;
        $product_7->type_wood_id = $typeWood_1->id;
        $product_7->type_product_id = $typeProduct_3->id;
        $product_7->way_pay_id = $WayPay_7->id;
        $product_7->qualitative_parameter_id = $qualitativeParameter->id;
        $product_7->price = 175;
        $product_7->total = $product_7->price;
        $product_7->save();

        $publication_7 = new \App\Publication;
        $publication_7->title = "Paraiso Creado por Seed product 8";
        $publication_7->description = "Descripcion 8 creado por el seed";
        $publication_7->company_id = $person_premium_8->company_id;
        $publication_7->user_id = $person_premium_8->user_id;
        $publication_7->contact_id = $person_premium_8->contact_id;
        $publication_7->address_id = $person_premium_8->address_id;
        $publication_7->typePublication = 'offert';
        $publication_7->status = 'approved';
        $publication_7->price = $product_7->price;
        $publication_7->total = $product_7->total;
        $publication_7->save();
        $publication_7->products()->attach([$product_7->id]);
        $publication_7->save();


        $WayPay_9 = \App\WayPay::create([
            'money' => '$',
            'ammount' => 850,
            'iva' => true,
        ]);

        $WayPay_9->paymentTypes()->attach([2, 4]);
        $WayPay_9->save();

        $qualitativeParameter = new QualitativeParameter();
        $qualitativeParameter->save();

        $product_9 = new \App\Product();
        $product_9->subcategory_id = 1;
        $product_9->availability_id = $availibity_1->id;
        $product_9->condition_id = $condition->id;
        $product_9->kind_id = $kind_2->id;
        $product_9->dimension_id = $dimension_4->id;
        $product_9->type_wood_id = $typeWood_1->id;
        $product_9->type_product_id = $typeProduct_7->id;
        $product_9->way_pay_id = $WayPay_9->id;
        $product_9->qualitative_parameter_id = $qualitativeParameter->id;
        $product_9->price = 850;
        $product_9->total = $product_9->price;
        $product_9->save();

        $publication_9 = new \App\Publication;
        $publication_9->title = "Tablas de paraiso Creado por Seed product 9";
        $publication_9->description = "Descripcion 9 creado por el seed";
        $publication_9->company_id = $person_premium_8->company_id;
        $publication_9->user_id = $person_premium_8->user_id;
        $publication_9->contact_id = $person_premium_8->contact_id;
        $publication_9->address_id = $person_premium_8->address_id;
        $publication_9->typePublication = 'offert';
        $publication_9->status = 'approved';
        $publication_9->price = $product_9->price;
        $publication_9->total = $product_7->total;
        $publication_9->save();
        $publication_9->products()->attach([$product_9->id]);
        $publication_9->save();


        $WayPay_10 = \App\WayPay::create([
            'money' => '$',
            'ammount' => 950,
            'iva' => true,
        ]);

        $WayPay_10->paymentTypes()->attach([2, 4]);
        $WayPay_10->save();

        $qualitativeParameter = new QualitativeParameter();
        $qualitativeParameter->save();

        $product_10 = new \App\Product();
        $product_10->subcategory_id = 1;
        $product_10->availability_id = $availibity_2->id;
        $product_10->condition_id = $condition->id;
        $product_10->kind_id = $kind_2->id;
        $product_10->dimension_id = $dimension_1->id;
        $product_10->type_wood_id = $typeWood_1->id;
        $product_10->type_product_id = $typeProduct_8->id;
        $product_10->way_pay_id = $WayPay_10->id;
        $product_10->qualitative_parameter_id = $qualitativeParameter->id;
        $product_10->price = 950;
        $product_10->total = $product_10->price;
        $product_10->save();

        $publication_10 = new \App\Publication;
        $publication_10->title = "Tablas de araucaria Creado por Seed product 10";
        $publication_10->description = "Descripcion 10 creado por el seed";
        $publication_10->company_id = $person_premium_8->company_id;
        $publication_10->user_id = $person_premium_8->user_id;
        $publication_10->contact_id = $person_premium_8->contact_id;
        $publication_10->address_id = $person_premium_8->address_id;
        $publication_10->typePublication = 'offert';
        $publication_10->status = 'approved';
        $publication_10->price = $product_10->price;
        $publication_10->total = $product_7->total;
        $publication_10->save();
        $publication_10->products()->attach([$product_10->id]);
        $publication_10->save();
    }


    private function createPublicationWithOffertUser4()
    {
        $availibity_1 = \App\Availability::create([
            'cantitude' => 30,
            'availability' => 'limited',
            'unitMeasurement' => Unit::PIETABLAR,
            'measurementTerm' => null,
        ]);

        $availibity_2 = \App\Availability::create([
            'cantitude' => 30,
            'availability' => 'continuous',
            'unitMeasurement' => Unit::PIETABLAR,
            'measurementTerm' => null,
        ]);

        //Condiciones

        $condition = \App\Condition::create([
            'piece' => 1,
            'weight' => 1,
            'length' => 1,
            'tall' => 11,
            'zunchos' => 'No',
            //envoltorio
            'wrapping' => 'Yes'
        ]);


        //dimensiones
        $dimension_1 = \App\Dimension::create([
            'actualThickness' => 39,
            'actualWidth' => 59,
            'actualLength' => 2900,
        ]);


        $dimension_2 = \App\Dimension::create([
            'actualThickness' => 69,
            'actualWidth' => 299,
            'actualLength' => 3599,
        ]);

        $dimension_3 = \App\Dimension::create([
            'actualThickness' => 89,
            'actualWidth' => 299,
            'actualLength' => 1599,
        ]);

        $dimension_4 = \App\Dimension::create([
            'actualThickness' => 79,
            'actualWidth' => 259,
            'actualLength' => 859,
        ]);
        //kind
        $kind_1 = \App\Kind::find(1);
        $kind_2 = \App\Kind::find(2);
        $kind_3 = \App\Kind::find(3);
        $kind_4 = \App\Kind::find(4);
        $kind_5 = \App\Kind::find(5);
        $kind_6 = \App\Kind::find(6);
        $kind_7 = \App\Kind::find(7);
        $kind_8 = \App\Kind::find(8);
        $kind_9 = \App\Kind::find(9);
        $kind_10 = \App\Kind::find(10);
        $kind_11 = \App\Kind::find(11);
        $kind_12 = \App\Kind::find(12);
        $kind_13 = \App\Kind::find(13);
        $kind_14 = \App\Kind::find(14);

        //nativa 1 implantada 2
        $typeWood_1 = \App\TypeWood::find(1);
        $typeWood_2 = \App\TypeWood::find(2);

        $typeProduct_1 = \App\TypeProduct::find(1);

        $typeProduct_2 = \App\TypeProduct::find(2);
        $typeProduct_3 = \App\TypeProduct::find(3);
        $typeProduct_4 = \App\TypeProduct::find(4);
        $typeProduct_5 = \App\TypeProduct::find(5);
        $typeProduct_6 = \App\TypeProduct::find(6);
        $typeProduct_7 = \App\TypeProduct::find(7);
        $typeProduct_8 = \App\TypeProduct::find(8);
        $typeProduct_9 = \App\TypeProduct::find(9);
        $typeProduct_10 = \App\TypeProduct::find(10);
        $typeProduct_11 = \App\TypeProduct::find(11);

        //forma de pago

        $WayPay = \App\WayPay::create([
            'money' => '$',
            'ammount' => 100,
            'iva' => true,
        ]);

        $WayPay->paymentTypes()->attach([1, 2, 3]);
        $WayPay->save();

        $qualitativeParameter = new QualitativeParameter();
        $qualitativeParameter->save();

        $person_premium_8 = \App\Person::find(7);

        $qualitativeParameter = new QualitativeParameter();
        $qualitativeParameter->save();

        $product = new \App\Product();
        $product->subcategory_id = 1;
        $product->availability_id = $availibity_2->id;
        $product->condition_id = $condition->id;
        $product->kind_id = $kind_1->id;
        $product->dimension_id = $dimension_2->id;
        $product->type_wood_id = $typeWood_1->id;
        $product->type_product_id = $typeProduct_2->id;
        $product->qualitative_parameter_id = $qualitativeParameter->id;

        $product->way_pay_id = $WayPay->id;
        $product->price = 100;

        $product->total = $product->price;
        $product->save();

        $product->qualitative_parameter_id = $qualitativeParameter->id;$publication
        = new \App\Publication;
        $publication->title = "Pino Creado por Seed product 1";
        $publication->description = "Descripcion creado por el seed";
        $publication->company_id = $person_premium_8->company_id;
        $publication->user_id = $person_premium_8->user_id;
        $publication->contact_id = $person_premium_8->contact_id;
        $publication->address_id = $person_premium_8->address_id;
        $publication->typePublication = 'offert';
        $publication->status = 'approved';
        $publication->price = $product->price;
        $publication->total = $product->total;
        $publication->save();
        $publication->products()->attach([$product->id]);
        $publication->save();


        $WayPay_2 = \App\WayPay::create([
            'money' => '$',
            'ammount' => 110,
            'iva' => true,
        ]);

        $WayPay_2->paymentTypes()->attach([1, 3, 4]);
        $WayPay_2->save();

        $qualitativeParameter = new QualitativeParameter();
        $qualitativeParameter->save();

        $product_2 = new \App\Product();
        $product_2->subcategory_id = 1;
        $product_2->availability_id = $availibity_1->id;
        $product_2->condition_id = $condition->id;
        $product_2->kind_id = $kind_2->id;
        $product_2->dimension_id = $dimension_2->id;
        $product_2->type_wood_id = $typeWood_2->id;
        $product_2->type_product_id = $typeProduct_3->id;
        $product_2->way_pay_id = $WayPay_2->id;
        $product_2->qualitative_parameter_id = $qualitativeParameter->id;

        $product_2->price = 110;
        $product_2->total = $product_2->price;
        $product_2->save();

        $publication_2 = new \App\Publication;
        $publication_2->title = "Pino Creado por Seed product 2";
        $publication_2->description = "Descripcion 2 creado por el seed";
        $publication_2->company_id = $person_premium_8->company_id;
        $publication_2->user_id = $person_premium_8->user_id;
        $publication_2->contact_id = $person_premium_8->contact_id;
        $publication_2->address_id = $person_premium_8->address_id;
        $publication_2->typePublication = 'offert';
        $publication_2->status = 'approved';
        $publication_2->price = $product_2->price;
        $publication_2->total = $product_2->total;
        $publication_2->save();
        $publication_2->products()->attach([$product_2->id]);
        $publication_2->save();


        $WayPay_3 = \App\WayPay::create([
            'money' => '$',
            'ammount' => 200,
            'iva' => true,
        ]);

        $WayPay_3->paymentTypes()->attach([1]);
        $WayPay_3->save();

        $qualitativeParameter = new QualitativeParameter();
        $qualitativeParameter->save();

        $product_3 = new \App\Product();
        $product_3->subcategory_id = 1;
        $product_3->availability_id = $availibity_2->id;
        $product_3->condition_id = $condition->id;
        $product_3->kind_id = $kind_2->id;
        $product_3->dimension_id = $dimension_2->id;
        $product_3->type_wood_id = $typeWood_1->id;
        $product_3->type_product_id = $typeProduct_3->id;
        $product_3->way_pay_id = $WayPay_3->id;
        $product_3->qualitative_parameter_id = $qualitativeParameter->id;
        $product_3->price = 200;
        $product_3->total = $product_3->price;
        $product_3->save();

        $publication_3 = new \App\Publication;
        $publication_3->title = "Pino Creado por Seed product 3";
        $publication_3->description = "Descripcion 3 creado por el seed";
        $publication_3->company_id = $person_premium_8->company_id;
        $publication_3->user_id = $person_premium_8->user_id;
        $publication_3->contact_id = $person_premium_8->contact_id;
        $publication_3->address_id = $person_premium_8->address_id;
        $publication_3->typePublication = 'offert';
        $publication_3->status = 'approved';
        $publication_3->price = $product_3->price;
        $publication_3->total = $product_3->total;
        $publication_3->save();
        $publication_3->products()->attach([$product_3->id]);
        $publication_3->save();


        $WayPay_4 = \App\WayPay::create([
            'money' => '$',
            'ammount' => 250,
            'iva' => true,
        ]);

        $WayPay_4->paymentTypes()->attach([2]);
        $WayPay_4->save();

        $qualitativeParameter = new QualitativeParameter();
        $qualitativeParameter->save();

        $product_4 = new \App\Product();
        $product_4->subcategory_id = 1;
        $product_4->availability_id = $availibity_1->id;
        $product_4->condition_id = $condition->id;
        $product_4->kind_id = $kind_2->id;
        $product_4->dimension_id = $dimension_2->id;
        $product_4->type_wood_id = $typeWood_1->id;
        $product_4->type_product_id = $typeProduct_4->id;
        $product_4->way_pay_id = $WayPay_4->id;
        $product_4->qualitative_parameter_id = $qualitativeParameter->id;
        $product_4->price = 250;
        $product_4->total = $product_4->price;
        $product_4->save();

        $publication_4 = new \App\Publication;
        $publication_4->title = "Algarrobo Creado por Seed product 4";
        $publication_4->description = "Descripcion 4 creado por el seed";
        $publication_4->company_id = $person_premium_8->company_id;
        $publication_4->user_id = $person_premium_8->user_id;
        $publication_4->contact_id = $person_premium_8->contact_id;
        $publication_4->address_id = $person_premium_8->address_id;
        $publication_4->typePublication = 'offert';
        $publication_4->status = 'approved';
        $publication_4->price = $product_4->price;
        $publication_4->total = $product_4->total;
        $publication_4->save();
        $publication_4->products()->attach([$product_4->id]);
        $publication_4->save();


        $WayPay_5 = \App\WayPay::create([
            'money' => '$',
            'ammount' => 450,
            'iva' => true,
        ]);

        $WayPay_5->paymentTypes()->attach([2]);
        $WayPay_5->save();


        $qualitativeParameter = new QualitativeParameter();
        $qualitativeParameter->save();

        $product_5 = new \App\Product();
        $product_5->subcategory_id = 1;
        $product_5->availability_id = $availibity_2->id;
        $product_5->condition_id = $condition->id;
        $product_5->kind_id = $kind_2->id;
        $product_5->dimension_id = $dimension_3->id;
        $product_5->type_wood_id = $typeWood_1->id;
        $product_5->type_product_id = $typeProduct_6->id;
        $product_5->way_pay_id = $WayPay_5->id;
        $product_5->qualitative_parameter_id = $qualitativeParameter->id;
        $product_5->price = 450;
        $product_5->total = $product_5->price;
        $product_5->save();

        $publication_5 = new \App\Publication;
        $publication_5->title = "Algarrobo Creado por Seed product 5";
        $publication_5->description = "Descripcion 5 creado por el seed";
        $publication_5->company_id = $person_premium_8->company_id;
        $publication_5->user_id = $person_premium_8->user_id;
        $publication_5->contact_id = $person_premium_8->contact_id;
        $publication_5->address_id = $person_premium_8->address_id;
        $publication_5->typePublication = 'offert';
        $publication_5->status = 'approved';
        $publication_5->price = $product_5->price;
        $publication_5->total = $product_5->total;
        $publication_5->save();
        $publication_5->products()->attach([$product_5->id]);
        $publication_5->save();

        $WayPay_6 = \App\WayPay::create([
            'money' => '$',
            'ammount' => 402,
            'iva' => true,
        ]);

        $WayPay_6->paymentTypes()->attach([1, 2]);
        $WayPay_6->save();

        $qualitativeParameter = new QualitativeParameter();
        $qualitativeParameter->save();

        $product_6 = new \App\Product();
        $product_6->subcategory_id = 1;
        $product_6->availability_id = $availibity_1->id;
        $product_6->condition_id = $condition->id;
        $product_6->kind_id = $kind_2->id;
        $product_6->dimension_id = $dimension_4->id;
        $product_6->type_wood_id = $typeWood_1->id;
        $product_6->type_product_id = $typeProduct_7->id;
        $product_6->way_pay_id = $WayPay_6->id;
        $product_6->qualitative_parameter_id = $qualitativeParameter->id;

        $product_6->price = 402;
        $product_6->total = $product_6->price;
        $product_6->save();

        $publication_6 = new \App\Publication;
        $publication_6->title = "Tirante Creado por Seed product 6";
        $publication_6->description = "Descripcion 6 creado por el seed";
        $publication_6->company_id = $person_premium_8->company_id;
        $publication_6->user_id = $person_premium_8->user_id;
        $publication_6->contact_id = $person_premium_8->contact_id;
        $publication_6->address_id = $person_premium_8->address_id;
        $publication_6->typePublication = 'offert';
        $publication_6->status = 'approved';
        $publication_6->price = $product_6->price;
        $publication_6->total = $product_6->total;
        $publication_6->save();
        $publication_6->products()->attach([$product_6->id]);
        $publication_6->save();


        $WayPay_8 = \App\WayPay::create([
            'money' => '$',
            'ammount' => 375,
            'iva' => true,
        ]);

        $WayPay_8->paymentTypes()->attach([2]);
        $WayPay_8->save();

        $qualitativeParameter = new QualitativeParameter();
        $qualitativeParameter->save();

        $product_8 = new \App\Product();
        $product_8->subcategory_id = 1;
        $product_8->availability_id = $availibity_1->id;
        $product_8->condition_id = $condition->id;
        $product_8->kind_id = $kind_2->id;
        $product_8->dimension_id = $dimension_2->id;
        $product_8->type_wood_id = $typeWood_1->id;
        $product_8->type_product_id = $typeProduct_2->id;
        $product_8->way_pay_id = $WayPay_8->id;
        $product_8->qualitative_parameter_id = $qualitativeParameter->id;
        $product_8->price = 375;
        $product_8->total = $product_8->price;
        $product_8->save();

        $publication_8 = new \App\Publication;
        $publication_8->title = "Tirantes Creado por Seed product 7";
        $publication_8->description = "Descripcion 7 creado por el seed";
        $publication_8->company_id = $person_premium_8->company_id;
        $publication_8->user_id = $person_premium_8->user_id;
        $publication_8->contact_id = $person_premium_8->contact_id;
        $publication_8->address_id = $person_premium_8->address_id;
        $publication_8->typePublication = 'offert';
        $publication_8->status = 'approved';
        $publication_8->price = $product_8->price;
        $publication_8->total = $product_8->total;
        $publication_8->save();
        $publication_8->products()->attach([$product_8->id]);
        $publication_8->save();

        $WayPay_7 = \App\WayPay::create([
            'money' => '$',
            'ammount' => 175,
            'iva' => true,
        ]);

        $WayPay_7->paymentTypes()->attach([2]);
        $WayPay_7->save();

        $qualitativeParameter = new QualitativeParameter();
        $qualitativeParameter->save();

        $product_7 = new \App\Product();
        $product_7->subcategory_id = 1;
        $product_7->availability_id = $availibity_2->id;
        $product_7->condition_id = $condition->id;
        $product_7->kind_id = $kind_2->id;
        $product_7->dimension_id = $dimension_3->id;
        $product_7->type_wood_id = $typeWood_1->id;
        $product_7->type_product_id = $typeProduct_3->id;
        $product_7->way_pay_id = $WayPay_7->id;
        $product_7->qualitative_parameter_id = $qualitativeParameter->id;
        $product_7->price = 175;
        $product_7->total = $product_7->price;
        $product_7->save();

        $publication_7 = new \App\Publication;
        $publication_7->title = "Paraiso Creado por Seed product 8";
        $publication_7->description = "Descripcion 8 creado por el seed";
        $publication_7->company_id = $person_premium_8->company_id;
        $publication_7->user_id = $person_premium_8->user_id;
        $publication_7->contact_id = $person_premium_8->contact_id;
        $publication_7->address_id = $person_premium_8->address_id;
        $publication_7->typePublication = 'offert';
        $publication_7->status = 'approved';
        $publication_7->price = $product_7->price;
        $publication_7->total = $product_7->total;
        $publication_7->save();
        $publication_7->products()->attach([$product_7->id]);
        $publication_7->save();


        $WayPay_9 = \App\WayPay::create([
            'money' => '$',
            'ammount' => 850,
            'iva' => true,
        ]);

        $WayPay_9->paymentTypes()->attach([2, 4]);
        $WayPay_9->save();

        $qualitativeParameter = new QualitativeParameter();
        $qualitativeParameter->save();

        $product_9 = new \App\Product();
        $product_9->subcategory_id = 1;
        $product_9->availability_id = $availibity_1->id;
        $product_9->condition_id = $condition->id;
        $product_9->kind_id = $kind_2->id;
        $product_9->dimension_id = $dimension_4->id;
        $product_9->type_wood_id = $typeWood_1->id;
        $product_9->type_product_id = $typeProduct_7->id;
        $product_9->way_pay_id = $WayPay_9->id;
        $product_9->qualitative_parameter_id = $qualitativeParameter->id;
        $product_9->price = 850;
        $product_9->total = $product_9->price;
        $product_9->save();

        $publication_9 = new \App\Publication;
        $publication_9->title = "Tablas de paraiso Creado por Seed product 9";
        $publication_9->description = "Descripcion 9 creado por el seed";
        $publication_9->company_id = $person_premium_8->company_id;
        $publication_9->user_id = $person_premium_8->user_id;
        $publication_9->contact_id = $person_premium_8->contact_id;
        $publication_9->address_id = $person_premium_8->address_id;
        $publication_9->typePublication = 'offert';
        $publication_9->status = 'approved';
        $publication_9->price = $product_9->price;
        $publication_9->total = $product_7->total;
        $publication_9->save();
        $publication_9->products()->attach([$product_9->id]);
        $publication_9->save();


        $WayPay_10 = \App\WayPay::create([
            'money' => '$',
            'ammount' => 950,
            'iva' => true,
        ]);

        $WayPay_10->paymentTypes()->attach([2, 4]);
        $WayPay_10->save();

        $qualitativeParameter = new QualitativeParameter();
        $qualitativeParameter->save();

        $product_10 = new \App\Product();
        $product_10->subcategory_id = 1;
        $product_10->availability_id = $availibity_2->id;
        $product_10->condition_id = $condition->id;
        $product_10->kind_id = $kind_2->id;
        $product_10->dimension_id = $dimension_1->id;
        $product_10->type_wood_id = $typeWood_1->id;
        $product_10->type_product_id = $typeProduct_8->id;
        $product_10->way_pay_id = $WayPay_10->id;
        $product_10->qualitative_parameter_id = $qualitativeParameter->id;
        $product_10->price = 950;
        $product_10->total = $product_10->price;
        $product_10->save();

        $publication_10 = new \App\Publication;
        $publication_10->title = "Tablas de araucaria Creado por Seed product 10";
        $publication_10->description = "Descripcion 10 creado por el seed";
        $publication_10->company_id = $person_premium_8->company_id;
        $publication_10->user_id = $person_premium_8->user_id;
        $publication_10->contact_id = $person_premium_8->contact_id;
        $publication_10->address_id = $person_premium_8->address_id;
        $publication_10->typePublication = 'offert';
        $publication_10->status = 'approved';
        $publication_10->price = $product_10->price;
        $publication_10->total = $product_7->total;
        $publication_10->save();
        $publication_10->products()->attach([$product_10->id]);
        $publication_10->save();
    }

    private function createPublicationWithOffertUser5()
    {
        $availibity_1 = \App\Availability::create([
            'cantitude' => 30,
            'availability' => 'limited',
            'unitMeasurement' => Unit::PIETABLAR,
            'measurementTerm' => null,
        ]);

        $availibity_2 = \App\Availability::create([
            'cantitude' => 30,
            'availability' => 'continuous',
            'unitMeasurement' => Unit::PIETABLAR,
            'measurementTerm' => null,
        ]);

        //Condiciones

        $condition = \App\Condition::create([
            'piece' => 1,
            'weight' => 1,
            'length' => 1,
            'tall' => 11,
            'zunchos' => 'No',
            //envoltorio
            'wrapping' => 'Yes'
        ]);


        //dimensiones
        $dimension_1 = \App\Dimension::create([
            'actualThickness' => 37,
            'actualWidth' => 57,
            'actualLength' => 2700,
        ]);


        $dimension_2 = \App\Dimension::create([
            'actualThickness' => 67,
            'actualWidth' => 277,
            'actualLength' => 3577,
        ]);

        $dimension_3 = \App\Dimension::create([
            'actualThickness' => 87,
            'actualWidth' => 277,
            'actualLength' => 1577,
        ]);

        $dimension_4 = \App\Dimension::create([
            'actualThickness' => 77,
            'actualWidth' => 257,
            'actualLength' => 857,
        ]);
        //kind
        $kind_1 = \App\Kind::find(1);
        $kind_2 = \App\Kind::find(2);
        $kind_3 = \App\Kind::find(3);
        $kind_4 = \App\Kind::find(4);
        $kind_5 = \App\Kind::find(5);
        $kind_6 = \App\Kind::find(6);
        $kind_7 = \App\Kind::find(7);
        $kind_8 = \App\Kind::find(8);
        $kind_9 = \App\Kind::find(9);
        $kind_10 = \App\Kind::find(10);
        $kind_11 = \App\Kind::find(11);
        $kind_12 = \App\Kind::find(12);
        $kind_13 = \App\Kind::find(13);
        $kind_14 = \App\Kind::find(14);

        //nativa 1 implantada 2
        $typeWood_1 = \App\TypeWood::find(1);
        $typeWood_2 = \App\TypeWood::find(2);

        $typeProduct_1 = \App\TypeProduct::find(1);

        $typeProduct_2 = \App\TypeProduct::find(2);
        $typeProduct_3 = \App\TypeProduct::find(3);
        $typeProduct_4 = \App\TypeProduct::find(4);
        $typeProduct_5 = \App\TypeProduct::find(5);
        $typeProduct_6 = \App\TypeProduct::find(6);
        $typeProduct_7 = \App\TypeProduct::find(7);
        $typeProduct_8 = \App\TypeProduct::find(8);
        $typeProduct_9 = \App\TypeProduct::find(9);
        $typeProduct_10 = \App\TypeProduct::find(10);
        $typeProduct_11 = \App\TypeProduct::find(11);

        //forma de pago

        $WayPay = \App\WayPay::create([
            'money' => '$',
            'ammount' => 102,
            'iva' => true,
        ]);

        $WayPay->paymentTypes()->attach([1, 2, 3]);
        $WayPay->save();


        $person_premium_8 = \App\Person::find(8);
        $qualitativeParameter = new QualitativeParameter();
        $qualitativeParameter->save();

        $product = new \App\Product();
        $product->subcategory_id = 1;
        $product->availability_id = $availibity_2->id;
        $product->condition_id = $condition->id;
        $product->kind_id = $kind_1->id;
        $product->dimension_id = $dimension_2->id;
        $product->type_wood_id = $typeWood_1->id;
        $product->type_product_id = $typeProduct_2->id;
        $product->way_pay_id = $WayPay->id;
        $product->qualitative_parameter_id = $qualitativeParameter->id;
        $product->price = 102;
        $product->total = $product->price;
        $product->save();

        $product->qualitative_parameter_id = $qualitativeParameter->id;$publication
        = new \App\Publication;
        $publication->title = "Pino Creado por Seed product 1";
        $publication->description = "Descripcion creado por el seed";
        $publication->company_id = $person_premium_8->company_id;
        $publication->user_id = $person_premium_8->user_id;
        $publication->contact_id = $person_premium_8->contact_id;
        $publication->address_id = $person_premium_8->address_id;
        $publication->typePublication = 'offert';
        $publication->status = 'approved';
        $publication->price = $product->price;
        $publication->total = $product->total;
        $publication->save();
        $publication->products()->attach([$product->id]);
        $publication->save();


        $WayPay_2 = \App\WayPay::create([
            'money' => '$',
            'ammount' => 114,
            'iva' => true,
        ]);

        $WayPay_2->paymentTypes()->attach([1, 3, 4]);
        $WayPay_2->save();

        $qualitativeParameter = new QualitativeParameter();
        $qualitativeParameter->save();

        $product_2 = new \App\Product();
        $product_2->subcategory_id = 1;
        $product_2->availability_id = $availibity_1->id;
        $product_2->condition_id = $condition->id;
        $product_2->kind_id = $kind_2->id;
        $product_2->dimension_id = $dimension_2->id;
        $product_2->type_wood_id = $typeWood_2->id;
        $product_2->type_product_id = $typeProduct_3->id;
        $product_2->way_pay_id = $WayPay_2->id;
        $product_2->qualitative_parameter_id = $qualitativeParameter->id;
        $product_2->price = 114;
        $product_2->total = $product_2->price;
        $product_2->save();

        $publication_2 = new \App\Publication;
        $publication_2->title = "Pino Creado por Seed product 2";
        $publication_2->description = "Descripcion 2 creado por el seed";
        $publication_2->company_id = $person_premium_8->company_id;
        $publication_2->user_id = $person_premium_8->user_id;
        $publication_2->contact_id = $person_premium_8->contact_id;
        $publication_2->address_id = $person_premium_8->address_id;
        $publication_2->typePublication = 'offert';
        $publication_2->status = 'approved';
        $publication_2->price = $product_2->price;
        $publication_2->total = $product_2->total;
        $publication_2->save();
        $publication_2->products()->attach([$product_2->id]);
        $publication_2->save();


        $WayPay_3 = \App\WayPay::create([
            'money' => '$',
            'ammount' => 204,
            'iva' => true,
        ]);

        $WayPay_3->paymentTypes()->attach([1]);
        $WayPay_3->save();

        $qualitativeParameter = new QualitativeParameter();
        $qualitativeParameter->save();

        $product_3 = new \App\Product();
        $product_3->subcategory_id = 1;
        $product_3->availability_id = $availibity_2->id;
        $product_3->condition_id = $condition->id;
        $product_3->kind_id = $kind_2->id;
        $product_3->dimension_id = $dimension_2->id;
        $product_3->type_wood_id = $typeWood_1->id;
        $product_3->type_product_id = $typeProduct_3->id;
        $product_3->way_pay_id = $WayPay_3->id;
        $product_3->qualitative_parameter_id = $qualitativeParameter->id;
        $product_3->price = 204;
        $product_3->total = $product_3->price;
        $product_3->save();

        $publication_3 = new \App\Publication;
        $publication_3->title = "Pino Creado por Seed product 3";
        $publication_3->description = "Descripcion 3 creado por el seed";
        $publication_3->company_id = $person_premium_8->company_id;
        $publication_3->user_id = $person_premium_8->user_id;
        $publication_3->contact_id = $person_premium_8->contact_id;
        $publication_3->address_id = $person_premium_8->address_id;
        $publication_3->typePublication = 'offert';
        $publication_3->status = 'approved';
        $publication_3->price = $product_3->price;
        $publication_3->total = $product_3->total;
        $publication_3->save();
        $publication_3->products()->attach([$product_3->id]);
        $publication_3->save();


        $WayPay_4 = \App\WayPay::create([
            'money' => '$',
            'ammount' => 254,
            'iva' => true,
        ]);

        $WayPay_4->paymentTypes()->attach([2]);
        $WayPay_4->save();

        $qualitativeParameter = new QualitativeParameter();
        $qualitativeParameter->save();

        $product_4 = new \App\Product();
        $product_4->subcategory_id = 1;
        $product_4->availability_id = $availibity_1->id;
        $product_4->condition_id = $condition->id;
        $product_4->kind_id = $kind_2->id;
        $product_4->dimension_id = $dimension_2->id;
        $product_4->type_wood_id = $typeWood_1->id;
        $product_4->type_product_id = $typeProduct_4->id;
        $product_4->way_pay_id = $WayPay_4->id;
        $product_4->qualitative_parameter_id = $qualitativeParameter->id;
        $product_4->price = 254;
        $product_4->total = $product_4->price;
        $product_4->save();

        $publication_4 = new \App\Publication;
        $publication_4->title = "Algarrobo Creado por Seed product 4";
        $publication_4->description = "Descripcion 4 creado por el seed";
        $publication_4->company_id = $person_premium_8->company_id;
        $publication_4->user_id = $person_premium_8->user_id;
        $publication_4->contact_id = $person_premium_8->contact_id;
        $publication_4->address_id = $person_premium_8->address_id;
        $publication_4->typePublication = 'offert';
        $publication_4->status = 'approved';
        $publication_4->price = $product_4->price;
        $publication_4->total = $product_4->total;
        $publication_4->save();
        $publication_4->products()->attach([$product_4->id]);
        $publication_4->save();


        $WayPay_5 = \App\WayPay::create([
            'money' => '$',
            'ammount' => 454,
            'iva' => true,
        ]);

        $WayPay_5->paymentTypes()->attach([2]);
        $WayPay_5->save();

        $qualitativeParameter = new QualitativeParameter();
        $qualitativeParameter->save();

        $product_5 = new \App\Product();
        $product_5->subcategory_id = 1;
        $product_5->availability_id = $availibity_2->id;
        $product_5->condition_id = $condition->id;
        $product_5->kind_id = $kind_2->id;
        $product_5->dimension_id = $dimension_3->id;
        $product_5->type_wood_id = $typeWood_1->id;
        $product_5->type_product_id = $typeProduct_6->id;
        $product_5->way_pay_id = $WayPay_5->id;
        $product_5->qualitative_parameter_id = $qualitativeParameter->id;
        $product_5->price = 454;
        $product_5->total = $product_5->price;
        $product_5->save();

        $publication_5 = new \App\Publication;
        $publication_5->title = "Algarrobo Creado por Seed product 5";
        $publication_5->description = "Descripcion 5 creado por el seed";
        $publication_5->company_id = $person_premium_8->company_id;
        $publication_5->user_id = $person_premium_8->user_id;
        $publication_5->contact_id = $person_premium_8->contact_id;
        $publication_5->address_id = $person_premium_8->address_id;
        $publication_5->typePublication = 'offert';
        $publication_5->status = 'approved';
        $publication_5->price = $product_5->price;
        $publication_5->total = $product_5->total;
        $publication_5->save();
        $publication_5->products()->attach([$product_5->id]);
        $publication_5->save();

        $WayPay_6 = \App\WayPay::create([
            'money' => '$',
            'ammount' => 404,
            'iva' => true,
        ]);

        $WayPay_6->paymentTypes()->attach([1, 2]);
        $WayPay_6->save();

        $qualitativeParameter = new QualitativeParameter();
        $qualitativeParameter->save();

        $product_6 = new \App\Product();
        $product_6->subcategory_id = 1;
        $product_6->availability_id = $availibity_1->id;
        $product_6->condition_id = $condition->id;
        $product_6->kind_id = $kind_2->id;
        $product_6->dimension_id = $dimension_4->id;
        $product_6->type_wood_id = $typeWood_1->id;
        $product_6->type_product_id = $typeProduct_7->id;
        $product_6->way_pay_id = $WayPay_6->id;
        $product_6->qualitative_parameter_id = $qualitativeParameter->id;
        $product_6->price = 404;
        $product_6->total = $product_6->price;
        $product_6->save();

        $publication_6 = new \App\Publication;
        $publication_6->title = "Tirante Creado por Seed product 6";
        $publication_6->description = "Descripcion 6 creado por el seed";
        $publication_6->company_id = $person_premium_8->company_id;
        $publication_6->user_id = $person_premium_8->user_id;
        $publication_6->contact_id = $person_premium_8->contact_id;
        $publication_6->address_id = $person_premium_8->address_id;
        $publication_6->typePublication = 'offert';
        $publication_6->status = 'approved';
        $publication_6->price = $product_6->price;
        $publication_6->total = $product_6->total;
        $publication_6->save();
        $publication_6->products()->attach([$product_6->id]);
        $publication_6->save();


        $WayPay_8 = \App\WayPay::create([
            'money' => '$',
            'ammount' => 374,
            'iva' => true,
        ]);

        $WayPay_8->paymentTypes()->attach([2]);
        $WayPay_8->save();

        $qualitativeParameter = new QualitativeParameter();
        $qualitativeParameter->save();

        $product_8 = new \App\Product();
        $product_8->subcategory_id = 1;
        $product_8->availability_id = $availibity_1->id;
        $product_8->condition_id = $condition->id;
        $product_8->kind_id = $kind_2->id;
        $product_8->dimension_id = $dimension_2->id;
        $product_8->type_wood_id = $typeWood_1->id;
        $product_8->type_product_id = $typeProduct_2->id;
        $product_8->way_pay_id = $WayPay_8->id;
        $product_8->qualitative_parameter_id = $qualitativeParameter->id;
        $product_8->price = 374;
        $product_8->total = $product_8->price;
        $product_8->save();

        $publication_8 = new \App\Publication;
        $publication_8->title = "Tirantes Creado por Seed product 7";
        $publication_8->description = "Descripcion 7 creado por el seed";
        $publication_8->company_id = $person_premium_8->company_id;
        $publication_8->user_id = $person_premium_8->user_id;
        $publication_8->contact_id = $person_premium_8->contact_id;
        $publication_8->address_id = $person_premium_8->address_id;
        $publication_8->typePublication = 'offert';
        $publication_8->status = 'approved';
        $publication_8->price = $product_8->price;
        $publication_8->total = $product_8->total;
        $publication_8->save();
        $publication_8->products()->attach([$product_8->id]);
        $publication_8->save();

        $WayPay_7 = \App\WayPay::create([
            'money' => '$',
            'ammount' => 174,
            'iva' => true,
        ]);

        $WayPay_7->paymentTypes()->attach([2]);
        $WayPay_7->save();

        $qualitativeParameter = new QualitativeParameter();
        $qualitativeParameter->save();

        $product_7 = new \App\Product();
        $product_7->subcategory_id = 1;
        $product_7->availability_id = $availibity_2->id;
        $product_7->condition_id = $condition->id;
        $product_7->kind_id = $kind_2->id;
        $product_7->dimension_id = $dimension_3->id;
        $product_7->type_wood_id = $typeWood_1->id;
        $product_7->type_product_id = $typeProduct_3->id;
        $product_7->way_pay_id = $WayPay_7->id;
        $product_7->qualitative_parameter_id = $qualitativeParameter->id;
        $product_7->price = 174;
        $product_7->total = $product_7->price;
        $product_7->save();

        $publication_7 = new \App\Publication;
        $publication_7->title = "Paraiso Creado por Seed product 8";
        $publication_7->description = "Descripcion 8 creado por el seed";
        $publication_7->company_id = $person_premium_8->company_id;
        $publication_7->user_id = $person_premium_8->user_id;
        $publication_7->contact_id = $person_premium_8->contact_id;
        $publication_7->address_id = $person_premium_8->address_id;
        $publication_7->typePublication = 'offert';
        $publication_7->status = 'approved';
        $publication_7->price = $product_7->price;
        $publication_7->total = $product_7->total;
        $publication_7->save();
        $publication_7->products()->attach([$product_7->id]);
        $publication_7->save();


        $WayPay_9 = \App\WayPay::create([
            'money' => '$',
            'ammount' => 854,
            'iva' => true,
        ]);

        $WayPay_9->paymentTypes()->attach([2, 4]);
        $WayPay_9->save();

        $qualitativeParameter = new QualitativeParameter();
        $qualitativeParameter->save();

        $product_9 = new \App\Product();
        $product_9->subcategory_id = 1;
        $product_9->availability_id = $availibity_1->id;
        $product_9->condition_id = $condition->id;
        $product_9->kind_id = $kind_2->id;
        $product_9->dimension_id = $dimension_4->id;
        $product_9->type_wood_id = $typeWood_1->id;
        $product_9->type_product_id = $typeProduct_7->id;
        $product_9->way_pay_id = $WayPay_9->id;
        $product_9->qualitative_parameter_id = $qualitativeParameter->id;
        $product_9->price = 854;
        $product_9->total = $product_9->price;
        $product_9->save();

        $publication_9 = new \App\Publication;
        $publication_9->title = "Tablas de paraiso Creado por Seed product 9";
        $publication_9->description = "Descripcion 9 creado por el seed";
        $publication_9->company_id = $person_premium_8->company_id;
        $publication_9->user_id = $person_premium_8->user_id;
        $publication_9->contact_id = $person_premium_8->contact_id;
        $publication_9->address_id = $person_premium_8->address_id;
        $publication_9->typePublication = 'offert';
        $publication_9->status = 'approved';
        $publication_9->price = $product_9->price;
        $publication_9->total = $product_7->total;
        $publication_9->save();
        $publication_9->products()->attach([$product_9->id]);
        $publication_9->save();


        $WayPay_10 = \App\WayPay::create([
            'money' => '$',
            'ammount' => 954,
            'iva' => true,
        ]);

        $WayPay_10->paymentTypes()->attach([2, 4]);
        $WayPay_10->save();

        $qualitativeParameter = new QualitativeParameter();
        $qualitativeParameter->save();

        $product_10 = new \App\Product();
        $product_10->subcategory_id = 1;
        $product_10->availability_id = $availibity_2->id;
        $product_10->condition_id = $condition->id;
        $product_10->kind_id = $kind_2->id;
        $product_10->dimension_id = $dimension_1->id;
        $product_10->type_wood_id = $typeWood_1->id;
        $product_10->type_product_id = $typeProduct_8->id;
        $product_10->way_pay_id = $WayPay_10->id;
        $product_10->qualitative_parameter_id = $qualitativeParameter->id;
        $product_10->price = 954;
        $product_10->total = $product_10->price;
        $product_10->save();

        $publication_10 = new \App\Publication;
        $publication_10->title = "Tablas de araucaria Creado por Seed product 10";
        $publication_10->description = "Descripcion 10 creado por el seed";
        $publication_10->company_id = $person_premium_8->company_id;
        $publication_10->user_id = $person_premium_8->user_id;
        $publication_10->contact_id = $person_premium_8->contact_id;
        $publication_10->address_id = $person_premium_8->address_id;
        $publication_10->typePublication = 'offert';
        $publication_10->status = 'approved';
        $publication_10->price = $product_10->price;
        $publication_10->total = $product_7->total;
        $publication_10->save();
        $publication_10->products()->attach([$product_10->id]);
        $publication_10->save();
    }


    private function createPublicationWithOffertUser6()
    {
        $availibity_1 = \App\Availability::create([
            'cantitude' => 30,
            'availability' => 'limited',
            'unitMeasurement' => Unit::PIETABLAR,
            'measurementTerm' => null,
        ]);

        $availibity_2 = \App\Availability::create([
            'cantitude' => 30,
            'availability' => 'continuous',
            'unitMeasurement' => Unit::PIETABLAR,
            'measurementTerm' => null,
        ]);

        //Condiciones

        $condition = \App\Condition::create([
            'piece' => 1,
            'weight' => 1,
            'length' => 1,
            'tall' => 11,
            'zunchos' => 'No',
            //envoltorio
            'wrapping' => 'Yes'
        ]);


        //dimensiones
        $dimension_1 = \App\Dimension::create([
            'actualThickness' => 37,
            'actualWidth' => 57,
            'actualLength' => 2700,
        ]);


        $dimension_2 = \App\Dimension::create([
            'actualThickness' => 67,
            'actualWidth' => 277,
            'actualLength' => 3577,
        ]);

        $dimension_3 = \App\Dimension::create([
            'actualThickness' => 87,
            'actualWidth' => 277,
            'actualLength' => 1577,
        ]);

        $dimension_4 = \App\Dimension::create([
            'actualThickness' => 77,
            'actualWidth' => 257,
            'actualLength' => 857,
        ]);
        //kind
        $kind_1 = \App\Kind::find(1);
        $kind_2 = \App\Kind::find(2);
        $kind_3 = \App\Kind::find(3);
        $kind_4 = \App\Kind::find(4);
        $kind_5 = \App\Kind::find(5);
        $kind_6 = \App\Kind::find(6);
        $kind_7 = \App\Kind::find(7);
        $kind_8 = \App\Kind::find(8);
        $kind_9 = \App\Kind::find(9);
        $kind_10 = \App\Kind::find(10);
        $kind_11 = \App\Kind::find(11);
        $kind_12 = \App\Kind::find(12);
        $kind_13 = \App\Kind::find(13);
        $kind_14 = \App\Kind::find(14);

        //nativa 1 implantada 2
        $typeWood_1 = \App\TypeWood::find(1);
        $typeWood_2 = \App\TypeWood::find(2);

        $typeProduct_1 = \App\TypeProduct::find(1);

        $typeProduct_2 = \App\TypeProduct::find(2);
        $typeProduct_3 = \App\TypeProduct::find(3);
        $typeProduct_4 = \App\TypeProduct::find(4);
        $typeProduct_5 = \App\TypeProduct::find(5);
        $typeProduct_6 = \App\TypeProduct::find(6);
        $typeProduct_7 = \App\TypeProduct::find(7);
        $typeProduct_8 = \App\TypeProduct::find(8);
        $typeProduct_9 = \App\TypeProduct::find(9);
        $typeProduct_10 = \App\TypeProduct::find(10);
        $typeProduct_11 = \App\TypeProduct::find(11);

        //forma de pago

        $WayPay = \App\WayPay::create([
            'money' => '$',
            'ammount' => 102,
            'iva' => true,
        ]);

        $WayPay->paymentTypes()->attach([1, 2, 3]);
        $WayPay->save();

        $person_premium_8 = \App\Person::find(8);

        $qualitativeParameter = new QualitativeParameter();
        $qualitativeParameter->save();

        $product = new \App\Product();
        $product->subcategory_id = 1;
        $product->availability_id = $availibity_2->id;
        $product->condition_id = $condition->id;
        $product->kind_id = $kind_1->id;
        $product->dimension_id = $dimension_2->id;
        $product->type_wood_id = $typeWood_1->id;
        $product->type_product_id = $typeProduct_2->id;
        $product->way_pay_id = $WayPay->id;
        $product->qualitative_parameter_id = $qualitativeParameter->id;
        $product->price = 102;
        $product->total = $product->price;
        $product->save();

        $publication = new \App\Publication;
        $publication->title = "Pino Creado por Seed product 1";
        $publication->description = "Descripcion creado por el seed";
        $publication->company_id = $person_premium_8->company_id;
        $publication->user_id = $person_premium_8->user_id;
        $publication->contact_id = $person_premium_8->contact_id;
        $publication->address_id = $person_premium_8->address_id;
        $publication->typePublication = 'offert';
        $publication->status = 'approved';
        $publication->price = $product->price;
        $publication->total = $product->total;
        $publication->save();
        $publication->products()->attach([$product->id]);
        $publication->save();


        $WayPay_2 = \App\WayPay::create([
            'money' => '$',
            'ammount' => 114,
            'iva' => true,
        ]);

        $WayPay_2->paymentTypes()->attach([1, 3, 4]);
        $WayPay_2->save();

        $qualitativeParameter = new QualitativeParameter();
        $qualitativeParameter->save();

        $product_2 = new \App\Product();
        $product_2->subcategory_id = 1;
        $product_2->availability_id = $availibity_1->id;
        $product_2->condition_id = $condition->id;
        $product_2->kind_id = $kind_2->id;
        $product_2->dimension_id = $dimension_2->id;
        $product_2->type_wood_id = $typeWood_2->id;
        $product_2->type_product_id = $typeProduct_3->id;
        $product_2->way_pay_id = $WayPay_2->id;
        $product_2->qualitative_parameter_id = $qualitativeParameter->id;

        $product_2->price = 114;
        $product_2->total = $product_2->price;
        $product_2->save();

        $publication_2 = new \App\Publication;
        $publication_2->title = "Pino Creado por Seed product 2";
        $publication_2->description = "Descripcion 2 creado por el seed";
        $publication_2->company_id = $person_premium_8->company_id;
        $publication_2->user_id = $person_premium_8->user_id;
        $publication_2->contact_id = $person_premium_8->contact_id;
        $publication_2->address_id = $person_premium_8->address_id;
        $publication_2->typePublication = 'offert';
        $publication_2->status = 'approved';
        $publication_2->price = $product_2->price;
        $publication_2->total = $product_2->total;
        $publication_2->save();
        $publication_2->products()->attach([$product_2->id]);
        $publication_2->save();


        $WayPay_3 = \App\WayPay::create([
            'money' => '$',
            'ammount' => 204,
            'iva' => true,
        ]);

        $WayPay_3->paymentTypes()->attach([1]);
        $WayPay_3->save();

        $qualitativeParameter = new QualitativeParameter();
        $qualitativeParameter->save();

        $product_3 = new \App\Product();
        $product_3->subcategory_id = 1;
        $product_3->availability_id = $availibity_2->id;
        $product_3->condition_id = $condition->id;
        $product_3->kind_id = $kind_2->id;
        $product_3->dimension_id = $dimension_2->id;
        $product_3->type_wood_id = $typeWood_1->id;
        $product_3->type_product_id = $typeProduct_3->id;
        $product_3->way_pay_id = $WayPay_3->id;
        $product_3->qualitative_parameter_id = $qualitativeParameter->id;

        $product_3->price = 204;
        $product_3->total = $product_3->price;
        $product_3->save();

        $publication_3 = new \App\Publication;
        $publication_3->title = "Pino Creado por Seed product 3";
        $publication_3->description = "Descripcion 3 creado por el seed";
        $publication_3->company_id = $person_premium_8->company_id;
        $publication_3->user_id = $person_premium_8->user_id;
        $publication_3->contact_id = $person_premium_8->contact_id;
        $publication_3->address_id = $person_premium_8->address_id;
        $publication_3->typePublication = 'offert';
        $publication_3->status = 'approved';
        $publication_3->price = $product_3->price;
        $publication_3->total = $product_3->total;
        $publication_3->save();
        $publication_3->products()->attach([$product_3->id]);
        $publication_3->save();


        $WayPay_4 = \App\WayPay::create([
            'money' => '$',
            'ammount' => 254,
            'iva' => true,
        ]);

        $WayPay_4->paymentTypes()->attach([2]);
        $WayPay_4->save();

        $qualitativeParameter = new QualitativeParameter();
        $qualitativeParameter->save();

        $product_4 = new \App\Product();
        $product_4->subcategory_id = 1;
        $product_4->availability_id = $availibity_1->id;
        $product_4->condition_id = $condition->id;
        $product_4->kind_id = $kind_2->id;
        $product_4->dimension_id = $dimension_2->id;
        $product_4->type_wood_id = $typeWood_1->id;
        $product_4->type_product_id = $typeProduct_4->id;
        $product_4->way_pay_id = $WayPay_4->id;
        $product_4->qualitative_parameter_id = $qualitativeParameter->id;
        $product_4->price = 254;
        $product_4->total = $product_4->price;
        $product_4->save();

        $publication_4 = new \App\Publication;
        $publication_4->title = "Algarrobo Creado por Seed product 4";
        $publication_4->description = "Descripcion 4 creado por el seed";
        $publication_4->company_id = $person_premium_8->company_id;
        $publication_4->user_id = $person_premium_8->user_id;
        $publication_4->contact_id = $person_premium_8->contact_id;
        $publication_4->address_id = $person_premium_8->address_id;
        $publication_4->typePublication = 'offert';
        $publication_4->status = 'approved';
        $publication_4->price = $product_4->price;
        $publication_4->total = $product_4->total;
        $publication_4->save();
        $publication_4->products()->attach([$product_4->id]);
        $publication_4->save();


        $WayPay_5 = \App\WayPay::create([
            'money' => '$',
            'ammount' => 454,
            'iva' => true,
        ]);

        $WayPay_5->paymentTypes()->attach([2]);
        $WayPay_5->save();

        $qualitativeParameter = new QualitativeParameter();
        $qualitativeParameter->save();

        $product_5 = new \App\Product();
        $product_5->subcategory_id = 1;
        $product_5->availability_id = $availibity_2->id;
        $product_5->condition_id = $condition->id;
        $product_5->kind_id = $kind_2->id;
        $product_5->dimension_id = $dimension_3->id;
        $product_5->type_wood_id = $typeWood_1->id;
        $product_5->type_product_id = $typeProduct_6->id;
        $product_5->way_pay_id = $WayPay_5->id;
        $product_5->qualitative_parameter_id = $qualitativeParameter->id;
        $product_5->price = 454;
        $product_5->total = $product_5->price;
        $product_5->save();

        $publication_5 = new \App\Publication;
        $publication_5->title = "Algarrobo Creado por Seed product 5";
        $publication_5->description = "Descripcion 5 creado por el seed";
        $publication_5->company_id = $person_premium_8->company_id;
        $publication_5->user_id = $person_premium_8->user_id;
        $publication_5->contact_id = $person_premium_8->contact_id;
        $publication_5->address_id = $person_premium_8->address_id;
        $publication_5->typePublication = 'offert';
        $publication_5->status = 'approved';
        $publication_5->price = $product_5->price;
        $publication_5->total = $product_5->total;
        $publication_5->save();
        $publication_5->products()->attach([$product_5->id]);
        $publication_5->save();

        $WayPay_6 = \App\WayPay::create([
            'money' => '$',
            'ammount' => 404,
            'iva' => true,
        ]);

        $WayPay_6->paymentTypes()->attach([1, 2]);
        $WayPay_6->save();


        $qualitativeParameter = new QualitativeParameter();
        $qualitativeParameter->save();

        $product_6 = new \App\Product();
        $product_6->subcategory_id = 1;
        $product_6->availability_id = $availibity_1->id;
        $product_6->condition_id = $condition->id;
        $product_6->kind_id = $kind_2->id;
        $product_6->dimension_id = $dimension_4->id;
        $product_6->type_wood_id = $typeWood_1->id;
        $product_6->type_product_id = $typeProduct_7->id;
        $product_6->way_pay_id = $WayPay_6->id;
        $product_6->qualitative_parameter_id = $qualitativeParameter->id;

        $product_6->price = 404;
        $product_6->total = $product_6->price;
        $product_6->save();

        $publication_6 = new \App\Publication;
        $publication_6->title = "Tirante Creado por Seed product 6";
        $publication_6->description = "Descripcion 6 creado por el seed";
        $publication_6->company_id = $person_premium_8->company_id;
        $publication_6->user_id = $person_premium_8->user_id;
        $publication_6->contact_id = $person_premium_8->contact_id;
        $publication_6->address_id = $person_premium_8->address_id;
        $publication_6->typePublication = 'offert';
        $publication_6->status = 'approved';
        $publication_6->price = $product_6->price;
        $publication_6->total = $product_6->total;
        $publication_6->save();
        $publication_6->products()->attach([$product_6->id]);
        $publication_6->save();


        $WayPay_8 = \App\WayPay::create([
            'money' => '$',
            'ammount' => 374,
            'iva' => true,
        ]);

        $WayPay_8->paymentTypes()->attach([2]);
        $WayPay_8->save();

        $qualitativeParameter = new QualitativeParameter();
        $qualitativeParameter->save();

        $product_8 = new \App\Product();
        $product_8->subcategory_id = 1;
        $product_8->availability_id = $availibity_1->id;
        $product_8->condition_id = $condition->id;
        $product_8->kind_id = $kind_2->id;
        $product_8->dimension_id = $dimension_2->id;
        $product_8->type_wood_id = $typeWood_1->id;
        $product_8->type_product_id = $typeProduct_2->id;
        $product_8->way_pay_id = $WayPay_8->id;
        $product_8->qualitative_parameter_id = $qualitativeParameter->id;
        $product_8->price = 374;
        $product_8->total = $product_8->price;
        $product_8->save();

        $publication_8 = new \App\Publication;
        $publication_8->title = "Tirantes Creado por Seed product 7";
        $publication_8->description = "Descripcion 7 creado por el seed";
        $publication_8->company_id = $person_premium_8->company_id;
        $publication_8->user_id = $person_premium_8->user_id;
        $publication_8->contact_id = $person_premium_8->contact_id;
        $publication_8->address_id = $person_premium_8->address_id;
        $publication_8->typePublication = 'offert';
        $publication_8->status = 'approved';
        $publication_8->price = $product_8->price;
        $publication_8->total = $product_8->total;
        $publication_8->save();
        $publication_8->products()->attach([$product_8->id]);
        $publication_8->save();

        $WayPay_7 = \App\WayPay::create([
            'money' => '$',
            'ammount' => 174,
            'iva' => true,
        ]);

        $WayPay_7->paymentTypes()->attach([2]);
        $WayPay_7->save();

        $qualitativeParameter = new QualitativeParameter();
        $qualitativeParameter->save();

        $product_7 = new \App\Product();
        $product_7->subcategory_id = 1;
        $product_7->availability_id = $availibity_2->id;
        $product_7->condition_id = $condition->id;
        $product_7->kind_id = $kind_2->id;
        $product_7->dimension_id = $dimension_3->id;
        $product_7->type_wood_id = $typeWood_1->id;
        $product_7->type_product_id = $typeProduct_3->id;
        $product_7->way_pay_id = $WayPay_7->id;
        $product_7->qualitative_parameter_id = $qualitativeParameter->id;
        $product_7->price = 174;
        $product_7->total = $product_7->price;
        $product_7->save();

        $publication_7 = new \App\Publication;
        $publication_7->title = "Paraiso Creado por Seed product 8";
        $publication_7->description = "Descripcion 8 creado por el seed";
        $publication_7->company_id = $person_premium_8->company_id;
        $publication_7->user_id = $person_premium_8->user_id;
        $publication_7->contact_id = $person_premium_8->contact_id;
        $publication_7->address_id = $person_premium_8->address_id;
        $publication_7->typePublication = 'offert';
        $publication_7->status = 'approved';
        $publication_7->price = $product_7->price;
        $publication_7->total = $product_7->total;
        $publication_7->save();
        $publication_7->products()->attach([$product_7->id]);
        $publication_7->save();


        $WayPay_9 = \App\WayPay::create([
            'money' => '$',
            'ammount' => 854,
            'iva' => true,
        ]);

        $WayPay_9->paymentTypes()->attach([2, 4]);
        $WayPay_9->save();

        $qualitativeParameter = new QualitativeParameter();
        $qualitativeParameter->save();

        $product_9 = new \App\Product();
        $product_9->subcategory_id = 1;
        $product_9->availability_id = $availibity_1->id;
        $product_9->condition_id = $condition->id;
        $product_9->kind_id = $kind_2->id;
        $product_9->dimension_id = $dimension_4->id;
        $product_9->type_wood_id = $typeWood_1->id;
        $product_9->type_product_id = $typeProduct_7->id;
        $product_9->way_pay_id = $WayPay_9->id;
        $product_9->qualitative_parameter_id = $qualitativeParameter->id;

        $product_9->price = 854;
        $product_9->total = $product_9->price;
        $product_9->save();

        $publication_9 = new \App\Publication;
        $publication_9->title = "Tablas de paraiso Creado por Seed product 9";
        $publication_9->description = "Descripcion 9 creado por el seed";
        $publication_9->company_id = $person_premium_8->company_id;
        $publication_9->user_id = $person_premium_8->user_id;
        $publication_9->contact_id = $person_premium_8->contact_id;
        $publication_9->address_id = $person_premium_8->address_id;
        $publication_9->typePublication = 'offert';
        $publication_9->status = 'approved';
        $publication_9->price = $product_9->price;
        $publication_9->total = $product_7->total;
        $publication_9->save();
        $publication_9->products()->attach([$product_9->id]);
        $publication_9->save();


        $WayPay_10 = \App\WayPay::create([
            'money' => '$',
            'ammount' => 954,
            'iva' => true,
        ]);

        $WayPay_10->paymentTypes()->attach([2, 4]);
        $WayPay_10->save();

        $qualitativeParameter = new QualitativeParameter();
        $qualitativeParameter->save();

        $product_10 = new \App\Product();
        $product_10->subcategory_id = 1;
        $product_10->availability_id = $availibity_2->id;
        $product_10->condition_id = $condition->id;
        $product_10->kind_id = $kind_2->id;
        $product_10->dimension_id = $dimension_1->id;
        $product_10->type_wood_id = $typeWood_1->id;
        $product_10->type_product_id = $typeProduct_8->id;
        $product_10->way_pay_id = $WayPay_10->id;
        $product_10->qualitative_parameter_id = $qualitativeParameter->id;
        $product_10->price = 954;
        $product_10->total = $product_10->price;
        $product_10->save();

        $publication_10 = new \App\Publication;
        $publication_10->title = "Tablas de araucaria Creado por Seed product 10";
        $publication_10->description = "Descripcion 10 creado por el seed";
        $publication_10->company_id = $person_premium_8->company_id;
        $publication_10->user_id = $person_premium_8->user_id;
        $publication_10->contact_id = $person_premium_8->contact_id;
        $publication_10->address_id = $person_premium_8->address_id;
        $publication_10->typePublication = 'offert';
        $publication_10->status = 'approved';
        $publication_10->price = $product_10->price;
        $publication_10->total = $product_7->total;
        $publication_10->save();
        $publication_10->products()->attach([$product_10->id]);
        $publication_10->save();
    }

    public function shippingTypeAddPublications(){
       $shippingType = ShippingType::find(1);

       $product = Product::find(1);
       $product->shipping_type_id = $shippingType->id;
       $product->save();

       $shippingType = ShippingType::find(1);

       $product = Product::find(2);
       $product->shipping_type_id = $shippingType->id;
       $product->save();

       $shippingType = ShippingType::find(1);

       $product = Product::find(3);
       $product->shipping_type_id = $shippingType->id;
       $product->save();

       $shippingType = ShippingType::find(2);

       $product = Product::find(4);
       $product->shipping_type_id = $shippingType->id;
       $product->save();

       $shippingType = ShippingType::find(3);

       $product = Product::find(5);
       $product->shipping_type_id = $shippingType->id;
       $product->save();

       $shippingType = ShippingType::find(4);

       $product = Product::find(6);
       $product->shipping_type_id = $shippingType->id;
       $product->save();

       $shippingType = ShippingType::find(5);

       $product = Product::find(7);
       $product->shipping_type_id = $shippingType->id;
       $product->save();

       $shippingType = ShippingType::find(1);

       $product = Product::find(8);
       $product->shipping_type_id = $shippingType->id;
       $product->save();

       $shippingType = ShippingType::find(2);

       $product = Product::find(9);
       $product->shipping_type_id = $shippingType->id;
       $product->save();

       $shippingType = ShippingType::find(4);

       $product = Product::find(10);
       $product->shipping_type_id = $shippingType->id;
       $product->save();

       $shippingType = ShippingType::find(5);

       $product = Product::find(11);
       $product->shipping_type_id = $shippingType->id;
       $product->save();

       $shippingType = ShippingType::find(1);

       $product = Product::find(12);
       $product->shipping_type_id = $shippingType->id;
       $product->save();

       $shippingType = ShippingType::find(2);

       $product = Product::find(14);
       $product->shipping_type_id = $shippingType->id;
       $product->save();

       $shippingType = ShippingType::find(3);

       $product = Product::find(15);
       $product->shipping_type_id = $shippingType->id;
       $product->save();

       $shippingType = ShippingType::find(4);

       $product = Product::find(16);
       $product->shipping_type_id = $shippingType->id;
       $product->save();

       $shippingType = ShippingType::find(5);

       $product = Product::find(17);
       $product->shipping_type_id = $shippingType->id;
       $product->save();

       $shippingType = ShippingType::find(1);

       $product = Product::find(18);
       $product->shipping_type_id = $shippingType->id;
       $product->save();

       $shippingType = ShippingType::find(2);

       $product = Product::find(19);
       $product->shipping_type_id = $shippingType->id;
       $product->save();

       $shippingType = ShippingType::find(3);

       $product = Product::find(20);
       $product->shipping_type_id = $shippingType->id;
       $product->save();


       $product = Product::find(21);
       $product->shipping_type_id = $shippingType->id;
       $product->save();

       $shippingType = ShippingType::find(4);

       $product = Product::find(22);
       $product->shipping_type_id = $shippingType->id;
       $product->save();

       $shippingType = ShippingType::find(5);

       $product = Product::find(24);
       $product->shipping_type_id = $shippingType->id;
       $product->save();

       $shippingType = ShippingType::find(1);

       $product = Product::find(25);
       $product->shipping_type_id = $shippingType->id;
       $product->save();

       $shippingType = ShippingType::find(2);

       $product = Product::find(26);
       $product->shipping_type_id = $shippingType->id;
       $product->save();

       $shippingType = ShippingType::find(3);

       $product = Product::find(27);
       $product->shipping_type_id = $shippingType->id;
       $product->save();

       $shippingType = ShippingType::find(4);

       $product = Product::find(28);
       $product->shipping_type_id = $shippingType->id;
       $product->save();

       $shippingType = ShippingType::find(5);

       $product = Product::find(29);
       $product->shipping_type_id = $shippingType->id;
       $product->save();

       $shippingType = ShippingType::find(1);

       $product = Product::find(30);
       $product->shipping_type_id = $shippingType->id;
       $product->save();

       $shippingType = ShippingType::find(2);

       $product = Product::find(31);
       $product->shipping_type_id = $shippingType->id;
       $product->save();

       $shippingType = ShippingType::find(3);

       $product = Product::find(32);
       $product->shipping_type_id = $shippingType->id;
       $product->save();

       $shippingType = ShippingType::find(4);

       $product = Product::find(34);
       $product->shipping_type_id = $shippingType->id;
       $product->save();

       $shippingType = ShippingType::find(5);

       $product = Product::find(35);
       $product->shipping_type_id = $shippingType->id;
       $product->save();

       $shippingType = ShippingType::find(1);

       $product = Product::find(36);
       $product->shipping_type_id = $shippingType->id;
       $product->save();

       $shippingType = ShippingType::find(2);

       $product = Product::find(37);
       $product->shipping_type_id = $shippingType->id;
       $product->save();

       $shippingType = ShippingType::find(3);

       $product = Product::find(38);
       $product->shipping_type_id = $shippingType->id;
       $product->save();

       $shippingType = ShippingType::find(4);

       $product = Product::find(39);
       $product->shipping_type_id = $shippingType->id;
       $product->save();

       $shippingType = ShippingType::find(5);

       $product = Product::find(40);
       $product->shipping_type_id = $shippingType->id;
       $product->save();

       $shippingType = ShippingType::find(1);

       $product = Product::find(41);
       $product->shipping_type_id = $shippingType->id;
       $product->save();

       $shippingType = ShippingType::find(2);

       $product = Product::find(42);
       $product->shipping_type_id = $shippingType->id;
       $product->save();

       $shippingType = ShippingType::find(3);

       $product = Product::find(44);
       $product->shipping_type_id = $shippingType->id;
       $product->save();

       $shippingType = ShippingType::find(4);

       $product = Product::find(45);
       $product->shipping_type_id = $shippingType->id;
       $product->save();

       $shippingType = ShippingType::find(5);

       $product = Product::find(46);
       $product->shipping_type_id = $shippingType->id;
       $product->save();

       $shippingType = ShippingType::find(1);

       $product = Product::find(47);
       $product->shipping_type_id = $shippingType->id;
       $product->save();

       $shippingType = ShippingType::find(1);

       $product = Product::find(48);
       $product->shipping_type_id = $shippingType->id;
       $product->save();

       $shippingType = ShippingType::find(2);

       $product = Product::find(49);
       $product->shipping_type_id = $shippingType->id;
       $product->save();

       $shippingType = ShippingType::find(3);

       $product = Product::find(50);
       $product->shipping_type_id = $shippingType->id;
       $product->save();

       $shippingType = ShippingType::find(4);

       $product = Product::find(51);
       $product->shipping_type_id = $shippingType->id;
       $product->save();

       $shippingType = ShippingType::find(5);

       $product = Product::find(52);
       $product->shipping_type_id = $shippingType->id;
       $product->save();

       $shippingType = ShippingType::find(1);

       $product = Product::find(55);
       $product->shipping_type_id = $shippingType->id;
       $product->save();

       $shippingType = ShippingType::find(1);

       $product = Product::find(55);
       $product->shipping_type_id = $shippingType->id;
       $product->save();

       $shippingType = ShippingType::find(1);

       $product = Product::find(56);
       $product->shipping_type_id = $shippingType->id;
       $product->save();

       $shippingType = ShippingType::find(1);

       $product = Product::find(57);
       $product->shipping_type_id = $shippingType->id;
       $product->save();

       $shippingType = ShippingType::find(1);

       $product = Product::find(58);
       $product->shipping_type_id = $shippingType->id;
       $product->save();

       $shippingType = ShippingType::find(1);

       $product = Product::find(59);
       $product->shipping_type_id = $shippingType->id;
       $product->save();

       $shippingType = ShippingType::find(1);

       $product = Product::find(60);
       $product->shipping_type_id = $shippingType->id;
       $product->save();

       $shippingType = ShippingType::find(1);

       $product = Product::find(61);
       $product->shipping_type_id = $shippingType->id;
       $product->save();

       $shippingType = ShippingType::find(1);

       $product = Product::find(62);
       $product->shipping_type_id = $shippingType->id;
       $product->save();

       $shippingType = ShippingType::find(1);

       $product = Product::find(65);
       $product->shipping_type_id = $shippingType->id;
       $product->save();

       $shippingType = ShippingType::find(1);

       $product = Product::find(65);
       $product->shipping_type_id = $shippingType->id;
       $product->save();

       $shippingType = ShippingType::find(1);

       $product = Product::find(66);
       $product->shipping_type_id = $shippingType->id;
       $product->save();

       $shippingType = ShippingType::find(1);

       $product = Product::find(67);
       $product->shipping_type_id = $shippingType->id;
       $product->save();

       $shippingType = ShippingType::find(1);

       $product = Product::find(68);
       $product->shipping_type_id = $shippingType->id;
       $product->save();

       $shippingType = ShippingType::find(1);

       $product = Product::find(69);
       $product->shipping_type_id = $shippingType->id;
       $product->save();

       $shippingType = ShippingType::find(1);

       $product = Product::find(70);
       $product->shipping_type_id = $shippingType->id;
       $product->save();

       $product = Product::find(61);
       $product->shipping_type_id = $shippingType->id;
       $product->save();

       $shippingType = ShippingType::find(1);

       $product = Product::find(62);
       $product->shipping_type_id = $shippingType->id;
       $product->save();

       $shippingType = ShippingType::find(1);

       $product = Product::find(75);
       $product->shipping_type_id = $shippingType->id;
       $product->save();

       $shippingType = ShippingType::find(1);

       $product = Product::find(75);
       $product->shipping_type_id = $shippingType->id;
       $product->save();

       $shippingType = ShippingType::find(1);

       $product = Product::find(76);
       $product->shipping_type_id = $shippingType->id;
       $product->save();

       $shippingType = ShippingType::find(1);

       $product = Product::find(77);
       $product->shipping_type_id = $shippingType->id;
       $product->save();

       $shippingType = ShippingType::find(1);

       $product = Product::find(78);
       $product->shipping_type_id = $shippingType->id;
       $product->save();

       $shippingType = ShippingType::find(1);

       $product = Product::find(79);
       $product->shipping_type_id = $shippingType->id;
       $product->save();

       $shippingType = ShippingType::find(1);

       $product = Product::find(80);
       $product->shipping_type_id = $shippingType->id;
       $product->save();

    }
}
