<?php

use App\Attribute;
use Illuminate\Database\Seeder;


class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categoria_1 = factory(App\Category::class)->create(['name'=>'industries']);
        $categoria_1->subcategories()->save(factory(App\Subcategory::class)->create(['name'=>'sawmills']));
        $categoria_2 = factory(App\Category::class)->create(['name'=>'corrals']);
        $categoria_2->subcategories()->save(factory(App\Subcategory::class)->create(['name'=>'corrals']));
    }
}