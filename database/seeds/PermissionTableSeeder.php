<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Route;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\PermissionRegistrar;
use Spatie\Permission\Models\Role;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::query()->delete();
        app()[PermissionRegistrar::class]->forgetCachedPermissions();

        $permissionList = [

            ['group' => 'Observaciones', 'permissions' => ['Ver.observación', 'Crear.observación', 'Modificar.observación', 'Eliminar.observación','Aceptar.observación','Listar.observaciones']],
            ['group' => 'Comentarios', 'permissions' => ['Ver.comentario', 'Crear.comentario', 'Modificar.comentario', 'Eliminar.comentario','Listar.comentarios']],
            ['group' => 'Usuarios', 'permissions' => ['Ver.usuario', 'Crear.usuario', 'Modificar.usuario', 'Eliminar.usuario','Listar.usuarios']],
            ['group' => 'Usuarios de comisionistas', 'permissions' =>['Ver.usuario.comisionista', 'Modificar.usuario.comisionista', 'Eliminar.usuario.comisionista','Crear.usuario.comisionista', 'Listar.usuarios.comisionistas']],
            ['group' => 'Roles', 'permissions' => ['Ver.rol', 'Crear.rol', 'Modificar.rol', 'Eliminar.rol', 'Lista.rol']],
            ['group' => 'Personas', 'permissions' => ['Ver.persona', 'Crear.persona', 'Modificar.persona', 'Eliminar.persona', 'Listar.personas']],
            ['group' => 'Comisionitas', 'permissions' => ['Ver.comisionista', 'Modificar.comisionista', 'Eliminar.comisionista','Crear.comisionista', 'Listar.comisionistas']],
            ['group' => 'Ofertas', 'permissions' => ['Ver.oferta', 'Crear.oferta', 'Modificar.oferta', 'Eliminar.oferta', 'Listar.ofertas']],
            ['group' => 'Demandas', 'permissions' => ['Ver.demanda', 'Crear.demanda', 'Modificar.demanda', 'Eliminar.demanda', 'Listar.demandas']],
            ['group' => 'Publicaciones', 'permissions' => ['Ver.publicacion', 'Modificar.publicacion', 'Eliminar.publicacion','Crear.publicacion',  'Cambiar.estado.publicacion','Listar.publicaciones']],
            ['group' => 'Categorias', 'permissions' => ['Listar.categorias']],
            ['group' => 'Tipos de maderas', 'permissions' =>['Ver.tipo.de.madera', 'Modificar.tipo.de.madera', 'Eliminar.tipo.de.madera','Crear.tipo.de.madera', 'Listar.tipos.de.maderas']],
            ['group' => 'Tipo de producto', 'permissions' => ['Ver.tipo.de.producto', 'Modificar.tipo.de.producto', 'Eliminar.tipo.de.producto','Crear.tipo.de.producto', 'Listar.tipos.de.productos']],
            ['group' => 'Perfil empresa', 'permissions' => ['Ver.perfil.empresa', 'Modificar.perfil.empresa']],

            ]
        ;

        foreach ($permissionList as $key => $namePermission) {
            $controller = $namePermission['group'];
            foreach ($namePermission['permissions'] as  $permission) {
                    $permissionNew = new Permission;
                    $permissionNew->name = $permission;
                    $permissionNew->controller = $controller;
                    $permissionNew->save();
            }
        }

        $permissionRootInit = [
            ['group' => 'Observaciones', 'permissions' => ['Ver.observación', 'Crear.observación', 'Modificar.observación', 'Eliminar.observación','Aceptar.observación','Listar.observaciones']],
            ['group' => 'Comentarios', 'permissions' => ['Ver.comentario', 'Crear.comentario', 'Modificar.comentario', 'Eliminar.comentario','Listar.comentarios']],
            ['group' => 'Usuarios', 'permissions' => ['Ver.usuario', 'Crear.usuario', 'Modificar.usuario', 'Eliminar.usuario','Listar.usuarios']],
            ['group' => 'Roles', 'permissions' => ['Ver.rol', 'Crear.rol', 'Modificar.rol', 'Eliminar.rol', 'Lista.rol']],
            ['group' => 'Personas', 'permissions' => ['Ver.persona', 'Crear.persona', 'Modificar.persona', 'Eliminar.persona', 'Listar.personas']],
            ['group' => 'Publicaciones', 'permissions' => ['Ver.publicacion', 'Modificar.publicacion', 'Eliminar.publicacion','Crear.publicacion', 'Cambiar.estado.publicacion', 'Listar.publicaciones']],
            ['group' => 'Categorias', 'permissions' => ['Listar.categorias']],
            ['group' => 'Tipos de maderas', 'permissions' =>['Ver.tipo.de.madera', 'Modificar.tipo.de.madera', 'Eliminar.tipo.de.madera','Crear.tipo.de.madera', 'Listar.tipos.de.maderas']],
            ['group' => 'Usuarios de comisionistas', 'permissions' =>['Ver.usuario.comisionista', 'Modificar.usuario.comisionista', 'Eliminar.usuario.comisionista','Crear.usuario.comisionista', 'Listar.usuarios.comisionistas']],
            ['group' => 'Comisionitas', 'permissions' => ['Ver.comisionista', 'Modificar.comisionista', 'Eliminar.comisionista','Crear.comisionista', 'Listar.comisionistas']],
            ['group' => 'Ofertas', 'permissions' => ['Ver.oferta', 'Crear.oferta', 'Modificar.oferta', 'Eliminar.oferta', 'Listar.ofertas']],
            ['group' => 'Demandas', 'permissions' => ['Ver.demanda', 'Crear.demanda', 'Modificar.demanda', 'Eliminar.demanda', 'Listar.demandas']],
            ['group' => 'Tipo de productos', 'permissions' => ['Ver.tipo.de.producto', 'Modificar.tipo.de.producto', 'Eliminar.tipo.de.producto','Crear.tipo.de.producto', 'Listar.tipos.de.productos']],
            ['group' => 'Perfil empresa', 'permissions' => ['Ver.perfil.empresa', 'Modificar.perfil.empresa']],

        ];

        $permissionAdminInit = [

            ['group' => 'Observaciones', 'permissions' => ['Ver.observación', 'Crear.observación', 'Modificar.observación', 'Eliminar.observación','Aceptar.observación','Listar.observaciones']],
            ['group' => 'Comentarios', 'permissions' => ['Ver.comentario', 'Crear.comentario', 'Modificar.comentario', 'Eliminar.comentario','Listar.comentarios']],
            ['group' => 'Usuarios', 'permissions' => ['Ver.usuario', 'Crear.usuario', 'Modificar.usuario', 'Eliminar.usuario', 'Listar.usuarios']],
            ['group' => 'Usuarios de comisionistas', 'permissions' =>['Ver.usuario.comisionista', 'Modificar.usuario.comisionista', 'Eliminar.usuario.comisionista','Crear.usuario.comisionista', 'Listar.usuarios.comisionistas']],
            ['group' => 'Comisionitas', 'permissions' => ['Ver.comisionista', 'Modificar.comisionista', 'Eliminar.comisionista','Crear.comisionista', 'Listar.comisionistas']],
            ['group' => 'Personas', 'permissions' => ['Ver.persona', 'Crear.persona', 'Modificar.persona', 'Eliminar.persona', 'Listar.personas']],
            ['group' => 'Ofertas', 'permissions' => ['Ver.oferta', 'Crear.oferta', 'Modificar.oferta', 'Eliminar.oferta', 'Listar.ofertas']],
            ['group' => 'Demandas', 'permissions' => ['Ver.demanda', 'Crear.demanda', 'Modificar.demanda', 'Eliminar.demanda', 'Listar.demandas']],
            ['group' => 'Publicaciones', 'permissions' => ['Ver.publicacion', 'Modificar.publicacion', 'Eliminar.publicacion','Crear.publicacion', 'Cambiar.estado.publicacion','Listar.publicaciones']],
            ['group' => 'Categorias', 'permissions' => ['Listar.categorias']],
            ['group' => 'Tipos de maderas', 'permissions' =>['Ver.tipo.de.madera', 'Modificar.tipo.de.madera', 'Eliminar.tipo.de.madera','Crear.tipo.de.madera', 'Listar.tipos.de.maderas']],
            ['group' => 'Tipo de producto', 'permissions' => ['Ver.tipo.de.producto', 'Modificar.tipo.de.producto', 'Eliminar.tipo.de.producto','Crear.tipo.de.producto', 'Listar.tipos.de.productos']],
            ['group' => 'Perfil empresa', 'permissions' => ['Ver.perfil.empresa', 'Modificar.perfil.empresa']],

         ];

        $permissionPremiumInit = [

            ['group' => 'Observaciones', 'permissions' => ['Ver.observación', 'Crear.observación', 'Modificar.observación', 'Eliminar.observación','Aceptar.observación','Listar.observaciones']],
            ['group' => 'Comentarios', 'permissions' => ['Ver.comentario', 'Crear.comentario', 'Modificar.comentario', 'Eliminar.comentario','Listar.comentarios']],
            ['group' => 'Ofertas', 'permissions' => ['Ver.oferta', 'Crear.oferta', 'Modificar.oferta', 'Eliminar.oferta', 'Listar.ofertas']],
            ['group' => 'Demandas', 'permissions' => ['Ver.demanda', 'Crear.demanda', 'Modificar.demanda', 'Eliminar.demanda', 'Listar.demandas']],
            ['group' => 'Personas', 'permissions' => ['Ver.persona', 'Crear.persona', 'Modificar.persona', 'Eliminar.persona', 'Listar.personas']],
            ['group' => 'Publicaciones', 'permissions' => ['Ver.publicacion', 'Modificar.publicacion', 'Eliminar.publicacion','Crear.publicacion', 'Cambiar.estado.publicacion','Listar.publicaciones']],
            ['group' => 'Usuarios de comisionistas', 'permissions' =>['Ver.usuario.comisionista', 'Modificar.usuario.comisionista', 'Eliminar.usuario.comisionista','Crear.usuario.comisionista', 'Listar.usuarios.comisionistas']],
            ['group' => 'Comisionitas', 'permissions' => ['Ver.comisionista', 'Modificar.comisionista', 'Eliminar.comisionista','Crear.comisionista', 'Listar.comisionistas']],
            ['group' => 'Perfil empresa', 'permissions' => ['Ver.perfil.empresa', 'Modificar.perfil.empresa']],
        ];

        $permissionNormalInit = [
            ['group' => 'Observaciones', 'permissions' => ['Ver.observación', 'Crear.observación', 'Modificar.observación', 'Eliminar.observación','Aceptar.observación','Listar.observaciones']],
            ['group' => 'Publicaciones', 'permissions' => ['Ver.publicacion', 'Modificar.publicacion', 'Eliminar.publicacion','Crear.publicacion', 'Listar.publicaciones']],
            //['group' => 'Perfil empresa', 'permissions' => ['Ver.perfil.empresa', 'Modificar.perfil.empresa']],

        ];


        $permissionComisionistInit =[

            ['group' => 'Observaciones', 'permissions' => ['Ver.observación', 'Crear.observación', 'Modificar.observación', 'Eliminar.observación','Aceptar.observación','Listar.observaciones']],
            ['group' => 'Comentarios', 'permissions' => ['Ver.comentario', 'Crear.comentario', 'Modificar.comentario', 'Eliminar.comentario','Listar.comentarios']],
            ['group' => 'Ofertas', 'permissions' => ['Ver.oferta', 'Crear.oferta', 'Modificar.oferta', 'Eliminar.oferta', 'Listar.ofertas']],
            ['group' => 'Demandas', 'permissions' => ['Ver.demanda', 'Crear.demanda', 'Modificar.demanda', 'Eliminar.demanda', 'Listar.demandas']],
            ['group' => 'Publicaciones', 'permissions' => ['Ver.publicacion', 'Modificar.publicacion', 'Eliminar.publicacion','Crear.publicacion', 'Cambiar.estado.publicacion','Listar.publicaciones']],
            ['group' => 'Perfil empresa', 'permissions' => ['Ver.perfil.empresa', 'Modificar.perfil.empresa']],

        ];

        //PERMISOS ROOT


        app()[PermissionRegistrar::class]->forgetCachedPermissions();

        $permissionsRoot = []; // an empty array of stored permission IDs
        foreach ($permissionRootInit as $key => $namePermission) {
            $controller = $namePermission['group'];
            foreach ($namePermission['permissions'] as  $permission) {
                $permission_check = Permission::where(
                    ['name' => $permission]
                )->first();
                    $permissionsRoot[] = $permission_check->name;

            }
        }
        $root_role = Role::where('name', 'ROOT')->first();
        $root_role->givePermissionTo($permissionsRoot);

        //PERMISOS ADMIN

        app()[PermissionRegistrar::class]->forgetCachedPermissions();

        $permissionsAdmin = []; // an empty array of stored permission IDs
        foreach ($permissionAdminInit as $key => $namePermission) {
            $controller = $namePermission['group'];
            foreach ($namePermission['permissions'] as  $permission) {
                $permission_check = Permission::where(
                    ['name' => $permission]
                )->first();
                $permissionsAdmin[] = $permission_check->name;

            }
        }
        $admin_role = Role::where('name', 'Administrador')->first();
        $admin_role->givePermissionTo($permissionsAdmin);

        //PERMISOS NORMAL

        app()[PermissionRegistrar::class]->forgetCachedPermissions();

        $permissionsNormal = []; // an empty array of stored permission IDs
        foreach ($permissionNormalInit as $key => $namePermission) {
            $controller = $namePermission['group'];
            foreach ($namePermission['permissions'] as  $permission) {
                $permission_check = Permission::where(
                    ['name' => $permission]
                )->first();
                $permissionsNormal[] = $permission_check->name;
            }
        }

        $normal_role = Role::where('name', 'Normal')->first();
        $normal_role->givePermissionTo($permissionsNormal);

        //PERMISOS PREMIUM

        app()[PermissionRegistrar::class]->forgetCachedPermissions();

        $permissionsPremium = []; // an empty array of stored permission IDs
        foreach ($permissionPremiumInit as $key => $namePermission) {
            $controller = $namePermission['group'];
            foreach ($namePermission['permissions'] as  $permission) {
                $permission_check = Permission::where(
                    ['name' => $permission]
                )->first();
                $permissionsPremium[] = $permission_check->name;
            }
        }

        $premium_role = Role::where('name', 'Premium')->first();
        $premium_role->givePermissionTo($permissionsPremium);

        //PERMISOS PREMIUM

        app()[PermissionRegistrar::class]->forgetCachedPermissions();

        $permissionsComisionist = []; // an empty array of stored permission IDs
        foreach ($permissionComisionistInit as $key => $namePermission) {
            $controller = $namePermission['group'];
            foreach ($namePermission['permissions'] as  $permission) {
                $permission_check = Permission::where(
                    ['name' => $permission]
                )->first();
                $permissionsComisionist[] = $permission_check->name;

            }
        }
        $comisionist_role = Role::where('name', 'Comisionista')->first();
        $comisionist_role->givePermissionTo($permissionsComisionist);
    }
}
