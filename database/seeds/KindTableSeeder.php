
<?php

use App\Attribute;
use App\Kind;
use App\TypeWood;
use Illuminate\Database\Seeder;


class KindTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $nativa = TypeWood::create(['name' => 'Nativa']);
        $implantada = TypeWood::create(['name' => 'Implantada']);

        Kind::create(['name' => 'Cedro', 'type_wood_id' => $nativa->id]);
        Kind::create(['name' => 'Loro Negro', 'type_wood_id' => $nativa->id]);
        Kind::create(['name' => 'Anchico', 'type_wood_id' => $nativa->id]);
        Kind::create(['name' => 'Cañafistola', 'type_wood_id' => $nativa->id]);
        Kind::create(['name' => 'Insienso', 'type_wood_id' => $nativa->id]);
        Kind::create(['name' => 'Guatambú', 'type_wood_id' => $nativa->id]);
        Kind::create(['name' => 'Isapuy', 'type_wood_id' => $nativa->id]);
        Kind::create(['name' => 'Marmelero', 'type_wood_id' => $nativa->id]);
        Kind::create(['name' => 'Guayubira', 'type_wood_id' => $nativa->id]);
        Kind::create(['name' => 'Laurel Negro', 'type_wood_id' => $nativa->id]);
        Kind::create(['name' => 'Laurel Guaicá', 'type_wood_id' => $nativa->id]);
        Kind::create(['name' => 'Laurel Amarillo', 'type_wood_id' => $nativa->id]);
        Kind::create(['name' => 'Cancharana', 'type_wood_id' => $nativa->id]);
        Kind::create(['name' => 'Rabo Itá', 'type_wood_id' => $nativa->id]);
        Kind::create(['name' => 'Rabo Molle', 'type_wood_id' => $nativa->id]);
        Kind::create(['name' => 'Grapia', 'type_wood_id' => $nativa->id]);
        Kind::create(['name' => 'Persiguero', 'type_wood_id' => $nativa->id]);
        Kind::create(['name' => 'Maria Preta', 'type_wood_id' => $nativa->id]);
        Kind::create(['name' => 'Timbó', 'type_wood_id' => $nativa->id]);
        Kind::create(['name' => 'Azota Caballo', 'type_wood_id' => $nativa->id]);
        Kind::create(['name' => 'Mora Amarilla', 'type_wood_id' => $nativa->id]);

        //////////////////////////////////////////////

        //madera implantada

        Kind::create(['name' => 'Pino', 'type_wood_id' => $implantada->id]);
        Kind::create(['name' => 'Eucalipto', 'type_wood_id' => $implantada->id]);
        Kind::create(['name' => 'Paraiso', 'type_wood_id' => $implantada->id]);
        Kind::create(['name' => 'Araucarea', 'type_wood_id' => $implantada->id]);
        Kind::create(['name' => 'Grevillea', 'type_wood_id' => $implantada->id]);
        Kind::create(['name' => 'Cedro Australeano', 'type_wood_id' => $implantada->id]);
        Kind::create(['name'  => 'Otro', 'type_wood_id' => $implantada->id]);
    }
}
