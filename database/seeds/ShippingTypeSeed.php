<?php

use Illuminate\Database\Seeder;

class ShippingTypeSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        \App\ShippingType::create(['send'=>'free']);
        \App\ShippingType::create(['send'=>'query']);
        \App\ShippingType::create(['send'=>'with_send']);
        \App\ShippingType::create(['send'=>'free_from']);
        \App\ShippingType::create(['send'=>'without_send']);

    }
}
