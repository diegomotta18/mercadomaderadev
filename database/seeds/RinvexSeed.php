<?php

use Illuminate\Database\Seeder;
use Rinvex\Subscriptions\Models\Plan;
use Rinvex\Subscriptions\Models\PlanFeature;

class RinvexSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $plan1 = Plan::create([

            'name' => 'Gratis',
            'description' => "<div style=\"text-align: center;\"><span style=\"font-size: 24px;\">Publicaci�n gratuita de productos</span></div><div style=\"text-align: center;\"><span style=\"font-size: 24px;\"><br></span></div><div style=\"text-align: center;\"><span style=\"font-size: 14px;\">?? Cuenta gratuita x 1 a�o</span></div><div style=\"text-align: center;\"><span style=\"font-size: 14px;\">?? Sin limites de productos publicados </span></div><div style=\"text-align: center;\"><span style=\"font-size: 14px;\">?? Visualizaci�n de&nbsp;</span><span style=\"font-size: 14px;\">Titulo, Descripci�n&nbsp;y precio en cada producto</span></div><div style=\"text-align: center;\"><span style=\"font-size: 14px;\">? No recibir�&nbsp;de notificaciones por compra</span></div><div style=\"text-align: center;\"><span style=\"font-size: 14px;\">?Recibir</span><span style=\"font-size: 14px;\">&nbsp;comentarios</span></div><div style=\"text-align: center;\"><span style=\"font-size: 14px;\">?Recibir calificaciones</span></div><div style=\"text-align: center;\"><span style=\"font-size: 14px;\">?Sin portal de informaci�n del negocio</span><span style=\"font-size: 14px;\"><br></span><span style=\"font-size: 14px;\">?Sin aportar mas caracteristicas al producto</span><span style=\"font-size: 14px;\"><br></span><br><br><br><br></div>", 
            'price' => 0,
            'signup_fee' => 0,
            'invoice_period' => 0,
            'signup_fee' => 0,
            'invoice_interval' => 'month',
            'currency' => 'USD',
            'sort_order' => 1,
            'trial_period' => 365,
            'trial_interval' => "day"
            
        ]);
    

        $plan1 = Plan::create([

            'name' => 'Membresia por 3 mes',
            'description' => "<div style=\"text-align: center;\"><span style=\"font-size: 24px;\">Membresia x 3 meses&nbsp;</span></div><div style=\"text-align: center;\"><span style=\"font-size: 24px;\"><br></span></div><div style=\"text-align: center;\"><span style=\"font-size: 14px;\">?? Cuenta premium x 3 meses</span></div><div style=\"text-align: center;\"><span style=\"font-size: 14px;\">?? Sin limites de productos publicados</span></div><div style=\"text-align: center;\"><span style=\"font-size: 14px;\">?? Visualizaci�n de todas las caracter�sticas&nbsp;del producto</span></div><div style=\"text-align: center;\"><div><span style=\"font-size: 14px;\">?? Buscar cualquier producto dentro de la plataforma</span></div><div><div><span style=\"font-size: 14px;\">?? Recibir emails y notificaciones por compras y comentarios</span></div><div><div><span style=\"font-size: 14px;\">?? Recibir calificaciones</span></div><div><div><span style=\"font-size: 14px;\">??&nbsp; Mas Beneficios</span></div></div></div></div><div><span style=\"font-size: 14px;\"><br></span></div></div>", 
            'price' => 19,
            'signup_fee' => 0,
            'invoice_period' => 1,
            'signup_fee' => 0,
            'invoice_interval' => 'month',
            'currency' => 'USD',
            'sort_order' => 1,
            'trial_period' => 0,
            'trial_interval' => "day"
            
        ]);
    
        $plan2 = Plan::create([

            'name' => 'Membresia por 1 año',
            'description' => "<div style=\"text-align: center;\"><span style=\"font-size: 24px;\">Membresia x 1 a�o&nbsp;</span></div><div style=\"text-align: center;\"><span style=\"font-size: 24px;\"><br></span></div><div style=\"text-align: center;\"><span style=\"font-size: 14px;\">?? Cuenta premium x 1 a�o</span></div><div style=\"text-align: center;\"><div><span style=\"font-size: 14px;\">?? Precio especial por la compra de este plan</span></div></div><div style=\"text-align: center;\"><span style=\"font-size: 14px;\">?? Sin limites de productos publicados</span></div><div style=\"text-align: center;\"><span style=\"font-size: 14px;\">?? Visualizaci�n de todas las caracter�sticas&nbsp;del producto</span></div><div style=\"text-align: center;\"><div><span style=\"font-size: 14px;\">?? Buscar cualquier producto dentro de la plataforma</span></div><div><div><span style=\"font-size: 14px;\">?? Recibir emails y notificaciones por compras y comentarios</span></div><div><div><span style=\"font-size: 14px;\">?? Recibir calificaciones</span></div><div><span style=\"font-size: 14px;\">??&nbsp; Mas Beneficios</span></div></div></div><div><span style=\"font-size: 14px;\"><br></span></div></div>", 
            'price' => 199,
            'signup_fee' => 0,
            'invoice_period' => 12,
            'signup_fee' => 0,
            'invoice_interval' => 'month',
            'currency' => 'USD',
            'sort_order' => 1,
            'trial_period' => 0,
            'trial_interval' => "day"
            
        ]);

    }
}
