<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        $this->call(CountryProvinceDeparmentCitySeeder::class);
        $this->call(RolesAndPermissionsSeeder::class);
        $this->call(PermissionTableSeeder::class);
        $this->call(CategorySeeder::class);
        $this->call(KindTableSeeder::class);
        $this->call(ShippingTypeSeed::class);
        $this->call(TypeAvailabilitySeed::class);
        $this->call(TypeProductTableSeeder::class);
        $this->call(PaymentTypeSeeder::class);
        $this->call(PublicationSeeder::class);
        $this->call(RinvexSeed::class);

    }
}
