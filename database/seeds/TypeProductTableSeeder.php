
<?php

use App\Attribute;
use App\Kind;
use App\TypeProduct;
use App\TypeWood;
use Illuminate\Database\Seeder;


class TypeProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\TypeProduct::class)->create(['name'=>'Listón']);
        factory(\App\TypeProduct::class)->create(['name'=>'Machimbre']);
        factory(\App\TypeProduct::class)->create(['name'=>'Puntal']);
        factory(\App\TypeProduct::class)->create(['name'=>'Retazo']);

        factory(\App\TypeProduct::class)->create(['name'=>'Rodrigon']);
        factory(\App\TypeProduct::class)->create(['name'=>'Sandwich']);
        factory(\App\TypeProduct::class)->create(['name'=>'Sin Cantear']);
        factory(\App\TypeProduct::class)->create(['name'=>'Tirante']);
        factory(\App\TypeProduct::class)->create(['name'=>'Tablas']);
        factory(\App\TypeProduct::class)->create(['name'=>'Varilla']);
        factory(\App\TypeProduct::class)->create(['name'=>'Viga']);
    }
}