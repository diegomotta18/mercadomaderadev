<?php

use App\ShippingType;
use Illuminate\Database\Seeder;

class ShippingTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        ShippingType::create([
            'send' => 'query'
        ]);
        ShippingType::create([
            'send' => 'free'
        ]);
        ShippingType::create([
            'send' => 'with_send'
        ]);
        ShippingType::create([
            'send' => 'without_send'
        ]);
        ShippingType::create([
            'send' => 'free_from'
        ]);
    }
}
