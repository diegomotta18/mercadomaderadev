
<?php

use App\Attribute;
use App\Kind;
use App\TypeWood;
use Illuminate\Database\Seeder;


class PaymentTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\PaymentType::class)->create(['paymentType'=>'Efectivo']);
        factory(\App\PaymentType::class)->create(['paymentType'=>'Cheque']);
        factory(\App\PaymentType::class)->create(['paymentType'=>'Débito']);
        factory(\App\PaymentType::class)->create(['paymentType'=>'Crédito']);
        factory(\App\PaymentType::class)->create(['paymentType'=>'Transferencia']);
        factory(\App\PaymentType::class)->create(['paymentType'=>'Consultar']);
    }
}
