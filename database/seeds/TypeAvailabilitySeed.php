<?php

use Illuminate\Database\Seeder;

class TypeAvailabilitySeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        \App\TypeAvailability::create([
            'name' => "continuous",
        ]);
        \App\TypeAvailability::create([
            'name' => "limited",
        ]);
    }
}
