<?php

use App\Item;
use App\ItemSuscription;
use App\Suscription;
use Illuminate\Database\Seeder;

class SuscriptionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createItems();
        $this->createSuscription();
    }

    private function createItems()
    {
        Item::create([
            'description' => "items de ejemplo",
        ]);
        Item::create([
            'description' => "items de ejemplo",
        ]);
        Item::create([
            'description' => "items de ejemplo",
        ]);
        Item::create([
            'description' => "items de ejemplo",
        ]);
        Item::create([
            'description' => "items de ejemplo",
        ]);
        Item::create([
            'description' => "items de ejemplo",
        ]);
    }

    private function createSuscription()
    {
        $suscription_basica = Suscription::create([
            'amount' => 350,
            'description' => "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quidem eaque labore accusamus, quo reiciendis ratione delectus maxime autem repellat voluptatem quibusdam tenetur neque porro quod quos quis voluptatum praesentium placeat.",
            'title' => "Suscripcion Basica x 1 mes"
        ]);

        $suscripcion_normal = Suscription::create([
            'amount' => 450,
            'description' => "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quidem eaque labore accusamus, quo reiciendis ratione delectus maxime autem repellat voluptatem quibusdam tenetur neque porro quod quos quis voluptatum praesentium placeat.",
            'title' => "Suscripcion Normal x 6 meses "
        ]);

        $suscricion_premium = Suscription::create([
            'amount' => 600,
            'description' => "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quidem eaque labore accusamus, quo reiciendis ratione delectus maxime autem repellat voluptatem quibusdam tenetur neque porro quod quos quis voluptatum praesentium placeat.",
            'title' => "Suscripcion Premium x 12 meses"
        ]);

        $this->asociateItems($suscription_basica, $suscripcion_normal, $suscricion_premium);
    }

    private function asociateItems($suscricion_basica, $suscripcion_normal, $suscricion_premium)
    {
        // basica
        ItemSuscription::create([
            'suscription_id'=> $suscricion_basica->id,
            'item_id' => 1
        ]);
        ItemSuscription::create([
            'suscription_id'=> $suscricion_basica->id,
            'item_id' => 2
        ]);
        ItemSuscription::create([
            'suscription_id'=> $suscricion_basica->id,
            'item_id' => 3
        ]);
        // normal
        ItemSuscription::create([
            'suscription_id'=> $suscripcion_normal->id,
            'item_id' => 1
        ]);
        ItemSuscription::create([
            'suscription_id'=> $suscripcion_normal->id,
            'item_id' => 2
        ]);
        ItemSuscription::create([
            'suscription_id'=> $suscripcion_normal->id,
            'item_id' => 3
        ]);
        ItemSuscription::create([
            'suscription_id'=> $suscripcion_normal->id,
            'item_id' => 4
        ]);
        // premium
        ItemSuscription::create([
            'suscription_id'=> $suscricion_premium->id,
            'item_id' => 1
        ]);
        ItemSuscription::create([
            'suscription_id'=> $suscricion_premium->id,
            'item_id' => 2
        ]);
        ItemSuscription::create([
            'suscription_id'=> $suscricion_premium->id,
            'item_id' => 3
        ]);
        ItemSuscription::create([
            'suscription_id'=> $suscricion_premium->id,
            'item_id' => 4
        ]);
        ItemSuscription::create([
            'suscription_id'=> $suscricion_premium->id,
            'item_id' => 5
        ]);
        ItemSuscription::create([
            'suscription_id'=> $suscricion_premium->id,
            'item_id' => 6
        ]);
    }

}
