<?php

use App\Contact;
use App\Model\Address\Address;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class RolesAndPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()['cache']->forget('spatie.permission.cache');

        Role::create(['name' => 'ROOT']);
        Role::create(['name' => 'Administrador']);
        Role::create(['name' => 'Premium']);
        Role::create(['name' => 'Normal']);
        Role::create(['name' => 'Comisionista']);

        $file = public_path().'/img/default.jpeg';
        $logo = public_path().'/img/logo_default.jpeg';// asset('img/logo_default.jpeg');
        /** @var \App\User $user */
        $root = factory(\App\User::class)->create([
            'name' => 'Diego Root',
            'email' => 'diegomottadev@gmail.com',
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
            'is_verified' => 1
        ]);

        $root->addMediaFromUrl($file)->preservingOriginal()->toMediaCollection('profile');
        $root->assignRole('ROOT');

        $root = factory(\App\User::class)->create([
            'name' => 'root',
            'email' => 'root@root.com',
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
            'is_verified' => 1
        ]);
        $root->assignRole('ROOT');

        $address_root = Address::create([
            'street' => 'ROOT',
            'number' => '2020',
            'dpto' => '8',
            'floor' => 'B',
            'country_id' => 1,
            'province_id' => 1,
            'department_id' => 1,
            'city_id' => 1
        ]);

        $contact_root = Contact::create([
            'telphone' => '376435354',
            'celphone'  => '345435345',
            'email'     => 'root@root.com',
            'web'       => 'root.com'
        ]);

        $address_normal = Address::create([
            'street' => 'Normal',
            'number' => '2020',
            'dpto' => '8',
            'floor' => 'B',
            'country_id' => 1,
            'province_id' => 1,
            'department_id' => 1,
            'city_id' => 1
        ]);

        $contact_normal = Contact::create([
            'telphone' => '376435354',
            'celphone'  => '345435345',
            'email'     => 'normal@normal.com',
            'web'       => 'normal.com'
        ]);


        $address = Address::create([
            'street' => 'Admin',
            'number' => '2020',
            'dpto' => '8',
            'floor' => 'B',
            'country_id' => 1,
            'province_id' => 1,
            'department_id' => 1,
            'city_id' => 1
        ]);

        $contact = Contact::create([
            'telphone' => '376435354',
            'celphone'  => '345435345',
            'email'     => 'admin@admin.com',
            'web'       => 'admin.com'
        ]);

        $address1 = Address::create([
            'street' => 'Miranda',
            'number'=>'2020',
            'dpto' => '8',
            'floor' => 'B',
            'country_id' => 1,
            'province_id' => 1,
            'department_id' => 1,
            'city_id' => 1
        ]);

        $contact1 = Contact::create([
            'telphone' => '376435354',
            'celphone'  => '345435345',
            'email'     => 'premium@premium.com',
            'web'       => 'premium.com'
        ]);

        $address2 = Address::create([
            'street' => 'Cerati',
            'number'=>'5050',
            'dpto' => null,
            'floor' => null,
            'country_id' => 1,
            'province_id' => 1,
            'department_id' => 1,
            'city_id' => 1
        ]);

        $contact2 = Contact::create([
            'telphone' => '123456789',
            'celphone'  => '123456789',
            'email'     => 'premium2@premium2.com',
            'web'       => ''
        ]);


        $address3 = Address::create([
            'street' => 'Marcela Sosa',
            'number' => '4040',
            'dpto' => null,
            'floor' => null,
            'country_id' => 1,
            'province_id' => 1,
            'department_id' => 1,
            'city_id' => 1
        ]);

        $contact3 = Contact::create([
            'telphone' => '123456789',
            'celphone'  => '123456789',
            'email'     => 'premium3@premium3.com',
            'web'       => ''
        ]);


        $address4 = Address::create([
            'street' => 'Joselo Shuap',
            'number' => '2020',
            'dpto' => null,
            'floor' => null,
            'country_id' => 1,
            'province_id' => 1,
            'department_id' => 1,
            'city_id' => 1
        ]);

        $contact4 = Contact::create([
            'telphone' => '123456789',
            'celphone'  => '123456789',
            'email'     => 'diegomotta@premium.com',
            'web'       => ''
        ]);

        $address5 = Address::create([
            'street' => 'Hatahualpa Yapanki',
            'number' => '3020',
            'dpto' => null,
            'floor' => null,
            'country_id' => 1,
            'province_id' => 1,
            'department_id' => 1,
            'city_id' => 1
        ]);

        $contact5 = Contact::create([
            'telphone' => '123456789',
            'celphone'  => '123456789',
            'email'     => 'rubenmovinski@premium.com',
            'web'       => ''
        ]);

        /** @var \App\Company $company */
        $company_root = factory(\App\Company::class)->create([
            'name' => 'Corp. Root',
            'user_id' =>  $root->id,
            'address_id' => $address_root->id,
            'contact_id' => $contact_root->id,
        ]);

        $company_root->addMediaFromUrl($logo)->preservingOriginal()->toMediaCollection('logo');
        /** @var \App\Person $person */
        $person = factory(\App\Person::class)->create([
            'name' => 'root',
            'surname' => 'root',
            'dni' => '354563451',
            'user_id' => $root->id,
            'company_id' => $company_root->id,
            'address_id' => $address_root->id,
            'contact_id' => $contact_root->id
        ]);

        $administrador = factory(\App\User::class)->create([
            'name' => 'Admin',
            'email' => 'admin@admin.com',
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
            'is_verified' => 1
        ]);
        $administrador->addMediaFromUrl($file)->preservingOriginal()->toMediaCollection('profile');
        $administrador->assignRole('Administrador');

        $company_administrador = factory(\App\Company::class)->create([
            'name' => 'Admin',
            'user_id' =>  $administrador->id,
            'address_id' => $address->id,
            'contact_id' => $contact->id
        ]);
        $company_administrador->addMediaFromUrl($logo)->preservingOriginal()->toMediaCollection('logo');

        /** @var \App\Person $person */
        $person_administrador = factory(\App\Person::class)->create([
            'name' => 'administrador',
            'surname' => 'administrador',
            'dni' => '354563451',
            'user_id' => $administrador->id,
            'company_id' => $company_administrador->id,
            'address_id' => $address->id,
            'contact_id' => $contact->id
        ]);



        $normal = factory(\App\User::class)->create([
            'name' => 'normal',
            'email' => 'normal@normal.com',
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
            'is_verified' => 1
        ]);
        $normal->addMediaFromUrl($file)->preservingOriginal()->toMediaCollection('profile');
        $normal->assignRole('Normal');
        /** @var \App\Company $company */
        $company_normal = factory(\App\Company::class)->create([
            'name' => 'company_normal',
            'user_id' =>  $normal->id
        ]);
        $company_normal->addMediaFromUrl($logo)->preservingOriginal()->toMediaCollection('logo');

        /** @var \App\Person $person */
        $person_normal = factory(\App\Person::class)->create([
            'name' => 'Normal',
            'surname' => 'Noraml',
            'dni' => '354563451',
            'user_id' => $normal->id,
            'company_id' => $company_normal->id,
            'address_id' => $address_normal->id,
            'contact_id' => $contact_normal->id
        ]);

        $premium = factory(\App\User::class)->create([
            'name' => 'premium',
            'email' => 'premium@premium.com',
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
            'is_verified' => 1
        ]);

        $premium->addMediaFromUrl($file)->preservingOriginal()->toMediaCollection('profile');
        $premium->assignRole('Premium');


        $premium2 = factory(\App\User::class)->create([
            'name' => 'premium2',
            'email' => 'premium2@premium2.com',
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
            'is_verified' => 1
        ]);

        $premium2->addMediaFromUrl($file)->preservingOriginal()->toMediaCollection('profile');
        $premium2->assignRole('Premium');

        $premium3 = factory(\App\User::class)->create([
            'name' => 'premium3',
            'email' => 'premium3@premium3.com',
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
            'is_verified' => 1
        ]);

        $premium3->addMediaFromUrl($file)->preservingOriginal()->toMediaCollection('profile');
        $premium3->assignRole('Premium');


        $premium4 = factory(\App\User::class)->create([
            'name' => 'Diego Ivan Motta',
            'email' => 'diegomotta@premium.com',
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
            'is_verified' => 1
        ]);

        $premium4->addMediaFromUrl($file)->preservingOriginal()->toMediaCollection('profile');
        $premium4->assignRole('Premium');


        $premium5 = factory(\App\User::class)->create([
            'name' => 'Ruben Movinski',
            'email' => 'rubenmovinski@premium.com',
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
            'is_verified' => 1
        ]);

        $premium5->addMediaFromUrl($file)->preservingOriginal()->toMediaCollection('profile');
        $premium5->assignRole('Premium');

        /** @var \App\Company $company */
        $company_premium = factory(\App\Company::class)->create([
            'name' => 'premium_company_1',
            'user_id' =>  $premium->id,
            'address_id' => $address1->id,
            'contact_id' => $contact1->id
        ]);
        $company_premium->addMediaFromUrl($logo)->preservingOriginal()->toMediaCollection('logo');

        /** @var \App\Person $person */
        $person_premium = factory(\App\Person::class)->create([
            'name' => 'premium_1',
            'surname' => 'premium_1',
            'dni' => '354563451',
            'user_id' => $premium->id,
            'company_id' => $company_premium->id,
            'address_id' => $address1->id,
            'contact_id' => $contact1->id
        ]);

        $company_premium2 = factory(\App\Company::class)->create([
            'name' => 'premium_company_2',
            'user_id' =>  $premium2->id,
            'address_id' => $address2->id,
            'contact_id' => $contact2->id
        ]);
        $company_premium2->addMediaFromUrl($logo)->preservingOriginal()->toMediaCollection('logo');

        /** @var \App\Person $person */
        $person_premium2 = factory(\App\Person::class)->create([
            'name' => 'premium_2',
            'surname' => 'premium_2',
            'dni' => '354563451',
            'user_id' => $premium2->id,
            'company_id' => $company_premium2->id,
            'address_id' => $address2->id,
            'contact_id' => $contact2->id
        ]);




        $company_premium3 = factory(\App\Company::class)->create([
            'name' => 'premium_company_2',
            'user_id' =>  $premium3->id,
            'address_id' => $address3->id,
            'contact_id' => $contact3->id
        ]);
        $company_premium3->addMediaFromUrl($logo)->preservingOriginal()->toMediaCollection('logo');

        /** @var \App\Person $person */
        $person_premium3 = factory(\App\Person::class)->create([
            'name' => 'premium_3',
            'surname' => 'premium_3',
            'dni' => '354563451',
            'user_id' => $premium3->id,
            'company_id' => $company_premium3->id,
            'address_id' => $address3->id,
            'contact_id' => $contact3->id
        ]);

        $company_premium4 = factory(\App\Company::class)->create([
            'name' => 'Corp. Motta',
            'user_id' =>  $premium4->id,
            'address_id' => $address4->id,
            'contact_id' => $contact4->id
        ]);

        $company_premium4->addMediaFromUrl($logo)->preservingOriginal()->toMediaCollection('logo');

        /** @var \App\Person $person */
        $person_premium4 = factory(\App\Person::class)->create([
            'name' => 'Diego Ivan',
            'surname' => 'Motta',
            'dni' => '354563451',
            'user_id' => $premium4->id,
            'company_id' => $company_premium4->id,
            'address_id' => $address4->id,
            'contact_id' => $contact4->id
        ]);


        $company_premium5 = factory(\App\Company::class)->create([
            'name' => 'Corp. Movinski',
            'user_id' =>  $premium5->id,
            'address_id' => $address5->id,
            'contact_id' => $contact5->id
        ]);

        $company_premium5->addMediaFromUrl($logo)->preservingOriginal()->toMediaCollection('logo');

        /** @var \App\Person $person */
        $person_premium5 = factory(\App\Person::class)->create([
            'name' => 'Ruben',
            'surname' => 'Movinski',
            'dni' => '354563451',
            'user_id' => $premium5->id,
            'company_id' => $company_premium5->id,
            'address_id' => $address5->id,
            'contact_id' => $contact5->id
        ]);


        $comisionista = factory(\App\User::class)->create([
            'name' => 'comisionista',
            'email' => 'comisionista@comisionista.com',
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi'
        ]);
        $comisionista->assignRole('Comisionista');

        /** @var \App\Company $company */
        $company_comisionista = factory(\App\Company::class)->create([
            'name' => 'comisionista_company',
            'user_id' =>  $comisionista->id,
            'address_id' => $address->id,
            'contact_id' => $contact->id
        ]);
        $company_comisionista->addMediaFromUrl($logo)->preservingOriginal()->toMediaCollection('logo');

        /** @var \App\Person $person */
        $person_comisionista = factory(\App\Person::class)->create([
            'name' => 'comisionista',
            'surname' => 'comisionista',
            'dni' => '354563451',
            'user_id' => $comisionista->id,
            'company_id' => $company_comisionista->id,
            'address_id' => $address->id,
            'contact_id' => $contact->id
        ]);

    }
}
