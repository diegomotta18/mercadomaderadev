sudo git reset --hard HEAD
sudo git pull origin master
composer install
php artisan route:clear
php artisan view:clear
php artisan config:clear
php artisan cache:clear
composer dumpautoload
php artisan migrate:rollback
php artisan migrate --force
php artisan db:seed
php artisan db:seed --class=PublicationSeeder
sudo chown -R $USER:www-data storage
sudo chown -R $USER:www-data bootstrap/cache
sudo rm -r public/storage
php artisan storage:link
chmod -R 775 storage
chmod -R 775 bootstrap/cache
php artisan route:clear
php artisan config:clear
php artisan cache:clear
php artisan view:cache
php artisan ziggy:generate "resources/js/ziggy.js"
