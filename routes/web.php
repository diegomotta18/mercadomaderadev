<?php

use App\Mail\TestAmazonSes;
use App\Plan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'FrontEnd\HomeController@index')->name('public.home');
//obtener paises, provincia, localidad y ciudades por id
Route::get('/countries', 'BackEnd\CountryController@getCountries')->name('countries');
Route::get('/country/{id}/provinces', 'BackEnd\ProvinceController@getProvinces')->name('provinces');
Route::get('/province/{id}/cities', 'BackEnd\CityController@getCities')->name('cities');
Route::get('/city/{id}/towns', 'BackEnd\TownController@getTowns')->name('towns');

//obtener paises, provincia, localidad y ciudades por id
Route::get('/country/{id}', 'BackEnd\CountryController@getCountry')->name('country');
Route::get('/province/{id}', 'BackEnd\ProvinceController@getProvince')->name('province');
Route::get('/city/{id}', 'BackEnd\CityController@getCity')->name('city');
Route::get('/town/{id}', 'BackEnd\TownController@getTown')->name('town');

Route::get('/publications/{id}', 'FrontEnd\PublicationController@show')->name('view.publications.show');
Route::get('/publications/{publication}/company/{company}', 'FrontEnd\PublicationController@showCompany')->name('view.company.show');
Route::get('/publications/{id}/auction', 'FrontEnd\AuctionController@auction')->name('view.publications.auction');
Route::post('/publications/{id}/auction/store', 'FrontEnd\AuctionController@makeATender')->name('publication.auction.makeATender');
Route::get('/publications/{id}/auction/create', 'FrontEnd\AuctionController@createATender')->name('publication.auction.createTinder');

Route::get('/offerts', 'FrontEnd\OffertController@index')->name('offerts.index');
Route::get('/demands', 'FrontEnd\DemandController@index')->name('demands.index');
Route::get('/auctions', 'FrontEnd\AuctionController@index')->name('auctions.index');
Route::get('/categories/{subcategory}/publications', 'FrontEnd\CategoryController@index')->name('bycategories.index');
Route::get('/filter','FrontEnd\FilterController@productsBySearch')->name('filter.byProducts');

Route::get('/ours-plans',function(){
    $plans = Plan::all();
    return view('public.plans.index',compact('plans'));
})->name('ours-plans');

Route::get('/clear-cache', function() {
    Artisan::call('cache:clear');
    return "Cache is cleared";
});

Auth::routes();
//confirmacion de cuenta creada
Route::get('verify/{verification_code}','MailController@verify')->name('verify');

//redireccion a home
Route::get('/home', 'HomeController@index')->name('home');

//Agregar personas
Route::get('/persons', 'BackEnd\PersonController@index')->name('persons.index');
Route::post('/persons', 'BackEnd\PersonController@store')->name('persons.store');
Route::get('/persons/new', 'BackEnd\PersonController@new')->name('persons.new');
Route::get('/persons/{id}/show', 'BackEnd\PersonController@show')->name('persons.show');
Route::get('/persons/{id}/edit', 'BackEnd\PersonController@edit')->name('persons.edit');
Route::put('/persons/{id}/update', 'BackEnd\PersonController@update')->name('persons.update');
Route::delete('/persons/{id}', 'BackEnd\PersonController@destroy')->name('persons.destroy');
Route::get('/persons/search', 'BackEnd\PersonController@search')->name('persons.search');

Route::get('/admin/users/persons','BackEnd\UserController@getPersons')->name('users.getpersons');

//Alta, baja, modificar y listar publicaciones con productos
Route::get('/publications', 'BackEnd\PublicationController@index')->name('publications.index');

Route::get('/publications/new', 'BackEnd\PublicationController@new')->name('publications.new');
Route::get('/subcategory/{subcategory_id}/publications/{id}/edit', 'BackEnd\PublicationController@edit')->name('publications.edit');
Route::delete('/publications/{id}', 'BackEnd\PublicationController@destroy')->name('publications.destroy');

Route::post('/publications/search', 'BackEnd\PublicationController@search')->name('publications.search');
Route::get('/publications/state/{publication}/edit', 'BackEnd\PublicationController@editPublicationStatus')->name('publications.status.edit');
Route::put('/publications/state/{publication}/update', 'BackEnd\PublicationController@updatePublicationStatus')->name('publications.status.update');

//Seleccion de categorias para ofertas o demandas
Route::get('/offerts/publications/categories/','BackEnd\PublicationController@getCategoriesToOffert')->name('offerts.publications.categories');
Route::get('/demands/publications/categories/','BackEnd\PublicationController@getCategoriesToDemand')->name('demands.publications.categories');

//recuperar las especies por tipo de madera
Route::get('/kinds/getKinds/{id}','BackEnd\KindController@getKinds')->name('kinds.getKinds');
Route::get('/subcategories/{id}','BackEnd\CategoryController@getSubcategory')->name('publications.subcategory');

//publicaciones de oferta y demanda de aserradero
Route::get('/{type_publication}/publication/{publication}/{product}/edit', 'BackEnd\ProductController@edit')->name('products.edit');
Route::put('/publication/{publication}/{product}/update', 'BackEnd\ProductController@update')->name('products.update');
Route::post('{type_publication}/categories/{subcategory_id}/store', 'BackEnd\ProductController@store')->name('products.store');
Route::get('/categories/{subcategory_id}/{id}/show', 'BackEnd\ProductController@show')->name('products.show');
Route::delete('/categories/{subcategory_id}/{id}/destroy', 'BackEnd\ProductController@destroy')->name('products.destroy');
Route::get('/{type_publication}/categories/{subcategory_id}/create','BackEnd\ProductController@create')->name('products.create');

//publicaciones simples
Route::get('simples','BackEnd\SimpleController@index')->name('simples.index');
Route::get('/categories/{subcategory_id}/simples/new','BackEnd\SimpleController@new')->name('simples.new');
Route::post('/categories/{subcategory_id}/simples/store','BackEnd\SimpleController@store')->name('simples.store');
Route::put('simples/{id}/update','BackEnd\SimpleController@update')->name('simples.update');
Route::get('/simples/{id}/edit','BackEnd\SimpleController@edit')->name('simples.edit');
Route::get('/simples/{id}/show','BackEnd\SimpleController@show')->name('simples.show');
Route::delete('/simples/{id}/destroy','BackEnd\SimpleController@destroy')->name('simples.destroy');
Route::get('/simples/categories','BackEnd\SimpleController@getCategoriesToSimple')->name('simples.categories');
Route::get('/simples/search','BackEnd\SimpleController@search')->name('simples.search');


// Suscripciones
Route::get('/suscriptions', 'BackEnd\SuscriptionController@index')->name('suscriptions.index');

// Payments
Route::post('/pay-suscription', 'BackEnd\PaymentController@store')->name('pay_suscription.store');

Route::get('/success', 'HomeController@index')->name('success');
Route::get('/pending', 'HomeController@index')->name('pending');
Route::get('/failure', 'HomeController@index')->name('failure');

//perfil de empresa y de usuario
Route::get('/profile/user','BackEnd\ProfileController@show')->name('profile.user');
Route::get('/profile/company','BackEnd\ProfileCompanyController@show')->name('profile.company');
Route::get('/profile/user/edit','BackEnd\ProfileController@edit')->name('profile.edit');
Route::put('/profile/user/update','BackEnd\ProfileController@update')->name('profile.update');
Route::put('/profile/company/update','BackEnd\ProfileCompanyController@update')->name('profile.company.update');
Route::get('/profile/company/edit','BackEnd\ProfileCompanyController@edit')->name('profile.company.edit');
Route::put('/profile/company/updateLogo','BackEnd\ProfileCompanyController@updateLogo')->name('profile.company.changeLogo');
Route::get('/profile/company/editLogo','BackEnd\ProfileCompanyController@editLogo')->name('profile.company.editLogo');
Route::put('/profile/company/updateImgs','BackEnd\ProfileCompanyController@updateImgs')->name('profile.company.updateImgs');
Route::get('/profile/company/editImgs','BackEnd\ProfileCompanyController@editImgs')->name('profile.company.editImgs');

Route::get('/profile/user/changePhoto','BackEnd\ProfileController@changePhotoProfile')->name('profile.change.photo');
Route::put('/profile/user/updatePhoto','BackEnd\ProfileController@updatePhotoProfile')->name('profile.update.photo');


//comisionistas
Route::get('/comisionistAgentUsers', 'BackEnd\ComisionistAgentUserController@index')->name('comisionistAgentUsers.index');
Route::post('/comisionistAgentUsers', 'BackEnd\ComisionistAgentUserController@store')->name('comisionistAgentUsers.store');
Route::get('/comisionistAgentUsers/new', 'BackEnd\ComisionistAgentUserController@new')->name('comisionistAgentUsers.new');
Route::get('/comisionistAgentUsers/{user}/edit', 'BackEnd\ComisionistAgentUserController@edit')->name('comisionistAgentUsers.edit');
Route::put('/comisionistAgentUsers/{user}/update', 'BackEnd\ComisionistAgentUserController@update')->name('comisionistAgentUsers.update');
Route::delete('/comisionistAgentUsers/{id}/destroy', 'BackEnd\ComisionistAgentUserController@destroy')->name('comisionistAgentUsers.destroy');
Route::get('/comisionistAgentUsers/{user}/show', 'BackEnd\ComisionistAgentUserController@show')->name('comisionistAgentUsers.show');
Route::get('/comisionistAgentUsers/getpersons','BackEnd\ComisionistAgentUserController@getPersons')->name('comisionistAgentUsers.getpersons');
Route::get('/comisionistAgentUsers/search','BackEnd\ComisionistAgentUserController@search')->name('comisionistAgentUsers.search');
Route::get('/comisionistAgentUsers/companies','BackEnd\ComisionistAgentUserController@getCompanies')->name('comisionistAgentUsers.companies');

//Agregar personas
Route::get('/comisionistAgents', 'BackEnd\ComisionistAgentController@index')->name('comisionistAgents.index');
Route::post('/comisionistAgents', 'BackEnd\ComisionistAgentController@store')->name('comisionistAgents.store');
Route::get('/comisionistAgents/new', 'BackEnd\ComisionistAgentController@new')->name('comisionistAgents.new');
Route::get('/comisionistAgents/{id}/show', 'BackEnd\ComisionistAgentController@show')->name('comisionistAgents.show');
Route::get('/comisionistAgents/{id}/edit', 'BackEnd\ComisionistAgentController@edit')->name('comisionistAgents.edit');
Route::put('/comisionistAgents/{id}/update', 'BackEnd\ComisionistAgentController@update')->name('comisionistAgents.update');
Route::delete('/comisionistAgents/{id}', 'BackEnd\ComisionistAgentController@destroy')->name('comisionistAgents.destroy');

//Listar categorias
Route::get('/parameters/categories', 'BackEnd\CategoryController@index')->name('categories.index');
Route::get('/parameters/categories/search', 'BackEnd\CategoryController@search')->name('categories.search');

//Tipo de madera
Route::get('/parameters/typewoods', 'BackEnd\TypeWoodController@index')->name('typewoods.index');
Route::post('/parameters/typewoods', 'BackEnd\TypeWoodController@store')->name('typewoods.store');
Route::get('/parameters/typewoods/new', 'BackEnd\TypeWoodController@create')->name('typewoods.create');
Route::get('/parameters/typewoods/{id}/show', 'BackEnd\TypeWoodController@show')->name('typewoods.show');
Route::get('/parameters/typewoods/{id}/edit', 'BackEnd\TypeWoodController@edit')->name('typewoods.edit');
Route::put('/parameters/typewoods/{id}/update', 'BackEnd\TypeWoodController@update')->name('typewoods.update');
Route::delete('/parameters/typewoods/{id}', 'BackEnd\TypeWoodController@destroy')->name('typewoods.destroy');
Route::get('/parameters/typewoods/search', 'BackEnd\TypeWoodController@search')->name('typewoods.search');

Route::get('/parameters/typewoods/{typewood_id}/kinds', 'BackEnd\KindController@index')->name('kinds.index');
Route::post('/parameters/typewoods/{typewood_id}/kinds', 'BackEnd\KindController@store')->name('kinds.store');
Route::get('/parameters/typewoods/{typewood_id}/kinds/create', 'BackEnd\KindController@create')->name('kinds.create');
Route::get('/parameters/typewoods/kinds/{id}/show', 'BackEnd\KindController@show')->name('kinds.show');
Route::get('/parameters/typewoods/kinds/{id}/edit', 'BackEnd\KindController@edit')->name('kinds.edit');
Route::put('/parameters/typewoods/kinds/{id}/update', 'BackEnd\KindController@update')->name('kinds.update');
Route::delete('/parameters/typewoods/kinds/{id}', 'BackEnd\KindController@destroy')->name('kinds.destroy');
Route::get('/parameters/typewoods/kinds/{id}/search', 'BackEnd\KindController@search')->name('kinds.search');

Route::get('/parameters/typeproducts/', 'BackEnd\TypeProductController@index')->name('typeproducts.index');
Route::post('/parameters/typeproducts', 'BackEnd\TypeProductController@store')->name('typeproducts.store');
Route::get('/parameters/typeproducts/create', 'BackEnd\TypeProductController@create')->name('typeproducts.create');
Route::get('/parameters/typeproducts/{id}/show', 'BackEnd\TypeProductController@show')->name('typeproducts.show');
Route::get('/parameters/typeproducts/{id}/edit', 'BackEnd\TypeProductController@edit')->name('typeproducts.edit');
Route::put('/parameters/typeproducts/{id}/update', 'BackEnd\TypeProductController@update')->name('typeproducts.update');
Route::delete('/parameters/typeproducts/{id}', 'BackEnd\TypeProductController@destroy')->name('typeproducts.destroy');
Route::get('/parameters/typeproducts/search', 'BackEnd\TypeProductController@search')->name('typeproducts.search');

//usuarios

Route::get('/admin/users', 'BackEnd\UserController@index')->name('users.index');
Route::post('/admin/users', 'BackEnd\UserController@store')->name('users.store');
Route::get('/admin/users/new', 'BackEnd\UserController@new')->name('users.new');
Route::get('/admin/users/{id}/show', 'BackEnd\UserController@show')->name('users.show');
Route::get('/admin/users/{id}/edit', 'BackEnd\UserController@edit')->name('users.edit');
Route::put('/admin/users/{id}/update', 'BackEnd\UserController@update')->name('users.update');
Route::delete('/admin/users/{id}', 'BackEnd\UserController@destroy')->name('users.destroy');
Route::get('/admin/users/search', 'BackEnd\UserController@search')->name('users.search');
Route::put('/admin/users/{user}/stop', 'BackEnd\UserController@stop')->name('users.stop');
Route::put('/admin/users/{user}/active', 'BackEnd\UserController@active')->name('users.active');
Route::post('/admin/users/{userDelete}/restore', 'BackEnd\UserController@restore')->name('users.restore');

Route::post('/admin/archivos/store','BackEnd\FileUploadController@store')->name('admin.archivos.store');
Route::delete('/admin/archivos/{id}/delete/{file}','BackEnd\FileUploadController@deleteImage')->name('admin.archivos.delete');
Route::delete('/admin/archivos/{id}/deleteImageCompany/{file}','BackEnd\FileUploadController@deleteImageCompany')->name('deleteImageCompany');
Route::delete('/admin/archivos/{id}/deleteLogoCompany/{file}','BackEnd\FileUploadController@deleteLogoCompany')->name('deleteLogoCompany');

Route::get('/publications/{publication}/observations/create','BackEnd\ObservationController@create')->name('observations.create');
Route::get('/publications/{publication}/observations/{observation}/show','BackEnd\ObservationController@show')->name('observations.show');
Route::post('/publications/{publication}/observations/store','BackEnd\ObservationController@store')->name('observations.store');
Route::put('/publications/{publication}/observations/{observation}/update','BackEnd\ObservationController@update')->name('observations.update');
Route::put('/publications/{publication}/observations/{observation}/check','BackEnd\ObservationController@check')->name('observations.check');

Route::delete('/publications/{publication}/observations/{observation}/destroy','BackEnd\ObservationController@destroy')->name('observations.destroy');

Route::post('/comments/{publication}/store','BackEnd\CommentController@store')->name('comments.store');
Route::put('/comments/{comment}/update','BackEnd\CommentController@update')->name('comments.reply');
Route::get('/comments','BackEnd\CommentController@index')->name('comments.index');
Route::get('/replies','BackEnd\CommentController@replies')->name('comments.replies');
Route::get('/calculator/{publication}','BackEnd\CalculatorController@index')->name('calculator.index');
Route::get('/comments/{comment}','BackEnd\CommentController@show')->name('comments.show');

//carrito de compras
Route::get('/shopping_cart','FrontEnd\CartController@index')->name('shopping_cart.index');

Route::post('/shopping_cart/add','FrontEnd\CartController@productsToBeConfirmed')->name('shopping_cart.add');
Route::post('/shopping_cart/delete','FrontEnd\CartController@delete')->name('shopping_cart.delete');

Route::post('/shopping_cart/confirm','FrontEnd\CartController@confirmCart')->name('shopping_cart.confirm');
Route::get('/thanks','FrontEnd\CartController@thanks')->name('shopping_cart.thanks');


Route::get('/califications/thanks',function(){
    return view('public/califications/thanks');
})->name('califications.thanks');

Route::get('/califications/{buy}','FrontEnd\CalificationController@index')->name('califications.index');

Route::get('/califications/{buy}/item/{item}/create','FrontEnd\CalificationController@create')->name('califications.create');
Route::post('/califications/{buy}/item/{item}/store','FrontEnd\CalificationController@store')->name('califications.store');

//Rutas para todo el modulo de suscripciones
Route::prefix('plans')->group(function () {
    Route::get('/',  'BackEnd\Suscription\PlanController@index')->name('plans.index');
    Route::get('/get-all', 'BackEnd\Suscription\PlanController@getAll')->name('plans.getAll');
    Route::get('/create', 'BackEnd\Suscription\PlanController@create')->name('plans.create');
    Route::post('/store', 'BackEnd\Suscription\PlanController@store')->name('plans.store');
    Route::get('/show/{id}', 'BackEnd\Suscription\PlanController@show')->name('plans.show');
    Route::get('/me', 'BackEnd\Suscription\PlanController@me')->name('plans.me');

    Route::get('/edit/{id}', 'BackEnd\Suscription\PlanController@edit')->name('plans.edit');
    Route::put('/update/{id}', 'BackEnd\Suscription\PlanController@update')->name('plans.update');
    Route::delete('/delete/{id}', 'BackEnd\Suscription\PlanController@destroy')->name('plans.destroy');
    Route::put('/change/{id}', 'BackEnd\Suscription\PlanController@change')->name('plans.change');

});   

Route::prefix('suscriptions')->group(function () {
    Route::post('/renew/{id}', 'BackEnd\Suscription\SuscriptionController@renewBoard')->name('suscriptions.renew');
});  