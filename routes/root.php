<?php

use Illuminate\Support\Facades\Route;

//roles

Route::get('/admin/roles', 'RolController@index')->name('roles.index');
Route::post('/admin/roles', 'RolController@store')->name('roles.store');
Route::get('/admin/roles/new', 'RolController@new')->name('roles.new');
Route::get('/admin/roles/{id}/edit', 'RolController@edit')->name('roles.edit');
Route::put('/admin/roles/{id}/update', 'RolController@update')->name('roles.update');
Route::get('/admin/roles/{id}', 'RolController@show')->name('roles.show');
Route::delete('/admin/roles/{id}', 'RolController@destroy')->name('roles.destroy');
Route::post('/admin/roles/search', 'RolController@search')->name('roles.search');

//permisos

Route::get('/admin/permissions/{id}', 'PermissionController@index')->name('permissions.index');
Route::post('/admin/permissions/{id}', 'PermissionController@store')->name('permissions.store');
Route::get('/admin/permissions//{id}/new', 'PermissionController@new')->name('permissions.new');
Route::get('/admin/permissions/{id}/edit', 'PermissionController@edit')->name('permissions.edit');
Route::put('/admin/permissions/{id}/update', 'PermissionController@update')->name('permissions.update');
Route::delete('/admin/permissions/{id}', 'PermissionController@destroy')->name('permissions.destroy');


